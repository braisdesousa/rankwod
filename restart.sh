#!/bin/bash
sh -c "app/console doc:data:drop --force"
sh -c "app/console doc:data:create"
sh -c "app/console doc:sche:up --force"
sh -c "app/console doc:fix:load -n --fixtures=src/BDS/RWFixturesBundle/DataFixtures/ORM/"