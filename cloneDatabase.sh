#!/bin/bash
databaseName="$1"
sh -c 'echo $databaseName'
sh -c "(ssh root@digitalOcean 'mysqldump --login-path=local -q --single-transaction $databaseName')|mysql --login-path=local  $databaseName"
