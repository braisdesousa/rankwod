<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new FOS\RestBundle\FOSRestBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Bazinga\Bundle\FakerBundle\BazingaFakerBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
            new Presta\SitemapBundle\PrestaSitemapBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
            new Oneup\UploaderBundle\OneupUploaderBundle(),
            new winzou\Bundle\StateMachineBundle\winzouStateMachineBundle(),

            new Sylius\Bundle\AddressingBundle\SyliusAddressingBundle(),
            new Sylius\Bundle\ResourceBundle\SyliusResourceBundle(),

            new BDS\CoreBundle\BDSCoreBundle(),
            new BDS\UserBundle\BDSUserBundle(),
            new BDS\GaufretteBundle\BDSGaufretteBundle(),
            new BDS\MultiTenantBundle\BDSMultiTenantBundle(),

            new BDS\RWWebBundle\BDSRWWebBundle(),
            new BDS\RWFixturesBundle\BDSRWFixturesBundle(),
            new BDS\RWCompetitionBundle\BDSRWCompetitionBundle(),
            new BDS\RWCategoryBundle\BDSRWCategoryBundle(),
            new BDS\RWMeasureBundle\BDSRWMeasureBundle(),
            new BDS\RWBoxBundle\BDSRWBoxBundle(),
            new BDS\RWCacheBundle\BDSRWCacheBundle(),
            new BDS\RWPaymentBundle\BDSRWPaymentBundle(),
            new BDS\RWApiBundle\BDSRWApiBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }
	public function getRootDir()
	{
		return __DIR__;
	}

	public function getCacheDir()
	{
		return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
	}

	public function getLogDir()
	{
		return dirname(__DIR__).'/var/logs';
	}
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
