<?php

namespace BDS\RWMeasureBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\RWCompetitionBundle\Entity\Event;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Measure
 *
 * @ORM\Table(name="measure")
 * @ORM\Entity
 */
class Measure extends AbstractNamedBaseEntity
{
    /**
     * Measure para una categoria, con nombre largo.
     * Irá en array Collection donde podremos marcas si es valorado aparte (2a,2b)
     * Si marco un 2b, lo que venga detras es forzado aparte.
     */
    const MEASURE_TYPE_REPS     = "MEASURE_TYPE_REPS";
    const MEASURE_TYPE_TIME     = "MEASURE_TYPE_TIME";
    const MEASURE_TYPE_OVERALL   = "MEASURE_TYPE_OVERALL";
    const MEASURE_TYPE_WEIGHT   = "MEASURE_TYPE_WEIGHT";
    const MEASURE_TYPE_CALORIES = "MEASURE_TYPE_CALORIES";
    const MEASURE_TYPE_METERS   = "MEASURE_TYPE_METERS";
    const MEASURE_TYPE_TIME_REPS   = "MEASURE_TYPE_TIME_REPS";
    const MEASURE_TYPE_CALORIES_AND_REPS   = "MEASURE_TYPE_CALORIES_AND_REPS";
    const MEASURE_TYPE_TIME_AND_KG   = "MEASURE_TYPE_TIME_AND_KG";
    const MEASURE_TYPE_REPS_AND_KG   = "MEASURE_TYPE_REPS_AND_KG";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="measure_type",type="string",length=50)
     */
    private $type;

    /**
     * @var Event
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="BDS\RWMeasureBundle\Entity\MeasureGroup",inversedBy="measurements",cascade={"all"})
     * @ORM\JoinColumn(name="measureGroup_id", referencedColumnName="id",nullable=false)
     */
    private $measureGroup;
    /**
     * @var bool
     * @ORM\Column(name="split_result",type="boolean")
     */
    private $splitResult;
    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    private $position;
    /**
     * @var boolean
     * @ORM\Column(name="has_tie_break",type="boolean",nullable=false)
     */
    private $tieBreak;

    public function __construct($type=null,MeasureGroup $measureGroup=null,$text=null,$splitResult=false)
    {

        $this->type=$type;
        $this->measureGroup=$measureGroup;
        $this->name=$text;
        $this->splitResult=$splitResult;
        $this->tieBreak=false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return MeasureGroup
     */
    public function getMeasureGroup()
    {
        return $this->measureGroup;
    }

    /**
     * @param MeasureGroup $measureGroup
     */
    public function setMeasureGroup($measureGroup)
    {
        $this->measureGroup = $measureGroup;
    }

    /**
     * @return boolean
     */
    public function isSplitResult()
    {
        return $this->splitResult;
    }

    /**
     * @param boolean $splitResult
     */
    public function setSplitResult($splitResult)
    {
        $this->splitResult = $splitResult;
    }

    static public function getMeasureTypes()
    {
        return [
            self::MEASURE_TYPE_CALORIES,
            self::MEASURE_TYPE_REPS,
            self::MEASURE_TYPE_TIME,
            self::MEASURE_TYPE_WEIGHT,
            self::MEASURE_TYPE_METERS,
        ];

    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return boolean
     */
    public function isTieBreak()
    {
        return $this->tieBreak;
    }

    /**
     * @param boolean $tieBreak
     */
    public function setTieBreak($tieBreak)
    {
        $this->tieBreak = $tieBreak;
    }
    public function hasTieBreak()
    {
        return $this->isTieBreak();
    }




}

