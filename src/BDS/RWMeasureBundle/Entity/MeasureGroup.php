<?php

namespace BDS\RWMeasureBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\RWCompetitionBundle\Entity\Event;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Measure
 *
 * @ORM\Table(name="measure_groups")
 * @ORM\Entity
 */
class MeasureGroup extends AbstractBaseEntity
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var Event
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Event",inversedBy="measureGroups")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",nullable=false)
     */
    private $event;
    /**
     * @var ArrayCollection|Measure[]
     * @ORM\OneToMany(targetEntity="BDS\RWMeasureBundle\Entity\Measure", mappedBy="measureGroup",cascade={"all"},orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     **/
    private $measurements;
    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    private $position;

    public function __construct(Event $event=null,ArrayCollection $collection=null)
    {
        $this->measurements=$collection?$collection:(new ArrayCollection());
        $this->event=$event;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }
    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }
    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }
    /**
     * @return \BDS\RWMeasureBundle\Entity\Measure[]|ArrayCollection
     */
    public function getMeasurements()
    {
        return $this->measurements;
    }

    /**
     * @param \BDS\RWMeasureBundle\Entity\Measure[]|ArrayCollection $measurements
     */
    public function setMeasurements($measurements)
    {
        $this->measurements = $measurements;
    }

    public function addMeasure(Measure $measure){
        if(!$this->measurements->contains($measure)){
            $this->measurements->add($measure);
        }

    }
    public function removeMeasure(Measure $measure){
        if($this->measurements->contains($measure)){
            $this->measurements->removeElement($measure);
        }
    }




}

