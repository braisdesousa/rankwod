<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 14/03/18
 * Time: 13:35
 */

namespace BDS\RWMeasureBundle\Service;


use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\Translator;

class MeasureService
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function createMeasuresForEvent(Event $event, $type)
    {
        $measureGroup = new MeasureGroup();
        $measureGroup->setEvent($event);
        $event->addMeasureGroup($measureGroup);
        switch ($type) {
            case Measure::MEASURE_TYPE_REPS:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_REPS, $measureGroup, "Repeticiones", false));
                break;
            case Measure::MEASURE_TYPE_TIME:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_TIME, $measureGroup, "Tiempo", false));
                break;
            case Measure::MEASURE_TYPE_TIME_REPS:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_TIME, $measureGroup, "Tiempo", false));
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_REPS, $measureGroup, "Repeticiones si Time Cap", false));
                break;
            case Measure::MEASURE_TYPE_WEIGHT:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_WEIGHT, $measureGroup, "Peso en Kg", false));
                break;
            case Measure::MEASURE_TYPE_METERS:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_METERS, $measureGroup, "Metros", false));
                break;
            case Measure::MEASURE_TYPE_CALORIES:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_CALORIES, $measureGroup, "Calorías", false));
                break;
            case Measure::MEASURE_TYPE_CALORIES_AND_REPS:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_CALORIES, $measureGroup, "Calorías", false));
                $newMeasureGroup = new MeasureGroup();
                $newMeasureGroup->setEvent($event);
                $event->addMeasureGroup($newMeasureGroup);
                $newMeasureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_REPS, $newMeasureGroup, "Repeticiones", false));
                break;
            case Measure::MEASURE_TYPE_TIME_AND_KG:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_TIME, $measureGroup, "Tiempo", false));
                $newMeasureGroup = new MeasureGroup();
                $newMeasureGroup->setEvent($event);
                $event->addMeasureGroup($newMeasureGroup);
                $newMeasureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_WEIGHT, $newMeasureGroup, "Kg", false));
                break;
            case Measure::MEASURE_TYPE_REPS_AND_KG:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_REPS, $measureGroup, "Reps", false));
                $newMeasureGroup = new MeasureGroup();
                $newMeasureGroup->setEvent($event);
                $event->addMeasureGroup($newMeasureGroup);
                $newMeasureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_WEIGHT, $newMeasureGroup, "Kg", false));
                break;
            default:
                $measureGroup->addMeasure(new Measure(Measure::MEASURE_TYPE_REPS, $measureGroup, "Repeticiones", false));
        }
        $this->entityManager->persist($event);
    }
    public function getMeasureName(Event $event):string {
        $countMeasureGroups=$event->getMeasureGroups()->count();
        $measureName="Unasigned";
        switch ($countMeasureGroups){
            case 0: $measureName="Unasigned"; break;
            case 1: $measureName=$this->getSingleMeasureName($event->getMeasureGroups()->first()); break;
            case 2: $measureName=$this->getDualMeasureName($event->getMeasureGroups()); break;
        }
        return $measureName;
    }
       private function getSingleMeasureName(MeasureGroup $measureGroup): string{
        if($measureGroup->getMeasurements()->count()==1){
            return $measureGroup->getMeasurements()->first()->getName();
        } else {
            $text="";
            foreach($measureGroup->getMeasurements() as $key=>$measure){
                $text.=$measure->getName();
                if($key<($measureGroup->getMeasurements()->count()-1)){
                    $text.=" ó ";
                }
            }
            return $text;
        }
    }
        private function getDualMeasureName(Collection $measureGroups): string{
        $alphabet=range("A","Z");
        $text="  Evento Separado:  ";
        foreach($measureGroups as $key=>$measureGroup){
                $text.=$alphabet[$key].") ".$this->getSingleMeasureName($measureGroup). " || ";
        }
        return $text;
    }

}