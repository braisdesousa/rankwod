<?php

namespace BDS\RWMeasureBundle\Form;

use BDS\RWMeasureBundle\Entity\Measure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MeasureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,["label"=>"Descripción"])
            ->add('type',ChoiceType::class,["label"=>"Tipo de Medida","choices"=>$this->getMeasureChoices()])
            ->add('tieBreak',ChoiceType::class,["label"=>"Desempate","choices"=>$this->getSplitChoices(),"expanded"=>true])
            ->add('splitResult',ChoiceType::class,
                    ["label"=>"Divide Wod?", "choices"=>$this->getSplitChoices(),"expanded"=>true]);

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWMeasureBundle\Entity\Measure'
        ));
    }
    public function getMeasureChoices(){
        return [
            "Cantidad"=>Measure::MEASURE_TYPE_REPS,
	        "Tiempo"=>Measure::MEASURE_TYPE_TIME
        ];
    }
    public function getSplitChoices(){
        return [
            "Si"=>true,
            "No"=>false,
        ];
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwmeasurebundle_measure';
    }
}
