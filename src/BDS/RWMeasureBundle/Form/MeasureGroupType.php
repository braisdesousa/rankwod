<?php

namespace BDS\RWMeasureBundle\Form;

use BDS\RWMeasureBundle\Entity\Measure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MeasureGroupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("measurements",CollectionType::class,[
            "label"=>" ",
            "entry_type"=>MeasureType::class,
            "allow_add"=>true,
            "allow_delete"=>true,
            "prototype"=>true,
        ]);

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWMeasureBundle\Entity\MeasureGroup'
        ));
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwmeasurebundle_measure_group';
    }
}
