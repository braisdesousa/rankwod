<?php

namespace BDS\RWBoxBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxAthleteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('box',EntityType::class,
                [
                    "class"=>'BDS\RWBoxBundle\Entity\Box',
                    "choice_label"=>"name",
                    "placeholder"=>"Encuentra tu Box",
                    "required"=>true
//                    "attr"=>["class"=>"hidden"]
                ])
            ->add('submit',SubmitType::class,["label"=>"Confirmar Box"]);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWBoxBundle\Entity\BoxAthlete'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwboxbundle_boxathlete';
    }
}
