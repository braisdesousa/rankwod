<?php

namespace BDS\RWBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,["label"=>"Nombre del Box","required"=>true])
            ->add('text',TextareaType::class,["label"=>"Breve Descripción del Box","required"=>false,"attr"=>["class"=>"trumbowyg"]]);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWBoxBundle\Entity\Box'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwboxbundle_box';
    }
}
