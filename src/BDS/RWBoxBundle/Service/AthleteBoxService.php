<?php

namespace BDS\RWBoxBundle\Service;
use BDS\RWBoxBundle\Entity\Box;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;


class AthleteBoxService
{
    private $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function createOrUpdateBoxAthleteFromUserId($user_id, Box $box=null){
        if($box){
            $boxAthlete=$this->findOrCreateBoxAthlete($user_id);
            $boxAthlete->setBox($box);
            $this->entityManager->persist($boxAthlete);
            return $boxAthlete;
        } elseif($boxAthlete=$this->findBoxAthlete($user_id)) {
//            $boxAthlete->setBox(null);
            $this->entityManager->persist($boxAthlete);
            return $boxAthlete;
        } else {
            $boxAthlete=$this->findOrCreateBoxAthlete($user_id);
            $this->entityManager->persist($boxAthlete);
            return $boxAthlete;
        }

    }
    public function createOrUpdateBoxAthleteFromUserEntity(User $user, Box $box=null){

        return $this->createOrUpdateBoxAthleteFromUserId($user->getId(),$box);

    }

    /**
     * @param $user_id
     * @return UserExtension
     */
    private function findOrCreateBoxAthlete($user_id){
        if($boxAthlete=$this->entityManager->getRepository("BDSRWCompetitionBundle:UserExtension")->findOneBy(["user_id"=>$user_id])){
            return $boxAthlete;
        }
        $boxAthlete= new UserExtension();
        $boxAthlete->setUserId($user_id);
        return $boxAthlete;
    }

    /**
     * @param $user_id
     * @return UserExtension|null
     */
    private function findBoxAthlete($user_id)
    {
        if($boxAthlete=$this->entityManager->getRepository("BDSRWCompetitionBundle:UserExtension")->findOneBy(["user_id"=>$user_id])){
            return $boxAthlete;
        }
        return false;
    }

}