<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 22/02/16
 * Time: 21:19
 */

namespace BDS\RWBoxBundle\Repository;


use BDS\RWBoxBundle\Entity\Box;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\UserBundle\Entity\User;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class BoxRepository extends EntityRepository
{

    public function findBoxByTextQB($text)
    {
        $text=sprintf("%s%s%s","%",$text,"%");
        $text=str_replace(" ","%",$text);
        $qb=$this->createQueryBuilder("boxRepository");
        $qb->where($qb->expr()->orX(
            $qb->expr()->like("boxRepository.name",":search"),
            $qb->expr()->like("boxRepository.text",":search")
        ));
        $qb->setParameter("search",$text,Type::STRING);
        return $qb;
    }
    /** @return  Box|null */
    public function findBoxByUserId(User $user){
        $qb=$this->createQueryBuilder("boxRepository");
        $qb->leftJoin("boxRepository.users","users");
        $qb->where($qb->expr()->eq("users.user_id",$qb->expr()->literal($user->getId())));
        return $qb->getQuery()->getOneOrNullResult();
    }
    public function findBoxByUserIdCached($userId){
        $qb=$this->createQueryBuilder("boxRepository");
        $qb->select("boxRepository.name");
        $qb->leftJoin("boxRepository.users","users");
        $qb->where($qb->expr()->eq("users.user_id",$qb->expr()->literal($userId)));
        $query=$qb->getQuery();
        $query->useResultCache(true,60,"box_repository_find_box_name");
        return $query->getOneOrNullResult();
    }
    public function findBoxesOrderedByNameQB(){
        $qb=$this->createQueryBuilder("boxRepository");
        $qb->orderBy("boxRepository.name","ASC");
        return $qb;
    }
}