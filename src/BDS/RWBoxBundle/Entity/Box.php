<?php

namespace BDS\RWBoxBundle\Entity;

use BDS\CoreBundle\Entity\AbstractTextBaseEntity;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Box
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BDS\RWBoxBundle\Repository\BoxRepository")
 */
class Box extends AbstractTextBaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var ArrayCollection|UserExtension[]
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\UserExtension",mappedBy="box",cascade={"all"})
     */
    private $users;

    public function __construct($name=null)
    {
        $this->name=$name;
//        $this->athletes=new ArrayCollection();
        $this->users=new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function addAthlete(UserExtension $boxAthlete){
        if(!$this->users->contains($boxAthlete)){
            $this->users->add($boxAthlete);
        }
    }
    public function removeAthlete(UserExtension $boxAthlete){
        if($this->users->contains($boxAthlete)){
            $this->users->removeElement($boxAthlete);
        }
    }
    public function addUser(UserExtension $boxAthlete){
        if(!$this->users->contains($boxAthlete)){
            $this->users->add($boxAthlete);
        }
    }
    public function removeUser(UserExtension $boxAthlete){
        if($this->users->contains($boxAthlete)){
            $this->users->removeElement($boxAthlete);
        }
    }

    /**
     * @return UserExtension[]|ArrayCollection
     */
    public function getAthletes()
    {
        return $this->users;

    }

    /**
     * @param UserExtension[]|ArrayCollection $athletes
     */
    public function setAthletes($athletes)
    {
        $this->users = $athletes;
    }
    /**
     * @return UserExtension[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;

    }

    /**
     * @param UserExtension[]|ArrayCollection $athletes
     */
    public function setUsers($athletes)
    {
        $this->users = $athletes;
    }

    
    

}

