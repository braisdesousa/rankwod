<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 25/02/16
 * Time: 11:37
 */

namespace BDS\RWCacheBundle\Service;
use BDS\CoreBundle\Helpers\RepositoryHelper;
use BDS\RWCacheBundle\Entity\CompetitionResultCache;
use BDS\RWCacheBundle\Entity\EventResultCache;
use BDS\RWCacheBundle\Entity\OverallResultCache;
use BDS\RWCategoryBundle\Service\AthleteService;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Overall;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Helpers\EventHelper;
use BDS\RWCompetitionBundle\Service\ListCompetitionResultService;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class CompetitionResultCacheService
{
    private $entityManager;
    private $userEntityManager;
    private $resultService;
    private $serializer;
    private $environment;
    private $athleteService;

    public function __construct(EntityManager $entityManager, EntityManager $userEntityManager, ListCompetitionResultService $resultService, Serializer $serializer,AthleteService $athleteService, $environment)
    {
        $this->entityManager = $entityManager;
        $this->userEntityManager= $userEntityManager;
        $this->serializer = $serializer;
        $this->resultService = $resultService;
        $this->environment=$environment;
        $this->athleteService=$athleteService;
    }

    public function hasUpToDateCache(Competition $competition){
        /** @var CompetitionResultCache $competitionCache */
        if($competitionCache = $this->entityManager->getRepository("BDSRWCacheBundle:CompetitionResultCache")->findOneByCompetition($competition)){
            return ($this->isEventUpdatedAfterCompetitionCache($competitionCache->getCreated(),$competition));    
        } 
        return false;
    }
    public function invalidateCache(Competition $competition, $andFlush = true)
    {
        /** @var CompetitionResultCache $competitionCache */
        if ($competitionCache = $this->entityManager->getRepository("BDSRWCacheBundle:CompetitionResultCache")->findOneByCompetition($competition)) {
            $competitionCache->setDeleted();
            $this->entityManager->persist($competitionCache);
            if ($andFlush) {
                $this->entityManager->flush();
            }
        }
    }
    public function getPointSystem(Competition $competition){
        $pointSystem=[];
        foreach($competition->getPhases() as $phase)
        {
            $pointSystem[$phase->getSlug()]=[];
            $pointSystem[$phase->getSlug()]["point_system"]=$phase->getTypeResults();
            $pointSystem[$phase->getSlug()]["events"]=[];
            foreach($phase->getEvents() as $event){
                $splitEventNameWithKey=EventHelper::getMeasureNames($event);
                foreach ($splitEventNameWithKey as $key=>$value){
                    $pointSystem[$phase->getSlug()]["events"][$key]=$event->getPointSystem()?:100;
                }
            }
        }
        return $pointSystem;
    }
    public function getCachedData(Competition $competition,$force=false,$andFlush=true)
    {
        $force=true;//($force||($this->environment=="dev"));
        /** @var null|CompetitionResultCache $competitionCache * */
        $competitionCache = $this->entityManager->getRepository("BDSRWCacheBundle:CompetitionResultCache")->findOneByCompetition($competition);
        if ($this->shouldUpdateCache($force,$competitionCache))
        {
            $competitionCache = $competitionCache ? $competitionCache : (new CompetitionResultCache($competition));
            $results = $this->getResults($competition,$force);
            $competitionCache->setSerializedData($results);
            $competitionCache->setUnDeleted();
            if($andFlush){
                $this->entityManager->persist($competitionCache);
                $this->entityManager->flush();    
            }
        }
        return $competitionCache->getSerializedData();
    }
    private function shouldUpdateCache($force,CompetitionResultCache $competitionResultCache=null){
        /** If no ResultCache exists OR i need to force it  */
        if($force || (!$competitionResultCache)){
            return true;
        }
        /** If Competition is marked as finished, only Forcing deletion would Work */
        if($competitionResultCache->getCompetition()->isOver()){
            return false;
        }
        /** If ResultCache is marked as deleted a new one should be created */
        if($competitionResultCache->isDeleted()){
            return true;
        }
        /** if a new Event has ven published since last cache creation we need to update it */
        if($this->isNewEventPublished($competitionResultCache,$competitionResultCache->getCompetition())){
            return true;
        }
        /** if a new event has ben updated since last chache creation we need to know if we have to update it */
        if($this->isEventUpdatedAfterCompetitionCache($competitionResultCache->getUpdated(),$competitionResultCache->getCompetition())) {
            return true;
        }
        return false;
    }
    private function isNewEventPublished(CompetitionResultCache $competitionCache, Competition $competition)
    {
        $lastEventExpiredDate=$this->entityManager->getRepository("BDSRWCompetitionBundle:Event")->getLastExpiredDate($competition);
        return ($competitionCache->getCreated()<$lastEventExpiredDate);
    }
    private function isEventUpdatedAfterCompetitionCache(\DateTime $dateTime,Competition $competition){
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Event")->countUpdatedEventsAfterCacheCreationByCompetition($dateTime,$competition)>0;
    }
    private function isEventUpdatedAfterEventCache(\DateTime $dateTime,Event $event){
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Event")->countUpdatedEventsAfterCacheCreationByEvent($dateTime,$event)>0;
    }
    private function isOverallUpdatedAfterOverallCache(\DateTime $dateTime,Overall $overall){
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Overall")->countUpdatedResultAfterCacheCreationByEvent($dateTime,$overall)>0;
    }
    private function getResults($competition,$force=false)
    {
        
        $boxNames=RepositoryHelper::reformatArrayWithKey($this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findCompetitionBoxes($competition),"user_id");
        $serialize=["box" => $boxNames, "user"=> $this->athleteService->getCompetitionAthletes($competition),"competition_j"=>$competition,"results"=>$this->getEventsResults($competition,$force)];
        $serialized=$this->serializer->serialize($serialize,"json",SerializationContext::create()->setGroups(["list","front_result"]));
        return $serialized;
    }
    private function getEventsResults(Competition $competition,$force=false)
    {
        $results=[];
        foreach($competition->getPhases() as $phase){
            if($phase->isPublished()||true){
                $results[$phase->getSlug()]=[];
                foreach($phase->getEvents() as $event){
                    if($event->isSortable()){
                        $results[$phase->getSlug()]=array_merge($results[$phase->getSlug()],$this->getEventResults($event,$force));
                    }
                }
                if($phase->getOverall()){
                    $results[$phase->getSlug()]["overall"]=$this->getOverallResults($phase,$force);
                }
            }
        }
        return $results;    
    }
    private function getOverallResults(Phase $phase,$force)
    {
        $overall=$phase->getOverall();
        
        /** @var OverallResultCache $cachedOveralResults */
        $cachedOverallResults=$this->entityManager->getRepository("BDSRWCacheBundle:OverallResultCache")->findOneBy(["overall"=>$overall]);
        if($force||(!$cachedOverallResults)||($this->isOverallUpdatedAfterOverallCache($cachedOverallResults->getUpdated(),$overall))){
            $cachedOverallResults = $cachedOverallResults ? $cachedOverallResults : (new OverallResultCache($overall));
            $results=$this->resultService->getOverallResults($phase);
            $cachedOverallResults->setSerializedData($results);
            $this->entityManager->persist($cachedOverallResults);
        }
        return $cachedOverallResults->getSerializedData();
    }

    private function shouldUpdateEvent(EventResultCache $cachedEventResults=null,Event $event,$force){
        $phase=$event->getPhase();
        if($force|| !$cachedEventResults){
            return true;
        }

        if($phase->getPublicResultType()==Phase::PUBLISH_RESULT_MANUAL ){
            false;
        } else {
            $this->isEventUpdatedAfterEventCache($cachedEventResults->getUpdated(),$event);
        }
    }
    private function getEventResults(Event $event,$force=false)
    {
        /** @var EventResultCache $cachedEventResults */
        $cachedEventResults=$this->entityManager->getRepository("BDSRWCacheBundle:EventResultCache")->findOneBy(["event"=>$event]);
        if($this->shouldUpdateEvent($cachedEventResults,$event,$force)){
            $cachedEventResults = $cachedEventResults ? $cachedEventResults : (new EventResultCache($event));
            $results=$this->resultService->reorderEventResults($event);
            $cachedEventResults->setSerializedData($results);
            $this->entityManager->persist($cachedEventResults);
        }
        return $cachedEventResults->getSerializedData();
    }


}