<?php

namespace BDS\RWCacheBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\RWCompetitionBundle\Entity\Competition;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="cache_competition_result")
 * @ORM\Entity()
 */
class CompetitionResultCache extends AbstractBaseEntity
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\Column(name="serialized_data",type="text",nullable=false)
     */
    private $serializedData;

    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Competition")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id",nullable=false,unique=true,onDelete="CASCADE")
     *
     */
    private $competition;

    public function __construct(Competition $competition,$serializedData=null)
    {
        $this->competition=$competition;
        $this->serializedData=$serializedData;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getSerializedData()
    {
        return $this->serializedData;
    }

    /**
     * @param mixed $serializedData
     */
    public function setSerializedData($serializedData)
    {
        $this->serializedData = $serializedData;
    }

    /**
     * @return Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }



}