<?php

namespace BDS\RWCacheBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="cache_event_result")
 * @ORM\Entity()
 */
class EventResultCache extends AbstractBaseEntity
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\Column(name="serialized_data",type="json_array",nullable=false)
     */
    private $serializedData;

    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Event")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",nullable=false,unique=true,onDelete="CASCADE")
     *
     */
    private $event;

    public function __construct(Event $event,$serializedData=null)
    {
        $this->event=$event;
        $this->serializedData=json_encode($serializedData,true);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getSerializedData()
    {
        return json_decode($this->serializedData,true);
    }

    /**
     * @param mixed $serializedData
     */
    public function setSerializedData($serializedData)
    {
        $this->serializedData = json_encode($serializedData,true);
    }

    /**
     * @return Competition
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Competition $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }
}