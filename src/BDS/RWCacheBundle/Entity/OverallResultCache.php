<?php

namespace BDS\RWCacheBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Overall;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="cache_overall_result")
 * @ORM\Entity()
 */
class OverallResultCache extends AbstractBaseEntity
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\Column(name="serialized_data",type="json_array",nullable=false)
     */
    private $serializedData;

    /**
     * @var Overall
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Overall")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",nullable=false,unique=true, onDelete="CASCADE")
     *
     */
    private $overall;

    public function __construct(Overall $overall,$serializedData=null)
    {
        $this->overall=$overall;
        $this->serializedData=json_encode($serializedData,true);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getSerializedData()
    {
        return json_decode($this->serializedData,true);
    }

    /**
     * @param mixed $serializedData
     */
    public function setSerializedData($serializedData)
    {
        $this->serializedData = json_encode($serializedData,true);
    }

    /**
     * @return Overall
     */
    public function getOverall()
    {
        return $this->overall;
    }

    /**
     * @param Overall $overall
     */
    public function setOverall($overall)
    {
        $this->overall = $overall;
    }

  
}