<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 8/29/14
 * Time: 12:39 PM
 */

namespace BDS\RWFixturesBundle\DataFixtures\ORM;


use BDS\RWBoxBundle\Entity\Box;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCategoryBundle\Entity\Restriction;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Faker\Generator;
use Faker\Provider\es_ES\Address;
use Faker\Provider\es_ES\Company;
use Faker\Provider\es_ES\Person;
use Faker\Provider\Lorem;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BoxFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface,FixtureInterface {

    const ORDER = 4;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->faker=new Generator();
        $this->faker->addProvider(new Lorem($this->faker));
        $this->em=$em;
        $this->createBoxes();
        $this->em->flush();
    }
    public function createBoxes()
    {
        foreach ($this->getBoxes() as $box)
        {
            $this->createBox($box);
        }
    }
    public function createBox($boxInfo){
        $box=new Box();
        $box->setName($boxInfo);
        $box->setText($this->faker->paragraphs(3,true));
        $this->em->persist($box);

    }
    public function getBoxes(){
        return [
            "CrossFit MZK",
            "CrossFit Almería",
            "CrossFit Virus",
            "CrossFit La Sombra Algeciras",
            "CrossFit Chiclana",
            "CrossFit El Puerto de Santa María",
            "CrossFit FiiBox",
            "CrossFit Blue Gorilla",
            "CrossFit San Fernando",
            "CrossFit Sanlúcar",
            "CrossFit Córdoba",
            "Triple XXX CrossFit",
            "CrossFit GRX",
            "CrossFit Bulls Factory",
            "Foering CrossFit",
            "Five CrossFit",
            "CrossFit Jaén",
            "CrossFit Alhaurin",
            "CrossFit Al Andalus",
            "CrossFit Fuengirola",
            "CrossFit Málaga",
            "CrossFit Olimpo",
            "Trucker CrossFit",
            "VCJ CrossFit",
            "Box 1315 CrossFit",
            "CrossFit Elviria",
            "CrossFit MCC",
            "CrossFit Zona Ocho",
            "CrossFit Marbella",
            "Mijas CrossFit",
            "CrossFit La Muralla",
            "Crossfit Olimpo TRM",
            "CrossFit RockHeart",
            "CrossFit Wolves Aljarafe",
            "CrossFit Rookies Box",
            "Feel CrossFit",
            "Giralda CrossFit",
            "CrossFit Sevilla",
            "CrossFit La Forja",
            "Indian CrossFit",
            "CrossFit Zaragoza",
            "Brick CrossFit Puerto Venecia",
            "CrossFit Avilés",
            "Ovetus CrossFit",
            "Brown Bears CrossFit",
            "Tu Box CrossFit",
            "CrossFit Gijón",
            "CrossFit Naranco",
            "CrossFit Exit 23",
            "CrossFit Lanzarote",
            "CrossFit Blind Crab",
            "CrossFit Fuerteventura",
            "CrossFit Sebadal",
            "CrossFit Canarias",
            "CrossFit Riders",
            "CrossFit 27",
            "CrossFit Survive",
            "Zona 0 CrossFit",
            "CrossFit Salvaje",
            "CrossFit Aguere",
            "Bestial CrossFit",
            "CrossFit Teide",
            "Mente CrossFit",
            "CrossFit TNF",
            "MadBull CrossFit",
            "CrossFit 942",
            "CrossFit Santander",
            "CrossFit Burgos",
            "CrossFit Vetón",
            "CrossFit DCP",
            "Frog CrossFit",
            "CrossFit MMXIV",
            "CrossFit Vacceos",
            "CrossFit Barbacana",
            "CrossFit Smile",
            "CrossFit Villarrobledo",
            "CrossFit Ciudad Real",
            "Crossfit Puertollano",
            "CrossFit Box 49",
            "CrossFit Guadalajara",
            "BDN CrossFit",
            "CrossFit Eixample",
            "Reebok CrossFit BCN",
            "Cathalo CrossFit",
            "CrossFit Can Drago",
            "CrossFit Poblenou",
            "CrossFit Gala",
            "CrossFit LCS",
            "La Huella CrossFit",
            "CrossFit Diagonal",
            "Condal CrossFit",
            "CrossFit Box Castelldefels",
            "CrossFit B23",
            "Ozona CrossFit",
            "CrossFit Igualada",
            "CrossFit Mataró",
            "CrossFit Edelweiss",
            "CrossFit Lynx",
            "CrossFit SJD",
            "CrossFit Blau",
            "CrossFit Occidental",
            "Area CrossFit",
            "CrossFit Terrassa",
            "CrossFit 77 Feet",
            "CrossFit La Guarida",
            "CrossFit Girona",
            "CrossFit Lleida",
            "Crossfit Lambda",
            "CrossFit Scipion",
            "Crossfit Tarragona",
            "CrossFit de Alcalá",
            "Quimera CrossFit",
            "M Team CrossFit",
            "CrossFit Tracius",
            "CrossFit Norte Algete",
            "CrossFit Aranjuez",
            "CrossFit Collado Villalba",
            "Ifrinn CrossFit",
            "CrossFit Coslada",
            "CrossFit Fuenlabrada",
            "CrossFit Getafe Capital Sur",
            "CrossFit A6",
            "CrossFit Las Rozas",
            "CrossFit Soforem",
            "CrossFit Quo Lab",
            "CrossFit Leganés",
            "CrossFit Peleo",
            "CrossFit Monkey",
            "CrossFit Bellum",
            "Urban Box CrossFit",
            "CrossFit Serrano",
            "Improve CrossFit",
            "CrossFit Maherit Río",
            "CrossFit Quevedo",
            "KLAN CrossFit",
            "CrossFit Distrito Chamberí",
            "555 CrossFit",
            "CrossFit Rayo",
            "CrossFit Casa Campo",
            "CrossFit Plenilunio",
            "CrossFit Singular Box",
            "CrossFit Vallecas",
            "CrossFit Atrium",
            "Caravel CrossFit",
            "CrossFit Abantos",
            "CrossFit Valdemoro",
            "Yo Soy CrossFIt",
            "CrossFit Rivas Arena",
            "Espacio CrossFit",
            "Stygia CrossFit",
            "CrossFit Minimal",
            "CrossFit Los Reyes",
            "Green Horse Crossfit Center",
            "CrossFit M5 Torrejón",
            "CrossFit 3 Cantos",
            "CrossFit Castle Villafranca",
            "CrossFit Acero",
            "CrossFit Alcoy",
            "CrossFit Albir",
            "CrossFit ALC",
            "CrossFit Alicante",
            "CrossFit Rabasa",
            "CrossFit Madrid",
            "CrossFit Elche",
            "CrossFit Marina Alta",
            "CrossFit Blue Wolf",
            "CrossFit Costa Blanca",
            "CrossFit Citius",
            "AV CrossFit",
            "CrossFit 03180",
            "CrossFit Remus",
            "CrossFit 393",
            "Vita CrossFit",
            "CV CrossFit",
            "CrossFit Northwest Paterna",
            "CrossFit Puerto de Sagunto",
            "CrossFit VLC",
            "Full CrossFit",
            "CrossFit Grau",
            "7FBA CrossFit",
            "Enso CrossFit",
            "CrossFit Naron",
            "Artabros Crossfit",
            "Brick CrossFit Santiago de Compostela",
            "Lucus CrossFit",
            "CrossFit Ourense",
            "CrossFit Pontevedra",
            "CrossFit Salnes",
            "CTS CrossFit",
            "Killian Crossfit",
            "CrossFit Vigo",
            "Isla Blanca CrossFit",
            "CrossFit Mallorca",
            "CrossFit Manacor",
            "CrossFit Limit Break",
            "FreeMove CrossFit",
            "CrossFit Entreno Cruzado",
            "C23 CrossFit",
            "CrossFit Hummer",
            "CrossFit Palma de Mallorca",
            "CrossFit Logroño",
            "CrossFit Colen",
            "CrossFit Pamplona",
            "Queiron CrossFit",
            "CrossFit Tudela",
            "CrossFit Gasteiz",
            "CrossFit Drakkar Hondarribia",
            "CrossFit Koroibos",
            "CrossFit Zurriola",
            "Indar CrossFit",
            "CrossFit Donosti Pasaia Box",
            "CrossFit Donosti",
            "CrossFit Icaro",
            "CrossFit Bilbao",
            "CrossFit Last Line",
            "Kumuka CrossFit",
            "Vivo Box CrossFit",
            "CrossFit El Tiro",
            "CrossFit Turoqua",
            "Old Metal CrossFit",
            "CrossFit L61",
            "Celtic Box CrossFit",
            "CrossFit Distrito 362"
        ];
    }

    
    
} 