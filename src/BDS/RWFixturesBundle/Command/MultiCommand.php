<?php

namespace BDS\RWFixturesBundle\Command;


use BDS\RWBoxBundle\Entity\Box;
use BDS\RWBoxBundle\Entity\BoxAthlete;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\RWCompetitionBundle\Helpers\EventHelper;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use BDS\RWPaymentBundle\Entity\Order;
use BDS\UserBundle\Entity\RoninFoxUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Egulias\EmailValidator\EmailValidator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class MultiCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    /** @var OutputInterface **/
    private $output;

    private $created;

    protected function configure()
    {
        $this
            ->setName('bds:rw:multi-command')
            ->setDescription('[DEV] MultiCommands');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $this->entityManager=$this->getContainer()->get("doctrine.orm.default_entity_manager");
        $this->output=$output;
        $output->writeln(sprintf("<info>MultiCommand</info>"));
        /** @var Phase $phase */
        $phase=$this->entityManager->getRepository("BDSRWCompetitionBundle:Phase")->find(28);
        /** @var CategoryAthlete $categoryAthlete */
        foreach($phase->getAthletes() as $categoryAthlete){
         if(!$categoryAthlete->isInvited()){
            $order=$categoryAthlete->getOrder();
            if($order->isPaid()){
                $order->setStatus(Order::STATUS_PAID);
                $this->entityManager->persist($order);
            }
            }
         }
        $this->entityManager->flush();
    }


}