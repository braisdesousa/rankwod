<?php

namespace BDS\RWFixturesBundle\Command;


use BDS\RWBoxBundle\Entity\Box;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use Doctrine\Common\Collections\ArrayCollection;
use Egulias\EmailValidator\EmailValidator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class SendUsersMailCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;
    private $userEntityManager;

    /** @var OutputInterface **/
    private $output;
    /** @var  Competition */
    private $competition;

    protected function configure()
    {
        $this
            ->setName('bds:rw:send-user-mail')
            ->setDescription('Send Mail for not yet Created Users');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em=$this->getContainer()->get("doctrine.orm.user_entity_manager");
        $dem=$this->getContainer()->get("doctrine.orm.default_entity_manager");
        /** @var Competition $competition */
        $competition=$dem->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug("galicia-fitforcare-affiliate-league");
        /** @var Phase $phase */
        $phase=$competition->getPhases()->first();
        $users=$em->getRepository("BDSUserBundle:User")->findUsersWithToken();
        $usersIds=$this->getUserIdArray($users);
        $usersCollection=$this->createUsersCollection($users);
        $athletes=$this->getAthletesToMail($usersCollection,$phase->getAthletes(),$usersIds);

        foreach($athletes as $key=>$categoryAthletes){
            $category=$categoryAthletes[0]->getCategory();
                $this->getContainer()->get("bdsrw_competition.mailer")->mailNewCompetitionAthletes($categoryAthletes,$phase,$category);
                /** @var CategoryAthlete $categoryAthlete */
                foreach($categoryAthletes as $categoryAthlete){
                    $output->writeln(sprintf("<info>Sent mail to <comment>%s</comment> account: <comment>%s</comment> </info>",$categoryAthlete->getRoninFoxUser()->getCompleteName(),$categoryAthlete->getRoninFoxUser()->getEmail()));
                }
        }




    }
    private function createUsersCollection($array){
        $usersCollection= new ArrayCollection();
        foreach($array as $user){
            $usersCollection->set($user->getId(),$user);
        }
        return $usersCollection;
    }
    private function getAthletesToMail(ArrayCollection $usersCollection,$athletes,$usersIds){
        $athletesToMail=[];
        /** @var CategoryAthlete $athlete */
        foreach($athletes as $athlete){
            if(in_array($athlete->getUserId(),$usersIds)){
                $athlete->setRoninFoxUser($usersCollection->get($athlete->getUserId()));
                $athletesToMail[$athlete->getCategory()->getSlug()][]=$athlete;
            }
        }
        return $athletesToMail;

    }
    private function getUserIdArray($users){
        $userIds=[];
        foreach($users as $user){
            $userIds[]=$user->getId();
        }
        return $userIds;
    }
}