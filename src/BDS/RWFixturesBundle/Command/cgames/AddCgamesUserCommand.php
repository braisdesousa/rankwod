<?php

namespace BDS\RWFixturesBundle\Command\cgames;


use BDS\CoreBundle\Helpers\RepositoryHelper;
use BDS\RWBoxBundle\Entity\Box;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\UserBundle\Entity\Role;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Egulias\EmailValidator\EmailValidator;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class AddCgamesUserCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;
    private $userEntityManager;
    private $userManager;

    /** @var OutputInterface **/
    private $output;
    /** @var  Competition */
    private $competition;

    protected function configure()
    {
        $this
            ->setName('bds:rw:cgames')
            ->setDescription('Cgames Overall');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->userEntityManager=$this->getContainer()->get("doctrine.orm.user_entity_manager");
        $this->entityManager=$this->getContainer()->get("doctrine.orm.default_entity_manager");
        $this->userManager=$this->getContainer()->get("fos_user.user_manager");
        $this->output=$output;
        $license=$this->userEntityManager->getRepository("BDSMultiTenantBundle:License")->findOneBySlug('rankwod');
        /** @var Competition $competition */
        $competition=$this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug("centaurus-games");
        /** @var Phase $phase */
        $overall=$this->getContainer()->get("bdsrw_competition.overall")->getTempOverall($competition->getFirstPhase());
        $athletes=$this->getContainer()->get("rankwod.athlete")->getCompetitionAthletes($competition);
        $ra=$athletes["athletes"]["online-3"];
        $ra=RepositoryHelper::reformatArrayWithKey($ra,"id");
        foreach($overall as $categoryKey=>$category){
            $output->writeln(sprintf("<info>Category <comment>%s</comment></info>",$categoryKey));
            foreach($category as $key=>$categoryOverall){
                if($key<20){
                    $email=$this->getEmail($ra[$categoryOverall]["user_id"]);
                    $output->write(sprintf("<info>#<comment>%s</comment> - <comment>%s</comment> - <comment>%s</comment></info>",(++$key),
                        ($athletes["users"][$ra[$categoryOverall]["user_id"]]["name"]),
                        $email
                    ));
                    if(!$this->existsOnCgames($email)){
                        $output->writeln(" <error> Does Not Exist</error>");
                    } else {
                        $output->writeln("#");
                        $this->updateAttendance($email);
                    }
                }

            }
        }
    }
    public function existsOnCgames($email){
        /** @var Connection $connection */
        $connection=$this->getContainer()->get("doctrine.dbal.cgames_connection");
        $result=$connection->executeQuery(sprintf('SELECT COUNT(*) as count FROM athlete where email like "%s"',$email));
        $x=$result->fetch();
        return $x["count"];

    }
    public function updateAttendance($email){
        /** @var Connection $connection */
        $connection=$this->getContainer()->get("doctrine.dbal.cgames_connection");
        $result=$connection->executeQuery(sprintf('UPDATE athlete SET granted_on_attendance=1 where email like "%s"',$email));
        $result->execute();
        return $result;

    }
    public function getEmail($userId){
        /** @var User $user */
        $user=$this->getContainer()->get("fos_user.user_manager")->findUserBy(["id"=>$userId]);
        return $user->getEmail();
    }


}