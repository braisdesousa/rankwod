<?php

namespace BDS\RWFixturesBundle\Command;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWWebBundle\Form\WGTeamType;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Form\FormError;

class WGamesTeamsCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    /** @var OutputInterface **/
    private $output;

    /** @var Generator */
    private $faker;

    protected function configure()
    {
        $this
            ->setName('bds:rw:wgames')
            ->setDescription('[DEV] Create Fake Team');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->output=$output;
        $this->faker = Factory::create("es_ES");
        $this->entityManager=$this->getContainer()->get("doctrine.orm.default_entity_manager");

        $output->writeln(sprintf("<info>Creating Fake Team</info>"));
        for($x=0;$x<10;$x++){
            $teamMembers=rand(4,6);
            $team=["name"=>$this->faker->company];
            for($i=0;$i<=$teamMembers;$i++){
                $team["athlete_".($i+1)]=$this->createTeamMember();
            }
            $this->handleForm($team);
        }

        

    }
    private function handleForm(array $team){
        /** @var Competition $competition */
        $competition=$this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug("winter-games-madrid");
        $form=$this->getContainer()->get("form.factory")->create(WGTeamType::class,null,["method"=>"POST"]);
        $form->submit($team);
        
            if($form->isValid()){
                $teamName=trim(($form->get("name")->getData()));
                if($this->entityManager->getRepository("BDSRWCompetitionBundle:Team")->findByNameAndCompetition($teamName,$competition)){
                    $form->get("name")->addError(new FormError("Ya existe otro equipo con este nombre"));
                    $this->output->writeln("<info>Error Ya existe otro equipo");
                    return;
                }
                $t1=$form->get("athlete_1")->getData();
                $t2=$form->get("athlete_2")->getData();
                $t3=$form->get("athlete_3")->getData();
                $t4=$form->get("athlete_4")->getData();
                $t5=$form->get("athlete_5")->getData();
                $t6=$form->get("athlete_6")->getData();
                $t7=$form->get("athlete_7")->getData();
                if($this->repeatedEmails([$t1,$t2,$t3,$t4,$t5,$t6,$t7])){
                    $form->addError(new FormError("No puede haber correos repetidos"));
                    $this->output->writeln("<info>Correo Repetido");
                    return;
                }
                $team=$this->getContainer()->get("bdsrw_competition.user")->getTeamForCompetition($competition->getFirstPhase(),$competition->getFirstPhase()->getCategories()->first(),$teamName,[$t1,$t2,$t3,$t4,$t5,$t6,$t7]);
                $this->entityManager->flush();
                $this->output->writeln(sprintf("<info>Team <comment>%s</comment> creado || <comment>%s</comment></info>",$team->getName(),$team->getSlug()));
                return;
            }
        $this->output->writeln("<info>Revisar Error</info>");
    }
    private function createTeamMember(){
        $tallas=["S","XS","M","L","XL"];
        shuffle($tallas);
        return [
            "first_name"=>$this->faker->firstName,
            "last_name"=>$this->faker->lastName,
            "email"=>$this->faker->email,
            "phone"=>$this->faker->phoneNumber,
            "tshirt"=>$tallas[0],
            "dni"=>$this->faker->firstName,
            "rm"=>rand(60,110),
        ];
    }
    private function repeatedEmails(array $athletes){
        $emails=[];
        foreach($athletes as $athlete){
            if(!$athlete["email"]){
                continue;
            }
            if(!in_array($athlete["email"],$emails)){
                $emails[]=$athlete["email"];
            }else {
                return true;
            }
        }
        return false;
    }
}