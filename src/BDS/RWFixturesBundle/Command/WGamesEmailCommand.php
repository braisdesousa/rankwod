<?php

namespace BDS\RWFixturesBundle\Command;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWWebBundle\Form\WGTeamType;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Form\FormError;

class WGamesEmailCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    /** @var OutputInterface **/
    private $output;

    /** @var Generator */
    private $faker;

    protected function configure()
    {
        $this
            ->setName('bds:rw:email-wg')
            ->setDescription('[DEV] Email WinterGames');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->output=$output;
        $this->faker = Factory::create("es_ES");
        $this->entityManager=$this->getContainer()->get("doctrine.orm.default_entity_manager");
        /** @var Competition $competition */
        $competition=$this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug("winter-games-madrid");
        /** @var Phase $phase */
        $phase=$competition->getFirstPhase();

        /** @var Team $team */
        foreach($this->entityManager->getRepository("BDSRWCompetitionBundle:Team")->findAll() as $team){
            $this->getContainer()->get("bdsrw_competition.mailer")->mailNewTeamInCompetition($team,$phase,"contacto@braisdesousa.com");
            $this->getContainer()->get("bdsrw_competition.mailer")->mailSucceedTeamInCompetition($team,$phase);
            break;
        }
    }
}