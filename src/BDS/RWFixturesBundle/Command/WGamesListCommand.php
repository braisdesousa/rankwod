<?php

namespace BDS\RWFixturesBundle\Command;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\RWWebBundle\Form\WGTeamType;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Form\FormError;

class WGamesListCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    /** @var OutputInterface **/
    private $output;

    /** @var Generator */
    private $faker;

    protected function configure()
    {
        $this
            ->setName('bds:rw:cgames-list')
            ->setDescription('[DEV] CreateList');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->output=$output;
        $this->entityManager=$this->getContainer()->get("doctrine.orm.default_entity_manager");
        $output->writeln(sprintf("<info>Creating Lists</info>"));
        /** @var Competition $competition */
        $competition=$this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug("winter-games-madrid");
        $phase=$competition->getFirstPhase();
        /** @var CategoryAthlete $team */
        foreach($phase->getAthletes() as $team){
            if(!$team->isTeam()){
                $output->writeln("[ERROR]");
            }
            /** @var TeamAthlete $athlete */
            foreach($team->getTeam()->getAthletes() as $athlete){
                $ed=$athlete->getExtraDataCollection()->first();
                $value=$ed?$ed->getFrontValue():"#";
                $output->writeln(sprintf("%s , %s , %s, %s, %s ",$team->getName(),$athlete->getName(),$athlete->getUserExtension()->getRoninFoxUser()->getEmail(),$value,$athlete->getUserExtension()->getTShirtSize()));
            }
    }

        

    }
}