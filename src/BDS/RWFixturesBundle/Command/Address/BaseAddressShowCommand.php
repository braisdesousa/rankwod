<?php

namespace BDS\RWFixturesBundle\Command\Address;


use BDS\RWCompetitionBundle\Entity\Phase;
use Sylius\Component\Addressing\Model\Country;
use Sylius\Component\Addressing\Model\Province;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BaseAddressShowCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    /** @var OutputInterface **/
    private $output;

    private $created;

    protected function configure()
    {
        $this
            ->setName('rw:show:address')
            ->setDescription('[FIX] Show Base Entities for Addressing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager=$this->getContainer()->get("doctrine.orm.default_entity_manager");
        $this->output=$output;
        $output->writeln(sprintf("<info>Showing Stored Country and Provinces</info>"));
        $countryRepo=$this->getContainer()->get("sylius.repository.country");
        $provincesRepo=$this->getContainer()->get("sylius.repository.province");
        $countriesCount=$countryRepo->findAll();
        $provincesCount=$provincesRepo->findAll();
        $output->writeln(sprintf("<info>Found <comment>%s</comment> Countries and <comment>%s</comment> Provinces</info>",count($countriesCount),count($provincesCount)));
        /**
         * @var Country $country
         */
        foreach($this->getContainer()->get("sylius.repository.country")->findAll() as $country){
            $output->writeln(sprintf("<info>Found Country <comment>%s</comment></info>",$country->getName('ES')));
            /** @var Province $province */
            foreach($country->getProvinces() as $province){
                $output->writeln(sprintf("    <info>Provincia <comment>%s</comment></info>",$province->getName()));
            }
        }

    }
}