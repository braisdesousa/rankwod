<?php

namespace BDS\RWFixturesBundle\Command\Address;


use BDS\RWCompetitionBundle\Entity\Phase;
use Sylius\Component\Addressing\Model\Country;
use Sylius\Component\Addressing\Model\Province;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BaseAddressFixturesCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    /** @var OutputInterface **/
    private $output;

    private $created;

    protected function configure()
    {
        $this
            ->setName('rw:fix:address')
            ->setDescription('[FIX] Create Base Entities for Addressing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager=$this->getContainer()->get("doctrine.orm.default_entity_manager");
        $this->output=$output;
        $output->writeln(sprintf("<info>Creating Country and Provinces</info>"));
        if(!$country=$this->getContainer()->get("sylius.repository.country")->findOneBy(["code"=>"ES"])){
            $country=$this->createCountry();
        }
        if($country->getProvinces()->count()==0){
            $this->createProvinces($country);
        }
        $this->entityManager->flush();

    }
    private function createProvinces(Country $country):void{
        foreach ($this->getProvinces() as $key=> $province){
            if(!$this->getContainer()->get("sylius.repository.province")->findOneBy(["code"=>$key])){
                $pr=new Province();
                $pr->setCountry($country);
                $pr->setName($province);
                $pr->setCode($key);
                $pr->setAbbreviation($key);
                $country->addProvince($pr);
                $this->entityManager->persist($pr);
                $this->entityManager->persist($country);
            }
        }
    }
    private function createCountry():Country{
        $country= new Country();
        $country->setCode("ES");
        $country->setEnabled(true);
        $this->entityManager->persist($country);
        return $country;
    }


private function getProvinces(){
    return [
        "A"=>"Alicante",
        "AB"=>"Albacete",
        "AL"=>"Almería",
        "AV"=>"Ávila",
        "B"=>"Barcelona",
        "BA"=>"Badajoz",
        "BI"=>"Vizcaya",
        "BU"=>"Burgos",
        "C"=>"A Coruña",
        "CA"=>"Cádiz",
        "CC"=>"Cáceres",
        "CE"=>"Ceuta",
        "CO"=>"Córdoba",
        "CR"=>"Ciudad Real",
        "CS"=>"Castellón",
        "CU"=>"Cuenca",
        "GC"=>"Las Palmas",
        "GI"=>"Girona",
        "GR"=>"Granada",
        "GU"=>"Guadalajara",
        "H"=>"Huelva",
        "HU"=>"Huesca",
        "J"=>"Jaén",
        "L"=>"Lleida",
        "LE"=>"León",
        "LO"=>"La Rioja",
        "LU"=>"Lugo",
        "M"=>"Madrid",
        "MA"=>"Málaga",
        "ML"=>"Melilla",
        "MU"=>"Murcia",
        "NA"=>"Navarra",
        "O"=>"Asturias",
        "OR"=>"Ourense",
        "P"=>"Palencia",
        "PM"=>"Palma de Mallorca",
        "PO"=>"Pontevedra",
        "S"=>"Cantabria",
        "SA"=>"Salamanca",
        "SE"=>"Sevilla",
        "SG"=>"Segovia",
        "SO"=>"Soria",
        "SS"=>"Gipuzkoa",
        "T"=>"Tarragona",
        "TE"=>"Teruel",
        "TF"=>"Santa Cruz de Tenerife",
        "TO"=>"Toledo",
        "V"=>"Valencia",
        "VA"=>"Valladolid",
        "VI"=>"Álava",
        "Z"=>"Zaragoza",
        "ZA"=>"Zamora"
    ];
    }
}