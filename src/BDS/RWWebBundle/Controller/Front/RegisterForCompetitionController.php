<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/13/16
 * Time: 1:08 PM
 */

namespace BDS\RWWebBundle\Controller\Front;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\RWCompetitionBundle\Form\ExtraData\RegisterWithAthleteExtraDataCollectionType;
use BDS\RWCompetitionBundle\Form\Register\EmailRegisterType;
use BDS\RWCompetitionBundle\Form\Register\RegisterType;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class RegisterForCompetitionController extends FOSRestController{

    /**
     * @Rest\Get("{slug}" , name="front_competition_redirect" ,requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     */
    public function easyRedirectAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return $this->redirectToRoute("front_index");
        }
        if((!$competition->isPublished())||(!$competition->hasPublishedPhases())){
            return $this->redirectToRoute("front_index");
        }
        $lastPhase=$competition->getLastPublishedPhase();
        return $this->redirectToRoute("front_competition",["slug"=>$slug,"phase_slug"=>$lastPhase->getSlug()]);

    }

    /**
     * @Rest\Get("{slug}/{phase_slug}/Register" , name="front_competition_register" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     */
    public function registerAction(Request $request,$slug,$phase_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        };
        if($this->getUser() instanceof User){
            if($this->get("bdsrw_competition.user")->isUserInPhase($this->getUser(),$phase)){
                /** @var CategoryAthlete $categoryAthlete */
                $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($this->getUser());
                $categoryAthlete=$this->get("bdsrw_competition.user")->getUserInPhase($this->getUser(),$phase);
                return $this->redirectionResponse($competition,$categoryAthlete,$userExtension);

            }
            $form=$this->createForm(RegisterType::class,null,["method"=>"POST","action"=>$this->generateUrl("front_post_competition_user_register",["slug"=>$slug,"phase_slug"=>$phase_slug]),"phase"=>$phase]);
            return new Response($this->renderView('@BDSRWWeb/Front/Pages/register/pre_register_user.html.twig',["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()]));
        } else {
            $form=$this->createForm(EmailRegisterType::class,null,["method"=>"POST","action"=>$this->generateUrl("front_post_competition_anonymus_register",["slug"=>$slug,"phase_slug"=>$phase_slug]),"phase"=>$phase]);
            return new Response($this->renderView('@BDSRWWeb/Front/Pages/register/pre_register_anonymus.html.twig',["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()]));
        }
    }
    /**
     * @Rest\Post("{slug}/{phase_slug}/Register/Anonymus" , name="front_post_competition_anonymus_register" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/register/pre_register_anonymus.html.twig")
     */
    public function registerAnonymusPostAction(Request $request,$slug,$phase_slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        };
        if($this->getUser() instanceof User){
            return $this->redirectToRoute("front_competition",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        }
        $form=$this->createForm(EmailRegisterType::class,null,["method"=>"POST","action"=>$this->generateUrl("front_post_competition_anonymus_register",["slug"=>$slug,"phase_slug"=>$phase_slug]),"phase"=>$phase]);
        $form->handleRequest($request);
        if($form->isValid()){
            $email=$form->get("email")->getData();
            /** @var Category $category */
            $category=$form->get("category")->getData();

            $user=$this->get("bdsrw_competition.user")->createRoninFoxUser($email,true);
            $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
            $categoryAthlete=$this->get("bdsrw_competition.user")->createCategoryAthlete($userExtension,$category);
            $this->get("bdsrw_competition.mailer")->mailNewRegisteredUser($user);
            $this->get("doctrine.orm.default_entity_manager")->flush();
            $license=$this->get("bds.multitenant.license_service")->getLicense();
            $user->setActiveLicense($license);
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            return $this->redirectionResponse($competition,$categoryAthlete,$userExtension);
        }
        return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Rest\Post("{slug}/{phase_slug}/Register/User" , name="front_post_competition_user_register" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/register/pre_register_anonymus.html.twig")
     */
    public function registerUserPostAction(Request $request,$slug,$phase_slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        };
        /** @var User $user */
        if(!($user=$this->getUser()) instanceof User){
            return $this->redirectToRoute("front_post_competition_anonymus_register",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        }

        $form=$this->createForm(RegisterType::class,null,["method"=>"POST","action"=>$this->generateUrl("front_post_competition_anonymus_register",["slug"=>$slug,"phase_slug"=>$phase_slug]),"phase"=>$phase]);
        $form->handleRequest($request);
        if($this->get("bdsrw_competition.user")->isUserInCompetition($user,$competition)){
            $form->addError(new FormError(sprintf("El usuario '%s' ya está inscrito para esta competición",$user->getCompleteName())));
        }
        if($form->isValid()){
            /** @var Category $category */
            $category=$form->get("category")->getData();
            $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
            $categoryAthlete=$this->get("bdsrw_competition.user")->createCategoryAthlete($userExtension,$category);
            $this->get("doctrine.orm.default_entity_manager")->flush();
            return $this->redirectionResponse($competition,$categoryAthlete,$userExtension);
        }
        return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * @Rest\Get("{slug}/{phase_slug}/{category_slug}/Register/Extra" ,  name="front_post_competition_register_extra_get" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","category_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Rest\Post("{slug}/{phase_slug}/{category_slug}/Register/Extra" , name="front_post_competition_register_extra" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","category_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/register/extra_register.html.twig")
     */
    public function registerExtraDataAction(Request $request,$slug,$phase_slug,$category_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        /** @var User $user */
        if(!($user=$this->getUser()) instanceof User){
            return $this->redirectToRoute("front_post_competition_anonymus_register",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        }
        /** @var CategoryAthlete $categoryAthlete */
        if(!$categoryAthlete=$this->get("bdsrw_competition.user")->getUserInPhase($this->getUser(),$phase)){
            return $this->redirectToRoute("front_competition_register",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        }
        /** @var Category $category */
        if(!$category=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:Category")->findOneByCompetitionAndSlug($competition,$category_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        if($category!==$categoryAthlete->getCategory()){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        $form=$this->createForm(RegisterWithAthleteExtraDataCollectionType::class,null,["method"=>"POST","competition"=>$competition,"athlete"=>$categoryAthlete,"action"=>$this->generateUrl("front_post_competition_register_extra",["slug"=>$slug,"phase_slug"=>$phase_slug,"category_slug"=>$category_slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                /** @var UserExtension $userExtension **/
                $userExtension=$form->get("user_extension")->getData();
                $user=$userExtension->getRoninFoxUser();
                $status=$this->get("bdsrw_competition.user")->getAthleteStatus($categoryAthlete);
                $categoryAthlete->setStatus($status);
                $this->get("fos_user.user_manager")->updateUser($user);
                $this->get("doctrine.orm.default_entity_manager")->persist($categoryAthlete);
                $this->get("doctrine.orm.default_entity_manager")->persist($userExtension);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_competition.mailer")->mailNewCompetitionAthletes([$categoryAthlete],$phase,$category);
                return $this->redirectionResponse($competition,$categoryAthlete,$userExtension);
            }
        }

        return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * @Rest\Get("{slug}/{phase_slug}/{category_slug}/Payment" , name="front_competition_register_payment" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","category_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/register/register_payment.html.twig")
     */
    public function paymentAction(Request $request,$slug,$phase_slug,$category_slug)
    {
        /** @var Competition  $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        if(!$this->get("bdsrw_competition.user")->isUserInPhase($this->getUser(),$phase)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        }
        /** @var Category $category */
        if(!$category=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:Category")->findOneByCompetitionAndSlug($competition,$category_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($this->getUser());
        /** @var CategoryAthlete $athlete */
        if(!$athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneBy(["userExtension"=>$userExtension,"category"=>$category])) {
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        }
        if($athlete->isPaid()){
            return $this->redirectToRoute("front_competition_register_thankyou",["phase_slug"=>$phase_slug,"slug"=>$slug,"category_slug"=>$category_slug]);
        }
        $this->get("bdsrw_payment.athlete_order_service")->createOrderFromCategory($athlete);
        $public_key=$competition->getGatewayConfig()->getConfig()["publishable_key"];

        return ["competition"=>$competition,"category"=>$category,"phase"=>$phase,"public_key"=>$public_key];
    }
    /**
     * @Rest\Post("{slug}/{phase_slug}/{category_slug}/Payment/Process", name="front_competition_register_payment_proccess",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","category_slug"="^[a-z0-9-]+$"} ,defaults={"_format"="json"},options={"expose"=true})
     */
    public function registerPaymentAction(Request $request,$slug,$phase_slug,$category_slug){
        /** @var Competition  $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        if(!$this->get("bdsrw_competition.user")->isUserInPhase($this->getUser(),$phase)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        }
        /** @var Category $category */
        if(!$category=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:Category")->findOneByCompetitionAndSlug($competition,$category_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($this->getUser());
        /** @var CategoryAthlete $athlete */
        if(!$athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneBy(["userExtension"=>$userExtension,"category"=>$category])) {
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        }
        $token=$request->get("stripeToken");
        $result=$this->get("bdsrw_payment.stripe")->createAthleteStripePayment($competition,$athlete,$userExtension->getRoninFoxUser(),$token);
        $url=$this->generateUrl("front_competition_register_thankyou",["slug"=>$slug,"phase_slug"=>$phase_slug,"category_slug"=>$category_slug]);

        return['success' =>$result["success"],"url"=>$url,"details"=>$result["message"]];
    }
    /**
     * @Security("has_role('ROLE_USER')")
     * @Rest\Get("{slug}/{phase_slug}/{category_slug}/ThankYou" , name="front_competition_register_thankyou" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/register/thank_you.html.twig")
     */
    public function thankYouAction(Request $request,$slug,$phase_slug,$category_slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            return $this->redirectToRoute("front_competition",["slug"=>$slug]);
        };
        /** @var User $user */
        if(!($user=$this->getUser()) instanceof User){
            return $this->redirectToRoute("front_post_competition_anonymus_register",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        }
        /** @var CategoryAthlete $categoryAthlete */
        if(!$categoryAthlete=$this->get("bdsrw_competition.user")->getUserInPhase($this->getUser(),$phase)){
            return $this->redirectToRoute("front_competition_register",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        }
        if(!$categoryAthlete->isPaid()){
            return $this->redirectToRoute("front_competition_register_payment",["phase_slug"=>$phase_slug,"slug"=>$slug,"category_slug"=>$category_slug]);
        }
        return ["competition"=>$competition,"phase"=>$phase,"category"=>$categoryAthlete->getCategory(),"athlete"=>$categoryAthlete];
    }


    private function redirectionResponse(Competition $competition,CategoryAthlete $categoryAthlete,UserExtension $userExtension){

        $category=$categoryAthlete->getCategory();
        $phase=$category->getPhase();
        $routeOptionsArray=["slug"=>$competition->getSlug(),"phase_slug"=>$phase->getSlug(),"category_slug"=>$category->getSlug()];
        if(!$this->get("bdsrw_competition.competition_service")->userIsCompleted($competition,$categoryAthlete,$userExtension)){
            return $this->redirectToRoute("front_post_competition_register_extra_get",$routeOptionsArray);
        }
        if($this->get("bdsrw_competition.competition_service")->athleteHasToPay($categoryAthlete)){
            return $this->redirectToRoute("front_competition_register_payment",$routeOptionsArray);
        } else {
            return $this->redirectToRoute("front_competition_register_thankyou",$routeOptionsArray);
        }
    }
}