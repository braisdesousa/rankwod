<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/13/16
 * Time: 1:08 PM
 */

namespace BDS\RWWebBundle\Controller\Front;


use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\Register\RegisterType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontController extends FOSRestController{

    /**
     * @Rest\Get("{slug}/{phase_slug}" , name="front_competition" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/competition/competition.twig")
     */
    public function showCompetitionAction(Request $request,$slug,$phase_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute("front_index");
        };
        $results=$this->get("bdsrw_cache.competition")->getCachedData($competition);
        $boxes=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:UserExtension")->findCompetitionBoxes($competition);
        return ["competition"=>$competition,"phase"=>$phase,"results"=>$results,"boxes"=>$boxes,"admin_results"=>false];
    }
    /**
     * @Rest\Get("{slug}/{phase_slug}/Workouts" , name="front_competition_workouts" ,requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/workouts/events.html.twig")
     */
    public function showCompetitionWorkoutsAction(Request $request,$slug,$phase_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute("front_index");
        };
        $results=$this->get("bdsrw_cache.competition")->getCachedData($competition);
        $boxes=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:UserExtension")->findCompetitionBoxes($competition);
        return ["competition"=>$competition,"phase"=>$phase,"results"=>$results,"boxes"=>$boxes,"admin_results"=>false];
    }
    /**
     * @Rest\Get("{slug}/Leaderboard" , name="front_competition_leaderboard" ,requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/results.html.twig")
     */
    public function resultsAction(Request $request,$slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        $results=$this->get("bdsrw_cache.competition")->getCachedData($competition);
        $boxes=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:UserExtension")->findCompetitionBoxes($competition);
        return ["competition"=>$competition,"results"=>$results,"boxes"=>$boxes,"admin_results"=>false];
    }
    /**
     * @Route("{slug}/{phase_slug}/{event_slug}/" , name="front_competition_event" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$", "event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/workout/event.html.twig")
     */
    public function eventAction(Request $request,$slug,$phase_slug,$event_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute("front_index");
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute("front_competition_redirect",["slug"=>$slug]);
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute("front_competition",["slug"=>$slug,"phase_slug"=>$phase_slug]);
        };

        return ["competition"=>$competition,"phase"=>$phase,"event"=>$event];
    }
    /**
     * @Route("{slug}/{phase_slug}/Heats/" , name="front_competition_rounds" ,requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/rounds.html.twig")
     */
    public function showFaseRoundsAction(Request $request,$slug,$phase_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('front_index');
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('front_index');
        };
        if(!$phase->hasRounds()){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('front_index');
        };
        if(!$heats=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Round")->findBy(["phase"=>$phase],["created"=>"ASC"])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        return ["heats"=>$heats,"competition"=>$competition,"phase"=>$phase];
    }
}