<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 6/13/16
 * Time: 9:58 AM
 */

namespace BDS\RWWebBundle\Controller\Front;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ResultController extends FOSRestController
{
    /**
     * @Get("{slug}/Resultados" , name="front_competition_results" ,requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/results.html.twig")
     */
    public function resultsAction(Request $request,$slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        $results=$this->get("bdsrw_cache.competition")->getCachedData($competition);
        $point_system=$this->get("bdsrw_cache.competition")->getPointSystem($competition);
        $athletes=$this->get("rankwod.athlete")->getCompetitionAthletes($competition);
        return ["competition"=>$competition,"results"=>$results,"point_system"=>$point_system,"boxes"=>$this->get("rankwod.athlete")->getCompetitionBoxes($competition),"athletes"=>$athletes,"admin_results"=>false];
    }
    /**
     * @Get("{slug}/Admin/" , name="front_competition_results_admin" ,requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/results.html.twig")
     */
    public function resultsAdminAction(Request $request,$slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        $user=$this->getUser();
        if(!($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminOrJudgeInCompetition($user,$competition))) {
            return $this->redirectToRoute("front_competition_results",["slug"=>$slug]);
        }
        $results=$this->get("bdsrw_cache.competition")->getCachedData($competition,true,false);
        $boxes=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:UserExtension")->findCompetitionBoxes($competition);
        return ["competition"=>$competition,"results"=>$results,"boxes"=>$boxes,"admin_results"=>true];
    }
    /**
     * @Get("{slug}/Delete-Caches/" , name="front_competition_results_delete_caches" ,requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     */
    public function deleteCachesAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            throw new NotFoundHttpException(sprintf("Competition '%s' not found",$slug));
        }
        $user=$this->getUser();
        if(!($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminOrJudgeInCompetition($user,$competition))) {
            return $this->redirectToRoute("front_competition_results",["slug"=>$slug]);
        }
        foreach($competition->getPhases() as $phase){
            foreach($phase->getEvents() as $event){
                if($eventCache=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCacheBundle:EventResultCache")->findOneBy(["event"=>$event])){
                    $this->get("doctrine.orm.default_entity_manager")->remove($eventCache);
                }
                if($phase->getOverall()){
                    if($overallCache=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCacheBundle:OverallResultCache")->findOneBy(["overall"=>$phase->getOverall()])){
                        $this->get("doctrine.orm.default_entity_manager")->remove($overallCache);
                    }
                }
            }

        }
        if($competitionCache=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCacheBundle:CompetitionResultCache")->findOneBy(["competition"=>$competition])){
            $this->get("doctrine.orm.default_entity_manager")->remove($competitionCache);
        }

        $this->get("doctrine.orm.default_entity_manager")->flush();
        return $this->redirectToRoute("front_competition_results",["slug"=>$slug]);
    }
}