<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 6/13/16
 * Time: 9:58 AM
 */

namespace BDS\RWWebBundle\Controller\Front;


use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends FOSRestController
{
    /**
     * @Get("/" , name="front_index" , defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Front/Pages/index/index.html.twig")
     */
    public function resultsAction(Request $request)
    {
        $phaseRepository=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase");
        $currentPhases=$phaseRepository->getCurrentPhases();
        $pastPhases=$phaseRepository->getPastPhases();
        $futurePhases=$phaseRepository->getFuturePhases();
        return ["current"=>$currentPhases,"past"=>$pastPhases,"future"=>$futurePhases];
    }
}