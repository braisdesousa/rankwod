<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/13/16
 * Time: 1:08 PM
 */

namespace BDS\RWWebBundle\Controller\Front;


use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class RegisterController extends FOSRestController{

    /**
     * @Rest\Get("/Register" , name="front_register" ,defaults={"_format"="html"},options={"expose"=true})
     * @Rest\Post("/Register" , name="front_register_post" ,defaults={"_format"="html"},options={"expose"=true})
     */
    public function registerAction(Request $request)
    {

    }
}