<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 14/02/16
 * Time: 18:20
 */

namespace BDS\RWWebBundle\Controller\Front;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\UserBundle\Entity\Role;
use BDS\UserBundle\Entity\User;
use BDS\UserBundle\Form\EditProfileType;
use BDS\UserBundle\Form\ReducedProfileType;
use BDS\UserBundle\Form\UserType;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Gedmo\Sluggable\Util\Urlizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @Route("/register/")
 * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
 */

class CreateUserController extends FOSRestController
{
    /**
     *  @Get("", name="get_front_register", defaults={"_format"="html"},options={"expose"=true})
     * @Post("", name="post_front_register", defaults={"_format"="html"},options={"expose"=true})
     * @Template("@FOSUser/Registration/register.html.twig")
     */

    public function registerUserAction(Request $request){

        $user= $this->get("fos_user.user_manager")->createUser();
        $form= $this->createForm(ReducedProfileType::class,$user,["action"=>$this->generateUrl("post_front_register"),"add_submit"=>true,"is_admin"=>true,"method"=>"POST"]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $role=$this->get("doctrine.orm.user_entity_manager")->getRepository("BDSUserBundle:Role")->findOneBy(["role"=>Role::ROLE_USER]);
                $user->setUsername($this->get("bds.user.service")->getUsername($user));
                $user->addRole($role);
                $user->setConfirmationToken($this->get("fos_user.util.token_generator")->generateToken());
                $this->get("fos_user.user_manager")->updateUser($user);
                $this->get("bdsrw_competition.mailer")->mailNewRegisteredUser($user);
                return $this->redirectToRoute("get_front_registered",["username"=>$user->getUsername()]);
            }
        }
        return ["form"=>$form];
    }
    /**
     *  @Get("Registered/{username}/", name="get_front_registered",requirements={"username"="^[a-z0-9-\.]+$"}, defaults={"_format"="html"},options={"expose"=true})
     *  @Template("@BDSRWWeb/Front/registered.html.twig")
     */

    public function registeredUserAction(Request $request,$username){


        if(!$user= $this->get("fos_user.user_manager")->findUserByUsername($username)){
            return $this->redirectToRoute('front_index');
        }
        if(!$user->getConfirmationToken()){
            return $this->redirectToRoute("fos_user_security_login");
        }
        return ["user"=>$user];
    }
    /**
     * @Get("Confirm/{token}/", name="get_front_register_confirm",requirements={"token"="^[a-zA-z0-9-\_]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("Confirm/{token}/", name="post_front_register_confirm",requirements={"token"="^[a-zA-z0-9-\_]+$"}, defaults={"_format"="html"},options={"expose"=true})
     */

    public function confirmUserAction(Request $request,$token){

        if(!$user=$this->get("fos_user.user_manager")->findUserByConfirmationToken($token)){
            $this->addFlash("token_error","error");
            return $this->redirectToRoute("fos_user_security_login");
        }
           $user->setConfirmationToken(null);
           $user->setEnabled(true);
           $this->get("fos_user.user_manager")->updateUser($user);
           $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
           $this->get('security.token_storage')->setToken($token);
           $this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
           $this->addFlash("new_user",true);
           return $this->redirectToRoute("v2_panel_competitions");
    }
}