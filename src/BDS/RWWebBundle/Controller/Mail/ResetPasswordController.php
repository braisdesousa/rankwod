<?php

namespace BDS\RWWebBundle\Controller\Mail;


use BDS\UserBundle\Entity\User;
use BDS\UserBundle\Form\ForgottenPasswordType;
use BDS\UserBundle\Form\UsernameAndPasswordType;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Gedmo\Sluggable\Util\Urlizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class MailController
 * @package BDS\RWWebBundle\Controller\Mail
 * @Route("/Resetting")
 */
class ResetPasswordController extends FOSRestController{


    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Get("/",name="get_reset_password", defaults={"_format"="html"},options={"expose"=true})
     * @Post("/",name="post_reset_password", defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/RessetPassword/resset_password.html.twig")
     */
    public function getForgottenPasswordAction(Request $request)
    {
        $form=$this->createForm(ForgottenPasswordType::class,null,[]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $mail=$form->get("email")->getData();
                if($user=$this->get("fos_user.user_manager")->findUserByUsernameOrEmail($mail)){
                    $user->setConfirmationToken(Urlizer::transliterate($this->get("bds.cryptographic.service")->encrypt($user->getUsername())));
                    $user->setPlainPassword(md5(time()));
                    $user->setEnabled(false);
                    $this->get("fos_user.user_manager")->updateUser($user);
                    /** @var User $user */
                    $this->get("bdsrw_competition.mailer")->sendForgottenPasswordMail($user);
                }
                $this->get("session")->getFlashBag()->add("reset",'something');
                return $this->redirectToRoute("fos_user_security_login");
            }
        }
        return ["form"=>$form->createView()];
    }
    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Get("/{token}",name="get_set_reset_password",requirements={"token"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{token}",name="post_set_reset_password",requirements={"token"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/RessetPassword/resset_password.html.twig")
     */
    public function setForgottenPasswordAction(Request $request,$token)
    {

        if(!$user=$this->get("doctrine.orm.user_entity_manager")->getRepository("BDSUserBundle:User")->findOneBy(["confirmationToken"=>$token])){
            return $this->redirectToRoute("fos_user_security_login");
        }
        $form=$this->createForm(UsernameAndPasswordType::class,$user,["method"=>"POST","action"=>$this->generateUrl("post_set_reset_password",["token"=>$token])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $user->setConfirmationToken(null);
                $user->setEnabled(true);
                $this->get("fos_user.user_manager")->updateUser($user);
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                return $this->redirectToRoute('v2_panel_competitions');
            }
        }
        return ["form"=>$form->createView()];
    }
}