<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/13/16
 * Time: 3:28 PM
 */

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;

use BDS\RWCompetitionBundle\Entity\Bot;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\Bot\BotCreateType;
use BDS\RWCompetitionBundle\Form\BotType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/C/{slug}/{phase_slug}/bot")
 * @Security("has_role('ROLE_USER')")
 */
class BotController extends FOSRestController
{

    /**
     * @Get("/New/",name="v2_panel_bots_get_create",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"},defaults={"_format"="html"},options={"expose"=true})
     * @Post("/New/",name="v2_panel_bots_post_create",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"},defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/athletes/add_bots_to_category.html.twig")
     */
    public function createBotAction(Request $request,$slug,$phase_slug)
    {
	    /** @var Competition $competition */
	    if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
		    $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
		    return $this->redirectToRoute('v2_panel_competitions');
	    };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            return ["message"=>"Se ha producido un error"];
        };
        if(!$phase->isPaid()){
            return $this->redirectToRoute("v2_panel_competition",["slug"=>$slug]);
        }
	    /** @var User $user **/
	    $user=$this->getUser();
	    if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
		    $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Error"));
		    return $this->redirectToRoute('v2_panel_competitions');
	    }
	    $form=$this->createForm(BotCreateType::class,null,["phase"=>$phase,"method"=>"POST","action"=>$this->generateUrl("v2_panel_bots_post_create",["slug"=>$slug,"phase_slug"=>$phase_slug])]);
	    if($request->getMethod()=="POST"){
		    $form->handleRequest($request);
		    if($form->isValid()){
			    $bots=$form->get('names')->getData();
			    $category=$form->get('category')->getData();
			    $this->get('bdsrw_competition.user')->createBots($category,$bots);
			    $this->get("doctrine.orm.default_entity_manager")->flush();
			    $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Bots creados"));
			    return $this->redirectToRoute("v2_panel_athletes_get",["slug"=>$slug]);
		    }
	    }
	    return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Get("/{bot_slug}/Edit/",name="v2_panel_bots_get_edit",requirements={"slug"="^[a-z0-9-]+$","bot_slug"="^[a-z0-9-]+$"},defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{bot_slug}/Edit/",name="v2_panel_bots_post_edit",requirements={"slug"="^[a-z0-9-]+$","bot_slug"="^[a-z0-9-]+$"},defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/Panel/Competition/competition_templates/bot/competition_bot_form.html.twig")
     */
    public function editBotAction(Request $request,$slug,$bot_slug)
    {
	    /** @var Competition $competition */
	    if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
		    $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
		    return $this->redirectToRoute('v2_panel_competitions');
	    };
	    /** @var Bot $bot */
	    if(!$bot=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Bot")->findOneBySlugAndCompetition($competition,$bot_slug)){
		    $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
		    return $this->redirectToRoute('v2_panel_competitions');
	    };
	    /** @var User $user **/
	    $user=$this->getUser();
	    if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
		    $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Error"));
		    return $this->redirectToRoute('v2_panel_competitions');
	    }

	    $form=$this->createForm(BotType::class,$bot,["method"=>"POST","action"=>$this->generateUrl("post_panel_competition_bot_edit",["slug"=>$slug,"bot_slug"=>$bot_slug])]);
        if($request->getMethod()=="POST"){
	        $form->handleRequest($request);
	        if($form->isValid()){
		        $this->get("doctrine.orm.default_entity_manager")->persist($bot);
		        $this->get("doctrine.orm.default_entity_manager")->flush();
		        return $this->redirectToRoute("get_panel_competition_bots",["slug"=>$slug]);
	        }
        }
        return ["competition"=>$competition,"form"=>$form->createView()];
    }
    /**
     * @Delete("/{bot_slug}/",name="v2_panel_bots_delete",defaults={"_format"="json"},requirements={"slug"="^[a-z0-9-]+$","bot_slug"="^[a-z0-9-]+$"},options={"expose"=true})
     */
    public function deleteBotAction(Request $request,$slug,$bot_slug)
    {
	    /** @var User $user */
	    if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
		    return ["error"=>1,"message"=>sprintf("La competicion '%s' no existe",$slug)];
	    };
	    /** @var Bot $bot */
	    if(!$bot=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Bot")->findOneBySlugAndCompetition($competition,$bot_slug)){
		    return ["error"=>1,"message"=>sprintf("El Bot '%s' no existe",$bot_slug)];
	    };
	    /** @var User $user **/
	    $user=$this->getUser();
	    if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
		    return ["error"=>1,"message"=>sprintf("No tienes permiso para relaizar esta accción")];
	    }
	    $this->get("doctrine.orm.default_entity_manager")->remove($bot);
	    $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
}