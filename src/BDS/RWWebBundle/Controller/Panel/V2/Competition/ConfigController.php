<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWCompetitionBundle\Form\PhaseType;
use BDS\RWPaymentBundle\Entity\Order;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Config")
 * @Security("has_role('ROLE_USER')")
 */
class ConfigController extends FOSRestController
{

    /**
     * @Get("/", name="v2_panel_config_get",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Put("/", name="v2_panel_config_put",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/competition_configuration.html.twig")
     */
    public function getCompetitionConfigAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $form=$this->createForm(CompetitionType::class,$competition,["method"=>"PUT","action"=>$this->generateUrl("v2_panel_config_put",["slug"=>$slug])]);
        if($request->getMethod()=="PUT"){
            $form->handleRequest($request);
            if($form->isValid()){
                $competition->preSave();
                $this->get("doctrine.orm.default_entity_manager")->persist($competition);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_config_get",["slug"=>$competition->getSlug()]);
            }
        }
        return ["competition"=>$competition,"form"=>$form->createView()];
    }

    /**
     * @Get("/Phases/", name="v2_panel_config_phase",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/event_configuration.html.twig")
     */
    public function getDefaultFaseConfigAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$competition->getPhases()->count()){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Crea tu primera fase"));
            return $this->redirectToRoute("v2_panel_config_get_phase_create",["slug"=>$slug]);
        };
        $phase=$competition->getPhases()->first();
        $form=$this->createForm(PhaseType::class,$phase,["method"=>"PUT","action"=>$this->generateUrl("v2_panel_config_put_phase",["slug"=>$slug,"phase_slug"=>$phase->getSlug()])]);
        return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Get("/{phase_slug}/", name="v2_panel_config_get_phase",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Put("/{phase_slug}/", name="v2_panel_config_put_phase",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/event_configuration.html.twig")
     */
    public function getPhaseConfigAction(Request $request,$slug,$phase_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $type=$phase->getPhaseType();
        $form=$this->createForm(PhaseType::class,$phase,["method"=>"PUT","action"=>$this->generateUrl("v2_panel_config_put_phase",["slug"=>$slug,"phase_slug"=>$phase_slug])]);
        if($request->getMethod()=="PUT"){
            $form->handleRequest($request);
            if($form->isValid()){
                if(!$phase->getPhaseType()){
                    $phase->setPhaseType($type);
                }
                $this->get("doctrine.orm.default_entity_manager")->persist($competition);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_config_get_phase",["slug"=>$slug,"phase_slug"=>$phase_slug]);
            }
        }
        return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Get("/New/", name="v2_panel_config_get_phase_create",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/New/", name="v2_panel_config_post_phase_create",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/event_configuration.html.twig")
     */
    public function createPhaseConfigAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $phase=new Phase();
        $form=$this->createForm(PhaseType::class,$phase,["method"=>"POST","action"=>$this->generateUrl("v2_panel_config_post_phase_create",["slug"=>$slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                if($competition->getPhases()->count()){
                    /** @var Phase $phase */
                    $oldPhase=$competition->getPhases()->first();
                    /** @var Category $category */
                    foreach($oldPhase->getCategories() as $category){
                        $nc=new Category();
                        $nc->setName($category->getName());
                        $nc->setCategoryType($category->getCategoryType());
                        $nc->setPhase($phase);
                        $nc->setMaxAthletes($category->getMaxAthletes());
                        $this->get("doctrine.orm.default_entity_manager")->persist($nc);
                        $phase->addCategory($nc);
                    }
                }
                $competition->addPhase($phase);
                $phase->setCompetition($competition);
                $this->get("bdsrw_payment.customer_order_service")->createOrderFromPhase($phase);
                $this->get("doctrine.orm.default_entity_manager")->persist($competition);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_config_get_phase",["slug"=>$slug,"phase_slug"=>$phase->getSlug()]);
            }
        }
        return ["competition"=>$competition,"form"=>$form->createView()];
    }
    /**
     * @Rest\Patch("/{phase_slug}/Publish/", name="v2_panel_config_patch_phase_publish",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function publishPhaseAction(Request $request,$slug,$phase_slug)
    {
        /** @var Competition $competition */
        if (!$competition = $this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR, sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if (!$phase = $this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR, sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if (!(($this->getUser()->isAdmin()) || $this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(), $competition))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR, sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $value = filter_var(($request->get("status", false)),FILTER_VALIDATE_BOOLEAN);
        if ($this->get("bdsrw_competition.competition_service")->isPhaseReadyToPublish($phase)) {
            $phase->setPublished($value);
            $this->get("doctrine.orm.default_entity_manager")->persist($phase);
            $this->get("doctrine.orm.default_entity_manager")->flush();
            return ["success" => 1,"message"=>sprintf("Fase %s %s",$phase->getName(),$phase->isPublished()?"Publicada":"Despublicada")];
        }
        return ["success" => 0,"message"=>sprintf("Fase %s no está lista para ser publicada",$phase->getName())];
    }
    /**
     * @Rest\Patch("/Publish/", name="v2_panel_config_patch_competition_publish",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function publishCompetitionAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

            $value = filter_var(($request->get("status", false)),FILTER_VALIDATE_BOOLEAN);
            $competition->setPublish($value);
            $this->get("doctrine.orm.default_entity_manager")->persist($competition);
            $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success" => 1,"message"=>sprintf("Copmetición %s %s",$competition->getName(),$competition->isPublished()?"Publicada":"Despublicada")];
    }
    /**
     * @Rest\Delete("/{phase_slug}/", name="v2_panel_config_delete_phase",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deletePhaseAction(Request $request,$slug,$phase_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if (!$phase = $this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR, sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $this->get("doctrine.orm.default_entity_manager")->remove($phase);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return [];
    }

}