<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Form\CategoryType;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Categories")
 * @Security("has_role('ROLE_USER')")
 */
class CategoriesController extends FOSRestController
{
    /**
     * @Get("/", name="v2_panel_categories_get",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/categories/categories.html.twig")
     */
    public function getCompetitionJudgesAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        return [
            "competition"=>$competition
        ];
    }
    /**
     * @Get("/C/Create", name="v2_panel_categories_get_create",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/C/Create", name="v2_panel_categories_post_create",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/categories/categories_form.html.twig")
     */
    public function getCompetitionCreateCategoryForm(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes permiso para estar aquí"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $category=new Category();
        $form=$this->createForm(CategoryType::class, $category,["method"=>"POST","action"=>$this->generateUrl("v2_panel_categories_post_create",["slug"=>$slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                foreach($competition->getPhases() as $phase){
                    $newCategory=clone $category;
                    $newCategory->setPhase($phase);
                    $phase->addCategory($category);
                    $this->get("doctrine.orm.default_entity_manager")->persist($phase);
                    $this->get("doctrine.orm.default_entity_manager")->persist($newCategory);
                }
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Categoría '%s' Añadida",$category->getName()));
                return $this->redirectToRoute("v2_panel_categories_get",["slug"=>$slug]);
            }
        }
        return ["competition"=>$competition,"form"=>$form->createView()];
    }
    /**
     * @Get ("/{phase_slug}/C/{category_slug}/Edit", name="v2_panel_categories_get_edit",requirements={"slug"="^[a-z0-9-]+$","category_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{phase_slug}/C/{category_slug}/Edit", name="v2_panel_categories_post_edit",requirements={"slug"="^[a-z0-9-]+$","category_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/categories/categories_form.html.twig")
     */
    public function getCompetitionEditCategoryForm(Request $request,$slug,$phase_slug,$category_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Category $category */
        if(!$category=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:Category")->findOneByCompetitionAndSlug($competition,$category_slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes permiso para estar aquí"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $form=$this->createForm(CategoryType::class, $category,["method"=>"POST","action"=>$this->generateUrl("v2_panel_categories_post_edit",["slug"=>$slug,"phase_slug"=>$phase_slug,"category_slug"=>$category_slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $this->get("doctrine.orm.default_entity_manager")->persist($category);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Categoría Editada Correctamente"));
                return $this->redirectToRoute("v2_panel_categories_get",["slug"=>$slug,"phase_slug"=>$phase_slug]);
            }
        }
        return ["competition"=>$competition,"phase"=>$phase,"category"=>$category,"form"=>$form->createView()];
    }

    /**
     * @param Request $request
     * @param $slug
     * @Delete("/C/{category_slug}/", name="v2_panel_categories_delete",requirements={"slug"="^[a-z0-9-]+$","category_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     */
    public function deleteCategory(Request $request,$slug,$phase_slug,$category_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Category $category */
        if(!$category=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:Category")->findOneByCompetitionAndSlug($competition,$category_slug)){
            return ["error"=>1,"message"=>sprintf("No existe la categoria '%s'",$category_slug)];
        };
        /** @var User $user **/
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            return ["error"=>1,"message"=>sprintf("No tienes permiso para eliminar '%s'",$category->getName())];
        }
        $this->get("doctrine.orm.default_entity_manager")->remove($category);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["sucess"=>1,"message"=>sprintf("Categoria '%s' correctamente eliminada ",$category->getName())];
    }
}