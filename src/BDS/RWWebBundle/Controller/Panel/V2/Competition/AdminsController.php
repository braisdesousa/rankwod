<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Form\AdminUsersType;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Admins")
 * @Security("has_role('ROLE_USER')")
 */
class AdminsController extends FOSRestController
{


    /**
     * @Get("/", name="v2_panel_admins_get",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"})
     * @Template("@BDSRWWeb/v2/competition/competition_admins.html.twig")
     */
    public function getCompetitionAdminsAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $pending_admins=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findByCompetitionAndStatus($competition,CompetitionAdmin::STATUS_PENDING);
        return [
            "competition"=>$competition,
            "pending_admins"=>count($pending_admins),
            "admins"=>$this->get("rankwod.admins")->getCompetitionAdmins($competition)];
    }
    /**
     * @Get("/Add/", name="v2_panel_admins_get_form",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/Add/",name="v2_panel_admins_get_form_post",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/admins/admins_form.html.twig")
     */
    public function getPanelCompetitionAdminFormAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $form=$this->createForm(AdminUsersType::class,null,["method"=>"POST","action"=>$this->generateUrl("v2_panel_admins_get_form_post",["slug"=>$slug])]);

        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                if($form->isValid()){
                    $emails=$form->get("text")->getData();
                    $admins=$form->get("admins")->getData();
                    $users=$this->get("bdsrw_competition.user")->getAdminsForCompetition($emails,$admins,$competition);
                    $this->get("bdsrw_competition.admin.mailer")->mailAdmins($users,$competition);
                    $this->get("doctrine.orm.default_entity_manager")->flush();
                    $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Administradores añadidos"));
                    return $this->redirectToRoute('v2_panel_admins_get',["slug"=>$competition->getSlug()]);
                }
            }
        }
        return ["form"=>$form->createView(),"competition"=>$competition];
    }
    /**
     * @Rest\Patch("/Accept/", name="v2_panel_admins_patch_accept",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelAdminAcceptAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        $user=$this->getUser();
        /** @var CompetitionAdmin $admin */
        if(!$admin=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findOneByCompetitionAndUserId($competition,$user)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Admin on this Competition",$user->getUsername())];
        }
        $admin->setStatus(CompetitionAdmin::STATUS_ACCEPTED);
        $this->get("doctrine.orm.default_entity_manager")->persist($admin);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1,"slug"=>$slug];
    }
    /**
     * @Rest\Patch("/Decline/", name="v2_panel_admins_patch_decline",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelAdminDeclineAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        $user=$this->getUser();
        /** @var CompetitionAdmin $admin */
        if(!$admin=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findOneByCompetitionAndUserId($competition,$user)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Admin on this Competition",$user->getUsername())];
        }
        $admin->setStatus(CompetitionAdmin::STATUS_DECLINED);
        $this->get("doctrine.orm.default_entity_manager")->persist($admin);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
    /**
     * @Rest\Patch("/re-email/{username}/", name="v2_panel_admins_patch_re_email_user",requirements={"slug"="^[a-z0-9-]+$","username"="^[a-z0-9-\.]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelReMailUserAction(Request $request,$slug,$username)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        if(!$user=$this->get("fos_user.user_manager")->findUserByUsername($username)){
            return ["error"=>1, "message"=>sprintf("User '%s' not found",$username)];
        }
        /** @var CompetitionAdmin $admin */
        if(!$admin=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findOneByCompetitionAndUserId($competition,$user)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Admin on this Competition",$username)];
        }
        $this->get("bdsrw_competition.admin.mailer")->mailAdmins([$admin],$competition);
        $admin->setStatus(CompetitionAdmin::STATUS_PENDING);
        $this->get("doctrine.orm.default_entity_manager")->persist($admin);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
    /**
     * @Rest\Patch("/re-email/", name="v2_panel_admins_patch_re_email",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelReMailAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        $admins=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findByCompetitionAndStatus($competition,CompetitionAdmin::STATUS_PENDING);
        $this->get("bdsrw_competition.admin.mailer")->mailAdmins($admins,$competition);
        return ["success"=>1];
    }
    /**
     * @Rest\Delete("/Delete/{username}/", name="v2_panel_admins_delete",requirements={"slug"="^[a-z0-9-]+$","username"="^[a-z0-9-\.]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deletePanelAdminAction(Request $request,$slug,$username)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        if(!$user=$this->get("fos_user.user_manager")->findUserByUsername($username)){
            return ["error"=>1, "message"=>sprintf("User '%s' not found",$username)];
        }
        if(!$admin=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findOneByCompetitionAndUserId($competition,$user)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Admin on this Competition",$username)];
        }

        $competition->deleteAdmin($admin);
        $this->get("doctrine.orm.default_entity_manager")->remove($admin);
        $this->get("doctrine.orm.default_entity_manager")->flush();

        return ["success"=>1];
    }
}