<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 21/03/18
 * Time: 9:16
 */

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;

use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\ImportPhaseAthletesType;
use BDS\RWWebBundle\Helper\NotyHelper;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Phases")
 * @Security("has_role('ROLE_USER')")
 */
class AthleteImporterController extends FOSRestController
{

    /**
     * @Get("{phase_slug}/{phase_old_slug}/AthleteImporter/", name="v2_panel_phase_import_get",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","phase_old_slug"="^[a-z0-9-]+$"} ,defaults={"_format"="html"},options={"expose"=true})
     * @Post("{phase_slug}/{phase_old_slug}/AthleteImporter/", name="v2_panel_phase_import_post",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","phase_old_slug"="^[a-z0-9-]+$"} ,defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/athletes/athlete_importer_from_phase.html.twig")
     */
    public function competitionPhaseFormAction(Request $request,$slug,$phase_slug,$phase_old_slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        if(!$phaseOld=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_old_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $autoInclude=($request->getMethod()=="GET");
        $form=$this->createForm(ImportPhaseAthletesType::class,null,["old_phase"=>$phaseOld,"phase"=>$phase,"auto_include"=>$autoInclude,"action"=>$this->generateUrl("v2_panel_phase_import_get",["slug"=>$slug,"phase_slug"=>$phase_slug,"phase_old_slug"=>$phase_old_slug])]);

        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $category=$form->get("category")->getData();
                $this->get("bdsrw_competition.phase_athlete_importer")->athleteImporter($phase,$category,$form->getData());
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Atletas añadidos a la phase '%s'",$phase->getSlug()));
                return $this->redirectToRoute("v2_panel_athletes_get",["slug"=>$slug]);
            }
        }

        return ["competition"=>$competition,"phase"=>$phase,"phase_old"=>$phaseOld,"form"=>$form->createView()];
    }
}