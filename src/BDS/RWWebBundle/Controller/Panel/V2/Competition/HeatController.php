<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Round;
use BDS\RWCompetitionBundle\Form\RoundType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Heats")
 * @Security("has_role('ROLE_USER')")
 */
class HeatController extends FOSRestController
{


    /**
     * @Get("/", name="v2_panel_heat_get",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/heats/competition_heats.html.twig")
     */
    public function getCompetitionJudgesAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $heats=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Round")->findByCompetition($competition);
        return ["competition"=>$competition,"heats"=>$heats];
    }
    /**
     * @Get("/{phase_slug}/Create/", name="v2_panel_heat_get_phase_create",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{phase_slug}/Create/", name="v2_panel_heat_post_phase_create",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/heats/competition_heats_new.html.twig")
     */
    public function getCreateHeatForPhaseAction(Request $request,$slug,$phase_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if($phase->getPhaseType()===Phase::TYPE_ONLINE){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No se pueden utilizar Tandas en una fase online"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var User $user **/
        $user=$this->getUser();
        if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $heat=new Round();
        $heat->setPhase($phase);
        $form= $this->createForm(RoundType::class,$heat,["method"=>"POST","phase"=>$phase,"action"=>$this->generateUrl("v2_panel_heat_post_phase_create",["slug"=>$slug,"phase_slug"=>$phase_slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $this->get("doctrine.orm.default_entity_manager")->persist($heat);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("La tanda '%s' se ha creado con éxito",$heat->getName()));
                return $this->redirectToRoute("v2_panel_heat_get",["slug"=>$slug]);
            }
        }
        return ["form"=>$form->createView(),"competition"=>$competition,"phase"=>$phase];

    }
    /**
     * @Get("/{phase_slug}/{heat_slug}/", name="v2_panel_heat_get_heat",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","heat_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/heats/competition_heats_view.html.twig")
     */
    public function getHeatAction(Request $request,$slug,$phase_slug,$heat_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if($phase->getPhaseType()===Phase::TYPE_ONLINE){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No se pueden utilizar Tandas en una fase online"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Round $heat */
        if(!$heats=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Round")->findBy(["phase"=>$phase],["created"=>"ASC"])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        return ["heats"=>$heats,"competition"=>$competition,"phase"=>$phase];

    }
    /**
     * @Get("/{phase_slug}/{heat_slug}/Edit", name="v2_panel_heat_get_phase_edit",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","heat_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{phase_slug}/{heat_slug}/Edit", name="v2_panel_heat_post_phase_edit",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","heat_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/heats/competition_heats_new.html.twig")
     */
    public function getEditHeatForPhaseAction(Request $request,$slug,$phase_slug,$heat_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if($phase->getPhaseType()===Phase::TYPE_ONLINE){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No se pueden utilizar Tandas en una fase online"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Round $heat */
        if(!$heat=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Round")->findOneBy(["slug"=>$heat_slug,"phase"=>$phase])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $form= $this->createForm(RoundType::class,$heat,["method"=>"POST","phase"=>$phase,"action"=>$this->generateUrl("v2_panel_heat_post_phase_edit",["slug"=>$slug,"phase_slug"=>$phase_slug,"heat_slug"=>$heat_slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $this->get("doctrine.orm.default_entity_manager")->persist($heat);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("La tanda '%s' se ha editado con éxito",$heat->getName()));
                return $this->redirectToRoute("v2_panel_heat_get",["slug"=>$slug]);
            }
        }
        return ["form"=>$form->createView(),"competition"=>$competition,"phase"=>$phase];

    }
    /**
     * @Delete("/{phase_slug}/{heat_slug}/", name="v2_panel_heat_delete",requirements={"slug"="^[a-z0-9-]+$","heat_slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deletePhaseHeatAction(Request $request,$slug,$phase_slug, $heat_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["message"=>"Se ha producido un error"];
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            return ["message"=>"Se ha producido un error"];
        };
        /** @var Round $heat */
        if(!$heat=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Round")->findOneBy(["slug"=>$heat_slug,"phase"=>$phase])){
            return ["message"=>"Se ha producido un error"];
        };
        if($phase->getPhaseType()===Phase::TYPE_ONLINE){
            return ["message"=>"Se ha producido un error"];
        }
        /** @var User $user **/
        $user=$this->getUser();
        if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
            return ["message"=>"Se ha producido un error"];
        }

        $heat->setAthletes(new ArrayCollection());
        $phase->removeRound($heat);
        $heat->setPhase(null);
        $this->get("doctrine.orm.default_entity_manager")->remove($heat);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>true];


    }
}