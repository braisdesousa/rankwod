<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Form\CompetitionTeamType;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CompetitionUsersType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Athletes")
 * @Security("has_role('ROLE_USER')")
 */
class AthletesController extends FOSRestController
{


    /**
     * @Get("/", name="v2_panel_athletes_get",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/competition_athletes.html.twig")
     */
    public function getCompetitionAthletesAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        return ["competition"=>$competition,"athletes"=>$this->get("rankwod.athlete")->getCompetitionAthletes($competition)];
    }
    /**
     * @Get("{phase_slug}/Team/Add/", name="v2_panel_athletes_team_get_form",    requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Rest\Post("{phase_slug}/Team/Add/",name="v2_panel_athletes_team_post",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/athletes/add_teams_to_category.html.twig")
     */
    public function getPanelCompetitionTeamFormAction(Request $request,$slug,$phase_slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            return ["message"=>"Se ha producido un error"];
        };
        if(!$phase->isPaid()){
            return $this->redirectToRoute("v2_panel_competition",["slug"=>$slug]);
        }
        $form=$this->createForm(CompetitionTeamType::class,null,["phase"=>$phase,"method"=>"POST","action"=>$this->generateUrl("v2_panel_athletes_post",["slug"=>$slug,"phase_slug"=>$phase_slug])]);

        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                /** @var Category $category */
                $category=$form->get("category")->getData();
                /** @var Phase $phase */
                $phase=$category->getPhase();
                $emails=$form->get("text")->getData();
                if($category->getTeamMembers()!==count($emails)){
                    $form->addError(new FormError('El número de atletas no corresponde con los neceseario para esta categoría'));
                    return ["form"=>$form->createView(),"phase"=>$phase,"competition"=>$competition];
                }
                $athletes=$form->get("athletes")->getData();
                $users=$this->get("bdsrw_competition.user")->getUsersForCompetition($emails,$athletes,$phase,$category);
                $this->get("bdsrw_competition.mailer")->mailNewCompetitionAthletes($users,$phase,$category);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute('v2_panel_athletes_get',["slug"=>$competition->getSlug()]);
            }
        }
        return ["form"=>$form->createView(),"phase"=>$phase,"competition"=>$competition];
    }
    /**
     * @Get("{phase_slug}/Athletes/Add/", name="v2_panel_athletes_get_form",    requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Rest\Post("{phase_slug}/Athletes/Add/",name="v2_panel_athletes_post",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/athletes/add_athletes_to_category.html.twig")
     */
    public function getPanelCompetitionAthleteFormAction(Request $request,$slug,$phase_slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            return ["message"=>"Se ha producido un error"];
        };
        if(!$phase->isPaid()){
            return $this->redirectToRoute("v2_panel_competition",["slug"=>$slug]);
        }
        $form=$this->createForm(CompetitionUsersType::class,null,["phase"=>$phase,"method"=>"POST","action"=>$this->generateUrl("v2_panel_athletes_post",["slug"=>$slug,"phase_slug"=>$phase_slug])]);

        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                /** @var Category $category */
                $category=$form->get("category")->getData();
                /** @var Phase $phase */
                $phase=$category->getPhase();
                $emails=$form->get("text")->getData();
                $athletes=$form->get("athletes")->getData();
                $invited=$form->get("invitation")->getData();
                $users=$this->get("bdsrw_competition.user")->getUsersForCompetition($emails,$athletes,$phase,$category,$invited);
                $this->get("bdsrw_competition.mailer")->mailNewCompetitionAthletes($users,$phase,$category);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute('v2_panel_athletes_get',["slug"=>$competition->getSlug()]);
            }
        }
        return ["form"=>$form->createView(),"phase"=>$phase,"competition"=>$competition];
    }
    /**
     * @Rest\Patch("/{id}/", name="v2_panel_athletes_patch_category",requirements={"slug"="^[a-z0-9-]+$","id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchCompetitionCategoryAction(Request $request,$slug,$id){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var CategoryAthlete $categoryAthlete */
        if(!$categoryAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($id)){
            return ["error"=>1];
        };
        $categorySlug=$request->get("category");
        /** @var Category $category */
        if(!$category=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:Category")->findOneBySlug($categorySlug)){
            return ["error"=>1];
        };
        $categoryAthlete->setCategory($category);
        $this->get("doctrine.orm.default_entity_manager")->persist($categoryAthlete);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
    /**
     * @Rest\Patch("/{id}/email", name="v2_panel_athletes_patch_athlete_email",requirements={"slug"="^[a-z0-9-]+$","id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchCompetitionAthleteEmailAction(Request $request,$slug,$id){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var CategoryAthlete $categoryAthlete */
        if(!$categoryAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($id)){
            return ["error"=>1];
        };
        $this->get("bdsrw_competition.mailer")->mailNewCompetitionAthletes([$categoryAthlete],$categoryAthlete->getCategory()->getPhase(),$categoryAthlete->getCategory());
        return ["success"=>1];
    }
    /**
     * @Rest\Patch("/{id}/invite", name="v2_panel_athletes_patch_athlete_invite",requirements={"slug"="^[a-z0-9-]+$","id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchCompetitionAthleteInviteAction(Request $request,$slug,$id){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var CategoryAthlete $categoryAthlete */
        if(!$categoryAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($id)){
            return ["error"=>1];
        };
        $payment=$categoryAthlete->getOrder()->getPayment();
        $this->get("doctrine.orm.default_entity_manager")->remove($payment);
        $categoryAthlete->getOrder()->setPaid(true);
        $this->get("doctrine.orm.default_entity_manager")->persist($payment);
        $this->get("doctrine.orm.default_entity_manager")->flush();


        return ["success"=>1];
    }
    /**
     * @Rest\Get("/Team/{team_slug}/", name="v2_panel_athletes_get_athlete_team",requirements={"slug"="^[a-z0-9-]+$","id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function getCompetitionAthleteTeamAction(Request $request,$slug,$team_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return ["error"=>1];
        };
        /** @var Team $team */
        if(!$team=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Team")->findOneBy(["competition"=>$competition,"slug"=>$team_slug])){
            return ["error"=>1];
        };
        return ["success"=>1,"html"=>$this->renderView('@BDSRWWeb/v2/competition/athletes/team_modal.html.twig',["competition"=>$competition,"team"=>$team])];
    }

    /**
     * @Delete("/{id}/", name="v2_panel_athletes_delete",requirements={"slug"="^[a-z0-9-]+$","id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteCompetitionAthleteAction(Request $request,$slug,$id){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return ["error"=>1];
        };
        /** @var CategoryAthlete $categoryAthlete */
        if(!$categoryAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($id)){
            return ["error"=>1];
        };

        $this->get("doctrine.orm.default_entity_manager")->remove($categoryAthlete);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
}