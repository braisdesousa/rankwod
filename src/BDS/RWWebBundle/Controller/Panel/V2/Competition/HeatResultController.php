<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Round;
use BDS\RWCompetitionBundle\Form\EventRoundResultType;
use BDS\RWWebBundle\Helper\NotyHelper;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Heats")
 * @Security("has_role('ROLE_USER')")
 */
class HeatResultController extends FOSRestController
{


    /**
     * @Get("/{phase_slug}/{event_slug}/{round_slug}/Results", name="v2_panel_heat_result_get",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","round_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{phase_slug}/{event_slug}/{round_slug}/Results", name="v2_panel_heat_result_post",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","round_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/heat_results/heats_results.html.twig")
     */
    public function postHeatResultAction(Request $request,$slug,$phase_slug,$round_slug,$event_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Round $round */
        if(!$round=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Round")->findOneBy(["slug"=>$round_slug,"phase"=>$phase])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Event $event */
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["slug"=>$event_slug,"phase"=>$phase])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if($phase->getPhaseType()===Phase::TYPE_ONLINE){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No se pueden utilizar Tandas en una fase online"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        
        $form= $this->createForm(EventRoundResultType::class,null,["athletes"=>$round->getAthletes(),"event"=>$event,"action"=>$this->generateUrl("v2_panel_heat_result_post",["slug"=>$slug,"phase_slug"=>$phase_slug,"round_slug"=>$round_slug,"event_slug"=>$event_slug])]);
        foreach($event->getMeasureGroups() as $measureGroup){
            $x=$measureGroup->getMeasurements()->getValues();
        }
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                //$this->get("doctrine.orm.default_entity_manager")->persist($round);
                foreach($form->all() as $child){
                    /** @var EventResult $eventResult */
                    $array=$child->getData()?$child->getData():[];
                    foreach($array as $eventResult){
                        if($eventResult->getResult()!=0){
                            $event->addResult($eventResult);
                            $this->get("doctrine.orm.default_entity_manager")->persist($event);
                            $this->get("doctrine.orm.default_entity_manager")->persist($eventResult);
                        }
                    };
                }
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Se han creado los resultados para '%s' - Ronda '%s'",$event->getName(),$round->getName()));
                return $this->redirectToRoute("v2_panel_workout_get",["slug"=>$slug,"phase_slug"=>$phase_slug,"event_slug"=>$event_slug]);
            }
        }
        return ["form"=>$form->createView(),"competition"=>$competition,"phase"=>$phase,'round'=>$round, 'event'=>$event];

    }
}