<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCompetitionBundle\Entity\AthleteExtraData;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\ExtraDataInterface;
use BDS\RWCompetitionBundle\Form\ExtraData\AthleteExtraDataCollectionType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Athletes/ExtraData")
 * @Security("has_role('ROLE_USER')")
 */
class AthleteExtraDataController extends FOSRestController
{
    /**
     * @Get("/{username}/", name="v2_panel_athlete_extra_data",requirements={"username"="^[a-z0-9-]+$","slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{username}/", name="v2_panel_athlete_extra_data_post",requirements={"username"="^[a-z0-9-]+$","slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/extraData/athlete_extra_data_edit.html.twig")
     */
    public function getCompetitionAthletesAction(Request $request,$slug,$username){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /**@var User $user */
        if(!$user=$this->get("fos_user.user_manager")->findUserByUsername($username)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        if(!($this->get("bdsrw_competition.user")->isUserInCompetition($user,$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $rbUser=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
        /** @var ExtraDataInterface $athlete */
        if(!$athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:TeamAthlete")->findOneByUserAndCompetition($rbUser,$competition)){
            $athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByCompetitionAndUser($competition,$rbUser);
        }
        $teams=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Team")->findByUserExtensionAndCompetition($rbUser,$competition);
        $team=$teams[0];
        $form=$this->createForm(AthleteExtraDataCollectionType::class,null,["competition"=>$competition,"athlete"=>$athlete,"action"=>$this->generateUrl('v2_panel_athlete_extra_data_post',["username"=>$username,"slug"=>$slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                foreach($form->all() as $element){
                    if($element->getData() instanceof AthleteExtraData){
                        /** @var AthleteExtraData $athleteExtraData */
                        $athleteExtraData=$element->getData();
                        $athlete->addExtraData($athleteExtraData);
                        $this->get("doctrine.orm.default_entity_manager")->persist($athlete);
                        $this->get("doctrine.orm.default_entity_manager")->persist($athleteExtraData);

                    }

                }
                $this->get("doctrine.orm.default_entity_manager")->persist($athlete);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_team_get_edit",["slug"=>$slug,"team_slug"=>$team->getSlug()]);
            }
        }

        return ["competition"=>$competition,"athlete"=>$user,"form"=>$form->createView()];
    }

}