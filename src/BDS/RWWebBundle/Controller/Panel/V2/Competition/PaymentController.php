<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 21/03/18
 * Time: 9:16
 */

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;

use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWPaymentBundle\Entity\Payment;
use BDS\RWWebBundle\Helper\NotyHelper;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Payum\Core\Request\GetHumanStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/")
 * @Security("has_role('ROLE_USER')")
 */
class PaymentController extends FOSRestController
{

    /**
     * @Get("{phase_slug}/payment/", name="v2_panel_pay",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"} ,defaults={"_format"="html"},options={"expose"=true})
     */
    public function prepareAction(Request $request,$slug,$phase_slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };


        $payment=$this->get("bdsrw_payment.order_service")->createPaymentForOrder($phase,$this->getUser());
        $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
            'stripe',
            $payment,
            'v2_panel_pay_done', // the route to redirect after capture,
            ['slug'=>$slug,"phase_slug"=>$phase_slug]
        );
        return $this->redirect(str_replace("http:","https:",$captureToken->getTargetUrl()));
    }
    /**
     * @Get("{phase_slug}/payment/done", name="v2_panel_pay_done",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","phase_old_slug"="^[a-z0-9-]+$"} ,defaults={"_format"="json"},options={"expose"=true})
     */
    public function doneAction(Request $request,$slug,$phase_slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        $token = $this->get('payum')->getHttpRequestVerifier()->verify($request);
        $gateway = $this->get('payum')->getGateway($token->getGatewayName());
        // You can invalidate the token, so that the URL cannot be requested any more:
        $this->get('payum')->getHttpRequestVerifier()->invalidate($token);
        // Once you have the token, you can get the payment entity from the storage directly.
        $gateway->execute($status = new GetHumanStatus($token));
        /** @var Payment $payment */
        $payment = $status->getFirstModel();
        $payment->getDetails();
        $phase->getOrder()->setPaid($status->isCaptured());
        $this->get("doctrine.orm.default_entity_manager")->persist($payment);
        $this->get("doctrine.orm.default_entity_manager")->persist($phase);
        $this->get("doctrine.orm.default_entity_manager")->flush();

        // Now you have order and payment status

        return['success' => $status->isCaptured(),"details"=>$payment->getDetails()];
    }

    /**
     * @Rest\Patch("{phase_slug}/payment/delete", name="v2_panel_pay_patch_remove",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","phase_old_slug"="^[a-z0-9-]+$"} ,defaults={"_format"="json"},options={"expose"=true})
     */
    public function deletePaymentAction(Request $request,$slug,$phase_slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        $order=$phase->getOrder();
        $order->setPaid(true);
        $this->get("doctrine.orm.default_entity_manager")->persist($order);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }

}