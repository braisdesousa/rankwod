<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\EventResultsType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Workouts/Result")
 * @Security("has_role('ROLE_USER')")
 */
class EventResultController extends FOSRestController
{
    /**
     * @Get("/{phase_slug}/{event_slug}/E-Result/", name="v2_panel_event_result_get_form",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{phase_slug}/{event_slug}/E-Result/", name="v2_panel_event_result_post_form",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/events/athlete_event_result.html.twig")
     */
    public function getEventResultFormAction(Request $request,$slug,$phase_slug,$event_slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if(!$athlete=$this->get("bdsrw_competition.user")->getUserFromCompetitionPhase($user,$phase)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $results=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->findBy(["event"=>$event,"athlete"=>$athlete]);
        $form=$this->createForm(EventResultsType::class,null,["event"=>$event,"is_judge"=>false,"method"=>"POST","results"=>$results,"action"=>$this->generateUrl('v2_panel_event_result_post_form',["slug"=>$slug,"phase_slug"=>$phase_slug,"event_slug"=>$event_slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $video=null;
                if($form->has("video")){
                    $video=$form->get("video")->getData();
                }
                if($results){
                    foreach($results as $result){
                        $this->get("doctrine.orm.default_entity_manager")->remove($result);
                    }
                    $this->get("doctrine.orm.default_entity_manager")->flush();
                }
                /** @var EventResult $eventResult */
                foreach($form->getData() as $eventResult){
                    if(($eventResult instanceof  EventResult)&&$eventResult->getResult()!=0){
                        $eventResult->setAthlete($athlete);
                        $eventResult->setCorrected(false);
                        $eventResult->setEvent($event);
                        $eventResult->setVideoUrl($video);
                        $event->setUpdated();
                        $this->get("doctrine.orm.default_entity_manager")->persist($event);
                        $this->get("doctrine.orm.default_entity_manager")->persist($eventResult);
                    }
                    $this->get("doctrine.orm.default_entity_manager")->persist($event);
                }
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_workout",["slug"=>$slug]);
            }
        }
        return ["competition"=>$competition,"event"=>$event,"form"=>$form->createView()];
    }
    /**
     * @Get("/{phase_slug}/{event_slug}/{athlete_id}/E-Result/", name="v2_panel_event_result_get_judge_form",requirements={"slug"="^[a-z0-9-]+$","athlete_id"="^[0-9]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     * @Post("/{phase_slug}/{event_slug}/{athlete_id}/E-Result/", name="v2_panel_event_result_post_judge_form",requirements={"slug"="^[a-z0-9-]+$","athlete_id"="^[0-9]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function getEventResultFormJudgeOrAdminAction(Request $request,$slug,$phase_slug,$event_slug,$athlete_id){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        /** @var CategoryAthlete $athlete */
        if(!$athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($athlete_id)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $user=$this->getUser();
        $judge=(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminOrJudgeInCompetition($user,$competition)));
        $results=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->findBy(["event"=>$event,"athlete"=>$athlete]);
        $form=$this->createForm(EventResultsType::class,null,["event"=>$event,"is_judge"=>$judge,"method"=>"POST","results"=>$results,"action"=>$this->generateUrl('v2_panel_event_result_post_judge_form',["slug"=>$slug,"phase_slug"=>$phase_slug,"event_slug"=>$event_slug,"athlete_id"=>$athlete_id])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                /** @var EventResult $eventResult */
                foreach($form->getData() as $eventResult){

                    if(($eventResult instanceof EventResult)&&($eventResult->getResult()!=0)){
                        $eventResult->setAthlete($athlete);
                        $eventResult->setCorrected(true);
                        $eventResult->setEvent($event);
                        if($phase->isVideoRequired()){
                            $eventResult->setVideoUrl($form->get("video")->getData());
                        }
                        if($form->has("comment")&&($comment=$form->get("comment")->getData())){
                            $eventResult->setComment($comment);
                        }
                        $event->setUpdated();
                        $this->get("doctrine.orm.default_entity_manager")->persist($event);
                        $this->get("doctrine.orm.default_entity_manager")->persist($eventResult);
                    }
                }
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_workout_get",["slug"=>$slug,"phase_slug"=>$phase_slug,"event_slug"=>$event_slug]);
            }
        }
        return ["html"=>$this->renderview("@BDSRWWeb/v2/competition/events/component/filled_modal_result.html.twig",["competition"=>$competition,"athlete"=>$athlete,"event"=>$event,"form"=>$form->createView()])];
    }
    /**
     * @Delete("/{phase_slug}/{event_slug}/{athlete_id}/E-Result/", name="v2_panel_event_result_delete",requirements={"slug"="^[a-z0-9-]+$","athlete_id"="^[0-9]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteEventResult(Request $request,$slug,$phase_slug,$event_slug,$athlete_id){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1,"message"=>"Se ha producido un error"];
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            return ["error"=>1,"message"=>"Se ha producido un error"];
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            return ["error"=>1,"message"=>"Se ha producido un error"];
        };
        /** @var CategoryAthlete $athlete */
        if(!$athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($athlete_id)) {
            return ["error"=>1,"message"=>"Se ha producido un error"];
        }
        $results=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->findBy(["event"=>$event,"athlete"=>$athlete]);
        foreach($results as $result){
            $this->get("doctrine.orm.default_entity_manager")->remove($result);
        }
        $event->setUpdated();
        $this->get("doctrine.orm.default_entity_manager")->persist($event);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>true,"message"=>"Results Deleted"];
    }
    /**
     * @Rest\Patch("/{phase_slug}/{event_slug}/{athlete_id}/E-Result/", name="v2_panel_event_result_patch_validate",requirements={"slug"="^[a-z0-9-]+$","athlete_id"="^[0-9]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function validateEventResult(Request $request,$slug,$phase_slug,$event_slug,$athlete_id){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1,"message"=>"Se ha producido un error"];
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            return ["error"=>1,"message"=>"Se ha producido un error"];
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            return ["error"=>1,"message"=>"Se ha producido un error"];
        };
        /** @var CategoryAthlete $athlete */
        if(!$athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($athlete_id)) {
            return ["error"=>1,"message"=>"Se ha producido un error"];
        }
        $results=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->findBy(["event"=>$event,"athlete"=>$athlete]);
        foreach($results as $result){
            $result->setCorrected(true);
            $this->get("doctrine.orm.default_entity_manager")->persist($result);
        }
        $event->setUpdated();
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>true,"message"=>"Results Deleted"];
    }
}