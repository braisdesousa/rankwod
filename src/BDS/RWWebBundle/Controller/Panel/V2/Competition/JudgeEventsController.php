<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\CoreBundle\Helpers\RepositoryHelper;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWCompetitionBundle\Form\JudgeResultsType;
use BDS\RWCompetitionBundle\Form\JudgeUsersType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Judge/Events")
 * @Security("has_role('ROLE_USER')")
 */
class JudgeEventsController extends FOSRestController
{
    /**
     * @Get("/", name="v2_panel_judge_event_get_competition",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("@BDSRWWeb/v2/competition/judges/judges_workouts_admin.html.twig")
     */
    public function getCompetitionJudgesAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        return ["competition"=>$competition];
    }
    /**
     * @Get("/{phase_slug}/{event_slug}/", name="v2_panel_judge_event_get_event",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/judges/event.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function getPanelCompetitionJudgeEventAction(Request $request, $slug, $phase_slug, $event_slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $results=$this->get("bdsrw_competition.result_list")->reorderEventResults($event);
        $judging_results=RepositoryHelper::reformatArrayWithKeyMultipleValues($this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findByEvent($event),"athlete_id");
        $athletes=$this->get("rankwod.athlete")->getCompetitionAthletes($competition);
        $judges=$this->get("rankwod.judges")->getCompetitionJudges($competition);
        $overall=$this->get("bdsrw_competition.overall")->getTempOverall($phase);
        return ["competition"=>$competition,"phase"=>$phase,"event"=>$event,"results"=>$results,"athletes"=>$athletes,"judges"=>$judges,"judged_athletes"=>$judging_results,"overall"=>$overall];

    }
    /**
     * @Post("/{phase_slug}/{event_slug}/", name="v2_panel_judge_event_post_event",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/judges/event.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function postPanelCompetitionJudgeEventAction(Request $request, $slug, $phase_slug, $event_slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        $form=$this->createForm(JudgeResultsType::class,null,["competition"=>$competition,"method"=>"POST"]);
        $form->handleRequest($request);
        if($form->isValid()){
            /** @var CompetitionJudge $judge */
            $judge=$form->get("judge")->getData();
            $athletes=$form->get("athletes")->getData();
            $y=count($form->get("athletes")->getViewData());
            $x=count($athletes);

            $eventResults=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->findEventsForJudging($event,$athletes);
            $message=sprintf("Got (%s|%s) athletes and found %s event results",$x,$y,count($eventResults));
            foreach($eventResults as $eventResult){
                $judge->addWorkout($eventResult);
            }
            $this->get("doctrine.orm.default_entity_manager")->persist($judge);
            $this->get("doctrine.orm.default_entity_manager")->flush();
            return ["success"=>1,"message"=>$message];
        }
        $results=$this->get("bdsrw_competition.result_list")->reorderEventResults($event);
        $athletes=$this->get("rankwod.athlete")->getCompetitionAthletes($competition);
        $judges=$this->get("rankwod.judges")->getCompetitionJudges($competition);
        return ["competition"=>$competition,"phase"=>$phase,"event"=>$event,"results"=>$results,"athletes"=>$athletes,"judges"=>$judges];

    }
    /**
     * @Get("/judge-results/", name="v2_panel_judge_event_get_judges_results",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/judges/judges/judges_list.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function getJudgeResultsAction(Request $request,$slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $results=RepositoryHelper::reformatArrayWithKey($this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findJudgeEvents($competition),"result_id");
        $pending=RepositoryHelper::reformatArrayWithKey($this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->countPendingJudgeEvents($competition),"judge_id");
        $corrected=RepositoryHelper::reformatArrayWithKey($this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->countCorrectedJudgeEvents($competition),"judge_id");
        $resultEntities=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->findByIds(array_keys($results));
        $athletes=$this->get("rankwod.athlete")->getCompetitionAthletes($competition);
        $judges=$this->get("rankwod.judges")->getCompetitionJudges($competition);
        return ["judges"=>$judges,"results"=>$resultEntities,"array_results"=>$results, "athletes"=>$athletes,"competition"=>$competition,"pending"=>$pending,"corrected"=>$corrected];

    }
    /**
     * @Get("/my-judging/", name="v2_panel_judge_event_get_my_results",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/judges/judges/judge_my_list.html.twig")
     * @Security("has_role('ROLE_USER')")
     */
    public function getJudgeResultsToJudgeAction(Request $request,$slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminOrJudgeInCompetition($user,$competition))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
        if(!$judge=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findOneBy(["userExtension"=>$userExtension])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $athletes=$this->get("rankwod.athlete")->getCompetitionAthletes($competition);
        return ["judge"=>$judge, "athletes"=>$athletes,"competition"=>$competition];

    }

    /**
     * @Rest\Patch("/{judge_id}/{result_id}/Unasign", name="v2_panel_judge_event_patch_unasign_result",requirements={"judge_id"="\d+","result_id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     * @Security("has_role('ROLE_USER')")
     */
    public function unassignJudgeAction(Request $request,$slug,$judge_id,$result_id){
        if(!$judge=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->find($judge_id)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$result=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->find($result_id)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        $judge->removeWorkout($result);
        $this->get("doctrine.orm.default_entity_manager")->persist($judge);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];



    }

}