<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWCompetitionBundle\Form\JudgeUsersType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Judges")
 * @Security("has_role('ROLE_USER')")
 */
class JudgeController extends FOSRestController
{
    /**
     * @Get("/", name="v2_panel_judges_get",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/competition_judges.html.twig")
     */
    public function getCompetitionJudgesAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $pending_judges=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findByCompetitionAndStatus($competition,CompetitionJudge::STATUS_PENDING);
        return [
            "competition"=>$competition,
            "pending_judges"=>count($pending_judges),
            "judges"=>$this->get("rankwod.judges")->getCompetitionJudges($competition)];
    }
    /**
     * @Get("/Add/", name="v2_panel_judges_get_form",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/Add/",name="v2_panel_judges_post_form",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/judges/judges_form.html.twig")
     */
    public function getPanelCompetitionJudgesFormAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $form=$this->createForm(JudgeUsersType::class,null,["method"=>"POST","action"=>$this->generateUrl("v2_panel_judges_post_form",["slug"=>$slug])]);

        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $emails=$form->get("text")->getData();
                $judges=$form->get("judges")->getData();
                $users=$this->get("bdsrw_competition.user")->getJudgesForCompetition($emails,$judges,$competition);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_competition.judge.mailer")->mailJudges($users,$competition);
                return $this->redirectToRoute('v2_panel_judges_get',["slug"=>$competition->getSlug()]);
            }
        }
        return ["form"=>$form->createView(),"competition"=>$competition];
    }
    /**
     * @Rest\Patch("/re-email/{username}/", name="v2_panel_judges_patch_re_email",requirements={"slug"="^[a-z0-9-]+$","username"="^[a-z0-9-\.]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelReMailUserAction(Request $request,$slug,$username)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        if(!$user=$this->get("fos_user.user_manager")->findUserByUsername($username)){
            return ["error"=>1, "message"=>sprintf("User '%s' not found",$username)];
        }
        /** @var CompetitionJudge $judge */
        if(!$judge=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findOneByCompetitionAndUserId($competition,$user)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Judge on this Competition",$username)];
        }
        $this->get("bdsrw_competition.judge.mailer")->mailJudges([$judge],$competition);
        $judge->setStatus(CompetitionJudge::STATUS_PENDING);
        $this->get("doctrine.orm.default_entity_manager")->persist($judge);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
    /**
     * @Rest\Patch("/Accept/", name="v2_panel_judges_patch_accept",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelJudgeAcceptAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        $user=$this->getUser();
        /** @var CompetitionJudge $judge */
        if(!$judge=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findOneByCompetitionAndUserId($competition,$user)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Judge on this Competition",$user->getUsername())];
        }
        $judge->setStatus(CompetitionJudge::STATUS_ACCEPTED);
        $this->get("doctrine.orm.default_entity_manager")->persist($judge);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1,"slug"=>$slug];
    }
    /**
     * @Rest\Patch("/Decline/{username}/", name="v2_panel_judges_patch_decline",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelJudgeDeclineAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        $user=$this->getUser();
        /** @var CompetitionJudge $judge */
        if(!$judge=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findOneByCompetitionAndUserId($competition,$user)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Judge on this Competition",$user->getUsername())];
        }
        $judge->setStatus(CompetitionJudge::STATUS_DECLINED);
        $this->get("doctrine.orm.default_entity_manager")->persist($judge);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
    /**
     * @Rest\Patch("/re-email/", name="v2_panel_judges_re_email_all",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPanelReMailAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        $judges=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findByCompetitionAndStatus($competition,CompetitionJudge::STATUS_PENDING);
        $this->get("bdsrw_competition.judge.mailer")->mailJudges($judges,$competition);
        return ["success"=>1];
    }
    /**
     * @Rest\Delete("/Delete/{username}/", name="v2_panel_judges_delete",requirements={"slug"="^[a-z0-9-]+$","username"="^[a-z0-9-\.]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deletePanelJudgeAction(Request $request,$slug,$username)
    {
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1, "message"=>sprintf("Competition '%s' not found",$slug)];
        };
        /** @var User $user */
        if(!$user=$this->get("fos_user.user_manager")->findUserByUsername($username)){
            return ["error"=>1, "message"=>sprintf("User '%s' not found",$username)];
        }
        $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
        /** @var CompetitionJudge $judge */
        if(!$judge=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findOneByCompetitionAndUserExtension($competition,$userExtension)){
            return ["error"=>1, "message"=>sprintf("User '%s' is not Judge on this Competition",$username)];
        }

        $competition->deleteJudge($judge);
        $this->get("doctrine.orm.default_entity_manager")->remove($judge);
        $this->get("doctrine.orm.default_entity_manager")->persist($competition);
        $this->get("doctrine.orm.default_entity_manager")->flush();

        return ["success"=>1];
    }

}