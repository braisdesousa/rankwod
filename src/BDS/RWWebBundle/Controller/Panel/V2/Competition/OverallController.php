<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 *
 * @Route("/C/{slug}")
 * @Security("has_role('ROLE_USER')")
 */
class OverallController extends FOSRestController
{


    /**
     * @Get("/{phase_slug}/Create-Overall", name="v2_panel_overall_get",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     */
    public function postCompetitionPhaseCreateOverallAction(Request $request,$slug,$phase_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if(!(($user->isAdmin())||($this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)))) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            $this->redirectToRoute('v2_panel_competitions');
        }
        
        try{
            $this->get("doctrine.orm.default_entity_manager")->beginTransaction();
            $overall=$this->get("bdsrw_competition.overall")->createOverall($phase);
            $this->get("doctrine.orm.default_entity_manager")->commit();
            return $this->redirectToRoute('v2_panel_workout',["slug"=>$slug]);
        } catch (\Exception $e){
            $this->get("doctrine.orm.default_entity_manager")->rollback();
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }


    }
    /**
     * @Get("/{phase_slug}/Overall", name="v2_panel_overall_get_phase",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/overall/overall.html.twig")
     */
    public function getOverallAction($slug,$phase_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneByCompetitionAndSlug($competition,$phase_slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$overall=$phase->getOverall()){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        foreach($phase->getCategories() as $category){
            $overallResults[$category->getSlug()]=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:OverallResult")->findByCategoryAndOverallOrderByResult($category,$overall,($phase->isDesertedResultLikeGames()));
        }
        return["overall_results"=>$overallResults,"competition"=>$competition,"phase"=>$phase,"category"=>$category];
    }
    /**
     * @Delete("/{phase_slug}/Overall/Delete",   name="v2_panel_overall_delete",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteOverallAction(Request $request,$slug,$phase_slug)
    {
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        };
        if(!$overall=$phase->getOverall()){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        };
        $phase->setOverall(null);
        $this->get("doctrine.orm.default_entity_manager")->remove($overall);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1,"message"=>"success"];
        
    }
    /**
     * @Patch("/{phase_slug}/{athlete_id}/Overall/TieBreakUP",   name="v2_panel_overall_patch_tiebreak_up",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","athlete_id"="^[0-9]+$"}, defaults={"_format"="json"},options={"expose"=true})
     * @Patch("/{phase_slug}/{athlete_id}/Overall/TieBreakDown", name="v2_panel_overall_patch_tiebreak_down",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","athlete_id"="^[0-9]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function tieBreakUpOverallAction(Request $request,$slug,$phase_slug,$athlete_id){
        /** @var Competition $competition */
	    if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["slug"=>$phase_slug,"competition"=>$competition])){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        };
        if(!$overall=$phase->getOverall()){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        };
        /** @var User $user */
        if(!$athlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($athlete_id)){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        };

        if(!$overallResult=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:OverallResult")->findOneByAthleteAndPhase($athlete,$phase)){
            return ["error"=>true, "message"=>"Se ha producido un error"];
        }
        if($request->get("_route")=="patch_panel_competition_phase_overall_tiebreak_up"){
            $overallResult->tieBreakerUp();
        } else {
            $overallResult->tieBreakerDown();
        }
        $this->get("doctrine.orm.default_entity_manager")->persist($overallResult);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1,"message"=>"Success","slug"=>$slug,"phase_slug"=>$phase_slug,"category_slug"=>$overallResult->getCategory()->getSlug()];
    }


    
}