<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Form\CompetitionTeamType;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CompetitionUsersType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWCompetitionBundle\Form\EditProfileType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/User")
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends FOSRestController
{


    /**
     * @Get("/{username}/Edit", name="v2_panel_competition_user_edit_get",requirements={"slug"="^[a-z0-9-]+$","username"="^[a-z0-9-]+$"},  defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{username}/Edit", name="v2_panel_competition_user_edit_post",requirements={"slug"="^[a-z0-9-]+$","username"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/user/profile_edit.html.twig")
     */
    public function editCompetitionUserAction(Request $request,$slug,$username){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user */
        if(!$user=$this->get("fos_user.user_manager")->findUserByUsername($username)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No existe este usuario"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        if(!($this->get("bdsrw_competition.user")->doesUserParticipate($user,$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Este usuario no forma parte de la competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        $disableEdit=$this->getUser()->isSuperAdmin()?false:($user->getLastLogin()&&1);

        $form=$this->createForm(EditProfileType::class,$user,["is_admin"=>true,"disable_edit"=>$disableEdit,"action"=>$this->generateUrl("v2_panel_competition_user_edit_post",["slug"=>$slug,"username"=>$username])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $this->get("fos_user.user_manager")->updateUser($user);
                $tShirt=$form->get("tshirt")->getData();
                $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
                $userExtension->setTShirtSize($tShirt);
                $this->get("doctrine.orm.default_entity_manager")->persist($userExtension);
                $this->get("doctrine.orm.default_entity_manager")->flush($userExtension);

                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,"Perfil Actualizado");
                /** @var Team $team */
                $teams=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Team")->findByUserExtensionAndCompetition($userExtension,$competition);
                $team=$teams[0];
                return $this->redirectToRoute("v2_panel_team_get_edit",["slug"=>$competition->getSlug(),"team_slug"=>$team->getSlug()]);
            }
            return ["form"=>$form->createView()];
        }

        return ["form"=>$form->createView(),"user"=>$user];
    }
}