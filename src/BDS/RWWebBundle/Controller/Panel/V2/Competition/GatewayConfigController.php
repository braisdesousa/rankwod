<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 21/03/18
 * Time: 9:16
 */

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;

use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\ImportPhaseAthletesType;
use BDS\RWPaymentBundle\Entity\GatewayConfig;
use BDS\RWPaymentBundle\Entity\Payment;
use BDS\RWPaymentBundle\Form\GateWayConfigType;
use BDS\RWWebBundle\Helper\NotyHelper;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C/{slug}/Gateway")
 * @Security("has_role('ROLE_USER')")
 */
class GatewayConfigController extends FOSRestController
{

    /**
     * @Get("/", name="v2_panel_gateway_get",requirements={"slug"="^[a-z0-9-]+$"} ,defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/gateway/gateway.html.twig")
     */
    public function configPaymentAction(Request $request,$slug)
    {
        /** @var Competition $competition */
        if (!$competition = $this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR, sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        return ["competition"=>$competition];
    }
    /**
     * @Get("/Config", name="v2_panel_gateway_get_config",requirements={"slug"="^[a-z0-9-]+$"} ,defaults={"_format"="html"},options={"expose"=true})
     * @Post("/Config", name="v2_panel_gateway_post_config",requirements={"slug"="^[a-z0-9-]+$"} ,defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/gateway/gateway_edit.html.twig")
     */
    public function editGateWayConfigAction(Request $request,$slug){
        /** @var Competition $competition */
        if (!$competition = $this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR, sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var GatewayConfig $gateWayConfig */
        $gateWayConfig=$this->get("bdsrw_payment.gateway_service")->createGateWayConfig($competition);
        $data=$gateWayConfig->getDataForForm();
        $form=$this->createForm(GateWayConfigType::class,$data,["method"=>"POST","action"=>$this->generateUrl('v2_panel_gateway_post_config',["slug"=>$slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $private=$form->get("private_key")->getData();
                $publishable=$form->get("publishable_key")->getData();
                $gateWayConfig=$this->get("bdsrw_payment.gateway_service")->createGateWayConfig($competition,$private,$publishable);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_gateway_get",['slug'=>$slug]);
            }
        }
        return ["competition"=>$competition,"form"=>$form->createView()];
    }

    /**
     * @Rest\Patch("/{phase_slug}/", name="v2_panel_gateway_patch",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchPhasePriceAction(Request $request,$slug,$phase_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBySlug($phase_slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        $price=str_replace(",",".",$request->get("price"));
        $phase->setPrice(floatval($price)*100);
        $this->get("doctrine.orm.default_entity_manager")->persist($phase);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return [];
    }
 }