<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWCompetitionBundle\Form\SimpleEditEventType;
use BDS\RWCompetitionBundle\Form\SimpleEventType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 *
 * @Route("/C/{slug}/Workouts")
 * @Security("has_role('ROLE_USER')")
 */
class WorkoutController extends FOSRestController
{
    /**
     * @Get("/", name="v2_panel_workout",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/workouts/workouts_admin.html.twig")
     */
    public function getCompetitionWorkoutsAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if($this->get("bdsrw_competition.user")->isUserInCompetition($this->getUser(),$competition)){
            return new Response($this->renderView('@BDSRWWeb/v2/competition/workouts/workouts_athlete.html.twig',["competition"=>$competition]));
        }
        if($this->get("bdsrw_competition.user")->isUserJudgeInCompetition($this->getUser(),$competition)){
            return new Response($this->renderView('@BDSRWWeb/v2/competition/workouts/workouts_judge.html.twig',["competition"=>$competition]));
        }
        return ["competition"=>$competition];
    }
    /**
     * @Get("/{phase_slug}/{event_slug}/", name="v2_panel_workout_get",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/events/event.html.twig")
     */
    public function getPanelCompetitionEventAction(Request $request, $slug, $phase_slug, $event_slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $results=$this->get("bdsrw_competition.result_list")->reorderEventResults($event);
        $athletes=$this->get("rankwod.athlete")->getCompetitionAthletes($competition);
        return ["competition"=>$competition,"phase"=>$phase,"event"=>$event,"results"=>$results,"athletes"=>$athletes];
    }
    /**
     * @param Request $request
     * @param $slug
     * @param $phase_slug
     * @param $event_slug
     * @return array|\Symfony\Component\HttpFoundation\Response
     * @Rest\Delete("/{phase_slug}/{event_slug}/Delete", name="v2_panel_workout_delete",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function getPanelCompetitionDeleteEventAction(Request $request, $slug, $phase_slug, $event_slug){
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            return ["error"=>1,"message"=>sprintf("La competición '%s' no existe",$slug)];
        };
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            return ["error"=>1,"message"=>sprintf("La fase '%s' no existe",$phase_slug)];
        };
        if(!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug])){
            return ["error"=>1,"message"=>sprintf("El evento '%s' no existe",$event_slug)];
        };
        if((!$this->getUser()->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))) {
            return ["error"=>1,"message"=>sprintf("El evento '%s' no puede eliminarse",$event_slug)];
        }
        $this->get("doctrine.orm.default_entity_manager")->remove($event);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1,"message"=>"Evento eliminado correctamente"];
    }
    /**
     * @Get("/{phase_slug}/Events/New", name="v2_panel_workout_get_form",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{phase_slug}/Events/New", name="v2_panel_workout_post_form",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("BDSRWWebBundle:v2/competition/events:event_form.html.twig")
     */
    public function getCompetitionEventFormAction(Request $request,$slug,$phase_slug)
    {
        /** @var Competition $competition **/
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)) {
            return $this->render("default/index.html.twig", ["slug" => $slug]);
        }
        /** @var Phase $phase */
        if(!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            throw new AuthenticationException(sprintf("User '%s' is not allowed here",$user->getCompleteName()));
        }
        $event=new Event();

        $form=$this->createForm(SimpleEventType::class,$event,["show_measures"=>true,'is_online'=>$phase->isOnline(),"final_available"=>$phase->isAttendance(),"has_points"=>$phase->hasPoints(),"method"=>"POST","action"=>$this->generateUrl("v2_panel_workout_post_form",["slug"=>$slug,"phase_slug"=>$phase_slug])]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $event->setPhase($phase);
                $phase->addEvent($event);
                $this->get("bdsrw_measure.service")->createMeasuresForEvent($event,$form->get('measureGroups')->getData());
                /** CREATE MEASURE GROUP */
                $this->get("doctrine.orm.default_entity_manager")->persist($event);
                $this->get("doctrine.orm.default_entity_manager")->persist($phase);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Se ha creado el evento '%s'",$event->getName()));
                return $this->redirectToRoute("v2_panel_workout",["slug"=>$competition->getSlug()]);
            }
        }
        return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Get("/{phase_slug}/{event_slug}/Edit", name="v2_panel_workout_get_edit_form",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Post("/{phase_slug}/{event_slug}/Edit", name="v2_panel_workout_post_edit_form",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("BDSRWWebBundle:v2/competition/events:event_form.html.twig")
     */
    public function getCompetitionEditEventFormAction(Request $request,$slug,$phase_slug,$event_slug)
    {
        /** @var Competition $competition **/
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)) {
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Phase $phase */
        if((!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug]))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Event */
        if((!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug]))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user **/
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            throw new AuthenticationException(sprintf("User '%s' is not allowed here",$user->getCompleteName()));
        }
        $updateMeasures=boolval($request->get("result-types"));
        $formClass=$updateMeasures?SimpleEventType::class:SimpleEditEventType::class;
        $urlOptions=["slug"=>$slug,"phase_slug"=>$phase_slug,"event_slug"=>$event_slug];
        if($updateMeasures){
            $urlOptions["result-types"]=1;
        }
        $url=$this->generateUrl("v2_panel_workout_post_edit_form",$urlOptions);
        $form=$this->createForm($formClass,$event,["show_measures"=>true,"has_points"=>$phase->hasPoints(),'is_online'=>$phase->isOnline(),"final_available"=>$phase->isAttendance(),"method"=>"POST","action"=>$url]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                if($updateMeasures){
                    $results=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:EventResult")->findBy(["event"=>$event]);
                    foreach($results as $result){
                        $this->get("doctrine.orm.default_entity_manager")->remove($result);
                    }
                    $event->clearMeasures();
                    foreach($event->getMeasureGroups() as $measureGroup){
                        $this->get("doctrine.orm.default_entity_manager")->remove($measureGroup);
                    }
                    $this->get("bdsrw_measure.service")->createMeasuresForEvent($event,$form->get('measureGroups')->getData());
                }
                /** CREATE MEASURE GROUP */
                $this->get("doctrine.orm.default_entity_manager")->persist($event);
                $this->get("doctrine.orm.default_entity_manager")->persist($phase);
                $this->get("doctrine.orm.default_entity_manager")->flush();
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Se ha creado el evento '%s'",$event->getName()));
                return $this->redirectToRoute("v2_panel_workout",["slug"=>$competition->getSlug()]);
            }
        }
        return ["competition"=>$competition,"phase"=>$phase,"form"=>$form->createView()];
    }
    /**
     * @Rest\Patch("/{phase_slug}/{event_slug}/Result-Visibility", name="v2_panel_workout_patch_result_visibility",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchResultVisibilityEvent(Request $request,$slug,$phase_slug,$event_slug)
    {
        /** @var Competition $competition **/
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)) {
            return ["error"=>1];
        }
        if((!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug]))){
            return ["error"=>1];
        };
        if((!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug]))){
            return ["error"=>1];
        };
        /** @var User $user **/
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            return ["error"=>1];
        }
        if(!$phase->isPublished()){
            $this->get("bds.noty.service")->addNoty(NotyHelper::TYPE_ALERT,"No puedes publicar un evento sin haber pagado");
            return ["success"=>1,"url"=>$this->generateUrl('get_competition_phase_payment',["slug"=>$slug,"phase_slug"=>$phase_slug])];
        }
        if(!$event->isPublished()){
            return ["error"=>1,"message"=>"No puedes publicar los resultados sin publicar antes el evento"];
        }

        $event->setManualPublished($event->isManualPublished()?Event::MANUAL_PUBLISH_UNPUBLISHED:Event::MANUAL_PUBLISH_PUBLISHED);
        $this->get("doctrine.orm.default_entity_manager")->persist($event);
        $this->get("doctrine.orm.default_entity_manager")->flush();

        return ["success"=>1,"message"=>sprintf("Los resultados del evento '%s' están %s",$event->getName(),$event->isManualPublished()?"Publicados":"No publicados")];
    }
    /**
     * @Rest\Patch("/{phase_slug}/{event_slug}/Visibility", name="v2_panel_workout_patch_event_visibility",requirements={"slug"="^[a-z0-9-]+$","phase_slug"="^[a-z0-9-]+$","event_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function patchVisibilityEvent(Request $request,$slug,$phase_slug,$event_slug)
    {
        /** @var Competition $competition **/
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)) {
            return ["error"=>1];
        }
        if((!$phase=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findOneBy(["competition"=>$competition,"slug"=>$phase_slug]))){
            return ["error"=>1];
        };
        if((!$event=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Event")->findOneBy(["phase"=>$phase,"slug"=>$event_slug]))){
            return ["error"=>1];
        };
        /** @var User $user **/
        $user=$this->getUser();
        if((!$user->isAdmin())&&(!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))) {
            return ["error"=>1];
        }
        if(!$phase->isPaid()){
            $this->get("bds.noty.service")->addNoty(NotyHelper::TYPE_ALERT,"No puedes publicar un evento sin haber pagado");
            return ["success"=>1,"url"=>$this->generateUrl('get_competition_phase_payment',["slug"=>$slug,"phase_slug"=>$phase_slug])];
        }
        if(!$phase->isPublished()){
            $this->get("bds.noty.service")->addNoty(NotyHelper::TYPE_ALERT,sprintf("No puedes publicar un evento sin haber publicado la fase '%s' antes",$phase->getName()));
            return ["success"=>1,"url"=>$this->generateUrl('get_competition_phase',["slug"=>$slug,"phase_slug"=>$phase_slug])];
        }
        if($event->isPublished()){
            $event->unPublish();
        } else {
            $event->publish();
        }
        $this->get("doctrine.orm.default_entity_manager")->persist($event);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1,"message"=>sprintf("El evento '%s' ha sido %s",$event->getName(),$event->isPublished()?"Publicado":" Despublicado")];
    }
}