<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\Competition;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\RWCompetitionBundle\Form\CompetitionTeamType;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CompetitionUsersType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 *
 * @Route("/C/{slug}/Team")
 * @Security("has_role('ROLE_USER')")
 */
class TeamController extends FOSRestController
{
    /**
     * @Get("/List/", name="v2_panel_list_team",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/athletes/team_list.html.twig")
     */
    public function getCompetitionTeamList(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user */
        $user=$this->getUser();
        if(!(($user->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)||$this->get("bdsrw_competition.user")->getUserInTeam($user,$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes suficientes privilegios para estar aqui"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
        /** @var Team $team */
        if(!$team=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Team")->findOneBy($userExtension,$competition)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var CategoryAthlete $categoryAthlete **/
        if(!$categoryAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByCompetitionAndTeam($competition,$team)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Competition $competition */
        $teams=$categoryAthlete->getCategory()->getAthletes();
        return ["competition"=>$competition,"category"=>$categoryAthlete->getCategory(),"team"=>$team];
    }
    /**
     * @Get("/{team_slug}/", name="v2_panel_team",requirements={"slug"="^[a-z0-9-]+$","team_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/athletes/team_show.html.twig")
     */
    public function getCompetitionTeam(Request $request,$slug,$team_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user */
        $user=$this->getUser();
        if(!(($user->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition)||$this->get("bdsrw_competition.user")->getUserInTeam($user,$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes suficientes privilegios para estar aqui"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Team $team */
        if(!$team=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Team")->findOneBy(["competition"=>$competition,"slug"=>$team_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        /** @var CategoryAthlete $categoryAthlete **/
        if(!$categoryAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByCompetitionAndTeam($competition,$team)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Competition $competition */

        return ["competition"=>$competition,"category"=>$categoryAthlete->getCategory(),"team"=>$team];
    }
    /**
     * @Get("/{team_slug}/Edit", name="v2_panel_team_get_edit",requirements={"slug"="^[a-z0-9-]+$","team_slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/athletes/team_edit.html.twig")
     */
    public function getCompetitionTeamEditAction(Request $request,$slug,$team_slug){

        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user */
        $user=$this->getUser();
        if((!$user->isAdmin()) &&
            (!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes suficientes privilegios para estar aqui"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Team $team */
        if(!$team=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Team")->findOneBy(["competition"=>$competition,"slug"=>$team_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        /** @var CategoryAthlete $categoryAthlete **/
        if(!$categoryAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByCompetitionAndTeam($competition,$team)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->isUserAdminInCompetition($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

     return ["competition"=>$competition,"category"=>$categoryAthlete->getCategory(),"team"=>$team];
    }
    /**
     * @Delete("/{id}/", name="v2_panel_team_athlete_delete",requirements={"slug"="^[a-z0-9-]+$","id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteTeamAthleteAction(Request $request,$slug,$id){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return ["error"=>1];
        };
        /** @var User $user */
        $user=$this->getUser();
        if((!$user->isAdmin()) &&
            (!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes suficientes privilegios para estar aqui"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var TeamAthlete $categoryAthlete */
        if(!$teamAthlete=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:TeamAthlete")->find($id)){
            return ["error"=>1];
        };
        /** @var Team $team */
        $team=$teamAthlete->getTeam();
        $team->removeAthlete($teamAthlete);
        $this->get("doctrine.orm.default_entity_manager")->persist($team);
        $this->get("doctrine.orm.default_entity_manager")->remove($teamAthlete);
        $this->get("doctrine.orm.default_entity_manager")->flush();
        return ["success"=>1];
    }
    /**
     * @param Request $request
     * @param         $slug
     * @param         $team_slug
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     * @Rest\Patch("/{team_slug}/AddAthlete", name="v2_panel_team_athlete_add",requirements={"slug"="^[a-z0-9-]+$","id"="\d+","team_slug"="^[a-z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function addTeamAthleteAction(Request $request,$slug,$team_slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var User $user */
        $user=$this->getUser();
        if((!$user->isAdmin()) &&
            (!$this->get("bdsrw_competition.user")->isUserAdminInCompetition($user,$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes suficientes privilegios para estar aqui"));
            return $this->redirectToRoute('v2_panel_competitions');
        }
        /** @var Team $team */
        if(!$team=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Team")->findOneBy(["competition"=>$competition,"slug"=>$team_slug])){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };

        $usernameOrEmail=str_replace(' ' ,'',$request->get("email"));
        try{
            $this->get("bdsrw_competition.team")->addAthleteToTeam($usernameOrEmail,$team,$competition);
        } catch (\Exception $exception){
            if($exception->getCode()==421){
                return ["error"=>1,"message"=>$exception->getMessage()];
            }
            if($this->getParameter("kernel.debug")){
                throw $exception;
            }
            return ["error"=>1,"message"=>"Se ha producido un error"];
        }
            return ["success"=>1];
    }
}