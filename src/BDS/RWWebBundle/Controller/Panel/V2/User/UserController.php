<?php

namespace BDS\RWWebBundle\Controller\Panel\V2\User;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Form\CompetitionType;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use BDS\UserBundle\Form\UpdatePasswordType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use BDS\RWCompetitionBundle\Form\EditProfileType;

/**
 *
 * @Route("/profile")
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends FOSRestController
{


    /**
     * @Get("/find/", name="v2__user_finder", defaults={"_format"="json"},options={"expose"=true})
     */
    public function getCompetitionAthletesAction(Request $request,$slug){


    }
    /**
     * @Get("/",name="v2_get_dashboard_get_profile",defaults={"_format"="html"},options={"expose"=true})
     * @Template("BDSRWWebBundle:v2/user:profile.html.twig")
     */
    public function indexAction()
    {
        return ["user"=>$this->getUser()];
    }
    /**
     * @Get("/Edit/",name="v2_get_dashboard_edit_profile",defaults={"_format"="html"},options={"expose"=true})
     * @Post("/Edit/",name="v2_post_dashboard_edit_profile",defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/user/profile_edit.html.twig")
     */
    public function editProfileAction(Request $request)
    {
        /** @var User $user */
        $user=$this->getUser();

        $form=$this->createForm(EditProfileType::class,$user,["is_admin"=>true,"action"=>$this->generateUrl("v2_post_dashboard_edit_profile")]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $this->get("fos_user.user_manager")->updateUser($user);
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,"Perfil Actualizado");
                return $this->redirectToRoute("v2_get_dashboard_get_profile");
            }
            return ["form"=>$form->createView()];
        }

        return ["form"=>$form->createView(),"user"=>$user];
    }
    /**
     * @Get("/EditPassword/",name="v2_get_dashboard_edit_password_profile",defaults={"_format"="html"},options={"expose"=true})
     * @Post("/EditPassword/",name="v2_post_dashboard_edit_password_profile",defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/user/profile_update_password.html.twig")
     */
    public function editPasswordAction(Request $request)
    {
        /** @var User $user */
        $user=$this->getUser();

        $form=$this->createForm(UpdatePasswordType::class,$user,["action"=>$this->generateUrl("v2_post_dashboard_edit_password_profile")]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $user->setEnabled(true);
                $this->get("fos_user.user_manager")->updateUser($user);
                $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,"Contraseña Actualizada");
                return $this->redirectToRoute("v2_get_dashboard_get_profile");
            }
        }
        return ["form"=>$form->createView(),"user"=>$user];
    }

    /**
     * @Rest\Patch("/firstPasssword/",name="v2_user_set_first_password",defaults={"_format"="json"},options={"expose"=true})
     */
    public function setFirstPasswordAction(Request $request)
    {
        /** @var User $user */
        $user=$this->getUser();
        $password=$request->get("pwd");
        $user->setPlainPassword($password);
        $user->setConfirmationToken(null);
        $this->get("fos_user.user_manager")->updateUser($user);
        return ["success"=>true];
    }
}