<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/13/16
 * Time: 3:28 PM
 */

namespace BDS\RWWebBundle\Controller\Panel\V2\User;


use BDS\RWBoxBundle\Entity\Box;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\RWCompetitionBundle\Form\UserExtension\BoxAthleteType;
use BDS\RWWebBundle\Helper\NotyHelper;
use BDS\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyInfo\Type;

/**
 * @Route("/Profile/Box")
 * @Security("has_role('ROLE_USER')")
 */
class BoxUserController extends FOSRestController
{
    /**
     * @Get("/Cancel/",name="v2_get_panel_cancel_box",defaults={"_format"="html"},options={"expose"=true})
     */
    public function cancelBoxAction(Request $request)
    {
        /** @var User $user */
        $user=$this->getUser();
        $box=null;
        /** @var UserExtension $userExtension */
        if($userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user)) {
            $box=$userExtension->getBox();
            $userExtension->setBox(null);
            $this->get("doctrine.orm.default_entity_manager")->persist($userExtension);
            $this->get("doctrine.orm.default_entity_manager")->flush();
        }
        $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Ya no perteneces a '%s'",($box instanceof Box)?$box->getName():"Independent"));
        return $this->redirectToRoute("v2_get_dashboard_get_profile");
    }
    /**
     * @Post("/Set/",name="v2_post_panel_set_box",defaults={"_format"="html"},options={"expose"=true})
     */
    public function findBoxAction(Request $request)
    {
        /** @var User $user */
        $user=$this->getUser();
        $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
        $form=$this->createForm(BoxAthleteType::class,$userExtension,["method"=>"POST","action"=>$this->generateUrl("v2_post_panel_set_box")]);
        $form->handleRequest($request);
        if($form->isValid()){
            $this->get("doctrine.orm.default_entity_manager")->persist($userExtension);
            $this->get("doctrine.orm.default_entity_manager")->flush();
        }
        $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_SUCCESS,sprintf("Bienvenido a '%s'",($userExtension->getBox()->getName())));
        return $this->redirectToRoute("v2_get_dashboard_get_profile");
    }
}