<?php

namespace BDS\RWWebBundle\Controller\Panel\V2;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Form\CreateCompetitionType;
use BDS\RWWebBundle\Helper\NotyHelper;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/C")
 * @Security("has_role('ROLE_USER')")
 */
class CompetitionController extends FOSRestController
{

    /**
     * @Get("/", name="v2_panel_competitions", defaults={"_format"="html"},options={"expose"=true})
     * @Template("BDSRWWebBundle:v2/panel:my_competitions.html.twig")
     */
    public function getCompetitionsAction(){
        /** @var \BDS\UserBundle\Entity\User $user **/
        $user=$this->getUser();
        if($user->isAdmin()){
	        $phases=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findBy([],["created"=>"DESC"]);
        } else {
            $userExtension=$this->get("rankwod.user_extension")->getOrCreateUserExtension($user);
	        $phases=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Phase")->findByUserExtension($userExtension);
        }
	    return ["phases"=>$phases];

    }

    /**
     * @Get("/{slug}/", name="v2_panel_competition",requirements={"slug"="^[a-z0-9-]+$"}, defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/competition_dashboard.html.twig")
     */
    public function getCompetitionAction(Request $request,$slug){
        /** @var Competition $competition */
        if(!$competition=$this->get("doctrine.orm.default_entity_manager")->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("Se ha Producido un error"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        /** @var Competition $competition */
        if(!(($this->getUser()->isAdmin())||$this->get("bdsrw_competition.user")->doesUserParticipate($this->getUser(),$competition))){
            $this->get("bdsrw_web.noty.service")->addNoty(NotyHelper::TYPE_ERROR,sprintf("No tienes Acceso a esta Competición"));
            return $this->redirectToRoute('v2_panel_competitions');
        };
        $last_registrations=$this->get("rankwod.athlete")->getLastRegistrations($competition);
        return ["competition"=>$competition,"last_registrations"=>$last_registrations,"publishable_key"=>'pk_test_DnFXjddOGHOueTotpxRmLEdM'];
    }
    /**
     * @Get("/New/", name="v2_panel_competitions_get_create", defaults={"_format"="html"},options={"expose"=true})
     * @Post("/New/", name="v2_panel_competitions_post_create", defaults={"_format"="html"},options={"expose"=true})
     * @Template("@BDSRWWeb/v2/competition/competition/new_competition_form.html.twig")
     */
    public function createCompetitionAction(Request $request)
    {
        $competition=new Competition();
        $form=$this->createForm(CreateCompetitionType::class,$competition,["method"=>"POST","action"=>$this->generateUrl("v2_panel_competitions_post_create")]);
        if($request->getMethod()=="POST"){
            $form->handleRequest($request);
            if($form->isValid()){
                $this->get("doctrine.orm.default_entity_manager")->persist($competition);
                if(!$this->getUser()->isAdmin()){
                    $this->get("bdsrw_competition.user")->addAdminToCompetition($competition,$this->getUser());
                    /** @var CompetitionAdmin $au */
                    $au=$competition->getAdminUsers()->first();
                    $au->setStatus(CompetitionAdmin::STATUS_ACCEPTED);
                    $this->get("doctrine.orm.default_entity_manager")->persist($au);
                }
                $this->get("doctrine.orm.default_entity_manager")->flush();
                return $this->redirectToRoute("v2_panel_competition",["slug"=>$competition->getSlug()]);
            }
        }
        return ["form"=>$form->createView()];
    }
}