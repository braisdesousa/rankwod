<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;


use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Helpers\EventHelper;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\ORM\EntityManager;


class ResultTwigExtension extends \Twig_Extension
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter("wod_result",[$this, 'wodResult']),
            new \Twig_SimpleFilter("json_result",[$this, 'getJsonResult']),
            new \Twig_SimpleFilter("alpha",[$this, 'getAlphas']),
            new \Twig_SimpleFilter("measure_names",[$this, 'getMeasureNames']),
            new \Twig_SimpleFilter("category_name",[$this, 'categoryName']),
        );
    }

    public function getAlphas($index){
        $alphas = range('A', 'Z');
        return $alphas[$index];
    }
    public function getMeasureNames(Event $event){

        return EventHelper::getMeasureNames($event);

    }
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction("are_results_corrected",[$this, 'areResultsCorrected']),
        );
    }

    public function areResultsCorrected(array $results)
    {
        if(empty($results)){
            return false;
        }
        $corrected=true;
        /** @var EventResult $result */
        foreach($results as $result){

            if(!$result->isCorrected()){
             $corrected=false;
            }
        }
        return $corrected;
    }
    public function wodResult(EventResult $eventResult)
    {
        if(!$eventResult->getMeasure()){
            return "-";
        }
        if($eventResult->getMeasure()->getType()==Measure::MEASURE_TYPE_TIME){
            return gmdate("i:s", $eventResult->getResult());
        }
        return $eventResult->getResult();
    }
    public function getJsonResult($result,$measure)
    {
        if($measure==Measure::MEASURE_TYPE_TIME){
            return gmdate("i:s", $result);
        }
        return $result;
    }
    public function categoryName($slug)
    {
        if($category=$this->entityManager->getRepository("BDSRWCategoryBundle:Category")->findOneBySlug($slug)){
            return $category->getName();
        }
        return "Uncategorized";
    }

    public function getName()
    {
        return "rw_web_bundle_result";
    }

}