<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Form\UserExtension\BoxAthleteType;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Routing\Router;


class FormTwigExtension extends \Twig_Extension
{
    private $entityManager;
    private $formFactory;
    private $router;
    public function __construct(EntityManager $entityManager,FormFactory $formFactory,Router $router)
    {
        $this->entityManager=$entityManager;
        $this->formFactory=$formFactory;
        $this->router=$router;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_athelte_box_form',[$this,"getAthleteBoxForm"]),
        ];
    }

    public function getAthleteBoxForm()
    {
        $form=$this->formFactory->create(BoxAthleteType::class,null,["method"=>"POST","action"=>$this->router->generate("v2_post_panel_set_box")]);
        return $form->createView();
    }
    public function getName()
    {
        return "rw_web_bundle_form";
    }

}