<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;


use BDS\RWCacheBundle\Service\CompetitionResultCacheService;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\PublishedInterface;
use BDS\RWCompetitionBundle\Service\CompetitionService;
use BDS\RWCompetitionBundle\Service\CompetitionUserService;
use BDS\RWCompetitionBundle\Service\UserExtensionService;
use BDS\RWMeasureBundle\Service\MeasureService;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;


class CompetitionTwigExtension extends \Twig_Extension
{
    private $entityManager;
    private $competitionUserService;
    private $competitionService;
    private $competitionResultCacheService;
    private $measureService;
    private $userExtensionService;
    private $webDir;
    public function __construct(
        EntityManager $entityManager,
        UserExtensionService $userExtensionService,
        CompetitionService $competitionService,
        CompetitionUserService $competitionUserService,
        CompetitionResultCacheService $competitionResultCacheService,
        MeasureService $measureService,
        $rootDir)
    {
        $this->entityManager=$entityManager;
        $this->userExtensionService=$userExtensionService;
        $this->competitionUserService=$competitionUserService;
        $this->competitionService=$competitionService;
        $this->competitionResultCacheService=$competitionResultCacheService;
        $this->measureService=$measureService;
        $this->webDir=$rootDir."/../web/";

    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter("competition_status_class",[$this,'getCompetitionStatusClass']),
            new \Twig_SimpleFilter("competition_status_copy",[$this,'getCompetitionStatusCopy']),
            new \Twig_SimpleFilter("has_image",[$this,'hasImage']),
            new \Twig_SimpleFilter("has_user",[$this,'hasUser']),
            new \Twig_SimpleFilter("is_competition_athletes_page",[$this,'isCompetitionAthletesPage']),
            new \Twig_SimpleFilter("is_competition_bots_page",[$this,'isCompetitionBotsPage']),
            new \Twig_SimpleFilter("is_competition_events_page",[$this,'isCompetitionEventsPage']),
            new \Twig_SimpleFilter("is_competition_round_page",[$this,'isCompetitionRoundPage']),
            new \Twig_SimpleFilter("is_competition_athletes_page_form",[$this,'isCompetitionAthletesPageForm']),
            new \Twig_SimpleFilter("are_results_setted",[$this,'areResultsSetted']),
            new \Twig_SimpleFilter("get_available",[$this,'getAvailable']),
            new \Twig_SimpleFilter("get_event_status_label",[$this,'getEventStatusLabel']),
            new \Twig_SimpleFilter("get_event_status_name",[$this,'getEventStatusName']),
            new \Twig_SimpleFilter("get_published",[$this,'getPublished']),
            new \Twig_SimpleFilter("has_pending_athletes",[$this,'hasPendingAthletes']),
            new \Twig_SimpleFilter("count_pending_athletes",[$this,'countPendingAthletes']),
            new \Twig_SimpleFilter("phases_with_overall",[$this,'phasesWithOverall']),
            new \Twig_SimpleFilter("is_invitation_mail",[$this,'isInvitationMail']),
            new \Twig_SimpleFilter("are_results_updatable",[$this,'areResultsUpdatable']),
            new \Twig_SimpleFilter("allowed_to_see_phase",[$this,'allowedToSeePhase']),
            new \Twig_SimpleFilter("should_show",[$this,'shouldShow']),
            new \Twig_SimpleFilter("athlete_competitions",[$this,'getAthleteCompetitions']),
            new \Twig_SimpleFilter("admin_competitions",[$this,'getAdminCompetitions']),
            new \Twig_SimpleFilter("judge_competitions",[$this,'getJudgeCompetitions']),
            new \Twig_SimpleFilter("get_competitions",[$this,'getCompetitions']),
            new \Twig_SimpleFilter("get_competition_icon",[$this,'getCompetitionIcon']),
            new \Twig_SimpleFilter("get_competition_type",[$this,'getCompetitionType']),
            new \Twig_SimpleFilter("can_register",[$this,'canRegister']),
            new \Twig_SimpleFilter("is_user_in_phase",[$this,'isUserInPhase']),
            new \Twig_SimpleFilter("is_user_in_category",[$this,'isUserInCategory']),
            new \Twig_SimpleFilter("get_athlete",[$this,'getAthlete']),
            new \Twig_SimpleFilter("find_athlete",[$this->competitionUserService,'getUserInPhase']),
            new \Twig_SimpleFilter("in_team", [$this,'inTeam']),
            new \Twig_SimpleFilter("get_team",[$this->competitionUserService,'getUserInTeam']),
            new \Twig_SimpleFilter("get_result_type",[$this->measureService,'getMeasureName']),
            new \Twig_SimpleFilter("ready_to_publish",[$this->competitionService,'isPhaseReadyToPublish']),
            new \Twig_SimpleFilter("is_athlete_complete",[$this->competitionService,'userIsCompleted']),
            new \Twig_SimpleFilter("athlete_has_to_pay",[$this->competitionService,'athleteHasToPay']),
            new \Twig_SimpleFilter("get_user_extension",[$this->userExtensionService,'getOrCreateUserExtension']),
        ];
    }
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction( 'competition_image_exists',[$this, 'competitionImageExists']),
            new \Twig_SimpleFunction( 'get_competition_phases',[$this, 'getCompetitionPhases']),
            new \Twig_SimpleFunction( 'get_user_event_result',[$this, 'getUserEventResult']),
            new \Twig_SimpleFunction( 'get_user_event_results',[$this, 'getUserEventResults']),
            new \Twig_SimpleFunction( 'get_athlete_event_results',[$this, 'getAthleteEventResults']),
            new \Twig_SimpleFunction( 'is_user_admin_in_competition',[$this, 'isUserAdminInCompetition']),
            new \Twig_SimpleFunction( 'is_user_judge_in_competition',[$this, 'isUserJudgeInCompetition']),
            new \Twig_SimpleFunction( 'can_create_phase',[$this, 'canCreatePhase']),
            new \Twig_SimpleFunction( 'is_user_admin_or_judge_in_competition',[$this, 'isUserAdminOrJudgeInCompetition']),
            new \Twig_SimpleFunction( 'is_user_in_phase',[$this, 'isUserInPhase']),
            new \Twig_SimpleFunction( 'convert_sort_fa',[$this, 'convertSortFa']),
            new \Twig_SimpleFunction( 'convert_sort',[$this, 'convertSort']),
            new \Twig_SimpleFunction("count_athletes_in_phase_and_category",[$this,'countAthletesInPhaseAndCategory']),
            new \Twig_SimpleFunction("get_user_competitions",[$this,'getUserCompetitions']),
            new \Twig_SimpleFunction("get_competitions",[$this,'getCompetitions']),
            new \Twig_SimpleFunction("has_pending_by_organizer",[$this,'hasPendingByOrganizer'])
        ];

    }
    public function inTeam(User $user, Competition $competition){
        return $this->competitionUserService->getUserInTeam($user,$competition)&&1;
    }
    public function getCompetitionStatusClass($published=false){
        return $published?"bg-teal":"bg-grey";
    }
    public function getCompetitionStatusCopy($published=false){
        return $published?"Publicada":"Borrador";
    }
    public function hasPendingByOrganizer(Phase $phase){
        return $this->competitionUserService->phaseHasPendingByOrganizer($phase);
    }
    public function isUserInCategory(User $user, Category $category){
    	return $this->competitionUserService->isUserInCategory($user,$category);
    }
    public function getAthlete(User $user,Category $category){
    	return $this->competitionUserService->findAthleteByUserAndCategory($user,$category);
    }

    public function canRegister(Competition $competition){
    	foreach($competition->getPublishedPhases() as $phase){
    		if($phase->isPaid() && $phase->AcceptApplies()){
    			return true;
		    }
	    }
	    return false;
    }
    public function shouldShow(Event $event,User $user){
    	 return $this->competitionUserService->shouldShowEvent($event,$user);
    }
    public function allowedToSeePhase(Phase $phase, User $user){
    	if(
    		($user->isAdmin())||
		    ($this->isUserAdminInCompetition($user,$phase->getCompetition()))||
		    (($this->isUserInPhase($user,$phase))&&$phase->isPublished())
	    ){
    		return true;
	    }
	    return false;
    }
    public function hasImage(Competition $competition){

    	$path=sprintf("%sbundles/bdsrwweb/images/competitions/%s.jpg",$this->webDir,$competition->getSlug());
    	return is_file($path);
    }
    public function ConvertSortFa($sort,$place){
        // Sort 0 Default
        // Sort 1 Name ASC
        // Sort 2 Name DESC
        // Sort 3 Box ASC
        // Sort 4 Box DESC
        // Sort 5 Status ASC
        // Sort 6 Status DESC
        // Sort 7 Category ASC
        // Sort 8 Category DESC
        $convertedSort="sort";
        switch($sort){
            case 0: return "sort"; break;
            case 1: return $convertedSort=(($place=="name")?"sort-asc":"sort"); break;
            case 2: return $convertedSort=(($place=="name")?"sort-desc":"sort"); break;
            case 3: return $convertedSort=(($place=="box")?"sort-asc":"sort"); break;
            case 4: return $convertedSort=(($place=="box")?"sort-desc":"sort"); break;
            case 5: return $convertedSort=(($place=="status")?"sort-asc":"sort"); break;
            case 6: return $convertedSort=(($place=="status")?"sort-desc":"sort"); break;
            case 7: return $convertedSort=(($place=="category")?"sort-asc":"sort"); break;
            case 8: return $convertedSort=(($place=="category")?"sort-desc":"sort"); break;
        }
        return $convertedSort;

    }
    
    public function ConvertSort($sort,$place){
        // Sort 0 Default
        // Sort 1 Name ASC
        // Sort 2 Name DESC
        // Sort 3 Box ASC
        // Sort 4 Box DESC
        // Sort 5 Status ASC
        // Sort 6 Status DESC
        // Sort 7 Category ASC
        // Sort 8 Category DESC

        switch($place){
            case "name": return ($sort==1)?2:1; break;
            case "box": return ($sort=3)?4:3; break;
            case "status": return ($sort==5)?6:5; break;
            case "category": return ($sort==7)?8:7; break;
        }
        return 1;

    }
    public function hasUser(Competition $competition,User $user)
    {
    	return $this->competitionUserService->doesUserParticipate($user,$competition);
    }
    public function competitionImageExists(Competition $competition)
    {
        $fs=new Filesystem();
        return ($fs->exists(sprintf("%s/../Resources/public/images/competitions/%s.jpg",__DIR__,$competition->getSlug())));
        
    }
    public function hasPendingAthletes(Competition $competition)
    {
        return $this->countPendingAthletes($competition)&&1;
    }
    public function countPendingAthletes(Competition $competition)
    {
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->countPendingAthletesInCompetition($competition);
    }
    public function countAthletesInPhaseAndCategory(Phase $competition,Category $category)
    {
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->countAthletesInPhaseAndCategory($competition,$category);
    }
    public function getEventStatusLabel(Event $event){
            if($event->isExpired()){
                return "default";
            }else{
                return "success";
            }
    }
    public function getEventStatusName(Event $event){
        if($event->isExpired()){
            return "Cerrado";
        }else{
            return "Abierto";
        }
    }
    /**
     * @param User $user
     * @param Event $event
     * @return \BDS\RWCompetitionBundle\Entity\EventResult|bool|null|object
     * @deprecated No debemos obtener solo un result, sino un array de ellos.
     */
    public function getUserEventResult(User $user,Event $event){
        return $this->competitionUserService->getUserEventResult($user,$event);
    }
    public function getUserEventResults(User $user,Event $event){
        return $this->competitionUserService->getUserEventResults($user,$event);
    }

    /**
     * @param EventResult[] $results
     */
    public function areResultsSetted(array $results){
           $unSetted=true;

            foreach($results as $result){
                if(intval($result->getResult())!==0){
                    $unSetted=false;
                }
            }
        return !$unSetted;
    }
    public function getAthleteEventResults(CategoryAthlete $athlete,Event $event){
        return $this->competitionUserService->getAthleteEventResults($athlete,$event);
    }
    public function isUserInPhase(User $user, Phase $phase){
        return $this->competitionUserService->isUserInPhase($user,$phase);
    }
    public function canCreatePhase(User $user,Competition $competition){
        return ($user->isAdmin() || ($this->competitionUserService->canCreatePhase($user,$competition)));
    }
    public function isUserAdminInCompetition(User $user,Competition $competition){
        return ($user->isAdmin()||$this->competitionUserService->isUserAdminInCompetition($user,$competition));
    }

    public function isUserJudgeInCompetition(User $user,Competition $competition){
        return $this->competitionUserService->isUserJudgeInCompetition($user,$competition);
    }
    public function isUserAdminOrJudgeInCompetition(User $user,Competition $competition){
        return $this->competitionUserService->isUserAdminOrJudgeInCompetition($user,$competition);
    }

    public function isCompetitionAthletesPage($routeName){
        return (strpos($routeName,'get_panel_competition_athlete')===0);
    }
    public function isCompetitionBotsPage($routeName){
        return (strpos($routeName,'get_panel_competition_bots')===0);
    }
    public function isCompetitionEventsPage($routeName){
        return (strpos($routeName,'get_panel_competition_event')===0);
    }
    public function isCompetitionRoundPage($routeName){
        return (strpos($routeName,'panel_competition_round')!==false);
    }
    public function isCompetitionAthletesPageForm($routeName){
        return (strpos($routeName,'get_panel_competition_athletes_form')===0);
    }
    public function phasesWithOverall(Collection $phases,Phase $showedPhase)
    {
        $phasesWithOverall=[];
        /** @var Phase $phase */
        foreach($phases as $phase){
            if($phase->getOverall()&&($showedPhase!==$phase)){
                $phasesWithOverall[]=$phase;
            }
        }
        return $phasesWithOverall;
    }

    public function getPublished(Collection $collection){
        $published=new ArrayCollection();
        /** @var Collection|PublishedInterface[] $collection  */
        foreach($collection as $element){
            if($element->isPublished()){
                $published->add($element);
            }
        }
        return $published;
    }
    public function getCompetitionPhases(Collection $phases)
    {
        $publishedPhases=new ArrayCollection();
        /** @var Phase $phase */
        foreach($phases as $phase){
            if($phase->isPublished()){
                $publishedPhases->add($phase);
            }
        }
        return $publishedPhases;
    }
    public function getAvailable(Collection $collection){
            foreach($collection as $key=>$value){
                if ($value instanceof PublishedInterface){
                    if(!$value->isPublished()){
                        $collection->remove($key);
                    }
                }
            }
        return $collection;
    }
    public function isInvitationMail($element)
    {
        return ($element instanceof PhaseInvitation);
    }
    public function areResultsUpdatable(Competition $competition)
    {
        return $this->competitionResultCacheService->hasUpToDateCache($competition);
    }
    public function getUserCompetitions(User $user,$over=false){
        return $this->competitionUserService->getUserCompetitions($user,$over);
    }
    public function getCompetitions(User $user){
        return $this->competitionUserService->getCompetitions($user);
    }
    public function getAdminCompetitions(User $user){
        return $this->competitionUserService->getAdminCompetitions($user);
    }
    public function getAthleteCompetitions(User $user){
        return $this->competitionUserService->getCompetitions($user);
    }
    public function getJudgeCompetitions(User $user){
        return $this->competitionUserService->getJudgeCompetitions($user);
    }
    public function getCompetitionIcon(Competition $competition, User $user){
        if($this->competitionUserService->isUserAdminInCompetition($user,$competition)){
            return 's7-id';
        } elseif($this->competitionUserService->isUserJudgeInCompetition($user,$competition)){
            return 's7-hammer';
        }
        else{
            return 's7-gym';
        }
    }

    public function getCompetitionType(Competition $competition, User $user){
        if($this->competitionUserService->isUserAdminInCompetition($user,$competition)){
            return 'Administrador';
        } elseif($this->competitionUserService->isUserJudgeInCompetition($user,$competition)){
            return 'Juez';
        }
        else{
            return 'Atleta';
        }
    }
    public function getName()
    {
        return "rw_web_bundle_competition";
    }

}