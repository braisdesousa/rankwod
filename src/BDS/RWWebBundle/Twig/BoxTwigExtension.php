<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;


class BoxTwigExtension extends \Twig_Extension
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('get_athlete_box_name',[$this,"getAthleteBoxName"]),
            new \Twig_SimpleFilter('get_user_box',[$this,"getUserBox"]),
            new \Twig_SimpleFilter('get_user_box_name',[$this,"getUserBoxName"]),

        ];
    }


    public function getAthleteBoxName(CategoryAthlete $categoryAthlete)
    {
        if($categoryAthlete->getTeam()){
            return "#";
        }
        if($boxAthlete=$this->entityManager->getRepository("BDSRWCompetitionBundle:UserExtension")->findOneBy(["user_id"=>$categoryAthlete->getUserId()])){
            if($boxAthlete->getBox()){
                return $boxAthlete->getBox()->getName();
            }
        }
        return "Independent";
    }
    public function getUserBox(User $user)
    {
        return $this->entityManager->getRepository("BDSRWBoxBundle:Box")->findBoxByUserId($user);
    }
    public function getUserBoxName(User $user)
    {
        if($box=$this->getUserBox($user)){
          return $box->getName();
        }
        return "Independent";
    }
    public function getName()
    {
        return "rw_web_bundle_box";
    }

}