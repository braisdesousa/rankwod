<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Service\NotificationService;
use BDS\UserBundle\Entity\User;
use FOS\UserBundle\Doctrine\UserManager;


class UserTwigExtension extends \Twig_Extension
{
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService=$notificationService;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('user_status_label',[$this,"UserStatusLabel"]),
        ];
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('count_notifications',array($this,"countNotifications")),
            new \Twig_SimpleFunction('get_notifications',  array($this,"getNotifications")),

        );
    }
    public function countNotifications(User $user)
    {
		return $this->notificationService->countNotifications($user);
    }
    public function getNotifications(User $user)
    {
	    return $this->notificationService->getNotifications($user);
    }
    public function userStatusLabel($status){

        switch ($status){
            case CategoryAthlete::STATUS_PENDING_USER: $label ="warning"; break;
            case CategoryAthlete::STATUS_ACCEPTED: $label ="success"; break;
            default: $label="default";
        }
        return $label;
    }

    public function getName()
    {
        return "rw_web_bundle_user";
    }

}