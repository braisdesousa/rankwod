<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;


class ChartTwigExtension extends \Twig_Extension
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function getFilters()
    {
     return [
         new \Twig_SimpleFilter("competition_athletes",[$this, 'competitionAthletes']),
         new \Twig_SimpleFilter("competition_boxes",[$this, 'competitionBoxes']),
         new \Twig_SimpleFilter("phase_athletes",[$this, 'phaseAthletes']),
     ];
    }
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction("donut_chart",[$this, 'donutChart']),
            new \Twig_SimpleFunction("get_team_from_description",[$this, 'getTeamFromDescription']),
        );
    }
    public function getTeamFromDescription(string $description){
        $order=$this->entityManager->getRepository("BDSRWPaymentBundle:Order")->findOneByPaymentDescription($description);
        /** @var CategoryAthlete $categoyTeam */
        $categoyTeam=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByOrder($order);
        return $categoyTeam->getTeam();
    }
    public function competitionBoxes(Competition $competition)
    {
        return count($this->entityManager->getRepository("BDSRWCompetitionBundle:UserExtension")->findCompetitionBoxes($competition));
    }
    public function competitionAthletes(Competition $competition)
    {
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->countCompetitionAthletes($competition);
    }
    public function phaseAthletes(Phase $phase)
    {
	    $count=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->countPhaseAthletes($phase);
        return $count?$count:1;
    }
    public function donutChart(Phase $phase)
    {
        $response=[];
        foreach ($phase->getCategories() as $category){
            $response[]=["label"=>$category->getName(),"value"=>$category->countAthletes()];
        }
        return json_encode($response);
    }


    public function getName()
    {
        return "rw_web_bundle_chart";
    }

}