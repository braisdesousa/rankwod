<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;




use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWPaymentBundle\Entity\Order;

class OrderTwigExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('order_paid',[$this,"isOrderPaid"]),
            new \Twig_SimpleFilter('order_invitation',[$this,"isOrderInvitation"]),
            new \Twig_SimpleFilter('order_unpaid',[$this,"isOrderUnPaid"]),
            new \Twig_SimpleFilter('athlete_accepted',[$this,"isAthleteAccepted"]),
        ];
    }

    public function isOrderPaid($order_status){
        return Order::STATUS_PAID===$order_status;
    }
    public function isOrderUnPaid($order_status){
        return Order::STATUS_UNPAID===$order_status;
    }
    public function isOrderInvitation($order_status){
        return Order::STATUS_INVITATION===$order_status;
    }
    public function isAthleteAccepted($athlete_status){
        return CategoryAthlete::STATUS_ACCEPTED===$athlete_status;
    }
    public function getName()
    {
        return "rw_web_order";
    }

}