<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/5/16
 * Time: 12:37 PM
 */

namespace BDS\RWWebBundle\Twig;


use BDS\MultiTenantBundle\Service\LicenseService;


class DistributorTwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var LicenseService
     */
    private $licenseService;

    public function __construct(LicenseService $licenseService)
    {
        $this->licenseService=$licenseService;
    }

    public function getGlobals()
    {
        return ["license"=>$this->licenseService->getLicense()];

    }
    public function getName()
    {
        return "rw_web_distributor";
    }

}