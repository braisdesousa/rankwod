<?php

namespace BDS\RWWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WGTeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add("name",TextType::class,["required"=>true,"label"=>"Nombre del Equipo","attr"=>["placeholder"=>"Nombre del Equipo"]]);
        $builder->add("athlete_1",WGAthleteType::class,["required"=>true,"label"=>"Capitan del Equipo | Atleta #1"]);
        $builder->add("athlete_2",WGAthleteType::class,["required"=>true,"label"=>"Atleta #2"]);
        $builder->add("athlete_3",WGAthleteType::class,["required"=>true,"label"=>"Atleta #3"]);
        $builder->add("athlete_4",WGAthleteType::class,["required"=>true,"label"=>"Atleta #4"]);
        $builder->add("athlete_5",WGAthleteType::class,["required"=>true,"label"=>"Atleta #5"]);
        $builder->add("athlete_6",WGAthleteType::class,["required"=>false,"label"=>"Atleta #6 (Opcional)"]);
        $builder->add("athlete_7",WGAthleteType::class,["required"=>false,"label"=>"Atleta #7 (Opcional)"]);
        $builder->add("submit",SubmitType::class,["label"=>"Siguiente Paso","attr"=>["class"=>'btn btn-info pull-right']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            "csrf_protection"=>false,
        ));
    }

    public function getBlockPrefix()
    {
        return 'bdsrwweb_bundle_wgathlete_type';
    }
}
