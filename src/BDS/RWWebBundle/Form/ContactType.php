<?php

namespace BDS\RWWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                            "label"=>false,
                            "attr"=>["placeholder"=>"Tu nombre"],
                            "constraints"=>[new Length(["min"=>3,"max"=>80])]
            ])
            ->add('email',EmailType::class,[
                            "label"=>false,
                            "attr"=>["placeholder"=>"@email"],
                            "constraints"=>[new Email()]
            ])
            ->add('text',TextareaType::class,[
                            "label"=>false,
                            "attr"=>["placeholder"=>"Cuéntanos lo que tienes en mente"],
                            "constraints"=>[new Length(["min"=>3,"max"=>5000])]
            ])
            ->add('submit',SubmitType::class,["label"=>"Enviar","attr"=>["class"=>"btn mt-button medium outline"]])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'method'=> "POST"
            
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwwebbundle_contact';
    }
}
