<?php

namespace BDS\RWWebBundle\Form\Transformer;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: bra
 * Date: 8/07/17
 * Time: 12:58
 */
class DateTimeToArrayTransformer extends \Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToArrayTransformer
{

    public function reverseTransform($value)
    {
        if (null === $value) {
            return;
        }

        if (!is_array($value)) {
            throw new TransformationFailedException('Expected an array.');
        }

        if ('' === implode('', $value)) {
            return;
        }
        if ('000' === implode('', $value)) {
            return;
        }
        return parent::reverseTransform($value);
    }
}