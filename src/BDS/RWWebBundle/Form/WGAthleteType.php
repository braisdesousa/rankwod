<?php

namespace BDS\RWWebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WGAthleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add("first_name",TextType::class,["required"=>true,"label"=>"Nombre","attr"=>["placeholder"=>"Nombre del Atleta"]]);
        $builder->add("last_name",TextType::class,["required"=>true,"label"=>"Apellidos","attr"=>["placeholder"=>"Apellidos"]]);
        $builder->add("email",EmailType::class,["required"=>true,"label"=>"Email","attr"=>["placeholder"=>"Email de Contacto"]]);
        $builder->add("rm",NumberType::class,["required"=>true,"label"=>"RM de Snatch","attr"=>["placeholder"=>"RM aproximado de SNATCH"]]);
        $builder->add("phone",TextType::class,["required"=>true,"label"=>"Teléfono","attr"=>["placeholder"=>"Teléfono de Contacto"]]);
        $builder->add("tshirt",ChoiceType::class,
            [
                "required"=>true,
                "label"=>"Talla de Camiseta",
                "choices"=>[
                    "XS"=>"XS",
                    "S"=>"S",
                    "M"=>"M",
                    "L"=>"L",
                    "XL"=>"XL",
                ],
                "attr"=>["placeholder"=>"Talla de Camiseta"]]);
        $builder->add("dni",TextType::class,["required"=>true,"label"=>"DNI","attr"=>["placeholder"=>"DNI o Passaporte"]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
        ));
    }

    public function getBlockPrefix()
    {
        return 'bdsrwweb_bundle_wgathlete_type';
    }
}
