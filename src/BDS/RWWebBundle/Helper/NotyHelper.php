<?php

namespace BDS\RWWebBundle\Helper;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/16/16
 * Time: 4:18 PM
 */
class NotyHelper
{
    CONST TYPE_ALERT= "alert";
    CONST TYPE_SUCCESS= "success";
    CONST TYPE_ERROR= "error";
    CONST TYPE_WARNING= "warning";
    CONST TYPE_INFORMATION= "information";

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session=$session;
    }
    public function addNoty($type,$message){
        $this->session->getFlashBag()->add('noty', ["type"=>$type,"message"=>$message]);
    }

}