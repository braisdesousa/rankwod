$(document).ready(function() {
  $("a.delete-overall").click(function(){
       $.ajax({
           headers: {'X-HTTP-Method-Override': 'DELETE'},
           method: "POST",
           url: Routing.generate("delete_panel_competition_overall",{'competition_slug': $(this).attr("data-competition"),'phase_slug':$(this).attr("data-phase")}),
           context: $(this).closest("div.col-md-6"),
           success: function(data){
               if(data.success){
                   notySuccess('Overall eliminado correctamente');
                   $(this).remove();
               } else {
                   notyError(data.message);
               }

           },
           error: function(jqXHR){
               notyError('Se ha producido un error');
           },
           dataType: 'JSON'
       });
   });
  $("a.delete-event").click(function(){
       $.ajax({
           headers: {'X-HTTP-Method-Override': 'DELETE'},
           method: "POST",
           url: Routing.generate("delete_panel_competition_event",{'competition_slug': $(this).attr("data-competition"),'phase_slug':$(this).attr("data-phase"),'event_slug':$(this).attr("data-event")}),
           context: $(this).closest("div.col-md-6"),
           success: function(data){
               if(data.success){
                   notySuccess('Evento Eliminado Correctamente');
                   $(this).remove();
               } else {
                   notyError(data.message);
               }

           },
           error: function(jqXHR){
               notyError('Se ha producido un error');
           },
           dataType: 'JSON'
       });
   });
  $("a.delete-event-results").click(function(){
       $.ajax({
           headers: {'X-HTTP-Method-Override': 'DELETE'},
           method: "POST",
           url: Routing.generate("delete_panel_competition_event_result",{'slug': $(this).attr("data-competition"),'phase_slug':$(this).attr("data-phase"),'event_slug':$(this).attr("data-event"),'athlete_id':$(this).attr("data-athlete")}),
           success: function(data){
               if(data.success){
                   notySuccess('Resultados eliminado correctamente recarga por ahora');
               } else {
                   notyError(data.message);
               }

           },
           error: function(jqXHR){
               notyError('Se ha producido un error');
           },
           dataType: 'JSON'
       });
   });
  $("a.delete-round").click(function(){
     $.ajax({
           headers: {'X-HTTP-Method-Override': 'DELETE'},
           method: "POST",
           context: $(this).closest("tr"),
           url: Routing.generate("delete_panel_competition_round",{'competition_slug': $(this).attr("data-competition"),'phase_slug':$(this).attr("data-round"),'round_slug':$(this).attr("data-round")}),
           success: function(data){
               if(data.success){
                   notySuccess('Ronda eliminada correctamente');
                  $(this).remove();
               } else {
                 notyError(data.message);
               }

           },
       error: function(jqXHR){
         notyError('Se ha producido un error');
       },
           dataType: 'JSON'
       });
   });
  $("a.delete-round-athlete").click(function(){
     $.ajax({
           headers: {'X-HTTP-Method-Override': 'DELETE'},
           method: "POST",
           context: $(this).closest("tr"),
           url: Routing.generate("delete_panel_competition_round_athlete",{
                          'competition_slug': $(this).closest('table').attr("data-competition"),
                          'phase_slug':$(this).closest('table').attr("data-phase"),
                          'round_slug':$(this).closest('table').attr("data-round"),
                          'username':$(this).attr("data-username")
           }),
           success: function(data){
               if(data.success){
                 notySuccess('Usuario eliminado de la ronda correctamente');
                  $(this).remove();
               } else {
                 notyError(data.message);
               }

           },
       error: function(jqXHR){
         notyError('Se ha producido un error');
       },
           dataType: 'JSON'
       });
   });
  $("a.accept_athlete").click(function(){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("post_panel_competition_accept_athlete_invitation",{'slug': $(this).closest(".panel-body").attr("data-competition"),'id':$(this).attr("data-id")}),
      context: $(this).closest("invitation"),
      success: function(data){
        if(data.success){
          notySuccess(data.message);
          $(this).remove();
        } else {
          notyError(data.message);
        }

      },
      error: function(jqXHR){
        notyError('Se ha producido un error');
      },
      dataType: 'JSON'
    });
  });
  $("a.deny_athlete").click(function(){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("post_panel_competition_deny_athlete_invitation",{'slug': $(this).closest(".panel-body").attr("data-competition"),'id':$(this).attr("data-id")}),
      context: $(this).closest("invitation"),
      success: function(data){
        if(data.success){
          notySuccess(data.message);
          $(this).remove();
        } else {
          notyError(data.message);
        }

      },
      error: function(jqXHR){
        notyError('Se ha producido un error');
      },
      dataType: 'JSON'
    });
  });
  $("input.show-result-selector").click(function(){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("patch_panel_competition_event_set_result_visibility",{'slug': $(".phase-info").attr("data-competition"),'phase_slug': $(".phase-info").attr("data-phase"),'event_slug':$(this).attr("data-event")}),
      data: {"visible": ($(this).is(':checked')?1:0)},
      success: function(data){
        if(data.success){
          if(data.url){
            window.location=data.url;
          }
          notySuccess(data.message);
        } else {
          notyError(data.message);
        }

      },
      error: function(jqXHR){
        notyError('Se ha producido un error');
      },
      dataType: 'JSON'
    });
  });
  $("input.publish-event-selector").click(function(){
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("patch_panel_competition_event_set_visibility",{'slug': $(".phase-info").attr("data-competition"),'phase_slug': $(".phase-info").attr("data-phase"),'event_slug':$(this).attr("data-event")}),
            data: {"visible": ($(this).is(':checked')?1:0)},
            success: function(data){
                if(data.success){
                    if(data.url){
                        window.location=data.url;
                    } else {
                        notySuccess(data.message);
                    }
                } else {
                    notyError(data.message);
                }

            },
            error: function(jqXHR){
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
  $("input#publish-competition").click(function(){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("patch_panel_competition_set_visibility",{'slug': $(this).attr("data-competition")}),
      // data: {"visible": ($(this).is(':checked')?1:0)},
      success: function(data){
        if(data.success){
          notySuccess(data.message);
        } else {
          notyError(data.message);
        }
      },
      error: function(jqXHR){
        notyError('Se ha producido un error');
      },
      dataType: 'JSON'
    });
  });

    var addUserToRound = function(competition_slug,phase_slug,round_slug,username)
    {
      $.ajax({
        headers: {'X-HTTP-Method-Override': 'PATCH'},
        method: "POST",
        url: Routing.generate("",{'competition_slug': competition_slug,'phase_slug':phase_slug,"round_slug":round_slug,"username":username}),
        success: function(data){
          if(data.success){
            notySucess('Atleta Añadido');
            location.reload();
          } else {
            notyError(data.message);
          }

        },
        error: function(jqXHR){
          notyError('Se ha producido un error');
        },
        dataType: 'JSON'
      });
    };
    function reloadOverall(competition_slug,phase_slug,category_slug)
    {
        $.ajax({
            method: "GET",
            url: Routing.generate(("get_panel_competition_phase_overall_category_template"), {
                'competition_slug': competition_slug,
                'phase_slug': phase_slug,
                "category_slug": category_slug
            }),
            success: function(data){
                $("#overall-table").html(data);
                TieBreakers();
            },
            error: function(jqXHR,qr,abc,x){
              notyError('Se ha producido un error');
            }
        })
    }
    function tieBreakResolver(tieBreakUp,competition_slug,phase_slug,athlete_id){
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate((tieBreakUp?"patch_panel_competition_phase_overall_tiebreak_up":"patch_panel_competition_phase_overall_tiebreak_down"),{'competition_slug': competition_slug,'phase_slug':phase_slug,"athlete_id":athlete_id}),
            success: function(data){
                if(data.success){
                    notySuccess('Empate Resuelto');
                    reloadOverall(data.competition_slug,data.phase_slug,data.category_slug);

                } else {
                  notyError(data.message);
                }

            },
          error: function(jqXHR){
            notyError('Se ha producido un error');
          },
            dataType: 'JSON'
        });
    }
    function TieBreakers(){
        $("a.tie_break_up").click(function(){
            tieBreakResolver(true,$(this).attr("data-competition"),$(this).attr("data-phase"),$(this).attr("data-athlete"));
        });
        $("a.tie_break_down").click(function(){
            tieBreakResolver(false,$(this).attr("data-competition"),$(this).attr("data-phase"),$(this).attr("data-athlete"));
        });
    }
    TieBreakers();
  
});
$("a.promote").click(function(){
  $.ajax({
    headers: {'X-HTTP-Method-Override': 'PATCH'},
    method: "POST",
    url: Routing.generate("patch_panel_admin_promote",{'username': $(this).attr("data-username")}),
    context: $(this).closest("tr"),
    success: function(data){
      if(data.success){
        notySuccess("Usuario Actualizado");
        $(this).remove();
      } else {
        notyError(data.message);
      }

    },
    error: function(jqXHR){
      notyError('Se ha producido un error');
    },
    dataType: 'JSON'
  });
});
$("a.demote").click(function(){
  $.ajax({
    headers: {'X-HTTP-Method-Override': 'PATCH'},
    method: "POST",
    url: Routing.generate("patch_panel_admin_demote",{'username': $(this).attr("data-username")}),
    context: $(this).closest("tr"),
    success: function(data){
      if(data.success){
        notySuccess("Usuario Actualizado");
        $(this).remove();
      } else {
            notyError(data.message);
      }

    },
    error: function(jqXHR){
      notyError('Se ha producido un error');
    },
    dataType: 'JSON'
  });
});
var addUserToRound = function(competition_slug,phase_slug,round_slug,username)
{
  $.ajax({
    headers: {'X-HTTP-Method-Override': 'PATCH'},
    method: "POST",
    url: Routing.generate("patch_panel_competition_round_athlete",{'competition_slug': competition_slug,'phase_slug':phase_slug,"round_slug":round_slug,"username":username}),
    success: function(data){
      if(data.success){
        notySuccess('Atleta Añadido');
        location.reload();
      } else {
        notyError(data.message);
      }

    },
    error: function(jqXHR){
      notyError('Se ha producido un error');
    },
    dataType: 'JSON'
  });
};