
$(function() {
  $('div#mod-competition-category-delete').on('show.bs.modal', function (e) {
    $(this).find("span#competition-category-name").html(e.relatedTarget.getAttribute("data-title"));
    $(this).find("button#delete_competition").attr("data-slug", e.relatedTarget.getAttribute("data-competition"));
    $(this).find("button#delete_competition").attr("data-category-slug", e.relatedTarget.getAttribute("data-category"));

  });
  $('div#mod-competition-category-delete').on('hidden.bs.modal', function (e) {
    $(this).find("div#competition-category-name").html("");
    $(this).find("button#delete_competition").attr("data-slug", "");
    $(this).find("button#delete_competition").attr("data-category-slug", "");
  });
  $("button#delete_competition_category").one("click",function (e) {
        if($(this).attr("data-slug")==""){
          e.preventDefault();
          return;
        }
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'DELETE'},
      method: "POST",
      url: Routing.generate("delete_panel_competition_category",{'slug': $(this).attr("data-slug"),'category-slug':$(this).attr("data-category-slug")}),
      context: $(this).closest("tr"),
      success: function(data){
        if(data.success){
          $('#mod-competition-category-delete').modal('hide');
          $(this).remove();

          noty(
              {
                layout: 'topCenter',
                text: data.message,
                type: 'success',
                timeout: 3000
              }
          );
          $(this).remove();
        } else {
          noty(
              {
                layout: 'topCenter',
                text: data.message,
                type: 'error',
                timeout: 3000
              }
          );
        }

      },
      error: function(jqXHR){
        noty(
            {
              layout: 'topCenter',
              text: 'Se ha producido un error',
              type: 'error',
              timeout: 3000
            }
        );
      },
      dataType: 'JSON'
    });
  });
});