$(function() {
  $('textarea.rw_summernote').summernote({
    height: 300,
    toolbar: [
      ['para', ['style']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']]
    ]
  });
});