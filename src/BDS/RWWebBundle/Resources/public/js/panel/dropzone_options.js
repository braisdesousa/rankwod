Dropzone.autoDiscover = false;
if(document.querySelectorAll("div.user-avatar.dropzone").length>0) {
  var myDropzone = new Dropzone("div.user-avatar.dropzone", {
    maxFilesize: 10, // MB
    url: document.querySelector("div.user-avatar.dropzone").getAttribute("data-action"),
    parrarellUploads: 1,
    addRemoveLinks: false,
    uploadMultiple: false,
    thumbnailWidth: 135,
    createImageThumbnails: true,
    acceptedFiles: "image/*",
    previewTemplate: false,
    method: "POST",
    addedfile: function (file) {
      console.log(file);
    },
    thumbnail: function (eventObject, dataUrl) {
      (document.getElementById('competition-image')).style.backgroundImage = "url('" + dataUrl + "')";
    },
    complete: function (file, b, c, d) {
      var image = JSON.parse(file.xhr.response);
      (document.getElementById('competition-image')).style.backgroundImage = "url('" + image.url + "')";
      myDropzone.removeAllFiles();
    }
  });
}
if(document.querySelectorAll("div.competition-background-image.dropzone").length>0){
  var BackgroundDropzone = new Dropzone("div.competition-background-image.dropzone", {
    maxFilesize: 10, // MB
    url: document.querySelector("div.competition-background-image.dropzone").getAttribute("data-action"),
    parrarellUploads: 1,
    addRemoveLinks: false,
    uploadMultiple: false,
    thumbnailWidth: 900,
    createImageThumbnails: true,
    acceptedFiles: "image/*",
    previewTemplate : false,
    method: "POST",
    addedfile: function (file) {
      console.log(file);
    },
    thumbnail: function (eventObject, dataUrl) {
      (document.getElementById('competition-background-image')).style.backgroundImage = "url('" + dataUrl + "')";
    },
    complete: function (file, b, c, d) {
      var image = JSON.parse(file.xhr.response);
      (document.getElementById('competition-background-image')).style.backgroundImage = "url('" + image.url + "')";
      BackgroundDropzone.removeAllFiles();
    }
  });

}
