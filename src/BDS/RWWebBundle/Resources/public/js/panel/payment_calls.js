function loadOk() {
  var handlebarOptions= {
    "competition": competition,
    "phase": phase,
    "url": Routing.generate('get_competition_phase',{'competition_slug':competition_slug,"phase_slug":phase_slug})

  };
  var content=Handlebars.templates['Panel/Competition/payment/payment_ok'](handlebarOptions);
  document.getElementById("handlebars-container").innerHTML=content;
}
if (is_paid){
  loadOk();
}
function loadKo(){
  var handlebarOptions= {
    "competition": competition,
    "phase": phase,
    "url": Routing.generate('get_competition_phase_payment',{'competition_slug':competition_slug,"phase_slug":phase_slug})
  };
  var content=Handlebars.templates['Panel/Competition/payment/payment_ko'](handlebarOptions);
  document.getElementById("handlebars-container").innerHTML=content;
}
$(function() {
  var stripe = Stripe('pk_test_DnFXjddOGHOueTotpxRmLEdM');
  var elements = stripe.elements();
  var card = elements.create('card', {
    iconStyle: 'solid',
    style: {
      base: {
        iconColor: '#8898AA',
        color: '#353535',
        lineHeight: '36px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '19px',

        '::placeholder': {
          color: '#8898AA',
        },
      },
      invalid: {
        iconColor: '#e85746',
        color: '#e85746',
      }
    },
    classes: {
      focus: 'is-focused',
      empty: 'is-empty',
    },
  });
  card.mount('#card-element');
  var inputs = document.querySelectorAll('input.field');
  Array.prototype.forEach.call(inputs, function(input) {
    input.addEventListener('focus', function() {
      input.classList.add('is-focused');
    });
    input.addEventListener('blur', function() {
      input.classList.remove('is-focused');
    });
    input.addEventListener('keyup', function() {
      if (input.value.length === 0) {
        input.classList.add('is-empty');
      } else {
        input.classList.remove('is-empty');
      }
    });
  });
  function setOutcome(result) {
    var successElement = document.querySelector('.success');
    var errorElement = document.querySelector('.error');
    successElement.classList.remove('visible');
    errorElement.classList.remove('visible');

    if (result.token) {
      // Use the token to create a charge or a customer
      // https://stripe.com/docs/charges
//          successElement.querySelector('.token').textContent = result.token.id;
//          successElement.classList.add('visible');
      $.ajax({
        headers: {'X-HTTP-Method-Override': 'PATCH'},
        method: "POST",
        url: Routing.generate("patch_competition_phase_payment", {
          'phase_slug': $("form.order").attr("data-phase"),
          'competition_slug': $("form.order").attr("data-competition")
        }),
        data: {'token': result.token},
        success: function (data) {
          if (data.success) {
            if(data.paid){
              loadOk();
            } else {
              loadKo();
            }
          } else {
            loadKo();
            notyError(data.message);
          }
        },
        error: function (jqXHR) {
          notyError("Se ha producido un Error");
          loadKo();
        },
        dataType: 'JSON'
      });
    } else if (result.error) {
      errorElement.textContent = result.error.message;
      errorElement.classList.add('visible');

    }
  }
  card.on('change', function(event) {
    setOutcome(event);
  });

  document.querySelector('form').addEventListener('submit', function(e) {
    e.preventDefault();
    var form = document.querySelector('form');
    var extraDetails = {
      name: form.querySelector('input[name=cardholder-name]').value,
    };
    stripe.createToken(card, extraDetails).then(setOutcome);
  });
});