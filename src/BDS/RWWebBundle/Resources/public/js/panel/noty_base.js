function notySuccess(message) {
    new Noty(
        {
            layout: 'topCenter',
            text: message,
            animation: {
                open: 'animated fadeInDown', // Animate.css class names
                close: 'animated fadeOutUp' // Animate.css class names
            },
            type: 'success',
            theme: 'metroui',
            timeout: 1500
        }
    ).show();
}
function notyWarning(message){
    if(message==undefined){
        message="Se ha producido un error";
    }
    new Noty(
        {
            layout: 'topCenter',
            text: message,
            animation: {
                open: 'animated fadeInDown', // Animate.css class names
                close: 'animated fadeOutUp' // Animate.css class names
            },
            type: 'warning',
            theme: 'metroui',
            timeout: 1500
        }
    ).show();
}
function notyError(message){
    if(message==undefined){
        message="Se ha producido un error";
    }
    new Noty(
        {
            layout: 'topCenter',
            text: message,
            animation: {
                open: 'animated fadeInDown', // Animate.css class names
                close: 'animated fadeOutUp' // Animate.css class names
            },
            type: 'error',
            theme: 'metroui',
            timeout: 1500
        }
    ).show();
}
