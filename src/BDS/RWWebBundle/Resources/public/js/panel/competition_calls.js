
$(function() {
  $("a.create-order").one('click',function(){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("panel_competition_phase_create_order", {
        'phase_slug': $(this).attr("data-phase"),
        'competition_slug': $(this).attr("data-competition")
      }),
      success: function (data) {
        if (data.success) {
          notySuccess("Creaccion Correcta");
          window.location.reload();
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.delete-phase-payment").one('click',function(){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'DELETE'},
      method: "POST",
      url: Routing.generate("panel_competition_phase_delete_order", {
        'phase_slug': $(this).attr("data-phase"),
        'competition_slug': $(this).attr("data-competition")
      }),
      success: function (data) {
        if (data.success) {
          notySuccess(data.message);
          $("div.pricing-table").toggle(500);
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.discount-phase-payment").one('click',function(){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("panel_competition_phase_discount_order", {
        'phase_slug': $(this).attr("data-phase"),
        'competition_slug': $(this).attr("data-competition")
      }),
      success: function (data) {
        if (data.success) {
          notySuccess(data.message);
          $("span.value").html(data.amount);
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $('div#mod-competition-delete').on('show.bs.modal', function (e) {
    $(this).find("span#competition-name").html(e.relatedTarget.getAttribute("data-title"));
    $(this).find("button#delete_competition").attr("data-slug", e.relatedTarget.getAttribute("data-slug"));

  });
  $('div#mod-competition-delete').on('hidden.bs.modal', function (e) {
    $(this).find("div#competition-name").html("");
    $(this).find("button#delete_competition").attr("data-slug", "");
  });
  $("button#delete_competition").one("click",function (e) {
        if($(this).attr("data-slug")==""){
          e.preventDefault();
          return;
        }
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'DELETE'},
      method: "POST",
      dataType: 'JSON',
      url: Routing.generate("get_delete_panel_competition",{'slug': $(this).attr("data-slug")}),
      context: $('div.competition[data-slug="'+$(this).attr("data-slug")+'"]'),
      success: function(data){
        if(data.success){
          $('#mod-competition-delete').modal('hide');
          $(this).remove();
          notySuccess('Competición eliminada correctamente');
          $(this).remove();
        } else {
          notyError(data.message);
        }

      },
      error: function(jqXHR){
        notyError('Se ha producido un error');
      }

    });
  });
  $("a.delete-phase").one("click",function (e) {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'DELETE'},
      method: "POST",
      dataType: 'JSON',
      url: Routing.generate("delete_competition_phase",{'competition_slug': $(this).attr("data-competition"),'phase_slug': $(this).attr("data-phase")}),
      context: $(this),
      success: function(data){
        if(data.success){
          window.location.href = Routing.generate("get_panel_competition",{'slug': $(this).attr("data-competition")});
        } else {
          notyError(data.message);
        }

      },
      error: function(jqXHR){
        notyError('Se ha producido un error');
      }

    });
  });
});