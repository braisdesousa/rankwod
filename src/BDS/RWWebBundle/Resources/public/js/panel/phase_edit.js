$(function() {


  if($('form[name="bds_rwcompetitionbundle_phase"]').length){

    var TYPE_ATTENDANCE=Handlebars.templates['Panel/Competition/phase_templates/phase_prices/TYPE_ATTENDANCE']();
    var TYPE_ONLINE=    Handlebars.templates['Panel/Competition/phase_templates/phase_prices/TYPE_ONLINE']();
    var TYPE_INTERNAL=  Handlebars.templates['Panel/Competition/phase_templates/phase_prices/TYPE_INTERNAL']();
    var phase_types={'TYPE_ATTENDANCE':TYPE_ATTENDANCE,'TYPE_ONLINE':TYPE_ONLINE,'TYPE_INTERNAL':TYPE_INTERNAL};
    $("div#hbs-content").html(TYPE_ONLINE);
      $("select#bds_rwcompetitionbundle_phase_phase_type").change(function(){
      $("div#hbs-content").html(phase_types[$(this).val()]);
    });
  }



  if($('div.phase-inline-edit').length){
    $('div.phase-inline-edit input').change(function (){
      var field=$(this).attr("data-field");
      var value=$(this).is(":checked");
      var data={};
      data['value']=value;
      console.log(data);
      updateField(field,data);
    });
    $('div.phase-inline-edit select').change(function (){
      var field=$(this).attr("data-field");
      var value=$(this).val();
      var data={};
      data['value']=value;
      console.log(data);
      updateField(field,data);
    });
  }

  updateField= function(field,data){
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      data: data,
      url: Routing.generate(field, {
        'phase_slug': $('div.phase-inline-edit').attr("data-phase"),
        'competition_slug': $('div.phase-inline-edit').attr("data-competition")
      }),
      success: function (data) {
        if (data.success) {
          console.log("Guardado con éxito");
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  }
});