$(function() {
  $('div#mod-register').on('show.bs.modal', function (e) {
    $(this).find("h4").html(e.relatedTarget.getAttribute("data-title"));
    $(this).find("h3").html(e.relatedTarget.getAttribute("data-title"));
    $(this).find("button#register").attr("data-category", e.relatedTarget.getAttribute("data-category"));
    $(this).find("button#register").attr("data-phase", e.relatedTarget.getAttribute("data-phase"));

  });
  $('div#mod-register').on('hidden.bs.modal', function (e) {
    $(this).find("h3").html("");
    $(this).find("h4").html("");
    $(this).find("button#register").attr("data-category", "");
    $(this).find("button#register").attr("data-phase", "");
  });

  $("button#register").one("click",function (e) {
    if($(this).attr("data-slug")==""){
      e.preventDefault();
      return;
    }
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      dataType: 'JSON',
      url: Routing.generate("panel_competition_athlete_subscribe",{'competition_slug': $(this).attr("data-competition"),'phase_slug': $(this).attr("data-phase"),'category_slug': $(this).attr("data-category")}),
      context: $(this),
      success: function(data){
        if(data.success){
          $('div#mod-register').modal('hide');
          window.location.reload();
        } else {
          notyError(data.message);
        }

      },
      error: function(jqXHR){
        notyError('Se ha producido un error');
      }

    });
  });
});
