/**
 * Created by bra on 25/02/16.
 */
$(document).ready(function() {
    $('[data-toggle="counter"]').each(function (i, e) {
        var _el = $(this);
        var prefix = '';
        var suffix = '';
        var start = 0;
        var end = 0;
        var decimals = 0;
        var duration = 2.5;

        if (_el.data('prefix')) {
            prefix = _el.data('prefix');
        }

        if (_el.data('suffix')) {
            suffix = _el.data('suffix');
        }

        if (_el.data('start')) {
            start = _el.data('start');
        }

        if (_el.data('end')) {
            end = _el.data('end');
        }

        if (_el.data('decimals')) {
            decimals = _el.data('decimals');
        }

        if (_el.data('duration')) {
            duration = _el.data('duration');
        }

        var count = new CountUp(_el.get(0), start, end, decimals, duration, {
            suffix: suffix,
            prefix: prefix,
        });

        count.start();
    });
});