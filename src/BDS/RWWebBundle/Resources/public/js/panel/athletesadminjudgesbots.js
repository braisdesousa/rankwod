$(document).ready(function() {

  /******************** MAILING ************************/
  $("a.reemail-admin").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("panel_competition_admin_re_email_user", {
        'username': $(this).attr("data-username"),
        'slug': $(this).attr("data-competition")
      }),
      context: $(this).closest("tr"),
      success: function (data) {
        if (data.success) {
          notySuccess("Email Enviado");
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.reemail-judge").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("panel_competition_judge_re_email_user", {
        'username': $(this).attr("data-username"),
        'slug': $(this).attr("data-competition")
      }),
      context: $(this).closest("tr"),
      success: function (data) {
        if (data.success) {
          notySuccess("Email Enviado");
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.reemail-athlete").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("get_panel_competition_send_mail_user", {
        'username': $(this).attr("data-username"),
        'phase_slug': $(this).attr("data-phase"),
        'slug': $(this).attr("data-competition")
      }),
      context: $(this).closest("tr"),
      success: function (data) {
        if (data.success) {
          notySuccess("Email Enviado");
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.reemail-all-athlete").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("get_panel_competition_send_mail", {
        'phase_slug': $(this).attr("data-phase"),
        'slug': $(this).attr("data-competition")
      }),
      success: function (data) {
        if (data.success) {
          notySuccess("Email Enviado");
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.reemail-all-admin").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("panel_competition_admin_re_email", {'slug': $(this).attr("data-competition")}),
      success: function (data) {
        if (data.success) {
          notySuccess("Email Enviado");
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.reemail-all-judge").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      url: Routing.generate("panel_competition_judge_re_email", {'slug': $(this).attr("data-competition")}),
      success: function (data) {
        if (data.success) {
          notySuccess("Email Enviado");
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
 /*********** DELETING *****************************/
 $("a.remove-judge").click(function () {
   $.ajax({
     headers: {'X-HTTP-Method-Override': 'DELETE'},
     method: "POST",
     context: $(this).closest("tr"),
     url: Routing.generate("panel_competition_judge_delete_user", {'slug': $(this).attr("data-competition"),"username":$(this).attr("data-username")}),
     success: function (data) {
       if (data.success) {
         notySuccess("Juez Eliminado");
         $(this).remove();
       } else {
         notyError(data.message);
       }
     },
     error: function (jqXHR) {
       notyError("Se ha producido un Error");
     },
     dataType: 'JSON'
   });
 });
 $("a.remove-admin").click(function () {
   $.ajax({
     headers: {'X-HTTP-Method-Override': 'DELETE'},
     method: "POST",
     context: $(this).closest("tr"),
     url: Routing.generate("panel_competition_admin_delete_user", {'slug': $(this).attr("data-competition"),"username":$(this).attr("data-username")}),
     success: function (data) {
       if (data.success) {
         notySuccess("Administrador Eliminado");
         $(this).remove();
       } else {
         notyError(data.message);
       }
     },
     error: function (jqXHR) {
       notyError("Se ha producido un Error");
     },
     dataType: 'JSON'
   });
 });
 $("a.remove-athlete").click(function () {
   $.ajax({
     headers: {'X-HTTP-Method-Override': 'DELETE'},
     method: "POST",
     context: $(this).closest("tr"),
     url: Routing.generate("delete_panel_competition_phase_athlete", {'slug': $(this).attr("data-competition"),'phase_slug': $(this).attr("data-phase"),"athlete_id":$(this).attr("data-athlete_id")}),
     success: function (data) {
       if (data.success) {
         notySuccess("Atleta Eliminado");
         $(this).remove();
       } else {
         notyError(data.message);
       }
     },
     error: function (jqXHR) {
       notyError("Se ha producido un Error");
     },
     dataType: 'JSON'
   });
 });
 /*************** HANDLING NOTIFICATIONS *******/
 $("a.accept-judge").click(function () {
   $.ajax({
     headers: {'X-HTTP-Method-Override': 'PATCH'},
     method: "POST",
     context: $(this).closest("div.competition-decision-alert"),
     url: Routing.generate("panel_competition_judge_accept", {'slug': $(this).attr("data-competition")}),
     success: function (data) {
       if (data.success) {
          notySuccess("Invitacion Aceptada");
         $(this).remove();
         window.location.href=Routing.generate('get_panel_competition',{"slug":data.slug})
       } else {
         notyError(data.message);
       }
     },
     error: function (jqXHR) {
       notyError("Se ha producido un Error");
     },
     dataType: 'JSON'
   });
 });
 $("a.decline-judge").click(function () {
   $.ajax({
     headers: {'X-HTTP-Method-Override': 'PATCH'},
     method: "POST",
     context: $(this).closest("tr"),
     url: Routing.generate("panel_competition_judge_decline", {'slug': $(this).attr("data-competition")}),
     success: function (data) {
       if (data.success) {
         notySuccess("Invitacion Rechazada");
         $(this).remove();
       } else {
         notyError(data.message);
       }
     },
     error: function (jqXHR) {
       notyError("Se ha producido un Error");
     },
     dataType: 'JSON'
   });
 });
 $("a.accept-admin").click(function () {
   $.ajax({
     headers: {'X-HTTP-Method-Override': 'PATCH'},
     method: "POST",
     context: $(this).closest("div.competition-decision-alert"),
     url: Routing.generate("panel_competition_admin_accept", {'slug': $(this).attr("data-competition")}),
     success: function (data) {
       if (data.success) {
          notySuccess("Invitacion Aceptada");
         $(this).remove();
         window.location.href=Routing.generate('get_panel_competition',{"slug":data.slug})
       } else {
         notyError(data.message);
       }
     },
     error: function (jqXHR) {
       notyError("Se ha producido un Error");
     },
     dataType: 'JSON'
   });
 });
 $("a.decline-admin").click(function () {
   $.ajax({
     headers: {'X-HTTP-Method-Override': 'PATCH'},
     method: "POST",
     context: $(this).closest("tr"),
     url: Routing.generate("panel_competition_admin_decline", {'slug': $(this).attr("data-competition")}),
     success: function (data) {
       if (data.success) {
         notySuccess("Invitacion Rechazada");
         $(this).remove();
       } else {
         notyError(data.message);
       }
     },
     error: function (jqXHR) {
       notyError("Se ha producido un Error");
     },
     dataType: 'JSON'
   });
 });
  $("a.accept-athlete").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      context: $(this).closest("div.competition-decision-alert"),
      url: Routing.generate("panel_competition_athlete_accept", {'slug': $(this).attr("data-competition"),"phase_slug":$(this).attr("data-phase")}),
      success: function (data) {
        if (data.success) {
          notySuccess("Invitacion Aceptada");
          $(this).remove();
          window.location.href=Routing.generate('get_panel_competition',{"slug":data.slug})
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.accept-all-pending").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      context: $(this).closest("div.competition-decision-alert"),
      url: Routing.generate("panel_competition_athlete_accept_all", {'slug': $(this).attr("data-competition"),"phase_slug":$(this).attr("data-phase")}),
      success: function (data) {
        if (data.success) {
          notySuccess("Usuarios Aceptados");
          window.location.reload();
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.decline-athlete").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      context: $(this).closest("div.competition-decision-alert"),
      url: Routing.generate("panel_competition_athlete_decline", {'slug': $(this).attr("data-competition"),"phase_slug":$(this).attr("data-phase")}),
      success: function (data) {
        if (data.success) {
          notySuccess("Invitacion Rechazada");
          $(this).remove();
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.admin-accept-athlete").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      context: $(this).closest("td"),
      url: Routing.generate("panel_competition_admin_athlete_accept", {'slug': $(this).attr("data-competition"),"phase_slug":$(this).attr("data-phase"),"athlete_id":$(this).attr("data-username")}),
      success: function (data) {
        if (data.success) {
          notySuccess("Peticion Aceptada");
          $(this).remove();
          window.location.reload();
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
  $("a.admin-decline-athlete").click(function () {
    $.ajax({
      headers: {'X-HTTP-Method-Override': 'PATCH'},
      method: "POST",
      context: $(this).closest("tr"),
      url: Routing.generate("panel_competition_admin_athlete_decline", {'slug': $(this).attr("data-competition"),"phase_slug":$(this).attr("data-phase"),"athlete_id":$(this).attr("data-username")}),
      success: function (data) {
        if (data.success) {
          notySuccess("Invitacion Rechazada");
          $(this).remove();
        } else {
          notyError(data.message);
        }
      },
      error: function (jqXHR) {
        notyError("Se ha producido un Error");
      },
      dataType: 'JSON'
    });
  });
/*********************BOOOOTS ************/
$("a.delete-bot").one('click',function () {
  $.ajax({
    headers: {'X-HTTP-Method-Override': 'DELETE'},
    method: "POST",
    context: $(this).closest("tr"),
    url: Routing.generate("delete_panel_competition_bots", {'competition_slug': $(this).attr("data-competition"),"bot_slug":$(this).attr("data-bot")}),
    success: function (data) {
      if (data.success) {
        notySuccess("Bot Eliminado");
        $(this).remove();
      } else {
        notyError(data.message);
      }
    },
    error: function (jqXHR) {
      notyError("Se ha producido un Error");
    },
    dataType: 'JSON'
  });
});

});
