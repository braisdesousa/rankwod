$(function() {
    $.get( token, function( data ) {
        $form=$('#payment-form');
        $( "#payment-token" ).html( data );
        $('#payment-form').submit(function (e) {
            var $form = $(this);
            // Disable the submit button to prevent repeated clicks
            $form.find('button').prop('disabled', true);

            // This identifies your website in the createToken call below
            Stripe.setPublishableKey(publishable_key);
            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from submitting with the default action
            return false;
        });
        var stripeResponseHandler = function (status, response) {
            var $form = $('#payment-form');

            if (response.error) {
                // Show the errors on the form
                $form.find('.payment-errors').text(response.error.message);
                $form.find('button').prop('disabled', false);
            } else {
                // token contains id, last4, and card type
                var token = response.id;
                // Insert the token into the form so it gets submitted to the server
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                // and re-submit
                $form.ajaxSubmit(
                    {
                        "success":function(data){
                            $('#paymentModal').modal('hide');
                            if(data.success){
                                swal({
                                    title: "¡Bien Hecho!",
                                    text: "El pago se ha registrado de forma satisfactoria.",
                                    type: "success",
                                    showCancelButton: false,
                                }, function () {
                                    window.location.href = Routing.generate("v2_panel_competition",{"slug":competition}).replace("http:","https:")
                                });
                            } else {
                                if(data.details.error.message){
                                    swal({
                                        title: "¡ops!",
                                        text: ("Parece que ha ocurrido un error con el pago. Por favor,  contacta con nuestro servicio técnico o prueba con otra tarjeta." + "|| "+(data.details.error.message)+"  ||"),
                                        type: "error",
                                        showCancelButton: false,
                                    }, function () {
                                        location.reload();
                                    });
                                } else {
                                    swal({
                                        title: "¡ops!",
                                        text: ("Parece que ha ocurrido un error con el pago. Por favor,  contacta con nuestro servicio técnico o prueba con otra tarjeta."),
                                        type: "error",
                                        showCancelButton: false,
                                    }, function () {
                                        location.reload();
                                    });
                                }

                            }

                        },
                        "error":function(e){
                            swal("Oops!", "Parece que ha ocurrido un error con el pago. Por favor,  contacta con nuestro servicio técnico o prueba con otra tarjeta.", "error")
                        }
                    });
            }
        };
    });
});