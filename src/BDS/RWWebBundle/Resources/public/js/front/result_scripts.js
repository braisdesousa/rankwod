;(function($) {
  "use strict";
    $(window).load(function() {
        /*----------------------------- Loader & Parallax --------------------------*/
        $(".loader-item").delay(700).fadeOut();
        $(".mask").delay(800).fadeOut("slow");

    });
})(jQuery);