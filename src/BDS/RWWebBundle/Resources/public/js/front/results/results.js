    var phase=$(".phase-selector.selected").attr("data-phase");
    event_categories_content=Handlebars.templates['Front/Results/event_categories'](getPhaseFromCompetition(competition['phases'],phase));
    $("#opt-test").html(event_categories_content);
    var event=$(".event-selector.selected").attr("data-event");
    var category=$(".category-selector.selected").attr("data-category");
    event_results_content=Handlebars.templates['Front/Results/event_results']({ 'results':results[phase][event][category],'phase_point_system':isPointResult(phase),'box':$("select#box-selector").val()});
    $("#block-results").html(event_results_content);

    var listeners= function() {
        $(".phase-selector").click(function(){
            $(".phase-selector").each(function (){
                $(this).removeClass("selected");
            });
            $(this).addClass("selected");
            phase=$(this).attr("data-phase");
            event_categories_content=Handlebars.templates['Front/Results/event_categories'](getPhaseFromCompetition(competition['phases'],phase));
            $("#opt-test").html(event_categories_content);
            event=$(".event-selector.selected").attr("data-event");
            category=$(".category-selector.selected").attr("data-category");
            event_results_content=Handlebars.templates['Front/Results/event_results']({ 'results':results[phase][event][category],'phase_point_system':isPointResult(phase),'box':$("select#box-selector").val()});
            $("#block-results").html(event_results_content);
            listeners();
        });
        $(".category-selector").click(function(){
            $(".category-selector").each(function (){
                $(this).removeClass("selected");
            });
            $(this).addClass("selected");
        });
        $(".event-selector").click(function(){
            $(".event-selector").each(function (){
                $(this).removeClass("selected");
            });
            $(this).addClass("selected");
        });
        $(".result-selector").click(function(){
            phase=$(".phase-selector.selected").attr("data-phase");
            event= $(".event-selector.selected").attr("data-event");
            category= $(".category-selector.selected").attr("data-category");
            var event_point_system=(event!=="overall")?isPointResult(phase):false;
            event_results_content=Handlebars.templates['Front/Results/event_results']({ 'results':results[phase][event][category],'phase_point_system':event_point_system,'box':$("select#box-selector").val()});
            $("#block-results").html(event_results_content);
        });
        $("select#box-selector").change(function(){
            fase=$(".phase-selector.selected").attr("data-phase");
            event= $(".event-selector.selected").attr("data-event");
            category= $(".category-selector.selected").attr("data-category");
            var event_point_system=(event!=="overall")?isPointResult(phase):false;
            event_results_content=Handlebars.templates['Front/Results/event_results']({ 'results':results[phase][event][category],'phase_point_system':event_point_system,'box':$("select#box-selector").val()});
            $("#block-results").html(event_results_content);
        })
    };
    listeners();
