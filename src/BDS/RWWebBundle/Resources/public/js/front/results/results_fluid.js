for (event in results[phase]){
    for(category in results[phase][event]){

       $(".block-results."+event+"-"+category).html(event_results.render({ 'results':results[phase][event][category],'box':false}));
    }

}
var scrollDown = function () {
  var target = $("#endOfPage");
  if (target.length) {
    $('html, body').animate({
      scrollTop: target.offset().top
    }, 100000,"swing");
  }
};


$(function() {
  scrollDown();
  $(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
      $('html, body').clearQueue();
      $(window).scrollTop(0);
        scrollDown()
    }
  });
});

