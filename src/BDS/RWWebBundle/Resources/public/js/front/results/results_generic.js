var resultpoints = {
    200: [200, 188, 176, 168, 160, 152, 144, 136, 128, 120, 116, 112, 108, 104, 100, 96, 92, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 48, 44, 40, 36, 32, 28, 24, 20, 16, 12, 8, 4, 0],
    100: [100, 94, 88, 84, 80, 76, 72, 68, 64, 60, 58, 56, 54, 52, 50, 48, 46, 44, 42, 40, 38, 36, 34, 32, 30, 28, 26, 24, 22, 20, 18, 16, 14, 13, 10, 8, 6, 4, 2, 0,],
    50: [50, 47, 44, 42, 40, 38, 36, 34, 32, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
};

function getPhaseFromCompetition(competition, phase) {
    for (var i = 0; i < competition.length; i++) {
        if (competition[i].slug == phase) {
            return {'phase': competition[i]};

        }
    }
}

function isPointResult(phaseKey) {
    return (point_system[phaseKey]["point_system"] === 'TYPE_RESULT_POINTS');
}

function getPoints(phaseKey, eventKey, resultKey) {
    resultKey--;
    if (point_system[phaseKey]["point_system"] === 'TYPE_RESULT_POINTS') {
        if(resultKey in resultpoints[point_system[phaseKey]["events"][eventKey]]){
            return resultpoints[point_system[phaseKey]["events"][eventKey]][(resultKey)];

        } else {
            return 0;
        }
    }
    return null;
}

function hasGamesOverall(phases, phase) {
    for (var i = 0; i < phases.length; i++) {
        if (phases[i].slug == phase) {
            return phases[i].deserted_resolution_type == "GAMES";

        }
    }
    return false;
}

function transformResult(result, type, tie_breaker) {

    if (result == 0) {
        return "-";
    }
    var real_result = "";
    if (type == "MEASURE_TYPE_TIME") {
        real_result = moment.utc(result * 1000).format("mm:ss");
    } else {
        real_result = result;
    }
    if ((typeof deprecated === "undefined") || (deprecated === false)) {
        return ((typeof tie_breaker !== "undefined") && (tie_breaker != 0)) ? (real_result + " (" + tie_breaker + ")") : real_result;
    } else {
        return real_result;
    }
}

function getUserOrTeamName(result, users) {
    if (result["athlete_name"]) {
        return result["athlete_name"];
    } else if (result["team_name"]) {
        return result["team_name"];
    } else {
        return users.users[result["user_id"]].name;
    }
}

function getUserIdOrTeamSlug(result) {
    if (result["athlete_slug"]) {
        return result["athlete_slug"];
    } else if (result["team_name"]) {
        return result["team_name"];
    } else {
        return result["user_id"];
    }
}

function assembleResults(results, boxes, users, competition) {
    for (var faseKey in results) {
        var overallGames = hasGamesOverall(competition.phases, faseKey);
        userWithoutAllEvents[faseKey] = [];
        for (var eventKey in results[faseKey]) {
            for (var categoryKey in results[faseKey][eventKey]) {
                var rank = 0;
                for (var resultKey = 0; resultKey < results[faseKey][eventKey][categoryKey].length; resultKey++) {
                    var actualResult = transformResult(results[faseKey][eventKey][categoryKey][resultKey]["result"], results[faseKey][eventKey][categoryKey][resultKey]["type"], results[faseKey][eventKey][categoryKey][resultKey]['tie_breaker']);
                    var lastResult = (resultKey != 0) ? transformResult(results[faseKey][eventKey][categoryKey][resultKey - 1]["result"], results[faseKey][eventKey][categoryKey][resultKey - 1]["type"], results[faseKey][eventKey][categoryKey][resultKey - 1]["tie_breaker"]) : 0;
                    if ((resultKey == 0) || (actualResult !== lastResult)) {
                        rank = resultKey + 1;
                    }
                    var newResult = {
                        "points": getPoints(faseKey, eventKey, rank),
                        "result": transformResult(results[faseKey][eventKey][categoryKey][resultKey]["result"], results[faseKey][eventKey][categoryKey][resultKey]["type"], results[faseKey][eventKey][categoryKey][resultKey]["tie_breaker"]),
                        "user": getUserOrTeamName(results[faseKey][eventKey][categoryKey][resultKey], users),
                        "box": boxes[results[faseKey][eventKey][categoryKey][resultKey]["user_id"]]?boxes[results[faseKey][eventKey][categoryKey][resultKey]["user_id"]]["box_name"]:"",
                        "user_id": getUserIdOrTeamSlug(results[faseKey][eventKey][categoryKey][resultKey]),
                        "rank": (((eventKey == "overall") && (results[faseKey][eventKey][categoryKey][resultKey]["incomplete"] == 1) && (overallGames)) ? rank + "*" : rank)
                    };
                    if ((actualResult == "-") && (userWithoutAllEvents[faseKey].indexOf(newResult["user_id"]) == -1)) {
                        userWithoutAllEvents[faseKey].push(newResult["user_id"]);
                    }
                    results[faseKey][eventKey][categoryKey][resultKey] = newResult;
                }
            }
        }
    }
    return results;
}

function createOverall(no_overall_results, competition) {
    var results = jQuery.extend(true, {}, no_overall_results);
    var unSetedOverallResults = {};
    var continues = [];
    for (var faseKey in results) {
        if (("overall" in results[faseKey]) || (Object.keys(results[faseKey]).length < 2)) {
            continues.push(faseKey);
            continue;
        }
        var overallGames = hasGamesOverall(competition.phases, faseKey);
        results[faseKey]["overall"] = {};
        unSetedOverallResults[faseKey] = {};
        for (var eventKey in results[faseKey]) {
            for (var categoryKey in results[faseKey][eventKey]) {
                if (!(categoryKey in results[faseKey]["overall"])) {
                    results[faseKey]["overall"][categoryKey] = {};
                    unSetedOverallResults[faseKey][categoryKey] = {};
                }
                for (var resultKey = 0; resultKey < results[faseKey][eventKey][categoryKey].length; resultKey++) {
                    if (overallGames && (userWithoutAllEvents[faseKey].indexOf(results[faseKey][eventKey][categoryKey][resultKey]["user_id"]) == -1)) {
                        if (!(results[faseKey][eventKey][categoryKey][resultKey]["user_id"] in results[faseKey]["overall"][categoryKey])) {
                            results[faseKey]["overall"][categoryKey][results[faseKey][eventKey][categoryKey][resultKey]["user_id"]] = {
                                "result": isPointResult(faseKey) ? results[faseKey][eventKey][categoryKey][resultKey]["points"] : results[faseKey][eventKey][categoryKey][resultKey]["rank"],
                                "user": results[faseKey][eventKey][categoryKey][resultKey]["user"],
                                "box": results[faseKey][eventKey][categoryKey][resultKey]["box"],
                                "user_id": results[faseKey][eventKey][categoryKey][resultKey]["user_id"],
                                "rank": 0
                            };
                        } else {
                            results[faseKey]["overall"][categoryKey][results[faseKey][eventKey][categoryKey][resultKey]["user_id"]]["result"] +=
                                isPointResult(faseKey) ?
                                    results[faseKey][eventKey][categoryKey][resultKey]["points"] :
                                    results[faseKey][eventKey][categoryKey][resultKey]["rank"];
                        }
                    } else {
                        if (!(results[faseKey][eventKey][categoryKey][resultKey]["user_id"] in unSetedOverallResults[faseKey][categoryKey])) {
                            unSetedOverallResults[faseKey][categoryKey][results[faseKey][eventKey][categoryKey][resultKey]["user_id"]] = {
                                "result": isPointResult(faseKey) ? results[faseKey][eventKey][categoryKey][resultKey]["points"] : results[faseKey][eventKey][categoryKey][resultKey]["rank"],
                                "user": results[faseKey][eventKey][categoryKey][resultKey]["user"],
                                "box": results[faseKey][eventKey][categoryKey][resultKey]["box"],
                                "user_id": results[faseKey][eventKey][categoryKey][resultKey]["user_id"],
                                "rank": 0,
                            };
                        } else {
                            unSetedOverallResults[faseKey][categoryKey][results[faseKey][eventKey][categoryKey][resultKey]["user_id"]]["result"] +=
                                isPointResult(faseKey) ?
                                    results[faseKey][eventKey][categoryKey][resultKey]["points"] :
                                    results[faseKey][eventKey][categoryKey][resultKey]["rank"];
                        }
                    }

                }
            }
        }
    }
    for (var faseKey in results) {
        if (continues.indexOf(faseKey) == -1) {
            for (var categoryKey in results[faseKey]["overall"]) {
                var array = $.map(results[faseKey]["overall"][categoryKey], function (value, index) {
                    return [value];
                });
                results[faseKey]["overall"][categoryKey] = array;
            }
        }

    }
    for (var faseKey in unSetedOverallResults) {
        if (continues.indexOf(faseKey) == -1) {
            for (var categoryKey in unSetedOverallResults[faseKey]) {
                var array = $.map(unSetedOverallResults[faseKey][categoryKey], function (value, index) {
                    return [value];
                });
                unSetedOverallResults[faseKey][categoryKey] = array;
            }
        }
    }
    for (var faseKey in results) {
        if (continues.indexOf(faseKey) == -1) {
            for (var categoryKey in results[faseKey]["overall"]) {
                results[faseKey]["overall"][categoryKey].sort(function (a, b) {
                    return parseFloat(a.result) - parseFloat(b.result);
                });
                if (isPointResult(faseKey)) {
                    results[faseKey]["overall"][categoryKey].reverse();
                }
            }

        }
    }
    for (var faseKey in unSetedOverallResults) {
        if (continues.indexOf(faseKey) == -1) {
            for (var categoryKey in unSetedOverallResults[faseKey]) {
                unSetedOverallResults[faseKey][categoryKey].sort(function (a, b) {
                    return parseFloat(a.result) - parseFloat(b.result);
                });
                if (isPointResult(faseKey)) {
                    unSetedOverallResults[faseKey][categoryKey].reverse();
                }
            }
        }
    }

    for (var faseKey in results) {
        if (continues.indexOf(faseKey) == -1) {
            for (var categoryKey in results[faseKey]["overall"]) {
                if (((unSetedOverallResults[faseKey] !== undefined) && (unSetedOverallResults[faseKey][categoryKey] !== undefined))) {
                    results[faseKey]["overall"][categoryKey] = results[faseKey]["overall"][categoryKey].concat(unSetedOverallResults[faseKey][categoryKey]);
                }

            }
        }
    }
    for (var faseKey in results) {
        if (continues.indexOf(faseKey) == -1) {
            for (var eventKey in results[faseKey]) {
                for (var categoryKey in results[faseKey][eventKey]) {
                    var rank = 0;
                    for (var resultKey = 0; resultKey < results[faseKey]["overall"][categoryKey].length; resultKey++) {
                        if ((resultKey == 0) || (results[faseKey]["overall"][categoryKey][resultKey]["result"] !== results[faseKey]["overall"][categoryKey][resultKey - 1]["result"])) {
                            rank = resultKey + 1;
                        }
                        var desertedResults = (userWithoutAllEvents[faseKey].indexOf(results[faseKey]["overall"][categoryKey][resultKey]['user_id']) !== -1);
                        results[faseKey]["overall"][categoryKey][resultKey]["rank"] = (overallGames && desertedResults) ? ("*" + rank) : rank;

                    }
                }
            }
        }
    }
    return results;
}

var test = function () {
    var test_results = jQuery.extend(true, {}, results);
    test_results.remove("wod-1");
};
var userWithoutAllEvents = [];
var eventResults = assembleResults(pre_compiled_results["results"], pre_compiled_results["box"], pre_compiled_results["user"], competition);
var originalResults = jQuery.extend(true, {}, eventResults);
var results = createOverall(eventResults, competition);