(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Front/Results/event_categories'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda, alias3=container.escapeExpression;

  return ((stack1 = helpers["if"].call(alias1,(data && data.first),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        <li><a class=\""
    + ((stack1 = helpers["if"].call(alias1,(data && data.first),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " event-selector result-selector\" data-event=\""
    + alias3(alias2((depth0 != null ? depth0.slug : depth0), depth0))
    + "\">"
    + alias3(alias2((depth0 != null ? depth0.name : depth0), depth0))
    + "</a></li>\n"
    + ((stack1 = helpers["if"].call(alias1,(data && data.last),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"clearfix\">\n        <ul class=\"clearfix options\">\n";
},"4":function(container,depth0,helpers,partials,data) {
    return " selected ";
},"6":function(container,depth0,helpers,partials,data) {
    return "        </ul>\n        </div>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda, alias3=container.escapeExpression;

  return ((stack1 = helpers["if"].call(alias1,(data && data.first),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        <li><a class=\""
    + ((stack1 = helpers["if"].call(alias1,(data && data.first),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " category-selector result-selector\" data-category=\""
    + alias3(alias2((depth0 != null ? depth0.slug : depth0), depth0))
    + "\">"
    + alias3(alias2((depth0 != null ? depth0.name : depth0), depth0))
    + "</a></li>\n"
    + ((stack1 = helpers["if"].call(alias1,(data && data.last),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return container.escapeExpression(helpers.log.call(alias1,depth0,{"name":"log","hash":{},"data":data}))
    + "\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.phase : depth0)) != null ? stack1.events : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "<div class=\"section-title\">\n    <span class=\"border\"></span>\n</div>\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.phase : depth0)) != null ? stack1.categories : stack1),{"name":"each","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['Front/Results/event_results'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.should_show_result || (depth0 && depth0.should_show_result) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.box : depth0),(depths[1] != null ? depths[1].box : depths[1]),{"name":"should_show_result","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "            <li>\n                <div class=\"row rank-row\">\n                    <div class=\"col-lg-2 col-xs-2\">\n                        <span class=\"rank-number\">"
    + alias2(alias1((depth0 != null ? depth0.rank : depth0), depth0))
    + "</span>\n                    </div>\n                    <div class=\"col-lg-6 col-xs-6\">\n                        <div class=\"row\">\n                            <div class=\"col-lg-12 col-xs-12\"><span class=\"rank-name\">"
    + alias2(alias1((depth0 != null ? depth0.user : depth0), depth0))
    + "</span></div>\n                            <div class=\"col-lg-12 col-xs-12\"><span class=\"rank-box\">"
    + alias2(alias1((depth0 != null ? depth0.box : depth0), depth0))
    + "</span></div>\n                        </div>\n                    </div>\n                    <div class=\"col-lg-4 col-xs-4\">\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? depths[1].phase_point_system : depths[1]),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "\n\n                    </div>\n\n                </div>\n            </li>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "                        <span class=\"rank-result\">"
    + alias2(alias1((depth0 != null ? depth0.points : depth0), depth0))
    + "</span>\n                        <span class=\"small-result\">"
    + alias2(alias1((depth0 != null ? depth0.result : depth0), depth0))
    + "</span>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "                        <span class=\"rank-result\">"
    + container.escapeExpression(container.lambda((depth0 != null ? depth0.result : depth0), depth0))
    + "</span>\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "        <li>\n            <div class=\"row rank-row\">\n                <div class=\"col-lg-2 col-xs-2\">\n                    <span class=\"rank-number\">0</span>\n                </div>\n                <div class=\"col-lg-6 col-xs-6\">\n                    <div class=\"row\">\n                        <div class=\"col-lg-12 col-xs-12\"><span class=\"rank-name\">Aún no hay Resultados</span></div>\n                        <div class=\"col-lg-12 col-xs-12\"><span class=\"rank-box\"></span></div>\n                    </div>\n                </div>\n                <div class=\"col-lg-4 col-xs-4\">\n                    <span class=\"rank-result\">0</span>\n                </div>\n            </div>\n        </li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<ul class=\"list-unstyled\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.results : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(7, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</ul>";
},"useData":true,"useDepths":true});
templates['v2/handlebars/change_category'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <input value=\""
    + alias4(((helper = (helper = helpers.key || (data && data.key)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" id=\""
    + alias4(((helper = (helper = helpers.key || (data && data.key)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "\" name=\"category\" type=\"radio\" "
    + ((stack1 = helpers["if"].call(alias1,(helpers.equal || (depth0 && depth0.equal) || alias2).call(alias1,depth0,(depths[1] != null ? depths[1].category : depths[1]),{"name":"equal","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n        <label for=\""
    + alias4(((helper = (helper = helpers.key || (data && data.key)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(container.lambda(depth0, depth0))
    + "</label>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " checked=\"checked\" ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<div class=\"demo-radio-button\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.categories : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true,"useDepths":true});
templates['Panel/Competition/payment/payment_ko'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"col-sm-offset-1 col-sm-10\">\n    <div class=\"panel panel-alt1\">\n        <div class=\"panel-heading\">\n            <span class=\"title\">Pago "
    + alias4(((helper = (helper = helpers.competition || (depth0 != null ? depth0.competition : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"competition","hash":{},"data":data}) : helper)))
    + " - "
    + alias4(((helper = (helper = helpers.phase || (depth0 != null ? depth0.phase : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"phase","hash":{},"data":data}) : helper)))
    + " <strong>ERROR</strong></span>\n        </div>\n        <div class=\"panel-body text-center\">\n            <div class=\"row\">\n                <div class=\"col-sm-12\">\n                    <i class=\"fa fa-4x fa-times-circle-o color-alt1\"></i>\n                </div>\n                <div class=\"col-sm-12\">\n                    <h3>Pago No Realizado</h3>\n                    <p>El pago no ha podido procesarse correctamente, puedes volver a intentarlo o contactar con nuestro servicio técnico en contacto@raknwod.com </p>\n                    <a href=\""
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-primary\"> Pagar </a>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['Panel/Competition/payment/payment_ok'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"col-sm-offset-1 col-sm-10\">\n    <div class=\"panel panel-alt3\">\n        <div class=\"panel-heading\">\n            <span class=\"title\">Pago "
    + alias4(((helper = (helper = helpers.competition || (depth0 != null ? depth0.competition : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"competition","hash":{},"data":data}) : helper)))
    + " - "
    + alias4(((helper = (helper = helpers.phase || (depth0 != null ? depth0.phase : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"phase","hash":{},"data":data}) : helper)))
    + " <strong>COMPLETADO</strong></span>\n        </div>\n        <div class=\"panel-body text-center\">\n            <div class=\"row\">\n                <div class=\"col-sm-12\">\n                    <i class=\"fa fa-4x fa-check-circle-o color-alt3\"></i>\n                </div>\n                <div class=\"col-sm-12\">\n                    <h3>Pago Realizado Correctamente</h3>\n                    <p>El pago se ha procesado correctamente, ya puedes publicar tu competición</p>\n                    <a href=\""
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn-primary\"> Continuar </a>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
templates['Panel/Competition/phase_templates/phase_prices/TYPE_ATTENDANCE'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"panel-divider panel-divider-lg\"></div>\n<div class=\"pricing-table pricing-table-alt2\">\n    <div class=\"pricing-table-title\">Fase Presencial</div>\n    <div class=\"pricing-table-price\"><span class=\"value\">199</span><span class=\"frecuency\">€</span></div>\n    <div class=\"pricing-table-description\">Donde se encuentran los heroes</div>\n</div>";
},"useData":true});
templates['Panel/Competition/phase_templates/phase_prices/TYPE_INTERNAL'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"panel-divider panel-divider-lg\"></div>\n<div class=\"pricing-table pricing-table-alt3\">\n    <div class=\"pricing-table-title\">Competición Interna</div>\n    <div class=\"pricing-table-price\"><span class=\"value\">99</span><span class=\"frecuency\">€</span></div>\n    <div class=\"pricing-table-description\">Disfruta en Tu Box!</div>\n</div>";
},"useData":true});
templates['Panel/Competition/phase_templates/phase_prices/TYPE_ONLINE'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"panel-divider panel-divider-lg\"></div>\n<div class=\"pricing-table pricing-table-primary\">\n    <div class=\"pricing-table-title\">Fase Online</div>\n    <div class=\"pricing-table-price\"><span class=\"value\">199</span><span class=\"frecuency\">€</span></div>\n    <div class=\"pricing-table-description\">Crea tu clasificatorio online</div>\n</div>";
},"useData":true});
})();