$(function() {
    $("button.admin-delete").click(function () {
        var admin_info = {
            'slug': $(this).data("competition"),
            'username': $(this).data("user")
        };
        swal({
            title: "¿Estas Seguro?",
            text: "Vas a eliminar a este administrador de la competición",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Borrar",
            cancelButtonText: "Cancelar",
        }).then((result)=>{
            if(result.value){
            $.ajax({
                url: Routing.generate('v2_panel_admins_delete', admin_info).replace("http:","https:"),
                dataType: 'json',
                method: "POST",
                headers: {'X-HTTP-Method-Override': 'DELETE'},
                success: function (result) {
                    swal("Borrado!", "Administrador Borrado", "success");
                    location.reload();
                }
            });
        }});
    });
    $("button.admin-accept").click(function () {
        var admin_info = {
            'slug': $(this).closest("table").data("competition"),
            'username': $(this).data("user")
        };
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_admins_patch_accept", admin_info).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.url) {
                        window.location = data.url;
                    }
                    notySuccess(data.message);
                } else {
                    notyError(data.message);
                }

            },
            error: function (jqXHR) {
                notyError('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
    $("button.admin-decline").click(function () {
        var admin_info = {
            'slug': $(this).closest("table").data("competition"),
            'username': $(this).data("user")
        };
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_admins_patch_decline", admin_info).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.url) {
                        window.location = data.url;
                    } else {
                        notySuccess(data.message);
                    }
                } else {
                    notyError(data.message);
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
    $("button.admin-re-email").click(function () {
        var admin_info = {
            'slug': $(this).data("competition"),
            'username': $(this).data("user")
        };
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_admins_patch_re_email_user", admin_info).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.url) {
                        window.location = data.url;
                    } else {
                        notySuccess("Mensaje enviado");
                    }
                } else {
                    notyError(data.message);
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
    $("button.admin-re-email-all").click(function () {
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_admins_patch_re_email", {'slug': $("table.table").data("competition")}).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.url) {
                        window.location = data.url;
                    } else {
                        notySuccess(data.message);
                    }
                } else {
                    notyError(data.message);
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
});
