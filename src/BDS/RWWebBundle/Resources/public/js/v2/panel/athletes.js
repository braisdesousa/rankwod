$(function() {
        $("button.reemail-athlete").click(function () {
            var event_info={
                'slug'      : $(this).closest("table").data("competition"),
                'id': $(this).data("athlete")
            };
            $.ajax({
                url: Routing.generate('v2_panel_athletes_patch_athlete_email',event_info).replace("http:","https:"),
                dataType: 'json',
                method:"POST",
                headers: {'X-HTTP-Method-Override': 'PATCH'},
                success: function (result) {
                    if(result.success){
                        notySuccess("Email enviado con exito");
                    } else {
                        notyError("Se ha producido un error en el envio del email");
                    }
                }
            });
        });
        $("button.invite-athlete").click(function () {
            var event_info={
                'slug'      : $(this).closest("table").data("competition"),
                'id': $(this).data("athlete")
            };
            $.ajax({
                url: Routing.generate('v2_panel_athletes_patch_athlete_invite',event_info).replace("http:","https:"),
                dataType: 'json',
                method:"POST",
                headers: {'X-HTTP-Method-Override': 'PATCH'},
                success: function (result) {
                    if(result.success){
                        notySuccess("Equipo Invitado");
                    } else {
                        notyError("Se ha producido un error al invitar al equipo");
                    }
                }
            });
        });
        $("button.change-category").click(function () {
            var event_info={
                    'slug'      : $(this).closest("table").data("competition"),
                    'id': $(this).data("athlete")
            };
            swal({
                title: "Cambio de Categoría para '"+$(this).data("athlete-name")+"'",
                type: "warning",
                html: (Handlebars.templates['v2/handlebars/change_category']({ 'categories':categories[$(this).data("phase")],'category':$(this).data('category')})),
                showCancelButton: true,
                confirmButtonColor: "#dd9800",
                confirmButtonText: "Si, Cambiar",
                cancelButtonText: "Cancelar",
            }).then((result)=>{
                if(result.value){
                    var category=$('input[name=category]:checked').val();
                    $.ajax({
                        url: Routing.generate('v2_panel_athletes_patch_category',event_info).replace("http:","https:"),
                        dataType: 'json',
                        data:{"category":category},
                        method:"POST",
                        headers: {'X-HTTP-Method-Override': 'PATCH'},
                        success: function (result) {

                            if(result.success){
                                swal("Cambiado!", "Categoría cambiada correctamente", "success");
                            } else {
                                swal("Error!", "Se ha producido un error", "error");
                            }
                        }
                    });
                }
            });
        });
        $("button.delete-athlete").click(function () {
            var event_info={
                    'slug'      : $(this).closest("table").data("competition"),
                    'id': $(this).data("athlete")
            };
            var name=$(this).data("athlete-name");
            var row=$(this).closest("tr");
            swal({
                title: "Eliminar a '"+$(this).data("athlete-name")+"'",
                type: "warning",
                text: "Vas a eliminar '"+name+"' de la competición, ¿estás seguro? ",
                showCancelButton: true,
                confirmButtonText: "Si, Eliminar",
                cancelButtonText: "Cancelar",
                confirmButtonClass:"bg-red",
            }).then((result)=> {
                if(result.value){
                    $.ajax({
                        url: Routing.generate('v2_panel_athletes_delete',event_info).replace("http:","https:"),
                        dataType: 'json',
                        method:"POST",
                        context: row,
                        headers: {'X-HTTP-Method-Override': 'DELETE'},
                        success: function (result) {

                            $(row).remove();
                            if(result.success){
                                swal("Eliminado!", "El atleta ya no forma parte de la competición", "success");
                            } else {
                                swal("Error!", "Se ha producido un error", "error");
                            }
                        }
                    });
                }

            });
        });
        $("button.show-team").click(function () {
            var event_info={
                    'slug'      : $(this).closest("table").data("competition"),
                    'team_slug': $(this).data("team")
            };
            var name=$(this).data("athlete-name");
            var row=$(this).closest("tr");
            $.ajax({
                url: Routing.generate('v2_panel_athletes_get_athlete_team',event_info).replace("http:","https:"),
                dataType: 'json',
                method:"GET",
                context: row,
                success: function (result) {
                    if(result.success){
                        swal({
                            title: "Equipo "+name,
                            type: "info",
                            html: result.html,
                            showCancelButton: false,
                            confirmButtonText: "Aceptar",
                        }
                        );
                    } else {
                        swal("Error!", "Se ha producido un error", "error");
                    }
                }
            });
        });
});