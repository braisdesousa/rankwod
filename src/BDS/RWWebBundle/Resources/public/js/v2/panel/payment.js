$(function () {
    $('#paymentModal').on('hide.bs.modal', function (e) {
        $("div.modal-body").html('<div class="preloader pl-size-xxl">\n' +
            '                    <div class="spinner-layer">\n' +
            '                        <div class="circle-clipper left">\n' +
            '                            <div class="circle"></div>\n' +
            '                        </div>\n' +
            '                        <div class="circle-clipper right">\n' +
            '                            <div class="circle"></div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>');
    });
    $('#paymentModal').on('show.bs.modal', function (e) {
        $.get(Routing.generate('v2_panel_pay', {
            "slug": $("a#payment-link").data('competition'),
            "phase_slug": $("a#payment-link").data('phase')
        }).replace("http:","https:"), function (data) {
            $("div.modal-body").html(data);
            $("div.modal-body").find("strong#phase-price").html($("a#payment-link").data('price'));
            $('#payment-form').submit(function (e) {
                var $form = $(this);
                // Disable the submit button to prevent repeated clicks
                $form.find('button').prop('disabled', true);

                // This identifies your website in the createToken call below
                Stripe.setPublishableKey(publishable_key);
                Stripe.card.createToken($form, stripeResponseHandler);

                // Prevent the form from submitting with the default action
                return false;
            });
            var stripeResponseHandler = function (status, response) {
                var $form = $('#payment-form');

                if (response.error) {
                    // Show the errors on the form
                    $form.find('.payment-errors').text(response.error.message);
                    $form.find('button').prop('disabled', false);
                } else {
                    // token contains id, last4, and card type
                    var token = response.id;
                    // Insert the token into the form so it gets submitted to the server
                    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                    // and re-submit
                    $form.ajaxSubmit({
                            "success":function(data){
                                $('#paymentModal').modal('hide');
                                if(data.success){
                                    swal({
                                        title: "¡Bien Hecho!",
                                        text: "El pago se ha registrado de forma satisfactoria. Ya puedes publicar esta fase",
                                        type: "success",
                                        showCancelButton: false,
                                    }, function () {
                                        location.reload();
                                    });
                                } else {
                                    if(data.details.error.message){
                                        swal({
                                            title: "¡ops!",
                                            text: ("Parece que ha ocurrido un error con el pago. Por favor,  contacta con nuestro servicio técnico o prueba con otra tarjeta." + "|| "+(data.details.error.message)+"  ||"),
                                            type: "error",
                                            showCancelButton: false,
                                        }, function () {
                                            location.reload();
                                        });
                                    } else {
                                        swal({
                                            title: "¡ops!",
                                            text: ("Parece que ha ocurrido un error con el pago. Por favor,  contacta con nuestro servicio técnico o prueba con otra tarjeta."),
                                            type: "error",
                                            showCancelButton: false,
                                        }, function () {
                                            location.reload();
                                        });
                                    }

                                }

                            },
                            "error":function(e){
                                swal("Oops!", "Parece que ha ocurrido un error con el pago. Por favor,  contacta con nuestro servicio técnico o prueba con otra tarjeta.", "error")
                            }
                        });
                }
            };
        });
    });
    $(".unpay").click(function(){
        var route_data={
            "slug":$(this).data("competition"),
            "phase_slug":$(this).data("phase"),
        };
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_pay_patch_remove", route_data).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                        notySuccess("Pago eliminado correctamente");
                } else {
                        notyError("Ha ocurrido un error al realizar el pago");
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });

    });

});