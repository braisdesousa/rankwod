$(function() {

    $("button.btn_add_athletes").click(function(e){
        $("form[name='"+$(this).attr('form')+"']").valid({
            highlight: function (input) {
                console.log("HighLight",$(input).parents('.form-line').length);
                $(input).parents('.form-line').addClass('error');
                },
            unhighlight: function (input) {
                console.log("UnHighLight",$(input).parents('.form-line').length);
                $(input).parents('.form-line').removeClass('error');},
            errorPlacement: function (error, element) {
                console.log("errorPlacement",$(input).parents('.form-group').length);
                $(element).parents('.form-group').append(error);},
            submitHandler: function(form){
                    var data = $('form.add_athletes').serializeArray().reduce(function(obj, item) {
                        obj[item.name] = item.value;
                        return obj;
                    });
                    console.log(data);
                },
            invalidHandler: function(){
                        var errors = validator.numberOfInvalids();
                        console.log("Invalid Handler");
                        if (errors) {
                            var message = errors == 1
                                ? 'You missed 1 field. It has been highlighted'
                                : 'You missed ' + errors + ' fields. They have been highlighted';
                            alert(message);
                    }
                }
        });
   });
});