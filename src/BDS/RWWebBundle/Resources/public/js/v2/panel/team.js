$(function() {
        $("button.delete-team-athlete").click(function () {
            var event_info={
                    'slug'      : $(this).data("competition"),
                    'id': $(this).data("athlete")
            };
            var name=$(this).data("athlete-name");
            var row=$(this).closest("tr");
            swal({
                title: "Eliminar a '"+$(this).data("athlete-name")+"' del equipo",
                type: "warning",
                text: "Vas a echar '"+name+"' del equipo, ¿estás seguro? ",
                showCancelButton: true,
                confirmButtonText: "Si, echar",
                cancelButtonText: "Cancelar",
                confirmButtonClass:"bg-red",
            }).then((result)=> {
                if(result.value){
                    $.ajax({
                        url: Routing.generate('v2_panel_team_athlete_delete',event_info).replace("http:","https:"),
                        dataType: 'json',
                        method:"POST",
                        context: row,
                        headers: {'X-HTTP-Method-Override': 'DELETE'},
                        success: function (result) {

                            $(row).remove();
                            if(result.success){
                                swal("Eliminado!", "El atleta ya no forma parte del equipo", "success");
                            } else {
                                swal("Error!", "Se ha producido un error", "error");
                            }
                        }
                    });
                }

            });
        });
        $("button.add-team-athlete").click(function () {
            var event_info={
                    'slug'      : $(this).data("competition"),
                    'team_slug'      : $(this).data("team")

            };
            var data={'email': $("input#add-team-athlete").val()};
            $.ajax({
                url: Routing.generate('v2_panel_team_athlete_add',event_info).replace("http:","https:"),
                dataType: 'json',
                method:"POST",
                data: data,
                headers: {'X-HTTP-Method-Override': 'PATCH'},
                success: function (result) {

                    if(result.success){
                        swal("Atleta Añadido!", "El atleta ya forma parte del equipo", "success").
                        then((result)=> {
                            if(result.value){
                                location.reload(true);
                            }
                        })
                    } else {
                        swal("Error!", result.message, "error");
                    }
                }
            });

        });
});