$(function() {
    $("button.event-result-edit").click(function () {
            var event_info={
                    'slug'      : $("div#event").data("competition"),
                    'phase_slug': $("div#event").data("phase"),
                    'event_slug': $("div#event").data("event"),
                    'athlete_id': $(this).data("athlete")
            };

            $.ajax({
                url: Routing.generate('v2_panel_event_result_get_judge_form',event_info).replace("http:","https:"),
                dataType: 'json',
                success: function (result) {
                    $("div#modal-container").html(result.html);
                    $("div#modal-container").find("select").selectpicker();
                    $("div#result-modal").modal('show');
                    $("div#modal-container").find("button[type='submit']").click(function(){
                        $("div#modal-container").find("form").ajaxSubmit({
                            success: function(){location.reload();}
                        });
                    });
                }
            });

        });
    $("button.event-result-validate").click(function () {
            var event_info={
                    'slug'      : $("div#event").data("competition"),
                    'phase_slug': $("div#event").data("phase"),
                    'event_slug': $("div#event").data("event"),
                    'athlete_id': $(this).data("athlete")
            };

            $.ajax({
                url: Routing.generate('v2_panel_event_result_patch_validate',event_info).replace("http:","https:"),
                method:"POST",
                headers: {'X-HTTP-Method-Override': 'PATCH'},
                dataType: 'json',
                success: function (result) {
                            location.reload();
                }
            });

        });
    $("button.event-result-judge-edit").click(function () {
        var event_info={
            'slug'      : $(this).data("competition"),
            'phase_slug': $(this).data("phase"),
            'event_slug': $(this).data("event"),
            'athlete_id': $(this).data("athlete")
        };

        $.ajax({
            url: Routing.generate('v2_panel_event_result_get_judge_form',event_info).replace("http:","https:"),
            dataType: 'json',
            success: function (result) {
                $("div#modal-container").html(result.html);
                $("div#modal-container").find("select").selectpicker();
                $("div#result-modal").modal('show');
                $("div#modal-container").find("button[type='submit']").click(function(){
                    $("div#modal-container").find("form").ajaxSubmit({
                        success: function(){location.reload();}
                    });
                });
            }
        });

    });
    $("button.event-result-judge-validate").click(function () {
        var event_info={
            'slug'      : $(this).data("competition"),
            'phase_slug': $(this).data("phase"),
            'event_slug': $(this).data("event"),
            'athlete_id': $(this).data("athlete")
        };

        $.ajax({
            url: Routing.generate('v2_panel_event_result_patch_validate',event_info).replace("http:","https:"),
            method:"POST",
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            dataType: 'json',
            success: function (result) {
                location.reload();
            }
        });

    });
    $("button.event-result-delete").click(function () {
            var event_info={
                    'slug'      : $("div#event").data("competition"),
                    'phase_slug': $("div#event").data("phase"),
                    'event_slug': $("div#event").data("event"),
                    'athlete_id': $(this).data("athlete")
            };
            swal({
                title: "¿Estas Seguro?",
                text: "Vas a eliminar el resultado, no podras recuperarlo",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass:"bg-red",
                confirmButtonText: "Si, Borrar",
                cancelButtonText: "Cancelar",
            }).then((result)=>{
                if(result.value){
                    $.ajax({
                        url: Routing.generate('v2_panel_event_result_delete',event_info).replace("http:","https:"),
                        dataType: 'json',
                        method:"POST",
                        headers: {'X-HTTP-Method-Override': 'DELETE'},
                        success: function (result) {
                            $("div#modal-container").find("form").ajaxForm({
                                    success: function(){

                                        swal("Borrado!", "Resultado Borrado", "success");
                                        location.reload();
                                    }
                                }
                            );
                        }
                    });
                }
            });
        });
});