$(function() {
    $("button.heat-delete").click(function () {
        var heat_info={
            'slug'      : $("table.dataTable").data("competition"),
            'phase_slug': $(this).data("phase"),
            'heat_slug': $(this).data("heat")
        };
        var element=$(this);
        swal({
            title: "¿Estas Seguro?",
            text: "Vas a eliminar esta tanda",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Borrar",
            cancelButtonText: "Cancelar",
            confirmButtonClass:"bg-red",
        }).then((result)=>{
            if(result.value){
                $.ajax({
                    url: Routing.generate('v2_panel_heat_delete',heat_info).replace("http:","https:"),
                    dataType: 'json',
                    context: element,
                    method:"POST",
                    headers: {'X-HTTP-Method-Override': 'DELETE'},
                    success: function (result) {
                        $(this).closest("tr").remove();

                        notySuccess("Ronda Eliminada");
                    }
                });
            }
            });
            })
});