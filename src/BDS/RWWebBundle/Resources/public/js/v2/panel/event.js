$(function() {
        $("button.event-delete").click(function () {
            var event_info={
                    'slug'      : $(this).closest("table").data("competition"),
                    'phase_slug': $(this).closest("table").data("phase"),
                    'event_slug': $(this).data("event")
            };
            swal({
                title: "¿Estas Seguro?",
                text: "Vas a eliminar el evento y los resultados asociados, no podras recuperarlo",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Si, Borrar",
                cancelButtonText: "Cancelar",
                confirmButtonClass:"bg-red"
            }).then((result)=>{
                if(result.value){
                    $.ajax({
                        url: Routing.generate('v2_panel_workout_delete',event_info).replace("http:","https:"),
                        dataType: 'json',
                        method:"POST",
                        headers: {'X-HTTP-Method-Override': 'DELETE'},
                        success: function (result) {
                            swal("Borrado!", "Evento Borrado", "success");
                            location.reload();
                        }
                    });
                }

            });
        });
        $("span.show-result-selector").click(function(){
                $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_workout_patch_result_visibility",{'slug': $("table.table").data("competition"),'phase_slug': $(this).data("phase"),'event_slug':$(this).data("event")}).replace("http:","https:"),
            success: function(data){
                if(data.success){
                    if(data.url){
                        window.location=data.url;
                    }
                    notySuccess(data.message);
                } else {
                    notyError(data.message);
                }

            },
            error: function(jqXHR){
                notyError('Se ha producido un error');
            },
            dataType: 'JSON'
        });
        });
        $("span.publish-event-selector").click(function(){
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_workout_patch_event_visibility",{'slug': $("table.table").data("competition"),'phase_slug': $(this).data("phase"),'event_slug':$(this).data("event")}).replace("http:","https:"),
            data: {"visible": ($(this).is(':checked')?1:0)},
            success: function(data){
                if(data.success){
                    if(data.url){
                        window.location=data.url;
                    } else {
                        notySuccess(data.message);
                    }
                } else {
                    notyError(data.message);
                }
            },
            error: function(jqXHR){
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
        
        $("button.overall-delete").click(function () {
            var overall_info={
                    'slug'      : $(this).closest("table").data("competition"),
                    'phase_slug': $(this).closest("table").data("phase")
            };
            swal({
                title: "¿Estas Seguro?",
                text: "Vas a eliminar la clasificacion final",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass:"bg-red",
                confirmButtonText: "Si, Borrar",
                cancelButtonText: "Cancelar",
            }).then((result)=>{
                if(result.value){
                    $.ajax({
                        url: Routing.generate('v2_panel_overall_delete',overall_info).replace("http:","https:"),
                        dataType: 'json',
                        method:"POST",
                        headers: {'X-HTTP-Method-Override': 'DELETE'},
                        success: function (result) {
                            swal("Borrado!", "Clasificación eliminada", "success");
                            location.reload();
                        }
                    });
                }

            });
        });

});