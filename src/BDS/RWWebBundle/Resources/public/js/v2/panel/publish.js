$(function () {
    $("input.publish-phase").change(function(){
        var route_value={
            "slug":$(this).data('competition'),
            "phase_slug":$(this).data('phase')
        };
        var value=$(this).is(":checked");
        $.ajax({
            url: Routing.generate('v2_panel_config_patch_phase_publish',route_value).replace("http:","https:"),
            method:"POST",
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            data: {"status":value},
            dataType: 'json',
            success: function (result) {
                if(result.success){
                    notySuccess(result.message);
                } else {
                    if(result.message){
                        notyWarning(result.message);
                    } else {
                        notyError("Se ha producido un error");
                    }

                }
            }
        });
    });
    $("input.publish-competition").change(function(){
        var route_value={"slug":$(this).data('competition')};
        var value=$(this).is(":checked");
        $.ajax({
            url: Routing.generate('v2_panel_config_patch_competition_publish',route_value).replace("http:","https:"),
            method:"POST",
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            data: {"status":value},
            dataType: 'json',
            success: function (result) {
                if(result.success){
                    notySuccess(result.message)
                } else {
                    notyError("Se ha producido un error")
                }
            }
        });
    });
    $("button.delete-phase").click(function(){
        var route_value={
            "slug":$(this).data('competition'),
            "phase_slug":$(this).data('phase')
        };
        $.ajax({
            url: Routing.generate('v2_panel_config_delete_phase',route_value).replace("http:","https:"),
            method:"DELETE",
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            dataType: 'json',
            success: function (result) {
                    location.reload();
            }
        });
    });
});