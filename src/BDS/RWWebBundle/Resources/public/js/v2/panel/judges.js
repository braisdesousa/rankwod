$(function() {
    $("button.judge-delete").click(function () {
        var judge_info = {
            'slug': $(this).closest("table").data("competition"),
            'username': $(this).data("user")
        };
        swal({
            title: "¿Estas Seguro?",
            text: "Vas a eliminar a este Juez de la competición",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Si, Eliminar",
            cancelButtonText: "Cancelar",
            confirmButtonClass:"bg-red",
            closeOnConfirm: false
        }).then((result)=>{
            if(result.value){
                $.ajax({
                    url: Routing.generate('v2_panel_judges_delete', judge_info).replace("http:","https:"),
                    dataType: 'json',
                    method: "POST",
                    headers: {'X-HTTP-Method-Override': 'DELETE'},
                    success: function (result) {
                        swal("Borrado!", "Juez Borrado", "success");
                        location.reload();
                    }
                });
            }
        });
    });
    $("button.judge-accept").click(function () {
        var judge_info = {
            'slug': $(this).closest("table").data("competition"),
            'username': $(this).data("user")
        };
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_judges_patch_accept", judge_info).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.url) {
                        window.location = data.url;
                    }
                    notySuccess(data.message);
                } else {
                    notyError(data.message);
                }

            },
            error: function (jqXHR) {
                notyError('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
    $("button.judge-decline").click(function () {
        var judge_info = {
            'slug': $(this).closest("table").data("competition"),
            'username': $(this).data("user")
        };
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_judges_patch_decline", judge_info).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    swal("Declinado!", "Juez Declinado", "success");
                    location.reload();
                } else {
                    swal("Atención!", "Se ha producido un error", "error");
                    location.reload();
                }
            },
            error: function (jqXHR) {
                notyError('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
    $("button.judge-re-email").click(function () {
        var judge_info = {
            'slug': $(this).closest("table").data("competition"),
            'username': $(this).data("user")
        };
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_judges_patch_re_email", judge_info).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.url) {
                        window.location = data.url;
                    } else {
                        notySuccess(data.message);
                    }
                } else {
                    notyError(data.message);
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
    $("button.judge-re-email-all").click(function () {
        $.ajax({
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            method: "POST",
            url: Routing.generate("v2_panel_judges_re_email_all", {'slug': $("table.table").data("competition")}).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.url) {
                        window.location = data.url;
                    } else {
                        notySuccess(data.message);
                    }
                } else {
                    notyError(data.message);
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });

    $("button#assign-results").click(function(){
        var athletes=[];
        $('input[name="judge_events"]:checked').each(function() {
            athletes.push(this.value);
        });
        var data= {
            'competition_judge_results':{
            "judge": $("select[name='judge']").val(),
            "athletes": athletes}

        };
        var routeData={
            "slug": $("div#event").data("competition"),
            "phase_slug":$("div#event").data("phase"),
            "event_slug":$("div#event").data("event")
        }
        console.log(athletes);
        $.ajax({
            method: "POST",
            url: Routing.generate("v2_panel_judge_event_post_event", routeData).replace("http:","https:"),
            data: data,
            success: function (data) {
                if (data.success) {
                    if (data.success) {
                        console.log(data.message);
                        swal("Asignado!", "Juez Asignado", "success");
                        location.reload();
                    } else {
                        swal("Atención!", "Se ha producido un error", "error");
                        location.reload();
                    }
                } else {
                    swal("Atención!", "Se ha producido un error", "error");
                    location.reload();
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
    $("button.unassign-result").click(function(){
        var routeData={
            "slug": $(this).data("competition"),
            "judge_id": $(this).data("judge"),
            "result_id":$(this).data("result"),
        }
        $.ajax({
            method: "POST",
            headers: {'X-HTTP-Method-Override': 'PATCH'},
            context: $(this).closest("span"),
            url: Routing.generate("v2_panel_judge_event_patch_unasign_result", routeData).replace("http:","https:"),
            success: function (data) {
                if (data.success) {
                    if (data.success) {
                        swal("Desasignado!", "Juez desasignado", "success");
                        $(this).remove();
                    } else {
                        swal("Atención!", "Se ha producido un error", "error");
                        location.reload();
                    }
                } else {
                    swal("Atención!", "Se ha producido un error", "error");
                    location.reload();
                }
            },
            error: function (jqXHR) {
                notySuccess('Se ha producido un error');
            },
            dataType: 'JSON'
        });
    });
});
