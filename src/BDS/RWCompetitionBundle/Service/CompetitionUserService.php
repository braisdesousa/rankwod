<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\CoreBundle\Service\CryptographicService;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\AthleteExtraData;
use BDS\RWCompetitionBundle\Entity\Bot;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\RWPaymentBundle\Service\AthleteOrderService;
use BDS\UserBundle\Entity\Role;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use Gedmo\Sluggable\Util\Urlizer;

class CompetitionUserService
{
    private $entityManager;
    private $userEntityManager;
    private $userManager;
    private $cryptographicService;
    private $defaultRole;
    private $userExtensionService;
    private $orderService;
    public function __construct(EntityManager $entityManager,EntityManager $userEntityManager,CompetitionService $competitionService, UserManager $userManager,UserExtensionService $userExtensionService,AthleteOrderService $orderService,CryptographicService $cryptographicService )
    {
        $this->entityManager=$entityManager;
        $this->competitionService=$competitionService;
        $this->userEntityManager=$userEntityManager;
        $this->userManager=$userManager;
        $this->cryptographicService=$cryptographicService;
        $this->userExtensionService=$userExtensionService;
        $this->orderService=$orderService;
    }
    public function phaseHasPendingByOrganizer(Phase $phase){
        return ($this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->countPendingByOrganizerAndPhase($phase)&&1);
    }
    public function addAdminToCompetition(Competition $competition,User $user){
    	$admin=$this->createCompetitionAdmin($user,$competition);
    	$competition->addAdmin($admin);
    	$this->entityManager->persist($competition);

    }
    public function doesUserParticipate(User $user, Competition $competition){
	    $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->hasUserExtension($competition,$userExtension);
    }
    public function isUserInCompetition(User $user, Competition $competition){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->isUserInCompetition($userExtension,$competition);
    }
    public function getUserInCompetition(User $user, Competition $competition){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->isUserInCompetition($userExtension,$competition);
    }
    public function isUserInCategory(User $user, Category $category){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->isUserInCategory($userExtension,$category);
    }
    public function isUserInPhase(User $user, Phase $phase){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->isUserInPhase($userExtension,$phase);
    }
    /** @return CategoryAthlete */
    public function getUserInPhase(User $user, Phase $phase){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->getAthleteInPhase($userExtension,$phase);
    }
    public function isUserAdminInCompetition(User $user, Competition $competition){
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->isUserAdmin($competition,$user);
    }
    public function getUserInTeam(User $user,Competition $competition){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        /** @var CategoryAthlete $categoryAthlete */
        $categoryAthlete=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->getTeamFromCompetition($userExtension,$competition);
        return $categoryAthlete?$categoryAthlete->getTeam():null;
    }
    public function findAthleteByUserAndCategory(User $user,Category $category){
    	return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByUserAndCategory($user,$category);
    }
    public function isUserJudgeInCompetition(User $user, Competition $competition){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->isUserJudge($competition,$userExtension);
    }
    public function isUserAdminOrJudgeInCompetition(User $user, Competition $competition){
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->isUserExtensionAdminOrJudge($competition,$userExtension);
    }

    public function getUserFromCompetitionPhase(User $user,Phase $phase)
    {
        $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
        return $this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findAthleteInPhase($userExtension,$phase);
    }
	public function shouldShowEvent(Event $event,User $user){
    	if($user->isAdmin()){
    		return true;
	    }
	    $competition=$event->getPhase()->getCompetition();
	    if($this->isUserAdminOrJudgeInCompetition($user,$competition)){
	    	return true;
	    }
	    if(!$this->isUserInCompetition($user,$competition)){
	    	return false;
	    }
	    return $event->isPublished();
	}
    /**
     * @param User $user
     * @param Event $event
     * @return \BDS\RWCompetitionBundle\Entity\EventResult|bool|null|object
     * @deprecated  Whe Should Always Get all posible results;
     */
    public function getUserEventResult(User $user,Event $event){

        if($athlete=$this->getUserFromCompetitionPhase($user,$event->getPhase())){
            return $this->entityManager->getRepository("BDSRWCompetitionBundle:EventResult")->findOneBy(["event"=>$event,"athlete"=>$athlete]);
        }
        return false;
    }
    public function getUserEventResults(User $user,Event $event){

        if($athlete=$this->getUserFromCompetitionPhase($user,$event->getPhase())){
            return $this->entityManager->getRepository("BDSRWCompetitionBundle:EventResult")->findByAthleteAndEventOrderByMeasure($athlete,$event);
        }
        return [];
    }
    public function getAthleteEventResults(CategoryAthlete $athlete,Event $event) {
	    return $this->entityManager->getRepository( "BDSRWCompetitionBundle:EventResult" )->findByAthleteAndEventOrderByMeasure( $athlete, $event );
    }
    public function createBots(Category $category, array $bots){

	    $botEntities=new ArrayCollection();
    	foreach($bots as $bot){
    		$botEntity=new Bot($bot);
    		$this->entityManager->persist($botEntity);
    		$botEntities->add($botEntity);
	    }
	    $this->addBotsInPhase($botEntities,$category);
	    return $botEntities;
    }
    public function addBotsInPhase(Collection $bots,Category $category)
    {
	    /** @var Bot $bot */
	    foreach($bots as $bot)
	    {
		    if(!$athlete=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByCategoryAndBot($category,$bot)) {
			    $this->createCategoryBotAthlete($bot, $category);
		    }
	    }
    }
    public function getTeamForCompetition(Phase $phase,Category $category,$teamName,$athletes){
            $team=new Team();
            $team->setName($teamName);
            $team->setCompetition($phase->getCompetition());
            foreach($athletes as $key=>$athlete){
                if(!$athlete["email"]){continue;}
                $user=$this->userExtensionService->getOrCreateUser($athlete);
                $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
                $userExtension->setTShirtSize($athlete["tshirt"]);
                $this->entityManager->persist($userExtension);
                $this->entityManager->flush($userExtension);
                $teamAthlete=new TeamAthlete(TeamAthlete::STATUS_ACCEPTED,$team,$userExtension);
                $teamAthlete->addExtraData(new AthleteExtraData($phase->getCompetition()->getExtraDataCollection()->first(),strval($athlete["rm"])));
                $this->entityManager->persist($teamAthlete);
                $team->addAthlete($teamAthlete);
            }
            $this->entityManager->persist($team);
            $categoyTeam=new CategoryAthlete();
            $categoyTeam->setTeam($team);
            $categoyTeam->setCategory($category);
            $categoyTeam->setStatus(CategoryAthlete::STATUS_ACCEPTED);
            $category->addAthlete($categoyTeam);
            $this->entityManager->persist($categoyTeam);
            return $team;



    }
    public function getUsersForCompetition($emails, Collection $athletes,Phase $phase,Category $category,$invited=false){

        $roninFoxUsers=$this->getRoninFoxUsers($emails);
        $this->userEntityManager->flush();
        $userExtensions=[];
        /** @var User $roninFoxUser */
        foreach($roninFoxUsers as $roninFoxUser){
            $userExtensions[]=$this->userExtensionService->getOrCreateUserExtension($roninFoxUser);
        }
        /** @var User $athlete */
        foreach($athletes as $athlete){
            $userExtensions[]=$this->userExtensionService->getOrCreateUserExtension($athlete);
        }
        $categoryUsers=[];
        /** @var User $userExtensions */
        foreach($userExtensions as $userExtension){
            if(!$athlete=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findOneByCategoryPhaseAndUserId($category,$phase,$userExtension)) {
                $categoryUsers[] = $this->createCategoryAthlete($userExtension,  $category,null,$invited);
            }
        }
        return $categoryUsers;
    }
    public function getJudgesForCompetition($emails, Collection $judges,Competition $competition){

        $roninFoxUsers=$this->getRoninFoxUsers($emails);
        $userExtensions=[];
        foreach($roninFoxUsers as $roninFoxUser){
            $userExtensions[]=$this->userExtensionService->getOrCreateUserExtension($roninFoxUser);
        }
        $competitionJudges=[];
        /** @var UserExtension $userExtension */
        foreach($userExtensions as $userExtension){
            if(!$athlete=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findOneByCompetitionAndUserExtension($competition,$userExtension)) {
	            $competitionJudges[] = $this->createJudgeAthlete($userExtension,  $competition);
            }
        }
        foreach($judges as $roninFoxUser){
	        if(!$athlete=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findOneByCompetitionAndUserExtension($competition,$userExtension)) {
		        $competitionJudges[] = $this->createJudgeAthlete($roninFoxUser,  $competition);
	        }
        }
        return $competitionJudges;

    }
    public function createJudgeAthlete(UserExtension $user,Competition $competition){
    	 $judge=new CompetitionJudge($competition,$user->getId());
    	 $judge->setUserExtension($user);
    	 $this->entityManager->persist($judge);
    	 return $judge;
    }
    /** TODO THIS IS ONLY FOR DEV PURPOSSES AND SHOULDNT BE USED  */
    public function getUsersForCompetitionFromCommand($users,Phase $phase){


        foreach($users as $commandUser){
            $user=$this->createRoninFoxUser($commandUser["mail"]);
            $user->setFirstName($commandUser["firstName"]);
            $user->setLastName($commandUser["lastName"]);
            $this->userManager->updateUser($user,false);
        }
        $this->userEntityManager->flush();
        foreach($users as $key=>$commandUser){
            if(!$user=$this->userManager->findUserByEmail($commandUser["mail"])){
              throw  new \Exception(sprintf("User %s not found",$commandUser["mail"]));
            }
            $users[$key]["rfox"]=$user;
        }
        $categoryUsers=[];
        foreach($users as $user){
            $categoryUsers[] = $this->createCategoryAthlete($user["rfox"], $phase, $user["category"]);
        }
        $this->entityManager->flush();
        return $users;
    }

    public function getAdminsForCompetition($emails,Collection $admins, Competition $competition){

	    $roninFoxUsers=$this->getRoninFoxUsers($emails);
	    $competitionAdmins=[];
	    /** @var User $roninFoxUser */
	    foreach($roninFoxUsers as $roninFoxUser){
		    if(!$athlete=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findOneByCompetitionAndUserId($competition,$roninFoxUser)) {
			    $competitionAdmins[] = $this->createCompetitionAdmin($roninFoxUser,  $competition);
		    }
	    }
	    foreach($admins as $roninFoxUser){
		    if(!$athlete=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findOneByCompetitionAndUserId($competition,$roninFoxUser)) {
			    $competitionAdmins[] = $this->createCompetitionAdmin($roninFoxUser,  $competition);
		    }
	    }
	    return $competitionAdmins;

    }
    private function createCompetitionAdmin(User $roninFoxUser,Competition $competition){
        $admin=new CompetitionAdmin();
        $admin->setUserId($roninFoxUser->getId());
        $admin->setUserExtension($this->userExtensionService->getOrCreateUserExtension($roninFoxUser));
        $admin->setCompetition($competition);
        $this->entityManager->persist($admin);
        return $admin;
    }
    private function createCategoryBotAthlete(Bot $bot,Category $category)
    {
	    $athlete= new CategoryAthlete();
	    $athlete->setStatus(CategoryAthlete::STATUS_ACCEPTED);
	    $athlete->setBot($bot);
	    $bot->setCategoryAthlete($athlete);
	    $athlete->setCategory($category);
	    $category->addAthlete($athlete);
	    $this->entityManager->persist($athlete);
	    $this->entityManager->persist($category);
	    $this->entityManager->persist($bot);
	    return $athlete;
    }

    public function createCategoryAthlete(UserExtension $userExtension,Category $category,$status=null,$invited=false){

        $status=$status?:$this->getAthleteFirstStatus($category,$userExtension);
        $athlete= new CategoryAthlete();
        $athlete->setStatus($status);
        $athlete->setUserExtension($userExtension);
        $athlete->setCategory($category);
        $category->addAthlete($athlete);
        $this->entityManager->persist($athlete);
        $this->entityManager->persist($category);
        $this->orderService->createOrderFromCategory($athlete,$invited);
        return $athlete;
    }
    public function getAthleteStatus(CategoryAthlete $categoryAthlete){
        $category=$categoryAthlete->getCategory();
        $userExtension=$categoryAthlete->getUserExtension();
        $phase=$category->getPhase();
        $competition=$phase->getCompetition();
        if($this->competitionService->userIsCompleted($competition,$categoryAthlete,$userExtension)){
            if((!$category->getPrice()) or $categoryAthlete->isInvited()){
                return CategoryAthlete::STATUS_ACCEPTED;
            }
            return $category->getPrice()?CategoryAthlete::STATUS_PENDING_PAYMENT:CategoryAthlete::STATUS_ACCEPTED;

        } else {
            return CategoryAthlete::STATUS_PENDING_EXTRA_DATA;
        }
    }
    private function getAthleteFirstStatus(Category $category,UserExtension $userExtension):string {
        if(!$category->getPhase()->getCompetition()->getExtraDataCollection()->isEmpty()){
            return CategoryAthlete::STATUS_PENDING_EXTRA_DATA;
        }
        if(!$userExtension->isUserCompleted()){
            return CategoryAthlete::STATUS_PENDING_EXTRA_DATA;
        }
        if($category->getPrice()){
            return CategoryAthlete::STATUS_PENDING_PAYMENT;
        }
        return CategoryAthlete::STATUS_ACCEPTED;

    }
    private function getRoninFoxUsers($emails){
        $roninFoxUsers=[];
        foreach($emails as $email){
            if($email){
	            if(!$roninFoxUser=$this->userManager->findUserByEmail($email)){
		            $roninFoxUsers[]=$this->createRoninFoxUser($email);
	            }else{
		            $roninFoxUsers[]=$roninFoxUser;
	            }
            }
        }
        $this->userEntityManager->flush();
        return $roninFoxUsers;
    }
    public function createRoninFoxUser(string $email, bool $andFlush=false):User
    {
        /** @var User $user */
        $user=$this->userManager->createUser();
        $user->setFirstName( strstr($email, '@', true));
        $user->setEmail($email);
        $user->setGender(User::GENDER_UNDEFINED);
        $user->setUsername(Urlizer::transliterate($email));
        $user->setConfirmationToken(Urlizer::transliterate($this->cryptographicService->encrypt($user->getUsername())));
        $user->setPlainPassword(md5(microtime()));
        $user->addRole($this->getDefaultRole());
        $this->userManager->updateUser($user,$andFlush);
        return $user;
    }

    private function getDefaultRole()
    {
        if(!$this->defaultRole){
            $this->defaultRole=$this->userEntityManager->getRepository("BDSUserBundle:Role")->findOneBy(["role"=>Role::ROLE_USER]);
        }
        return $this->defaultRole;

    }
    public function getAdminCompetitions(User $user)
    {
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findByAdminUser($user);
    }
    public function getAthleteCompetitions(User $user)
    {
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findByAthleteUser($user,CategoryAthlete::STATUS_ACCEPTED);
    }
    public function getJudgeCompetitions(User $user)
    {
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findByJudgeUser($user);
    }
    public function getCompetitions(User $user){
    	if($user->isAdmin()){
    		return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findBy([],["created"=>"DESC"]);
	    } else {
		    return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findByUser($user);
	    }
    }
    public function getUserCompetitions(User $user,$over=false)
    {
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findByUser($user,$over);
    }




}