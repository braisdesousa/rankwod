<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/06/17
 * Time: 12:18
 */

namespace BDS\RWCompetitionBundle\Service\PostUpload;


use BDS\GaufretteBundle\Service\FileService;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Service\CompetitionService;
use Oneup\UploaderBundle\Event\PostUploadEvent;
use Oneup\UploaderBundle\Uploader\File\GaufretteFile;
use Symfony\Component\HttpFoundation\RequestStack;

class CompetitionImagePostUpload {

	private $fileService;
	private $requestStack;
	private $competitionService;

	public function __construct(FileService $fileService,RequestStack $requestStack, CompetitionService $competitionService)
	{

		$this->fileService=$fileService;
		$this->requestStack=$requestStack;
		$this->competitionService=$competitionService;

	}
	public function onUpload(PostUploadEvent $event)
	{
		/** @var GaufretteFile $file */
		$file=$event->getFile();
		$databaseFile=$this->fileService->createFile($file);
		$competition=$this->findCompetition();
		if($event->getType()=='competition_background'){
			$this->competitionService->setImageBackground($competition,$databaseFile);
		} else {
			$this->competitionService->setImage($competition,$databaseFile);
		}

		$response=$event->getResponse();
		$response["url"]=$databaseFile->getPublicUrl();
		$response["id"]=$databaseFile->getId();
	}
	/** @return Competition|null */
	private function findCompetition(){
		$request=$this->requestStack->getCurrentRequest();
		$slug=$request->get("slug");
		return $this->competitionService->findCompetitionBySlug($slug);
	}
}