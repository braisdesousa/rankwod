<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service\MailerService;


use BDS\CoreBundle\Service\MailerService;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\PhaseInvitation;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;

class UserMailerService
{
    private $entityManager;
    private $userManager;
    private $mailerService;
    private $twig;
    public function __construct(EntityManager $entityManager, UserManager $userManager, MailerService $mailerService, \Twig_Environment $twig, $emailAccount)
    {
        $this->entityManager=$entityManager;
        $this->userManager=$userManager;
        $this->mailerService=$mailerService;
        $this->emailAcount=$emailAccount;
        $this->twig=$twig;
    }


    private function mailNewUserAndAthleteInCompetition(CategoryAthlete $athlete ,Phase $phase,Category $category){
        $user=$athlete->getRoninFoxUser();
    	$subject=sprintf('Atleta! Bienvenido a "%s"',$phase->getCompetition()->getName());
        $sender=[$this->emailAcount=>$phase->getCompetition()->getName()];
        $receivers=[($user->getUsername())=>$user->getEmail()];
        $body=$this->twig->render("@BDSRWWeb/Mail/Competition/User/newUserAndAthleteInCompetition.html.twig",["user"=>$user,"athlete"=>$athlete,"competition"=>$phase->getCompetition(),"phase"=>$phase,"category"=>$category]);
        $this->mailerService->mail($subject,$sender,$receivers,$body);
    }
	private function mailNewUserInCompetition(CategoryAthlete $athlete ,Phase $phase,Category $category)
	{
		$user=$athlete->getRoninFoxUser();
		$subject=sprintf("Bienvenido %s",$user->getUsername());
		$sender=[$this->emailAcount=>sprintf("Competición %s",$phase->getCompetition()->getName())];
		$receivers=[$user->getCompleteName()=>$user->getEmail()];
		$body=$this->twig->render("@BDSRWWeb/Mail/Competition/User/newAthleteInCompetition.html.twig",["user"=>$user,"athlete"=>$athlete,"competition"=>$phase->getCompetition(),"phase"=>$phase,"category"=>$category]);
		$this->mailerService->mail($subject,$sender,$receivers,$body);
	}
    public function mailNewTeamInCompetition(Team $team,Phase $phase,$email){
        $subject=sprintf("Bienvenidos Equipo %s",$team->getName());
        $sender=[$this->emailAcount=>sprintf("Competición %s",$phase->getCompetition()->getName())];
        $receivers=[$team->getName()=>$email];
        $body=$this->twig->render("@BDSRWWeb/WGames/emails/new_team.html.twig",["team"=>$team,"locale"=>"es"]);
        $this->mailerService->mail($subject,$sender,$receivers,$body);
    }
    public function mailSucceedTeamInCompetition(Team $team,Phase $phase){
        /** @var User $user */
        /** @var TeamAthlete $teamAthlete */
        $teamAthlete=$team->getAthletes()->first();
        $user=$teamAthlete->getUserExtension()->getRoninFoxUser();
        $subject=sprintf("Gracias Equipo %s",$team->getName());
        $sender=[$this->emailAcount=>sprintf("Competición %s",$phase->getCompetition()->getName())];
        $receivers=[$team->getName()=>$user->getEmail()];
        $body=$this->twig->render("@BDSRWWeb/WGames/emails/new_team_succeed_payment.html.twig",["team"=>$team,"locale"=>"es"]);
        $this->mailerService->mail($subject,$sender,$receivers,$body);
    }
    public function mailSucceedPayment(User $user,Category $category){
        try{
            /** @var User $user */
            $subject=sprintf("Gracias %s, pago acceptado",$user->getFirstName());
            $sender=[$this->emailAcount=>sprintf("Competición %s",$category->getPhase()->getCompetition()->getName())];
            $receivers=[$user->getCompleteName()=>$user->getEmail()];
            $competition=$category->getPhase()->getCompetition();
            $phase=$category->getPhase();
            $body=$this->twig->render("@BDSRWWeb/Mail/Competition/User/succeedPayment.html.twig",["user"=>$user,"competition"=>$competition,"phase"=>$phase,"category"=>$category,"locale"=>"es"]);
            $this->mailerService->mail($subject,$sender,$receivers,$body);
        } catch (\Exception $exception){

        }

    }

    /**
     * @param array|CategoryAthlete         $athletes
     * @param Phase         $phase
     * @param Category|null $category
     */
    public function mailNewCompetitionAthletes(array $athletes,Phase $phase,Category $category=null){
        /** @var CategoryAthlete $athlete **/
        foreach($athletes as $athlete){
	        $category=$category?$category:$athlete->getCategory();
            if((strpos($athlete->getRoninFoxUser()->getEmail(),"rankwod")===false)){
                if($athlete->getRoninFoxUser()->getConfirmationToken()){
                	$this->mailNewUserAndAthleteInCompetition($athlete,$phase,$category);
                } else {
	                $this->mailNewUserInCompetition($athlete,$phase,$category);
                }
            }
        }
    }
	/** NEW CREATED USER */
    public function mailNewRegisteredUser(User $user){
		$subject=sprintf("Bienvenido %s",$user->getUsername());
		$sender=[$this->emailAcount=>"RankWOD App"];
		$receivers=[($user->getUsername())=>$user->getEmail()];
		$body=$this->twig->render("@BDSRWWeb/Mail/Competition/newRegisteredUser.html.twig",["user"=>$user]);
		$this->mailerService->mail($subject,$sender,$receivers,$body);
	}
	/** FORGOTTEN PASSWORD USER */
    public function sendForgottenPasswordMail(User $user){
        $subject=sprintf("Recuperar Contraseña %s",$user->getFirstName());
        $sender=[$this->emailAcount=>"RankWOD App"];
        $receivers=[($user->getFirstName())=>$user->getEmail()];
        $body=$this->twig->render("@BDSRWWeb/Mail/ResetPassword/resetPassword.html.twig",["user"=>$user]);
        $this->mailerService->mail($subject,$sender,$receivers,$body);
    }
}