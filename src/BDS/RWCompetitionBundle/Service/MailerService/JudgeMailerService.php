<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service\MailerService;


use BDS\CoreBundle\Service\CryptographicService;
use BDS\CoreBundle\Service\MailerService;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\PhaseInvitation;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\UserBundle\Entity\Role;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use Gedmo\Sluggable\Util\Urlizer;

class JudgeMailerService
{
    private $entityManager;
    private $userManager;
    private $mailerService;
    private $twig;
    public function __construct(EntityManager $entityManager, UserManager $userManager, MailerService $mailerService, \Twig_Environment $twig, $emailAccount)
    {
        $this->entityManager=$entityManager;
        $this->userManager=$userManager;
        $this->mailerService=$mailerService;
        $this->emailAcount=$emailAccount;
        $this->twig=$twig;
    }
    public function mailJudges(array $judges, Competition $competition){
	    /** @var CompetitionJudge $judge */
    	foreach($judges as $judge){
		    if((strpos($judge->getUserExtension()->getRoninFoxUser()->getEmail(),"rankwod")===false)){
			    if($judge->getUserExtension()->getRoninFoxUser()->getConfirmationToken()){
				    $this->mailNewRegisteredJudgeInCompetition($judge,$competition);
			    } else {
				    $this->mailNewCompetitionJudges($judge,$competition);
			    }
		    }
	    }
    }
	public function mailNewRegisteredJudgeInCompetition(CompetitionJudge $judge ,Competition $competition){
		$user=$judge->getRoninFoxUser();
		$subject=sprintf("Bienvenido %s",$user->getEmail());
		$sender=[$this->emailAcount=>"RankWOD App"];
		$receivers=[($user->getUsername())=>$user->getEmail()];
		$body=$this->twig->render("@BDSRWWeb/Mail/Competition/Judge/newUserAndJudgeInCompetition.html.twig",["user"=>$user,"competition"=>$competition]);
		$this->mailerService->mail($subject,$sender,$receivers,$body);
	}
	public function mailNewCompetitionJudges(CompetitionJudge $judge ,Competition $competition){
		$user=$judge->getRoninFoxUser();
		$subject=sprintf("Hola %s",$user->getCompleteName());
		$sender=[$this->emailAcount=>"RankWOD App"];
		$receivers=[($user->getCompleteName())=>$user->getEmail()];
		$body=$this->twig->render("@BDSRWWeb/Mail/Competition/Judge/newJudgeInCompetition.html.twig",["user"=>$user,"competition"=>$competition]);
		$this->mailerService->mail($subject,$sender,$receivers,$body);
	}

}