<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service\MailerService;


use BDS\CoreBundle\Service\MailerService;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;

class TeamMailerService
{
    private $entityManager;
    private $userManager;
    private $mailerService;
    private $twig;
    public function __construct(EntityManager $entityManager, UserManager $userManager, MailerService $mailerService, \Twig_Environment $twig, $emailAccount)
    {
        $this->entityManager=$entityManager;
        $this->userManager=$userManager;
        $this->mailerService=$mailerService;
        $this->emailAcount=$emailAccount;
        $this->twig=$twig;
    }

	public function mailTeamAthletes(Team $team){

		foreach($team->getAthletes() as $athlete){
			if((strpos($athlete->getRoninFoxUser()->getEmail(),"rankwod")===false)){
				/** Position 0 is the one creating the team. The Captain so no mail is needed */
				if($athlete->getPosition()!==0){
					$this->mailTeamAthlete($athlete,$team);
				}
			}
		}
	}
	public function mailTeamAthlete(TeamAthlete $teamAthlete,Team $team)
	{
		$subject=sprintf("El team %s te necesita!",$team->getName());
		$sender=[$this->emailAcount=>"RankWOD App"];
		$receivers=[($teamAthlete->getRoninFoxUser()->getUsername())=>$teamAthlete->getRoninFoxUser()->getEmail()];
		$body=$this->twig->render("@BDSRWWeb/Mail/Competition/newTeamAthlete.html.twig",["user"=>$teamAthlete->getRoninFoxUser(),"team"=>$team]);
		$this->mailerService->mail($subject,$sender,$receivers,$body);
	}

}