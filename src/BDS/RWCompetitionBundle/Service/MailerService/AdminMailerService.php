<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service\MailerService;


use BDS\CoreBundle\Service\MailerService;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;

class AdminMailerService
{
    private $entityManager;
    private $userManager;
    private $mailerService;
    private $twig;
    public function __construct(EntityManager $entityManager, UserManager $userManager, MailerService $mailerService, \Twig_Environment $twig, $emailAccount)
    {
        $this->entityManager=$entityManager;
        $this->userManager=$userManager;
        $this->mailerService=$mailerService;
        $this->emailAcount=$emailAccount;
        $this->twig=$twig;
    }
    public function mailAdmins(array $admins, Competition $competition){
	    /** @var CompetitionAdmin $admin */
    	foreach($admins as $admin){
		    if((strpos($admin->getUserExtension()->getRoninFoxUser()->getEmail(),"rankwod")===false)){
			    if($admin->getUserExtension()->getRoninFoxUser()->getConfirmationToken()){
				    $this->mailNewRegisteredAdminInCompetition($admin,$competition);
			    } else {
				    $this->mailNewCompetitionAdmins($admin,$competition);
			    }
		    }
	    }
    }
	public function mailNewRegisteredAdminInCompetition(CompetitionAdmin $admin ,Competition $competition){
		$user=$admin->getUserExtension()->getRoninFoxUser();
		$subject=sprintf("Bienvenido %s",$user->getEmail());
		$sender=[$this->emailAcount=>"RankWOD App"];
		$receivers=[($user->getUsername())=>$user->getEmail()];
		$body=$this->twig->render("@BDSRWWeb/Mail/Competition/Admin/newUserAndAdminInCompetition.html.twig",["user"=>$user,"competition"=>$competition]);
		$this->mailerService->mail($subject,$sender,$receivers,$body);
	}
	public function mailNewCompetitionAdmins(CompetitionAdmin $admin ,Competition $competition){
		$user=$admin->getUserExtension()->getRoninFoxUser();
		$subject=sprintf("Hola %s",$user->getCompleteName());
		$sender=[$this->emailAcount=>"RankWOD App"];
		$receivers=[($user->getCompleteName())=>$user->getEmail()];
		$body=$this->twig->render("@BDSRWWeb/Mail/Competition/Admin/newAdminInCompetition.html.twig",["user"=>$user,"competition"=>$competition]);
		$this->mailerService->mail($subject,$sender,$receivers,$body);
	}

}