<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 26/02/18
 * Time: 16:04
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\CoreBundle\Helpers\RepositoryHelper;
use BDS\RWCompetitionBundle\Entity\Competition;
use Doctrine\ORM\EntityManager;

class AdminsService
{
    private $entityManager;
    private $userEntityManager;
    public function __construct(EntityManager $entityManager, EntityManager $userEntityManager)
    {
        $this->entityManager=$entityManager;
        $this->userEntityManager=$userEntityManager;
    }
    public function getCompetitionAdmins(Competition $competition){

        $admins=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->findByCompetition($competition);
        $admins_ids=RepositoryHelper::getIdsFromResult($admins,"user_id");
        $users=RepositoryHelper::reformatArrayWithKey($this->userEntityManager->getRepository("BDSUserBundle:User")->findBasicDataByIds($admins_ids),"id");
        return ["users"=>$users,"admins"=>$admins];
    }

}