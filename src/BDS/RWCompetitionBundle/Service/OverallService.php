<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 4/1/16
 * Time: 12:40 PM
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Overall;
use BDS\RWCompetitionBundle\Entity\OverallCategory;
use BDS\RWCompetitionBundle\Entity\OverallResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Helpers\EventHelper;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Gedmo\Sluggable\Util\Urlizer;

class OverallService
{
    private $entityManager;
    private $resultService;

    const OVERALL_POINTS=[
        200=> [200, 188, 176, 168, 160, 152, 144, 136, 128, 120, 116, 112, 108, 104, 100, 96, 92, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 48, 44, 40, 36, 32, 28, 24, 20, 16, 12, 8, 4, 0],
        100=> [100, 94, 88, 84, 80, 76, 72, 68, 64, 60, 58, 56, 54, 52, 50, 48, 46, 44, 42, 40, 38, 36, 34, 32, 30, 28, 26, 24, 22, 20, 18, 16, 14, 13, 10, 8, 6, 4, 2, 0,],
        50=> [50, 47, 44, 42, 40, 38, 36, 34, 32, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    ];

    public function __construct(EntityManager $entityManager,ListCompetitionResultService $resultService)
    {
        $this->entityManager=$entityManager;
        $this->resultService=$resultService;

    }
    public function getTempOverall(Phase $phase){
        /** Get Phase Results */
        $phaseResults=$this->resultService->getPhaseResults($phase);
        /** Process phase resutls */
        $overallResults=$this->processOverallResults($phaseResults);
        $auxOverallOrderedResults=[];
        $overallOrderedResults=[];
        foreach($overallResults as $category=>$values){
            $auxOverallOrderedResults[$category]=[];
            foreach($values as $key=>$singleOverallResult){
                $auxOverallOrderedResults[$category][$singleOverallResult["overall"]][]=$key;
            }
            ksort($auxOverallOrderedResults[$category]);
            foreach($auxOverallOrderedResults[$category] as $overallPosition){
                foreach($overallPosition as $overallAthlete){
                    $overallOrderedResults[$category][]=$overallAthlete;
                }
            }

        }
        return $overallOrderedResults;
    }
    public function createOverall(Phase $phase){
        /** Create Overall if already Exits */
        if($phase->getOverall()){
            $this->entityManager->remove($phase->getOverall());
        }
        /** Get Phase Results */
        $phaseResults=$this->resultService->getPhaseResults($phase);
        /** Process phase resutls */
        $overallResults=$this->processOverallResults($phaseResults);
        /** Save overall results in DB */
        $overall=$this->createOverallResults($overallResults,$phase);
        $this->entityManager->flush();
        return $overall;
    }
    private function processOverallResults(array $eventsResults)
    {
        $overallResults=[];
        foreach ($eventsResults as $eventKey=>$categorizedResults){
            foreach($categorizedResults as $categoryKey=>$categoryResults){
                if(!array_key_exists($categoryKey,$overallResults)){
                    $overallResults[$categoryKey]=[];
                }
                $order=1;
                $index=0;
                foreach($categoryResults as $key=>$result){
                    $index++;
                    if(($key==0)||($result["result"]===$categoryResults[$key-1]["result"])){
                        $eventsResults[$eventKey][$categoryKey][$key]["position"]=$order;
                    } else {
                        $order=$index;
                        $eventsResults[$eventKey][$categoryKey][$key]["position"]=$order;
                    }
                }
            }
        }
        foreach ($eventsResults as $eventKey=>$categorizedResults){
            foreach($categorizedResults as $categoryKey=>$categoryResults){
                foreach($categoryResults as $orderKey=>$result){
                    if(!array_key_exists($result["athlete_id"],$overallResults[$categoryKey])){
                        $overallResults[$categoryKey][$result["athlete_id"]]=["category"=>$categoryKey,"overall"=>($result["position"])];
                    } else {
                        $overallResults[$categoryKey][$result["athlete_id"]]["overall"]+=$result["position"];
                    }
                    if($result["result"]==0){
                        $overallResults[$categoryKey][$result["athlete_id"]]["incomplete"]=true;
                    }
                }
            }
        }
        return $overallResults;

    }
    private function createOverallResults(array $overallResults,Phase $phase)
    {
        $overall=new Overall($phase);
        $phase->setOverall($overall);
        foreach($overallResults as $categoryKey=>$categorizedResults){
            $category=$this->getCategory($categoryKey,$phase);
            foreach($categorizedResults as $athlete_id=>$result){
                $athlete=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->find($athlete_id);
                $overallResult=new OverallResult($category,$overall,$athlete,$result["overall"],array_key_exists("incomplete",$result ));
                $overall->addResult($overallResult);
            }
        }
        $this->entityManager->persist($overall);
        $this->entityManager->persist($phase);
        return $overall;
    }
    private function getCategory($categorySlug,Phase $phase)
    {
        foreach($phase->getCategories() as $category){
            if($category->getSlug()===$categorySlug){
                return $category;
            }
        }
        throw  new \Exception(sprintf("Category '%s' not found in phase '%s'",$categorySlug,$phase->getSlug()));
    }
}