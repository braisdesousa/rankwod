<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/8/16
 * Time: 9:15 AM
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\RWCompetitionBundle\Helpers\EventHelper;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;

class EntitySerializationListener implements EventSubscriberInterface
{
    private $userEntityManager;
    private $entityManager;
    public function __construct(EntityManager $userEntityManager,EntityManager $entityManager)
    {
        $this->userEntityManager=$userEntityManager;
        $this->entityManager=$entityManager;
    }

    public static function getSubscribedEvents()
    {
     return [
         [
             'event' => Events::POST_SERIALIZE,
             'format' => 'json',
             'class' => 'BDS\RWCompetitionBundle\Entity\Phase',
             'method' => 'onPostSerializePhase',
         ],
         [
             'event' => Events::POST_SERIALIZE,
             'format' => 'json',
             'class' => 'BDS\UserBundle\Entity\User',
             'method' => 'onPostSerializeUser',
         ],
     ];
    }
    public function onPostSerializeUser(ObjectEvent $event){

        if(
            $event->getContext()->attributes->containsKey("groups") &&
            (in_array("team_search",$event->getContext()->attributes->get("groups")->get()))){
            /** @var User $user */
            $user= $event->getObject();
            $event->getVisitor()->addData("username",$user->getId());
            $event->getVisitor()->addData("complete_name",$this->getUserSearchName($user));
        }
        if(
            $event->getContext()->attributes->containsKey("groups") &&
            (in_array("athlete_search",$event->getContext()->attributes->get("groups")->get()))){
            /** @var User $user */
            $user= $event->getObject();
            $event->getVisitor()->addData("username",$user->getId());
            $event->getVisitor()->addData("complete_name",$this->getUserSearchName($user));
        }
    }
    public function onPostSerializeEventResult(ObjectEvent $event){
        /** @var EventResult  $eventResult */
        $eventResult=$event->getObject();
        $event->getVisitor()->addData("result",$this->serializeResult($eventResult));
    }
    public function onPostSerializePhase(ObjectEvent $event){
        /** @var Phase $phase */
        $phase=$event->getObject();
        $names=EventHelper::getSplitEventsNamesForSerialization($phase->getEvents());
        $event->getVisitor()->addData("events",$names);
    }

    private function serializeResult(EventResult $eventResult){
        if(!$eventResult->getMeasure()){
            return "-";
        }
        if($eventResult->getMeasure()->getType()==Measure::MEASURE_TYPE_TIME){
            return gmdate("i:s", $eventResult->getResult());
        }
        return $eventResult->getResult();
    }
    private function getUserSearchName(User $user){
        $name=$user->getCompleteName()?$user->getCompleteName():$user->getUsername();
        if($extendedUser=$this->entityManager->getRepository("BDSRWCompetitionBundle:UserExtension")->findOneBy(["user_id"=>$user->getId()])) {
            if($extendedUser->getBox()){
                $name.=sprintf(" (%s)",$extendedUser->getBox()->getName());
            }
        }
        return $name;
    }
}