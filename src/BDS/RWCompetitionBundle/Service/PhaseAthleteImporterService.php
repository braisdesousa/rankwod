<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Service\MailerService\UserMailerService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;

class PhaseAthleteImporterService
{
    private $entityManager;
    private $competitionMailerService;

    public function __construct(EntityManager $entityManager,UserMailerService $competitionMailerService)
    {
        $this->entityManager=$entityManager;
        $this->competitionMailerService=$competitionMailerService;

    }

    public function athleteImporter(Phase $phase,Category $category,array $athletes=[])
    {

        foreach($athletes as $categoryAthletes){
                if($categoryAthletes instanceof Collection){

                    /** @var CategoryAthlete $categoryAthlete */
                    foreach($categoryAthletes as $categoryAthlete){
                        if(!$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findAthleteInPhase($categoryAthlete->getUserExtension(),$phase)){
                            $categoryAthleteEntity=new CategoryAthlete();
                            $categoryAthleteEntity->setCategory($category);
                            $categoryAthleteEntity->setStatus(CategoryAthlete::STATUS_ACCEPTED);
                            $categoryAthleteEntity->setUserId($categoryAthlete->getUserId());
                            $categoryAthleteEntity->setUserExtension($categoryAthlete->getUserExtension());
                            $this->entityManager->persist($categoryAthleteEntity);
                            $category->addAthlete($categoryAthlete);
                        }
                    }
                }
            }
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }

}