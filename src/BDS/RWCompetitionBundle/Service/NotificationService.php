<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Service\MailerService\UserMailerService;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

class NotificationService
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function countNotifications(User $user){

    	$admin= $this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->countByUserandType($user, CompetitionAdmin::STATUS_PENDING);
		$judge=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->countByUserandType($user, CompetitionAdmin::STATUS_PENDING);
		$athlete=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->countByUserandType($user, CompetitionAdmin::STATUS_PENDING);
		return $admin+$judge+$athlete;
    }
    public function getNotifications(User $user){
	    $admin= $this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionAdmin")->getByUserandType($user, CompetitionAdmin::STATUS_PENDING);
	    $judge=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->getByUserandType($user, CompetitionAdmin::STATUS_PENDING);
	    $athlete=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->getByUserandType($user, CategoryAthlete::STATUS_PENDING_USER);
    	return ["admin"=>$admin,"athlete"=>$athlete,"judge"=>$judge];
    }

}