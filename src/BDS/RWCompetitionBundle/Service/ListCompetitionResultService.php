<?php
namespace BDS\RWCompetitionBundle\Service;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Helpers\EventHelper;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Gedmo\Sluggable\Util\Urlizer;

class ListCompetitionResultService
{
    private $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;

    }
    
    public function reorderCompetitionResults(Competition $competition)
    {
        $results=[];
        foreach($competition->getPhases() as $phase){
            if($phase->isPublished()){
                $results[$phase->getSlug()]=[];
                foreach($phase->getEvents() as $event){
                    if($event->isSortable()){
                      $results[$phase->getSlug()]=array_merge($results[$phase->getSlug()],$this->reorderEventResults($event));
                    }
                }
            }
        }
        return $results;
    }
    public function getPhaseResults(Phase $phase)
    {
        $results=[];
        
        foreach($phase->getEvents() as $event){
                      $results=array_merge($results,$this->reorderEventResults($event));
                }
        return $results;
    }
    public function getOverallResults(Phase $phase)
    {
        $results=[];
        foreach($phase->getCategories() as $category){
            $results[$category->getSlug()]=$this->getOrderedOverallResults($phase,$category);
        }
        return $results;       
    }
    public function getOrderedOverallResults(Phase $phase,Category $category){
        if($phase->isDesertedResultLikeGames()){
            return $this->entityManager->getRepository("BDSRWCompetitionBundle:OverallResult")->findByCategoryAndOverallOrderByResultSerializable($category,$phase->getOverall(),true);
        }
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:OverallResult")->findByCategoryAndOverallOrderByResultSerializable($category,$phase->getOverall());
    }
    public function reorderEventResults(Event $event)
    {
        $results=[];
        $splitEventNameWithKey=EventHelper::getSplitEventNamesWithKey($event);
        foreach($event->getMeasureGroups() as $key=>$measureGroup){
            $results[Urlizer::transliterate($splitEventNameWithKey[$key][0])]=$this->getResultsFromEvent($event,$measureGroup);
        }
        return $results;
    }
    private function getResultsFromEvent(Event $event,MeasureGroup $measureGroup)
    {
        $results=[];
        foreach($event->getPhase()->getCategories() as $category){
            $results[$category->getSlug()]=$this->getOrderedResults($event,$category,$measureGroup);
        }
        return $results;

    }
    private function getOrderedResults(Event $event, Category $category,MeasureGroup $measureGroup){
        $measurements=$measureGroup->getMeasurements();
        $results=$this->getMeasurementGroupResults($event,$category,$measurements);
        return $results;

    }
    private function getMeasurementGroupResults(Event $event,Category $category,Collection $measurementGroup)
    {
        $results=[];
        foreach($measurementGroup as $key=>$measure){
            $eventResults=$this->getOrderedResultsFromMeasureEventAndCategory($event,$category,$measure);
            $results=array_merge($results,$eventResults);
        }
        
        return $this->createFakeResultsIfNeeded($category,$event,$results);
    }
    private function createFakeResultsIfNeeded(Category $category,Event $event,array $results=[])
    {
        $athletes=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findUserIdAndTeamNameByCategoryAndEvent($category,$event);
        $array=[];
        $alreadySettedResults=[];        
        foreach($results as $result){
            $alreadySettedResults[]=$result["athlete_id"];
        }
        /** @var CategoryAthlete $athlete */
        foreach($athletes as $athlete){
            if(!in_array($athlete["athlete_id"],$alreadySettedResults)){
                $array[]= [
                    "type"=>"MEASURE_TYPE_REPS",
                    "user_id"=>$athlete["user_id"],
	                "athlete_name"=>$athlete["athlete_name"],
	                "athlete_id"=>$athlete["athlete_id"],
	                "athlete_slug"=>$athlete["athlete_slug"],
                    "result"=>0,
                    "corrected"=>false,
                    "without_result"=>true,
                    "video"=>false,
                    "team_name"=>$athlete["team_name"],
                    "team_slug"=>$athlete["team_slug"],
                    "box_name"=>$athlete["box_name"],
                    "box_slug"=>$athlete["box_slug"],

                ];
            }
        }
        return array_merge($results,$array);
        
    }
    private function getOrderedResultsFromMeasureEventAndCategory(Event $event,Category $category,Measure $measure){
        if(in_array($measure->getType(),[Measure::MEASURE_TYPE_TIME])){
            $orderBy="ASC";
        } else {
            $orderBy="DESC";
        }
        return $this->entityManager->getRepository("BDSRWCompetitionBundle:EventResult")->findByEventCategoryAndMeasureOrderByResultWithUserId($event,$category,$measure,$orderBy);
    }
}