<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/02/16
 * Time: 22:05
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;

class TeamService
{
    private $entityManager;
    private $userManager;
    private $competitionUser;
    private $userExtensionService;
    public function __construct(EntityManager $entityManager,UserManager $userManager,CompetitionUserService $competitionUserService,UserExtensionService $userExtensionService)
    {
        $this->entityManager=$entityManager;
        $this->userManager=$userManager;
        $this->competitionUser=$competitionUserService;
        $this->userExtensionService=$userExtensionService;
    }
    public function isUserInTeam(User $user, Team $team){
        $userInTeam=false;
        foreach($team->getAthletes() as $athlete){
            if($athlete->getUserId()==$user->getId()){
                $userInTeam=true;
            }
        }
        return $userInTeam;
    }
    public function isUserTeamCaptain(User $user, Team $team){
        $captainInTeam=false;
        foreach($team->getAthletes() as $athlete){
            if(($athlete->getUserId()==$user->getId())&&$athlete->getPosition()==0){
                $captainInTeam=true;
            }
        }
        return $captainInTeam;
    }
    public function addAthleteToTeam(string $usernameOrEmail,Team $team,Competition $competition){
        /** @var User $user */
        if($user=$this->userManager->findUserByUsernameOrEmail($usernameOrEmail)){
            if($this->competitionUser->isUserInCompetition($user,$competition)){
                throw new \Exception("Este usuario ya participa en la competición",421);
            } else {
                $this->createTeamAthleteFromUser($user,$team);
                $this->entityManager->flush();
            }
        }elseif(filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
                $user=$this->competitionUser->createRoninFoxUser($usernameOrEmail,true);
                $this->createTeamAthleteFromUser($user,$team);
                $this->entityManager->flush();
        } else {
            throw new \Exception("El usuario no existe ó el email es incorrecto",421);
        }
    }
    private function createTeamAthleteFromUser(User $user,Team $team){
            $userExtension=$this->userExtensionService->getOrCreateUserExtension($user);
            $teamAthlete=new TeamAthlete(TeamAthlete::STATUS_ACCEPTED,$team,$userExtension);
            $team->addAthlete($teamAthlete);
            $this->entityManager->persist($teamAthlete);
            $this->entityManager->persist($team);

    }

}