<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/06/17
 * Time: 12:05
 */

namespace BDS\RWCompetitionBundle\Service\Namer;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Service\CompetitionService;
use BDS\RWCompetitionBundle\Service\CompetitionUserService;
use Oneup\UploaderBundle\Uploader\File\FileInterface;
use Oneup\UploaderBundle\Uploader\Naming\NamerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CompetitionFileNamer implements NamerInterface {


	private $requestStack;
	private $competitionService;

	public function __construct(RequestStack $requestStack,CompetitionService $competitionService) {
		$this->requestStack=$requestStack;
		$this->competitionService=$competitionService;
	}

	public function name(FileInterface $file)
	{
		$competition=$this->findCompetition();
		return sprintf("competition_images/%s/main_%s_%s.%s",$competition->getId(),uniqid(),$competition->getSlug(),$file->getExtension());
	}
	/** @return Competition|null */
	private function findCompetition(){
		$request=$this->requestStack->getCurrentRequest();
		$slug=$request->get("slug");
		return $this->competitionService->findCompetitionBySlug($slug);
	}
}