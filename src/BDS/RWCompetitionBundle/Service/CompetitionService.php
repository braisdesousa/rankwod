<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/06/17
 * Time: 12:07
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\GaufretteBundle\Entity\File;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use Doctrine\ORM\EntityManager;

class CompetitionService {

	private $entityManager;

	public function __construct(EntityManager $entityManager) {
		$this->entityManager=$entityManager;
	}
	public function findCompetitionBySlug($slug){
		if(!$competition=$this->entityManager->getRepository("BDSRWCompetitionBundle:Competition")->findOneBySlug($slug)){
			return false;
		}
		return $competition;
	}
	public function setImage(Competition $competition, File $file){
		$competition->setImage($file);
		$this->entityManager->persist($competition);
		$this->entityManager->flush();
	}
	public function setImageBackground(Competition $competition, File $file){
		$competition->setBackgroundImage($file);
		$this->entityManager->persist($competition);
		$this->entityManager->flush();
	}
	public function athleteHasToPay(CategoryAthlete $categoryAthlete){
        $category=$categoryAthlete->getCategory();
        if($category->getPrice() &&(!$categoryAthlete->getOrder()->isPaid())){
            return true;
        }
        return false;
    }
	public  function userIsCompleted(Competition $competition,CategoryAthlete $categoryAthlete,UserExtension $userExtension){
        $phase=($categoryAthlete->getCategory()->getPhase());
        if($phase->isOver()){
            return true;
        }
	    if(!$userExtension->isCompleted()){
            return false;
        }
        foreach($competition->getExtraDataCollection() as $extraData){
            if(!$categoryAthlete->hasExtraData($extraData)){
                return false;
            }
        }
        return true;
    }

    /**
     * @param Phase $phase
     *
     * @return bool
     * Una fase esta lista para publicar cuando
     *             a) La competición está publicada
     *             b) La fase está pagada
     *             b) las categorías estan creadas
     *             c) Si las categorias tiene precio de inscripción, está configurado el método de pago.
     */
    public function isPhaseReadyToPublish(Phase $phase):bool{
	    $gatewayConfig=$phase->getCompetition()->getGatewayConfig();
	    $gatewayEnabled=($gatewayConfig && $gatewayConfig->isFilled());
	    $competition=$phase->getCompetition();

	    if($phase->hasPaidCategories()){
	        return (
	            $competition->isPublished()&&
                $phase->isPaid()&&
                $phase->hasCategories()&&
                $gatewayEnabled);

        } else {
            return (
                $competition->isPublished()&&
                $phase->isPaid()&&
                $phase->hasCategories());
        }
    }
}