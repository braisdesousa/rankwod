<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 26/02/18
 * Time: 16:04
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\CoreBundle\Helpers\RepositoryHelper;
use BDS\RWCompetitionBundle\Entity\Competition;
use Doctrine\ORM\EntityManager;

class JudgesService
{
    private $entityManager;
    private $userEntityManager;
    public function __construct(EntityManager $entityManager, EntityManager $userEntityManager)
    {
        $this->entityManager=$entityManager;
        $this->userEntityManager=$userEntityManager;
    }
    public function getCompetitionJudges(Competition $competition){

        $judges=$this->entityManager->getRepository("BDSRWCompetitionBundle:CompetitionJudge")->findByCompetition($competition);
        $judges_ids=RepositoryHelper::getIdsFromResult($judges,"user_id");
        $users=RepositoryHelper::reformatArrayWithKey($this->userEntityManager->getRepository("BDSUserBundle:User")->findBasicDataByIds($judges_ids),"id");
        return ["users"=>$users,"judges"=>$judges];
    }

}