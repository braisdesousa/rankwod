<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/03/18
 * Time: 17:25
 */

namespace BDS\RWCompetitionBundle\Service;


use BDS\CoreBundle\Service\CryptographicService;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\UserBundle\Entity\Role;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use Gedmo\Sluggable\Util\Urlizer;

class UserExtensionService
{
    /** @var EntityManager  */
    private $entityManager;
    /** @var UserManager  */
    private $userManager;
    /** @var CryptographicService */
    private $cryptographicService;
    /** @var EntityManager */
    private $userEntityManager;

    private $defaultRole;
    /**
     * UserExtensionService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager,UserManager $userManager,EntityManager $userEntityManager,CryptographicService $cryptographicService)
    {
        $this->entityManager=$entityManager;
        $this->userManager=$userManager;
        $this->cryptographicService=$cryptographicService;
        $this->userEntityManager=$userEntityManager;
    }

    public function getOrCreateUserExtension(User $user){
        if(!$userExtension=$this->entityManager->getRepository("BDSRWCompetitionBundle:UserExtension")->findOneBy(["user_id"=>$user->getId()])){
            $userExtension=new UserExtension($user->getId());
            $userExtension->setRoninFoxUser($user);
            $this->entityManager->persist($userExtension);
            $this->entityManager->flush($userExtension);
        }
        return $userExtension;
    }
    public function getOrCreateUser($athlete){
        if(!$user=$this->userManager->findUserByEmail($athlete["email"])){
            $user=$this->createRoninFoxUser($athlete);
        } else  {
            $this->updateUser($user,$athlete);
        }
        return $user;
    }
    private function updateUser(User $user,$athlete){
        $user->setFirstName($athlete["first_name"]);
        $user->setLastName($athlete["last_name"]);
        $user->setTelephone($athlete["phone"]);
        $user->setDni($athlete["dni"]);
        $this->userManager->updateUser($user);
    }
    private function createRoninFoxUser($athlete)
    {
        /** @var User $user */
        $user=$this->userManager->createUser();
        $user->setFirstName($athlete["first_name"]);
        $user->setLastName($athlete["last_name"]);
        $user->setTelephone($athlete["phone"]);
        $user->setEmail($athlete["email"]);
        $user->setDni($athlete["dni"]);
        $user->setGender(User::GENDER_UNDEFINED);
        $user->setUsername($this->getFreeUserName(Urlizer::transliterate($user->getCompleteName())));
        $user->setConfirmationToken(Urlizer::transliterate($this->cryptographicService->encrypt($user->getUsername())));
        $user->setPlainPassword(md5(microtime()));
        $user->addRole($this->getDefaultRole());
        $this->userManager->updateUser($user,true);
        return $user;
    }
    private function getFreeUserName($name){
        $newName=$name;
        $aux=1;
        while($this->userManager->findUserByUsername($newName)){
            $newName=$name."_".$aux++;
        }
        return $newName;
    }
    private function getDefaultRole()
    {
        if(!$this->defaultRole){
            $this->defaultRole=$this->userEntityManager->getRepository("BDSUserBundle:Role")->findOneBy(["role"=>Role::ROLE_USER]);
        }
        return $this->defaultRole;

    }


}