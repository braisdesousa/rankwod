<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 3:21 PM
 */

namespace BDS\RWCompetitionBundle\Validator;


use Symfony\Component\Validator\Constraint;

class EmailArray extends Constraint
{
    public $message= 'The email "%email%" is not valid.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}