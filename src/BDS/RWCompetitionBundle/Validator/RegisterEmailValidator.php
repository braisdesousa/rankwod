<?php


namespace BDS\RWCompetitionBundle\Validator;


use Doctrine\ORM\EntityManager;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RegisterEmailValidator extends ConstraintValidator
{
    private $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $emailValidator = new EmailValidator();
	    $multipleValidations = new MultipleValidationWithAnd([
		    new RFCValidation(),
		    new DNSCheckValidation()
	    ]);

        	if($value){
		        $result = $emailValidator->isValid($value,$multipleValidations);
		        if (!$result) {
                        $this->context->buildViolation($constraint->message)
                            ->setParameter('%email%', $value)
                            ->addViolation();
                } else {
		            if($this->entityManager->getRepository("BDSUserBundle:User")->findOneByEmail($value)){
                        $this->context->buildViolation("Este email %email% ya está en uso, inicia sesión para continuar con tu cuenta")
                            ->setParameter('%email%', $value)
                            ->addViolation();
                    }
                }
        }
    }
}