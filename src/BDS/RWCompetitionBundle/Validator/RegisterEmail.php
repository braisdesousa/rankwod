<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 3:21 PM
 */

namespace BDS\RWCompetitionBundle\Validator;


use Symfony\Component\Validator\Constraint;

class RegisterEmail extends Constraint
{
    public $message= 'El email "%email%" no es válido.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}