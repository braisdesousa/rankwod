<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 3:21 PM
 */

namespace BDS\RWCompetitionBundle\Validator;


use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class EmailArrayValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $emailValidator = new EmailValidator();
	    $multipleValidations = new MultipleValidationWithAnd([
		    new RFCValidation(),
		    new DNSCheckValidation()
	    ]);
        $invalidMails = [];
        foreach ($value as $email) {
        	if($email){
		        $result = $emailValidator->isValid($email,$multipleValidations);
		        if ($result) {

		        } else if ($emailValidator->hasWarnings()) {
			        $invalidMails[]=$email;
		        } else {
			        $invalidMails[]=$email;
		        }
	        }
        }
        if (count($invalidMails)) {
            foreach($invalidMails as $invalidMail){
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%email%', $invalidMail)
                    ->addViolation();
            }
        }
    }
}