<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 1:11 PM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class EventRepository extends EntityRepository
{

    public function getLastExpiredDate(Competition $competition)
    {
        $qb=$this->createQueryBuilder("eventRepository");
        $qb->select("eventRepository.limitDate");
        $qb->setMaxResults(1);
        $qb->leftJoin("eventRepository.phase","phase");

        $qb->where($qb->expr()->andX(
                        $qb->expr()->lt("eventRepository.limitDate",":now")),
                        $qb->expr()->eq("phase.competition",$qb->expr()->literal($competition->getId()))
        );
        $qb->orderBy("eventRepository.limitDate","DESC");
        $qb->setParameter("now",(new \DateTime()),Type::DATETIME);
        return $qb->getQuery()->getOneOrNullResult();
    }
    public function findOneByEventCompetitionAndPhaseSlugs($eventSlug,$phaseSlug,$competitionSlug)
    {
        $qb=$this->createQueryBuilder("eventRepository");
        $qb->leftJoin("eventRepository.phase","phase");
        $qb->leftJoin("phase.competition","competition");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("eventRepository.slug",$qb->expr()->literal($eventSlug)),
            $qb->expr()->eq("phase.slug",$qb->expr()->literal($phaseSlug)),
            $qb->expr()->eq("competition.slug",$qb->expr()->literal($competitionSlug))
        ));
        return $qb->getQuery()->getOneOrNullResult();
    }
    public function countUpdatedEventsAfterCacheCreationByCompetition(\DateTime $dateTime,Competition $competition)
    {
        $qb=$this->createQueryBuilder("eventRepository");
        $qb->select($qb->expr()->count("eventRepository"));
        $qb->leftJoin("eventRepository.phase","phase");
        $qb->where($qb->expr()->andX(
            $qb->expr()->gt("eventRepository.updated",":datetime"),
            $qb->expr()->neq("phase.publicResultType",$qb->expr()->literal(Phase::PUBLISH_RESULT_MANUAL)),
            $qb->expr()->eq("phase.competition",$qb->expr()->literal($competition->getId()))
        ));
        $qb->setParameter("datetime",$dateTime,Type::DATETIME);
        return $qb->getQuery()->getSingleScalarResult();
    }
    public function countUpdatedEventsAfterCacheCreationByEvent(\DateTime $dateTime,Event $event)
    {
        $qb=$this->createQueryBuilder("eventRepository");
        $qb->select($qb->expr()->count("eventRepository"));
        $qb->where($qb->expr()->andX(
            $qb->expr()->gt("eventRepository.updated",":datetime"),
            $qb->expr()->eq("eventRepository.id",$qb->expr()->literal($event->getId()))
        ));
        $qb->setParameter("datetime",$dateTime,Type::DATETIME);
        return $qb->getQuery()->getSingleScalarResult();
    }
}