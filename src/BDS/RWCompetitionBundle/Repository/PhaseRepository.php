<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 1:11 PM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;

class PhaseRepository extends EntityRepository
{

    public function findByUserExtension(UserExtension $user){
        $qb=$this->createQueryBuilder("phase");
        $qb->leftJoin("phase.competition","competition");
        $qb->leftJoin("competition.adminUsers","adminUsers");
        $qb->leftJoin("competition.judgeUsers","judgeUsers");
        $qb->leftJoin("phase.categories","categories");
        $qb->leftJoin("categories.athletes","athletes");
        $qb->leftJoin("athletes.team","teams");
        $qb->leftJoin("teams.athletes","team_athletes");
        $qb->distinct();
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->eq("judgeUsers.userExtension",$qb->expr()->literal($user->getId()))
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->eq("adminUsers.userExtension",$qb->expr()->literal($user->getId())),
                        $qb->expr()->like("adminUsers.status",$qb->expr()->literal(CompetitionAdmin::STATUS_ACCEPTED)
                        )),
                    $qb->expr()->andX(
                        $qb->expr()->eq("competition.publish",$qb->expr()->literal(true)),
                        $qb->expr()->eq("phase.published",$qb->expr()->literal(true)),
                        $qb->expr()->orX(
                            $qb->expr()->andX(
                                $qb->expr()->eq("athletes.userExtension",$qb->expr()->literal($user->getId())),
                                $qb->expr()->orX(
                                    $qb->expr()->like("athletes.status",$qb->expr()->literal(CategoryAthlete::STATUS_ACCEPTED)),
                                    $qb->expr()->like("athletes.status",$qb->expr()->literal(CategoryAthlete::STATUS_PENDING_EXTRA_DATA)),
                                    $qb->expr()->like("athletes.status",$qb->expr()->literal(CategoryAthlete::STATUS_PENDING_PAYMENT))
                                )

                            ),
                            $qb->expr()->eq("team_athletes.userExtension",$qb->expr()->literal($user->getId()))
                        )

                    )


                )));

        $qb->orderBy("phase.startDate","DESC");
        return $qb->getQuery()->getResult();
    }
  public function getPastPhases(){
      $qb=$this->createQueryBuilder("phaseRepository");

      $qb->where(
          $qb->expr()->andX(
              $qb->expr()->lt("phaseRepository.endDate",":now"),
              $qb->expr()->eq("phaseRepository.published",$qb->expr()->literal(true))
          ));

      $qb->setParameter("now",(new \DateTime()),Type::DATETIME);
      $qb->orderBy("phaseRepository.endDate","DESC");
      return $qb->getQuery()->getResult();
  }
  public function getFuturePhases(){
      $qb=$this->createQueryBuilder("phaseRepository");

      $qb->where(
          $qb->expr()->andX(
              $qb->expr()->gt("phaseRepository.startDate",":now"),
              $qb->expr()->eq("phaseRepository.published",$qb->expr()->literal(true))
          ));

      $qb->setParameter("now",(new \DateTime()),Type::DATETIME);
      $qb->orderBy("phaseRepository.startDate","ASC");
      return $qb->getQuery()->getResult();
  }
  public function getCurrentPhases(){
      $qb=$this->createQueryBuilder("phaseRepository");

      $qb->where(
          $qb->expr()->andX(
              $qb->expr()->lt("phaseRepository.startDate",":now"),
              $qb->expr()->gt("phaseRepository.endDate",":now"),
              $qb->expr()->eq("phaseRepository.published",$qb->expr()->literal(true))
          ));

      $qb->setParameter("now",(new \DateTime()),Type::DATETIME);
      $qb->orderBy("phaseRepository.endDate","DESC");
      return $qb->getQuery()->getResult();
  }

  public function findFirstAttendantePhaseByCompetition(Competition $competition)
    {
        $qb=$this->createQueryBuilder("phaseRepository");
        $qb->setMaxResults(1);
        $qb->where($qb->expr()->andX(
                $qb->expr()->eq("phaseRepository.competition",$qb->expr()->literal($competition->getId())),
                $qb->expr()->eq("phaseRepository.phase_type",$qb->expr()->literal(Phase::TYPE_ATTENDANCE))    
        ));
        $qb->orderBy("phaseRepository.created","ASC");
        return $qb->getQuery()->getOneOrNullResult();

    }
  public function findByAthleteUser(UserInterface $user,$status)
    {
    $qb=$this->createQueryBuilder("phaseRepository");
    $qb->join("phaseRepository.categories","categories");
    $qb->where($qb->expr()->in("categories.id",$this->getEntityManager()->getRepository("BDSRWCategoryBundle:Category")->findByUserAndStatusQB($user,$status)->getDQL()));
    $qb->distinct(true);
    $qb->orderBy("phaseRepository.created","DESC");
    return $qb->getQuery()->getResult();
    }

    public function findOneByCompetitionAndSlug(Competition $competition,$slug){
        $qb= $this->createQueryBuilder("phaseRepository");
        $qb->setMaxResults(1);
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("phaseRepository.slug",$qb->expr()->literal($slug)),
            $qb->expr()->eq("phaseRepository.competition",$qb->expr()->literal($competition->getId()))
        ));
        return $qb->getQuery()->getOneOrNullResult();
    }
}