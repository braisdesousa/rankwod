<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 3/23/16
 * Time: 11:27 AM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use Doctrine\ORM\EntityRepository;

class TeamAthleteRepository extends EntityRepository
{
    public function findOneByUserAndCompetition(UserExtension $userExtension,Competition $competition){
        $qb=$this->createQueryBuilder("team_athlete");
        $qb->leftJoin("team_athlete.team","team");

        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("team_athlete.userExtension",$qb->expr()->literal($userExtension->getId())),
            $qb->expr()->eq("team.competition",$qb->expr()->literal($competition->getId()))
        ));
        return $qb->getQuery()->getOneOrNullResult();
    }

}