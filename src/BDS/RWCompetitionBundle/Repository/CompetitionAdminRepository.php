<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 30/05/17
 * Time: 13:14
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class CompetitionAdminRepository extends EntityRepository  {

    public function findByCompetition(Competition $competition){

        $qb=$this->createQueryBuilder("competitionAdmin");
        $qb->leftJoin("competitionAdmin.userExtension","userExtension");
        $qb->leftJoin("userExtension.box","box");
        $qb->select("competitionAdmin.status");
        $qb->addSelect("box.name as box_name");
        $qb->addSelect("userExtension.user_id");
        $qb->where($qb->expr()->eq("competitionAdmin.competition",$qb->expr()->literal($competition->getId())));
        return $qb->getQuery()->getArrayResult();
    }
	public function countByUserAndType(User $user, $type){
		$qb=$this->getByUserAndTypeQB($user,$type);
		$qb->select($qb->expr()->count("competitionAdmin"));
		return $qb->getQuery()->getSingleScalarResult();
	}
	public function getByUserAndType(User $user, $type){

		$qb=$this->getByUserAndTypeQB($user,$type);
		$qb->leftJoin("competitionAdmin.competition","competition");
		$qb->orderBy("competition.created","DESC");
		return $qb->getQuery()->getResult();
	}
	private function getByUserAndTypeQB(User $user,$type)
	{
		$qb=$this->createQueryBuilder("competitionAdmin");
		$qb->where($qb->expr()->andX(
			$qb->expr()->eq("competitionAdmin.user_id",$qb->expr()->literal($user->getId())),
			$qb->expr()->like("competitionAdmin.status",$qb->expr()->literal($type))
		));
		return $qb;
	}
	public function findOneByCompetitionAndUserId(Competition $competition,User $user){
		$qb=$this->createQueryBuilder("competitionAdmin");
		$qb->setMaxResults(1);
		$qb->where($qb->expr()->andX(
			$qb->expr()->eq("competitionAdmin.competition",$qb->expr()->literal($competition->getId())),
			$qb->expr()->eq("competitionAdmin.user_id",$qb->expr()->literal($user->getId()))
		));
		return $qb->getQuery()->getOneOrNullResult();
	}
	public function findByCompetitionAndStatus(Competition $competition,$status){
		$qb=$this->createQueryBuilder("competitionAdmin");
		$qb->where($qb->expr()->andX(
			$qb->expr()->eq("competitionAdmin.competition",$qb->expr()->literal($competition->getId())),
			$qb->expr()->like("competitionAdmin.status",$qb->expr()->literal($status))
		));
		return $qb->getQuery()->getResult();

	}

}