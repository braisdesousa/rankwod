<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 1:11 PM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;

class UserExtensionRepository extends EntityRepository
{
    public function findDistinctBoxes($userExtensionIds){

        $qb=$this->createQueryBuilder("userExtensionRepository");
        $qb->leftJoin("userExtensionRepository.box","box");
        $qb->select("DISTINCT(box.name) AS name");
        $qb->addSelect("box.slug AS slug");
        $qb->where($qb->expr()->in("userExtensionRepository.id",":ids"));
        $qb->setParameter("ids",$userExtensionIds);
        return $qb->getQuery()->getArrayResult();

    }
    public function findUserCompetitionBoxes(Competition $competition)
    {
        $qb=$this->createQueryBuilder("userExtensionRepository");
        $qb->leftJoin("userExtensionRepository.box","box");
        $qb->select("userExtensionRepository.user_id AS userId");
        $qb->addSelect("box.name AS name");
        $qb->where($qb->expr()->in("userExtensionRepository.user_id",":users_ids"));
        $qb->setParameter("users_ids",$this->getUsersId($competition));
        $query=$qb->getQuery();
        $query->useResultCache(true,60,"user_extension_repository_find_competition_boxes");
        return $query->getResult();
    }
    public function findCompetitionBoxes(Competition $competition){
        $qb=$this->createQueryBuilder("userExtensionRepository");
        $qb->leftJoin("userExtensionRepository.box","box");
        $qb->distinct(true);
        $qb->select("box.name");
        $qb->where($qb->expr()->in("userExtensionRepository.user_id",":users_ids"));
        $qb->setParameter("users_ids",$this->getUsersId($competition));
        return $qb->getQuery()->getResult();
    }
    private function getUsersId(Competition $competition){
        $usersIds=[];
        $users=$this->getEntityManager()->getRepository("BDSRWCategoryBundle:Category")->findUsersByCompetition($competition);
        foreach($users as $user){
            $usersIds[]=$user["user_id"];
        }
        return $usersIds;
    }
}