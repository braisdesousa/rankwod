<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 1:11 PM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionAdmin;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;

class CompetitionRepository extends EntityRepository
{
	public function findVisible(){
		$qb=$this->createQueryBuilder("competitionRepository");
		$qb->where($qb->expr()->eq("competitionRepository.publish",$qb->expr()->literal(true)));
		$qb->orderBy("competitionRepository.created","DESC");

		return $qb->getQuery()->getResult();
	}
	public function hasUser(Competition $competition,User $user){
		$qb=$this->createQueryBuilder("competitionRepository");
		$qb->select($qb->expr()->count("competitionRepository.id"));
		$qb->setMaxResults(1);
		$qb->leftJoin("competitionRepository.adminUsers","adminUsers");
		$qb->leftJoin("competitionRepository.judgeUsers","judgeUsers");
		$qb->leftJoin("competitionRepository.phases","phase");
		$qb->leftJoin("phase.categories","categories");
		$qb->leftJoin("categories.athletes","athletes");
		$qb->distinct();
		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq("competitionRepository.id",$qb->expr()->literal($competition->getId())),
				$qb->expr()->orX(
					$qb->expr()->eq("judgeUsers.user_id",$qb->expr()->literal($user->getId())),
					$qb->expr()->eq("adminUsers.user_id",$qb->expr()->literal($user->getId())),
					$qb->expr()->eq("athletes.user_id",$qb->expr()->literal($user->getId())))
			)
		);
		return $qb->getQuery()->getSingleScalarResult()&&1;
	}
	public function hasUserExtension(Competition $competition,UserExtension $user){
		$qb=$this->createQueryBuilder("competitionRepository");
		$qb->select($qb->expr()->count("competitionRepository.id"));
		$qb->setMaxResults(1);
		$qb->leftJoin("competitionRepository.adminUsers","adminUsers");
		$qb->leftJoin("competitionRepository.judgeUsers","judgeUsers");
		$qb->leftJoin("competitionRepository.phases","phase");
		$qb->leftJoin("phase.categories","categories");
		$qb->leftJoin("categories.athletes","athletes");
		$qb->leftJoin("athletes.team","team");
		$qb->leftJoin("team.athletes","team_athlete");
		$qb->distinct();
		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq("competitionRepository.id",$qb->expr()->literal($competition->getId())),
				$qb->expr()->orX(
					$qb->expr()->eq("judgeUsers.userExtension",$qb->expr()->literal($user->getId())),
					$qb->expr()->eq("adminUsers.userExtension",$qb->expr()->literal($user->getId())),
					$qb->expr()->eq("athletes.userExtension",$qb->expr()->literal($user->getId())),
					$qb->expr()->eq("team_athlete.userExtension",$qb->expr()->literal($user->getId())))
			)
		);
		return $qb->getQuery()->getSingleScalarResult()&&1;
	}
    public function findByUserExtension(UserExtension $user,$over=false)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->leftJoin("competitionRepository.adminUsers","adminUsers");
        $qb->leftJoin("competitionRepository.judgeUsers","judgeUsers");
        $qb->leftJoin("competitionRepository.phases","phase");
        $qb->leftJoin("phase.categories","categories");
        $qb->leftJoin("categories.athletes","athletes");
        $qb->leftJoin("athletes.team","teams");
        $qb->leftJoin("teams.athletes","team_athletes");
        $qb->distinct();
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("competitionRepository.isOver",$qb->expr()->literal($over)),
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->eq("judgeUsers.userExtension",$qb->expr()->literal($user->getId()))
                        ),
                    $qb->expr()->andX(
                        $qb->expr()->eq("adminUsers.userExtension",$qb->expr()->literal($user->getId())),
                        $qb->expr()->like("adminUsers.status",$qb->expr()->literal(CompetitionAdmin::STATUS_ACCEPTED)
                        )),
                    $qb->expr()->andX(
                        $qb->expr()->eq("competitionRepository.publish",$qb->expr()->literal(true)),
                        $qb->expr()->orX(
                            $qb->expr()->andX(
                                $qb->expr()->eq("athletes.userExtension",$qb->expr()->literal($user->getId())),
                                $qb->expr()->orX(
                                    $qb->expr()->like("athletes.status",$qb->expr()->literal(CategoryAthlete::STATUS_ACCEPTED)),
                                    $qb->expr()->like("athletes.status",$qb->expr()->literal(CategoryAthlete::STATUS_PENDING_EXTRA_DATA)),
                                    $qb->expr()->like("athletes.status",$qb->expr()->literal(CategoryAthlete::STATUS_PENDING_PAYMENT))
                                )

                            ),
                            $qb->expr()->eq("team_athletes.userExtension",$qb->expr()->literal($user->getId()))
                        )

                    )


                )));

        $qb->orderBy("competitionRepository.created","DESC");
        return $qb->getQuery()->getResult();
    }
    public function findByUser(User $user,$over=false)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->leftJoin("competitionRepository.adminUsers","adminUsers");
        $qb->leftJoin("competitionRepository.judgeUsers","judgeUsers");
        $qb->leftJoin("competitionRepository.phases","phase");
        $qb->leftJoin("phase.categories","categories");
        $qb->leftJoin("categories.athletes","athletes");
        $qb->distinct();
        $qb->where(
                $qb->expr()->andX(
                    $qb->expr()->eq("competitionRepository.isOver",$qb->expr()->literal($over)),
                    $qb->expr()->orX(
                        $qb->expr()->andX(
                        	$qb->expr()->eq("judgeUsers.user_id",$qb->expr()->literal($user->getId())),
                        	$qb->expr()->like("judgeUsers.status",$qb->expr()->literal(CompetitionJudge::STATUS_ACCEPTED)
                        )),
                        $qb->expr()->andX(
                            $qb->expr()->eq("adminUsers.user_id",$qb->expr()->literal($user->getId())),
	                        $qb->expr()->like("adminUsers.status",$qb->expr()->literal(CompetitionAdmin::STATUS_ACCEPTED)
                        )),
                        $qb->expr()->andX(
	                        $qb->expr()->eq("athletes.user_id",$qb->expr()->literal($user->getId())),
	                        $qb->expr()->like("athletes.status",$qb->expr()->literal(CategoryAthlete::STATUS_ACCEPTED))
                        )


        )));


        return $qb->getQuery()->getResult();
    }

    public function isUserAdmin(Competition $competition,UserInterface $user)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->join("competitionRepository.adminUsers","adminUsers");
        $qb->setMaxResults(1);
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("competitionRepository.id",$qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("adminUsers.user_id",$qb->expr()->literal($user->getId()))));

        return $qb->getQuery()->getOneOrNullResult()&&1;
    }
    public function isUserExtensionAdminOrJudge(Competition $competition,UserExtension $user)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->leftJoin("competitionRepository.adminUsers","adminUsers");
        $qb->leftJoin("adminUsers.userExtension","admin_extension");
        $qb->leftJoin("competitionRepository.judgeUsers","judgeUsers");
        $qb->leftJoin("judgeUsers.userExtension","judge_extension");
        $qb->setMaxResults(1);
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("competitionRepository.id",$qb->expr()->literal($competition->getId())),
            $qb->expr()->orX(
                $qb->expr()->eq("admin_extension.user_id",$qb->expr()->literal($user->getUserId())),
                $qb->expr()->eq("judge_extension.user_id",$qb->expr()->literal($user->getUserId()))))
        );

        return $qb->getQuery()->getOneOrNullResult()&&1;
    }
    public function isUserJudge(Competition $competition,UserExtension $userExtension)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->join("competitionRepository.judgeUsers","judgeUsers");
        $qb->setMaxResults(1);
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("competitionRepository.id",$qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("judgeUsers.userExtension",$qb->expr()->literal($userExtension->getId()))));

        return $qb->getQuery()->getOneOrNullResult()&&1;
    }

    public function findByAdminUser(UserInterface $userInterface)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->join("competitionRepository.adminUsers","adminUsers");
        $qb->where($qb->expr()->eq("adminUsers.user_id",$qb->expr()->literal($userInterface->getId())));
        $qb->orderBy("competitionRepository.created","DESC");
        return $qb->getQuery()->getResult();
    }
    public function findByJudgeUser(UserInterface $userInterface)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->join("competitionRepository.judgeUsers","judgeUsers");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("judgeUsers.user_id",$qb->expr()->literal($userInterface->getId())),
                $qb->expr()->isNull("competitionRepository.isOver")
            )
        );
        $qb->orderBy("competitionRepository.created","DESC");
        return $qb->getQuery()->getResult();
    }
    public function findByAthleteUser(UserInterface $userInterface,$status)
    {
        $qb=$this->createQueryBuilder("competitionRepository");
        $qb->join("competitionRepository.categories","categories");


        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->in("categories.id",$this->getEntityManager()->getRepository("BDSRWCategoryBundle:Category")->findByUserAndStatusQB($userInterface,$status)->getDQL()),
                $qb->expr()->isNull("competitionRepository.isOver")
            )
        );
        $qb->distinct(true);
        $qb->orderBy("competitionRepository.created","DESC");
        return $qb->getQuery()->getResult();
    }
}