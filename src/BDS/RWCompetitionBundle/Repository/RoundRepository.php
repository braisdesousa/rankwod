<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 3/23/16
 * Time: 11:27 AM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Round;
use Doctrine\ORM\EntityRepository;

class RoundRepository extends EntityRepository
{
    public function findByCompetition(Competition $competition){
        $qb=$this->createQueryBuilder("roundRepository");
        $qb->leftJoin("roundRepository.phase","phase");
        $qb->where($qb->expr()->eq("phase.competition",$qb->expr()->literal($competition->getId())));

        return $qb->getQuery()->getResult();
    }
    /**
     * @param Round $round
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAthletesInRoundQB(Round $round)
    {
        $qb=$this->createQueryBuilder("roundRepository");
        $qb->select("roundRepository.athletes");
        $qb->where($qb->expr()->eq("roundRepository.id",$qb->expr()->literal($round->getId())));
        return $qb;
    }

}