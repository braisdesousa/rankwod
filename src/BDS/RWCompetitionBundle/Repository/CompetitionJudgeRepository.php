<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 30/05/17
 * Time: 13:14
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\CompetitionJudge;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class CompetitionJudgeRepository extends EntityRepository  {

    public function findJudgeEvents(Competition $competition) {
        $qb=$this->createQueryBuilder("competitionJudge");
        $qb->leftJoin("competitionJudge.workouts","workouts");
        $qb->leftJoin("competitionJudge.userExtension","userExtension");
        $qb->select("workouts.id as result_id");
        $qb->addSelect("competitionJudge.id as judge_id");
        $qb->addSelect("userExtension.user_id as user_id");
        $qb->where($qb->expr()->eq("competitionJudge.competition",$qb->expr()->literal($competition->getId())));
        return $qb->getQuery()->getArrayResult();
    }
    public function countCorrectedJudgeEvents(Competition $competition) {
        $qb=$this->createQueryBuilder("competitionJudge");
        $qb->leftJoin("competitionJudge.workouts","workouts");
        $qb->leftJoin("competitionJudge.userExtension","userExtension");
        $qb->addSelect("competitionJudge.id as judge_id");
        $qb->addSelect("COUNT(workouts) as corrected");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("competitionJudge.competition",$qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("workouts.corrected",$qb->expr()->literal(true))
        ));
        $qb->groupBy("competitionJudge.id");
        return $qb->getQuery()->getArrayResult();
    }
    public function countPendingJudgeEvents(Competition $competition) {
        $qb=$this->createQueryBuilder("competitionJudge");
        $qb->leftJoin("competitionJudge.workouts","workouts");
        $qb->leftJoin("competitionJudge.userExtension","userExtension");
        $qb->addSelect("competitionJudge.id as judge_id");
        $qb->addSelect("COUNT(workouts) as pending");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("competitionJudge.competition",$qb->expr()->literal($competition->getId())),
            $qb->expr()->neq("workouts.corrected",$qb->expr()->literal(true))
        ));
        $qb->groupBy("competitionJudge.id");
        return $qb->getQuery()->getArrayResult();
    }
    public function findByEvent(Event $event){
        $qb=$this->createQueryBuilder("competitionJudge");
        $qb->leftJoin("competitionJudge.userExtension","userExtension");
        $qb->leftJoin("competitionJudge.workouts","workout_result");
        $qb->leftJoin("workout_result.athlete","athlete");
        $qb->leftJoin("workout_result.event","event");
        $qb->select("competitionJudge.id as judge_id");
        $qb->addSelect("userExtension.user_id as user_id");
        $qb->addSelect("athlete.id as athlete_id");
        $qb->where(
            $qb->expr()->eq("workout_result.event",$qb->expr()->literal($event->getId()))
        );

       return $qb->getQuery()->getResult();

    }
    public function findByCompetition(Competition $competition){

        $qb=$this->createQueryBuilder("competitionJudge");
        $qb->leftJoin("competitionJudge.userExtension","userExtension");
        $qb->leftJoin("userExtension.box","box");
        $qb->select("competitionJudge.status");
        $qb->addSelect("box.name as box_name");
        $qb->addSelect("userExtension.user_id");
        $qb->addSelect("competitionJudge.id as judge_id");
        $qb->where($qb->expr()->eq("competitionJudge.competition",$qb->expr()->literal($competition->getId())));
        return $qb->getQuery()->getArrayResult();
    }
	public function countByUserAndType(User $user, $type){
		$qb=$this->getByUserAndTypeQB($user,$type);
		$qb->select($qb->expr()->count("competitionJudge"));
		return $qb->getQuery()->getSingleScalarResult();
	}
	public function getByUserAndType(User $user, $type){

		$qb=$this->getByUserAndTypeQB($user,$type);
		$qb->leftJoin("competitionJudge.competition","competition");
		$qb->orderBy("competition.created","DESC");
		return $qb->getQuery()->getResult();
	}
	private function getByUserAndTypeQB(User $user,$type)
	{
		$qb=$this->createQueryBuilder("competitionJudge");
		$qb->where($qb->expr()->andX(
			$qb->expr()->eq("competitionJudge.user_id",$qb->expr()->literal($user->getId())),
			$qb->expr()->like("competitionJudge.status",$qb->expr()->literal($type))
		));
		return $qb;
	}
	public function findOneByCompetitionAndUserExtension(Competition $competition,UserExtension $user){
		$qb=$this->createQueryBuilder("competitionJudge");
		$qb->setMaxResults(1);
		$qb->where($qb->expr()->andX(
			$qb->expr()->eq("competitionJudge.competition",$qb->expr()->literal($competition->getId())),
			$qb->expr()->eq("competitionJudge.userExtension",$qb->expr()->literal($user->getId()))
		));
		return $qb->getQuery()->getOneOrNullResult();
	}
	public function findByCompetitionAndStatus(Competition $competition,$status){
		$qb=$this->createQueryBuilder("competitionJudge");
		$qb->where($qb->expr()->andX(
			$qb->expr()->eq("competitionJudge.competition",$qb->expr()->literal($competition->getId())),
			$qb->expr()->like("competitionJudge.status",$qb->expr()->literal($status))
		));
		return $qb->getQuery()->getResult();

	}
}