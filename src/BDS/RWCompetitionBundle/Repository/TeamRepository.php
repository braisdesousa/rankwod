<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 3/23/16
 * Time: 11:26 AM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class TeamRepository extends EntityRepository
{


    /**
     * @param UserExtension $extension
     * @param Competition   $competition
     *
     * @return array | Team[]
     */
    public function findByUserExtensionAndCompetition(UserExtension $extension,Competition $competition): array {
        $qb=$this->createQueryBuilder("team");
        $qb->leftJoin("team.athletes","athlete");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("team.competition",$qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("athlete.userExtension",$qb->expr()->literal($extension->getId()))
        ));

        return $qb->getQuery()->getResult();
    }
    public function findByNameAndCompetition(string $name, Competition $competition){
        $qb=$this->createQueryBuilder("team");
        $qb->select($qb->expr()->count("team"));
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("team.name",$qb->expr()->literal($name)),
                $qb->expr()->eq("team.competition",$qb->expr()->literal($competition->getId()))
            )
            );
        return $qb->getQuery()->getSingleScalarResult();
    }
    public function findByRegexAndCompetition(string $name, Competition $competition){
        $name=trim($name);
        $name=str_replace(" ","%",$name);
        $qb=$this->createQueryBuilder("team");
        $qb->select($qb->expr()->count("team"));
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("team.name",$qb->expr()->literal($name)),
                $qb->expr()->eq("team.competition",$qb->expr()->literal($competition->getId()))
            )
            );
        return $qb->getQuery()->getResult();
    }
    public function findByTextQB($text){
        $text=sprintf("%s%s%s","%",$text,"%");
        $text=str_replace(" ","%",$text);
        $qb=$this->createQueryBuilder("teamRepository");
        $qb->where($qb->expr()->orX(
            $qb->expr()->like("teamRepository.name",":text"),
            $qb->expr()->like("teamRepository.text",":text")
        ));
        $qb->setParameter("text",$text,Type::STRING);
        return $qb;
    }
    public function findAthleteTeams($user_id)
    {
        $qb=$this->createQueryBuilder("teamRepository");
        $qb->leftJoin("teamRepository.athletes","athlete");
        $qb->where($qb->expr()->andX(
            $qb->expr()->isNull("teamRepository.competition"),
            $qb->expr()->eq("athlete.user_id",$qb->expr()->literal($user_id))
        ));
        $qb->orderBy("teamRepository.updated","DESC");
        return $qb->getQuery()->getResult();
    }
    public function findAthleteCompetitionTeams($user_id)
    {
        $qb=$this->createQueryBuilder("teamRepository");
        $qb->leftJoin("teamRepository.athletes","athlete");
        $qb->where($qb->expr()->andX(
            $qb->expr()->isNotNull("teamRepository.competition"),
            $qb->expr()->eq("athlete.user_id",$qb->expr()->literal($user_id))
        ));
        $qb->orderBy("teamRepository.updated","DESC");
        return $qb->getQuery()->getResult();
    }

}