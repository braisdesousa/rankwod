<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 3/23/16
 * Time: 11:26 AM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Overall;
use BDS\RWCompetitionBundle\Entity\OverallResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class OverallResultRepository extends EntityRepository
{


    /**
     * @param Category $category
     * @param Overall $overall
     * @return OverallResult[]
     */
    public function findByCategoryAndOverallOrderByResult(Category $category,Overall $overall,$punishIncomplete=false)
    {
        $qb=$this->createQueryBuilder("overallResultRepository");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("overallResultRepository.category",$qb->expr()->literal($category->getId())),
            $qb->expr()->eq("overallResultRepository.overall",$qb->expr()->literal($overall->getId()))
        ));
        if($punishIncomplete){
            $qb->orderBy("overallResultRepository.incomplete","ASC");
            $qb->addOrderBy("overallResultRepository.result","ASC");
            $qb->addOrderBy("overallResultRepository.tieBreaker","DESC");
        } else {
            $qb->orderBy("overallResultRepository.result","ASC");
            $qb->addOrderBy("overallResultRepository.tieBreaker","DESC");
        }
        return $qb->getQuery()->getResult();
    }
    /**
     * @param Category $category
     * @param Overall $overall
     * @return OverallResult[]
     */
    public function findByCategoryAndOverallOrderByResultSerializable(Category $category,Overall $overall,$punishIncomplete=false)
    {
        $qb=$this->createQueryBuilder("overallResultRepository");
        $qb->leftJoin("overallResultRepository.athlete","athlete");
        $qb->leftJoin("athlete.team","team");
        $qb->leftJoin("athlete.bot","bot");
        $qb->leftJoin("athlete.userExtension","user_extension");
        $qb->leftJoin("user_extension.box","box");


        $qb->addSelect("box.slug AS box_slug");
        $qb->addSelect("box.name AS box_name");
        $qb->addSelect("bot.name AS athlete_name");
        $qb->addSelect("bot.slug AS athlete_slug");
        $qb->addSelect("user_extension.user_id AS user_id");
        $qb->addSelect("athlete.id AS athlete_id");
        $qb->addSelect("overallResultRepository.result AS result");
        $qb->addSelect("team.name AS team_name");
        $qb->addSelect("team.slug AS team_slug");
        $qb->addSelect("overallResultRepository.incomplete AS incomplete");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("overallResultRepository.category",$qb->expr()->literal($category->getId())),
            $qb->expr()->eq("overallResultRepository.overall",$qb->expr()->literal($overall->getId()))
        ));
        if($punishIncomplete){
            $qb->orderBy("overallResultRepository.incomplete","ASC");
            $qb->addOrderBy("overallResultRepository.result","ASC");
            $qb->addOrderBy("overallResultRepository.tieBreaker","DESC");
        } else {
            $qb->orderBy("overallResultRepository.result","ASC");
            $qb->addOrderBy("overallResultRepository.tieBreaker","DESC");
        }

        return $qb->getQuery()->getResult();
    }
    /**
     * @param User $user
     * @param Phase $phase
     * @return OverallResult|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByAthleteAndPhase(CategoryAthlete $athlete, Phase $phase)
    {
        $qb=$this->createQueryBuilder("overallResultRepository");
        $qb->leftJoin("overallResultRepository.overall","overall");
        $qb->leftJoin("overall.phase","phase");
        $qb->leftJoin("overallResultRepository.athlete","athlete");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("athlete.id",$qb->expr()->literal($athlete->getId())),
                $qb->expr()->eq("phase.id",$qb->expr()->literal($phase->getId()))
            ));
            
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findOrderedByResult(Category $category){
        $qb=$this->createQueryBuilder("overallResultRepository");
        $qb->where($qb->expr()->eq("overallResultRepository.category",$qb->expr()->literal($category->getId())));
        $qb->orderBy("overallResultRepository.result","ASC");
        $qb->addOrderBy("overallResultRepository.tieBreaker","DESC");
        return $qb->getQuery()->getResult();
    }
   public function findDuplicatedValuesByCategoryAndOverall(Category $category, Overall $overall)
   {
       $qb=$this->createQueryBuilder("overallResultRepository");
       $qb->where(
           $qb->expr()->andX(
               $qb->expr()->eq("overallResultRepository.category",$qb->expr()->literal($category->getId())),
               $qb->expr()->eq("overallResultRepository.overall",$qb->expr()->literal($overall->getId())),
               $qb->expr()->in("overallResultRepository.result",$this->findRepeatedValuesByCategoryAndOverallQB($category,$overall)->getDQL())

           ));
       $qb->orderBy("overallResultRepository.result","ASC");
       return $qb->getQuery()->getResult();
   }
    private function findRepeatedValuesByCategoryAndOverallQB(Category $category, Overall $overall){
        $qb=$this->createQueryBuilder("overallResultRepository_b");
        $qb->select("overallResultRepository_b.result");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("overallResultRepository_b.category",$qb->expr()->literal($category->getId())),
                $qb->expr()->eq("overallResultRepository_b.overall",$qb->expr()->literal($overall->getId()))

            ));
        $qb->groupBy("overallResultRepository_b.result");
        $qb->addGroupBy("overallResultRepository_b.tieBreaker");
        $qb->having("COUNT(overallResultRepository_b.id)>1");
        $qb->orderBy("overallResultRepository_b.result","ASC");
        return $qb;
    }

}