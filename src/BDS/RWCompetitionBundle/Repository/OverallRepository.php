<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 3/23/16
 * Time: 11:26 AM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Overall;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class OverallRepository extends EntityRepository
{
    public function countUpdatedResultAfterCacheCreationByEvent(\DateTime $dateTime, Overall $overall){
     
        $qb=$this->createQueryBuilder("overallRepository");
        $qb->select($qb->expr()->count("overallRepository"));
        $qb->leftJoin("overallRepository.results","results");
        $qb->where($qb->expr()->andX(
                $qb->expr()->eq("overallRepository.id",$qb->expr()->literal($overall->getId())),
                $qb->expr()->gt("results.updated",":dateTime"))
        );
        $qb->setParameter("dateTime",$dateTime,Type::DATETIME );
        return $qb->getQuery()->getSingleScalarResult();
    }

}