<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 1:11 PM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Competition;
use Doctrine\ORM\EntityRepository;

class InvitationRepository extends EntityRepository
{
    public function findByCompetition(Competition $competition)
    {
        $qb=$this->createQueryBuilder("invitationRepository");
        $qb->leftJoin("invitationRepository.phase","phase");
        $qb->where($qb->expr()->eq("phase.competition",$qb->expr()->literal($competition->getId())));
        return $qb->getQuery()->getResult();
    }
}