<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 3/23/16
 * Time: 11:26 AM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCompetitionBundle\Entity\Competition;
use Doctrine\ORM\EntityRepository;

class BotRepository extends EntityRepository
{
    public function findOneBySlugAndCompetition(Competition $competition,$bot_slug)
    {
	    $qb=$this->createQueryBuilder("botRepository");
	    $qb->where($qb->expr()->andX(
	    	$qb->expr()->eq("botRepository.competition",$qb->expr()->literal($competition->getId())),
	    	$qb->expr()->eq("botRepository.slug",$qb->expr()->literal($bot_slug))
	    ));
	    return $qb->getQuery()->getOneOrNullResult();
    }

}