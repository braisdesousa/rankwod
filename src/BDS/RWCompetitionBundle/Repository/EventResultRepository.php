<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 1:11 PM
 */

namespace BDS\RWCompetitionBundle\Repository;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Round;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Gedmo\Sortable\Entity\Repository\SortableRepository;

class EventResultRepository extends EntityRepository
{
    public function findByIds(array $ids){
        $qb=$this->createQueryBuilder("event_result");
        $qb->where($qb->expr()->in("event_result.id",":array_ids"));
        $qb->setParameter("array_ids",$ids);
        return $qb->getQuery()->getResult();
    }
    public function findEventsForJudging(Event $event, $athletes){

        $qb=$this->createQueryBuilder("event_result");
        /** @var CategoryAthlete $athlete */
        $athleteIds=[];
        foreach($athletes as $athlete){
            $athleteIds[]=$athlete->getId();
        }
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("event_result.event",$qb->expr()->literal($event->getId())),
                $qb->expr()->in("event_result.athlete",":athlete_ids")
            )
        );
        $qb->groupBy("event_result.athlete");
        $qb->setParameter("athlete_ids",$athleteIds);
        return $qb->getQuery()->getResult();
    }
    /**
     * @param CategoryAthlete $categoryAthlete
     * @param Phase $phase
     *
     * @return EventResult[]
     */
    public function findByAthleteAndPhase( CategoryAthlete $categoryAthlete, Phase $phase){
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.event","event");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("event.phase",$qb->expr()->literal($phase->getId())),
                $qb->expr()->eq("eventResultRepository.athlete",$qb->expr()->literal($categoryAthlete->getId()))
                
            ));
        return $qb->getQuery()->getResult();
    }

    public function findByRound(Round $round,Event $event, $eventName)
    {
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.athlete","athlete");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("eventResultRepository.eventName",$qb->expr()->literal($eventName)),
                $qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId())),
                $qb->expr()->in("athlete",$this->getEntityManager()->getRepository("BDSRWCompetitionBundle:Round")->findAthletesInRoundQB($round)->getDQL())
            ));
        return $qb->getQuery()->getResult();
    }
    public function findByAthleteEventAndEventNameOrderByMeasure( CategoryAthlete $athlete,Event $event,$eventName)
    {
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.measure","measure");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId())),
            $qb->expr()->eq("eventResultRepository.eventName",$qb->expr()->literal($eventName)),
            $qb->expr()->eq("eventResultRepository.athlete",$qb->expr()->literal($athlete->getId()))
        ));
        $qb->orderBy("measure.position","ASC");
        return $qb->getQuery()->getResult();
    }
    public function findByAthleteAndEventOrderByMeasure( CategoryAthlete $athlete,Event $event)
    {
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.measure","measure");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId())),
            $qb->expr()->eq("eventResultRepository.athlete",$qb->expr()->literal($athlete->getId()))
        ));
        $qb->orderBy("measure.position","ASC");
        return $qb->getQuery()->getResult();
    }
    public function findByEventCategoryAndMeasureOrderByResult(Event $event,Category $category,Measure $measure,$orderBy){
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.athlete","athlete");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("eventResultRepository.measure",$qb->expr()->literal($measure->getId())),
            $qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId())),
            $qb->expr()->eq("athlete.category",$qb->expr()->literal($category->getId()))
        ));
        $qb->orderBy("eventResultRepository.result",$orderBy);
        $qb->addOrderBy("eventResultRepository.tieBreaker","DESC");
        return $qb->getQuery()->getResult();
    }
    public function findByEventCategoryAndMeasureOrderByResultWithUserId(Event $event,Category $category,Measure $measure,$orderBy){
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.athlete","athlete");
        $qb->leftJoin("athlete.bot","bot");
        $qb->leftJoin("athlete.userExtension","user_extension");
        $qb->leftJoin("user_extension.box","box");
        $qb->leftJoin("athlete.team","team");
        $qb->leftJoin("eventResultRepository.measure","measure");
        $qb->select("measure.type");
        $qb->addSelect("eventResultRepository.id AS result_id");
        $qb->addSelect("athlete.id AS athlete_id");
        $qb->addSelect("eventResultRepository.videoUrl AS video");
        $qb->addSelect("eventResultRepository.corrected AS corrected");
        $qb->addSelect("user_extension.user_id AS user_id");
        $qb->addSelect("box.slug AS box_slug");
        $qb->addSelect("box.name AS box_name");
        $qb->addSelect("bot.name AS athlete_name");
        $qb->addSelect("bot.slug AS athlete_slug");
        $qb->addSelect("eventResultRepository.result AS result");
        $qb->addSelect("eventResultRepository.tieBreaker AS tie_breaker");
        $qb->addSelect("team.name AS team_name");
        $qb->addSelect("team.slug AS team_slug");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("eventResultRepository.measure",$qb->expr()->literal($measure->getId())),
            $qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId())),
            $qb->expr()->eq("athlete.category",$qb->expr()->literal($category->getId()))
        ));
        $qb->orderBy("eventResultRepository.result",$orderBy);
        $qb->addOrderBy("eventResultRepository.tieBreaker","DESC");
        return $qb->getQuery()->getResult();
    }
    public function findByEventCategoryOrderByResultGroupByEventName(Event $event,Category $category, $eventName){
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.athlete","athlete");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId())),
            $qb->expr()->eq("eventResultRepository.eventName",$qb->expr()->literal($eventName)),
            $qb->expr()->eq("athlete.category",$qb->expr()->literal($category->getId()))
        ));
        $qb->orderBy("eventResultRepository.position","ASC");
        $qb->addOrderBy("eventResultRepository.tieBreaker","DESC");
        return $qb->getQuery()->getResult();
    }
    public function findUsersInEventWithResultsQB(Event $event){
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.athlete","athlete");
        $qb->select("athlete.id");
        $qb->where($qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId())));
        $qb->groupBy("athlete");
        return $qb;
    }
    public function findUsersInEventNameWithResultsQB($eventName){
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.athlete","athlete");
        $qb->select("athlete.id");
        $qb->where($qb->expr()->eq("eventResultRepository.eventName",$qb->expr()->literal($eventName)));
        $qb->groupBy("athlete");
        return $qb;
    }
    public function findUsersByEventNameAndEventWithResultsQB($eventName,Event $event){
        $qb=$this->createQueryBuilder("eventResultRepository");
        $qb->leftJoin("eventResultRepository.athlete","athlete");
        $qb->select("athlete.id");
        $qb->where(
                $qb->expr()->andX(
                    $qb->expr()->eq("eventResultRepository.eventName",$qb->expr()->literal($eventName)),
                    $qb->expr()->eq("eventResultRepository.event",$qb->expr()->literal($event->getId()))
                    ));
        $qb->groupBy("athlete");
        return $qb;
    }
}