<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\CoreBundle\Entity\AbstractTextBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Phase
 *
 * @ORM\Table(name="competition_phase_overall")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\OverallRepository")
 */
class Overall extends AbstractNamedBaseEntity implements PublishedInterface
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var Phase
     * @ORM\OneToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Phase",mappedBy="overall")
     */
    private $phase;

    /**
     * @var ArrayCollection|OverallResult[];
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\OverallResult",cascade={"all"},mappedBy="overall",fetch="EXTRA_LAZY")
     * @ORM\OrderBy(value={"result"="ASC"})
     */
    private $results;

    /**
     * @var \DateTime $publishDate
     * @ORM\Column(name="publish_at",type="datetime")
     * @Groups({"front_result"})
     */
    private $publishedAt;
    


    public function __construct(Phase $phase)
    {
        $this->measureGroups=new ArrayCollection();
        $this->results=new ArrayCollection();
        $this->publishedAt= new \DateTime();
        $this->phase=$phase;
        $this->name="Overall";
    }

    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Phase
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * @param Phase $phase
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;
    }

    /**
     * @return OverallResult[]|ArrayCollection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param OverallResult[]|ArrayCollection $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }
    public function addResult(OverallResult $result){
        if(!$this->results->contains($result)){
            $this->results->add($result);
        }
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        $now=new \DateTime();
        return $now>$this->publishedAt;
    }
    /**
     *
     */
    public function publish()
    {
        $this->publishedAt=new \DateTime();
    }
    
}

