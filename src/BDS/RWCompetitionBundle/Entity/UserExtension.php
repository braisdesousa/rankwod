<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\RWBoxBundle\Entity\Box;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User Extension
 *
 * @ORM\Table(name="user_extension")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\UserExtensionRepository")
 * @UniqueEntity("user_id")
 */
class UserExtension extends AbstractBaseEntity implements RoninFoxUserInstance
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Box
     * @ORM\ManyToOne(targetEntity="BDS\RWBoxBundle\Entity\Box",fetch="EAGER", inversedBy="users")
     * @ORM\JoinColumn(name="box_id", referencedColumnName="id", nullable=true)
     */
    private $box;
    /**
     * @var integer
     * @ORM\Column(name="user_id",type="integer",nullable=false, unique=true)
     */
    private $user_id;

    /**
     * @var File
     * @ORM\ManyToOne(targetEntity="BDS\GaufretteBundle\Entity\File",fetch="EAGER")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
     */
    private $image;

    /**
     * @var string
     * @ORM\Column(name="tshirt_size",type="string",nullable=true)
     */
    private $tShirtSize;
    /**
     * @var User
     */
    private $roninFoxUser;

    /**
     * @var string
     * @ORM\Column(name="extra_data",type="text",nullable=true)
     */
    private $extraData;

    public function __construct($user_id)
    {
        $this->user_id=$user_id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return Box
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param Box $box
     */
    public function setBox($box)
    {
        $this->box = $box;
    }
    public function getBoxName():string {
        return $this->box?$this->box->getName():"";
    }
    /**
     * @return File
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param File $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return User
     */
    public function getRoninFoxUser()
    {
        return $this->roninFoxUser;
    }

    /**
     * @param User $roninFoxUser
     */
    public function setRoninFoxUser(User $roninFoxUser)
    {
        $this->roninFoxUser = $roninFoxUser;
    }

    /**
     * @return string
     */
    public function getTShirtSize():? string
    {
        return $this->tShirtSize;
    }

    /**
     * @param string $tShirtSize
     */
    public function setTShirtSize(string $tShirtSize): void
    {
        $this->tShirtSize = $tShirtSize;
    }

    /**
     * @return string
     */
    public function getExtraData():? string
    {
        return $this->extraData;
    }

    /**
     * @param string $extraData
     */
    public function setExtraData(string $extraData): void
    {
        $this->extraData = $extraData;
    }
    public function isCompleted():bool{
        $user=$this->getRoninFoxUser();
        return
                $this->getTShirtSize() &&
                $user->getFirstName() &&
                $user->getLastName() &&
                $user->getDni() &&
                $user->getTelephone() &&
                $user->getDateBirth();
    }
    public function isUserCompleted():bool{
        $user=$this->getRoninFoxUser();
        return
            $user->getFirstName() &&
            $user->getLastName() &&
            $user->getDni() &&
            $user->getTelephone() &&
            $user->getDateBirth();
    }
}

