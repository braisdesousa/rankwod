<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\CoreBundle\Entity\AbstractTextBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use JMS\Serializer\Annotation\Groups;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\TeamRepository")
 */
class Team extends AbstractTextBaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
     /**
     * @var array
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\TeamAthlete",mappedBy="team",cascade={"all"})
     */
    private $athletes;

    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Competition")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id")
     */
    private $competition;
    /**
     *
     */
    public function __construct()
    {
        $this->athletes=new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TeamAthlete[]|ArrayCollection
     */
    public function getAthletes()
    {
        return $this->athletes;
    }

    /**
     * @param array $athletes
     */
    public function setAthletes($athletes)
    {
        $this->athletes = $athletes;
    }
    public function addAthlete(TeamAthlete $categoryAthlete)
    {
        if($this->athletes->contains($categoryAthlete)){
            $this->athletes->add($categoryAthlete);
        }
    }
    public function removeAthlete(TeamAthlete $categoryAthlete)
    {
        if($this->athletes->contains($categoryAthlete)){
            $this->athletes->removeElement($categoryAthlete);
        }
    }

    /**
     * @return mixed
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param mixed $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

}

