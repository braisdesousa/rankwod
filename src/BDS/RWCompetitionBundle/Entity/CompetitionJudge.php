<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;

/**
 * Competition
 *
 * @ORM\Table(name="competition_judges")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\CompetitionJudgeRepository")
 */
class CompetitionJudge
{
    const STATUS_PENDING = "PENDING";
    const STATUS_ACCEPTED = "ACCEPTED";
    const STATUS_DECLINED = "DECLINED";
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Competition", inversedBy="judgeUsers",cascade={"persist"})
     */
    private $competition;
    /**
     * @var Competition
     * @ORM\Column(name="user_id",type="integer",nullable=false)
     */
    private $user_id;

    /**
     * @var string
     * @ORM\Column(name="status",type="string",length=50)
     */
    private $status;

    /**
     * @var ArrayCollection|EventResult
     * @ORM\ManyToMany(targetEntity="BDS\RWCompetitionBundle\Entity\EventResult")
     * @ORM\JoinTable(name="judge_workouts",
     *      joinColumns={@ORM\JoinColumn(name="judge_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="workout_id", referencedColumnName="id")}
     *      )
     */
    private $workouts;

    use UserExtensionTrait;

    public function __construct(Competition $competition=null,$user_id=null)
    {
        $this->status=self::STATUS_PENDING;
        $this->competition=$competition;
        $this->user_id=$user_id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }
    /**
     * @param string $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return User
     */
    public function getRoninFoxUser()
    {
        return $this->getUserExtension()->getRoninFoxUser();
    }


	public function hasAccepted(){
    	return $this->status===self::STATUS_ACCEPTED;
	}

    /**
     * @return EventResult|ArrayCollection
     */
    public function getWorkouts()
    {
        return $this->workouts;
    }
    public function getName(){
        $this->userExtension->getRoninFoxUser()->getCompleteName();
    }
    /**
     * @param EventResult|ArrayCollection $workouts
     */
    public function setWorkouts($workouts): void
    {
        $this->workouts = $workouts;
    }
    public function addWorkout(EventResult $eventResult): void{
        if(!$this->workouts->contains($eventResult)){
            $this->workouts->add($eventResult);
        }
    }
    public function removeWorkout(EventResult $eventResult): void{
        if($this->workouts->contains($eventResult)){
            $this->workouts->removeElement($eventResult);
        }
    }
    public function clearWorkouts(){

        $this->workouts->clear();
    }

}

