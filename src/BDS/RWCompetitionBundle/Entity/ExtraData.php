<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraData
 *
 * @ORM\Table(name="extra_data")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\ExtraDataRepository")
 */
class ExtraData  extends AbstractNamedBaseEntity
{
    const TYPE_STRING="string";
    const TYPE_INT="integer";
    const TYPE_BOOL="bool";
    const TYPE_KG="kg";
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Competition",inversedBy="extraDataCollection")
     */
    private $competition;
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set type.
     *
     * @param string $type
     *
     * @return ExtraData
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Competition
     */
    public function getCompetition(): Competition
    {
        return $this->competition;
    }

    /**
     * @param Competition $competition
     */
    public function setCompetition(Competition $competition): void
    {
        $this->competition = $competition;
    }

}
