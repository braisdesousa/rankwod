<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;

/**
 * Competition
 *
 * @ORM\Table(name="competition_admins")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\CompetitionAdminRepository")
 */
class CompetitionAdmin
{
    const STATUS_PENDING = "PENDING";
    const STATUS_ACCEPTED = "ACCEPTED";
    const STATUS_DECLINED = "DECLINED";
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Competition", inversedBy="adminUsers",cascade={"persist"})
     */
    private $competition;
    /**
     * @var Competition
     * @ORM\Column(name="user_id",type="integer",nullable=false)
     */
    private $user_id;

    /**
     * @var string
     * @ORM\Column(name="status",type="string",length=50)
     */
    private $status;

    /**
     * @var User
     */
    private $roninFoxUser;
    use UserExtensionTrait;

    public function __construct()
    {
        $this->status=self::STATUS_PENDING;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }
    /**
     * @param string $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return User
     */
    public function getRoninFoxUser()
    {
        return $this->roninFoxUser;
    }

    /**
     * @param User $roninFoxUser
     */
    public function setRoninFoxUser(User $roninFoxUser)
    {
        $this->roninFoxUser = $roninFoxUser;
    }

	/**
	 * @return string
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus( $status ) {
		$this->status = $status;
	}
	public function hasAccepted()
	{
		return $this->status===self::STATUS_ACCEPTED;
	}



}

