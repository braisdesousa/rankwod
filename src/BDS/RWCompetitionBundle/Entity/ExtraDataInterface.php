<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 26/10/18
 * Time: 10:36
 */

namespace BDS\RWCompetitionBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

interface ExtraDataInterface
{


    public function addExtraData(AthleteExtraData $athleteExtraData):void;
    public function removeExtraData(AthleteExtraData $athleteExtraData):void;
    public function hasExtraData(ExtraData $extraData):bool;
    public function getExtraDataValue(ExtraData $extraData):?string;
    public function getExtraDataCollection(): Collection;
    public function getExtraData(ExtraData $extraData):? AthleteExtraData;


}