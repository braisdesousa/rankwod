<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 22/02/18
 * Time: 23:08
 */

namespace BDS\RWCompetitionBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


trait UserExtensionTrait
{
    /**
     *  @var  UserExtension
     *  @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\UserExtension")
     */
    protected $userExtension;

    public function setUserExtension(UserExtension $extension):void{
        $this->userExtension=$extension;
    }
    public function getUserExtension():?UserExtension
    {
        return $this->userExtension;
    }
    public function getUserId(){
        return $this->userExtension->getUserId();
    }
    public function getOldUserId(){
        return $this->user_id;
    }
    public function getName(){
        return $this->userExtension->getRoninFoxUser()->getCompleteName();
    }
    public function getUsername(){
        return $this->userExtension->getRoninFoxUser()->getUsername();
    }
    public function getBox()
    {
        return $this->userExtension->getBox()?$this->userExtension->getBox()->getName():"Independiente";
    }
    public function getCompleteName(){
        return $this->userExtension->getRoninFoxUser()->getCompleteName();
    }
}