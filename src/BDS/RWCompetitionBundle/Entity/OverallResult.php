<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Phase
 *
 * @ORM\Table(name="competition_phase_overall_result")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\OverallResultRepository")
 * @UniqueEntity(fields={"user_id","category","overall"},errorPath="user_id",message="YA existe un resultado de Overall para este Usuario")
 */
class OverallResult extends AbstractBaseEntity
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var integer;
     * @ORM\Column(name="result",type="integer",nullable=false)
     */
    private $result;
    /**
     * @var boolean;
     * @ORM\Column(name="incomplete",type="boolean",nullable=false)
     */
    private $incomplete;
    /**
     * @var integer;
     * @ORM\Column(name="tie_breaker",type="integer",nullable=false)
     */
    private $tieBreaker;
    /**
     * @var CategoryAthlete
     * @ORM\ManyToOne(targetEntity="BDS\RWCategoryBundle\Entity\CategoryAthlete",inversedBy="results",fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="athlete_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $athlete;
    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="BDS\RWCategoryBundle\Entity\Category")
     */
    private $category;
    /**
     * @var Overall
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Overall",inversedBy="results",cascade={"all"})
     * @ORM\JoinColumn(name="overall_id", referencedColumnName="id",nullable=false)
     */
    private $overall;


    public function __construct(Category $category,Overall $overall,CategoryAthlete $athlete,$result,$incomplete=false)
    {
        $this->overall=$overall;
        $this->category=$category;
        $this->athlete=$athlete;
        $this->result=$result;
        $this->incomplete=$incomplete;
        $this->tieBreaker=0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


    /**
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param int $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return CategoryAthlete
     */
    public function getAthlete()
    {
        return $this->athlete;
    }

    /**
     * @param CategoryAthlete $athlete
     */
    public function setAthlete($athlete)
    {
        $this->athlete = $athlete;
    }
    

    /**
     * @return Overall
     */
    public function getOverall()
    {
        return $this->overall;
    }

    /**
     * @param Overall $overall
     */
    public function setOverall($overall)
    {
        $this->overall = $overall;
    }

    /**
     * @return int
     */
    public function getTieBreaker()
    {
        return $this->tieBreaker;
    }

    /**
     * @param int $tieBreaker
     */
    public function setTieBreaker($tieBreaker)
    {
        $this->tieBreaker = $tieBreaker;
    }
    public function tieBreakerUp()
    {
        $this->tieBreaker++;
    }
    public function tieBreakerDown()
    {
        if($this->tieBreaker>0){
            $this->tieBreaker--;
        }
    }

    /**
     * @return boolean
     */
    public function isIncomplete()
    {
        return $this->incomplete;
    }

    /**
     * @param boolean $incomplete
     */
    public function setIncomplete($incomplete)
    {
        $this->incomplete = $incomplete;
    }
    
}

