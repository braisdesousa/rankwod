<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWPaymentBundle\Entity\Order;
use BDS\RWPaymentBundle\Entity\WithOrderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Phase
 *
 * @ORM\Table(name="competition_phase")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\PhaseRepository")
 */
class Phase extends AbstractNamedBaseEntity implements PublishedInterface,WithOrderInterface
{
    const TIE_RESOLUTION_MANUAL= "TIE_RESOLUTION_MANUAL";
    const TIE_RESOLUTION_AUTO= "TIE_RESOLUTION_AUTO";
    
    const PUBLISH_RESULT_MANUAL= "PUBLISH_RESULT_MANUAL";
    const PUBLISH_RESULT_EVENT_EXPIRATION= "PUBLISH_RESULT_EVENT_EXPIRATION";
    const PUBLISH_RESULT_INSTANT= "PUBLISH_RESULT_INSTANT";
    
    const DESERTED_RESULT_GAMES= "GAMES";
    const DESERTED_RESULT_COMPLETE= "COMPLETE";

    const TYPE_ONLINE = "TYPE_ONLINE";
    const TYPE_ATTENDANCE= "TYPE_ATTENDANCE";
    const TYPE_INTERNAL= "TYPE_INTERNAL";

    const TYPE_RESULT_POINTS = "TYPE_RESULT_POINTS";
    const TYPE_RESULT_POSITION="TYPE_RESULT_POSITION";

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Competition",inversedBy="phases")
     */
    private $competition;
    /**
     * @var ArrayCollection|Event[]
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\Event",mappedBy="phase",cascade={"all"})
     */
    private $events;
    /**
     * @var ArrayCollection|Round[]
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\Round",mappedBy="phase",cascade={"all"})
     */
    private $rounds;
    /**
     * @var Overall
     * @ORM\OneToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Overall",inversedBy="phase",cascade={"all"})
     * @ORM\JoinColumn(name="overall_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
     */
    private $overall;

    /**
     * @var \DateTime
     * @ORM\Column(name="published_at",type="datetime",nullable=false)
     * $property=sprintf("%s_0",$property);
     */
    private $publishedAt;
    /**
     * @var ArrayCollection|Category[]
     * @ORM\OneToMany(targetEntity="BDS\RWCategoryBundle\Entity\Category",mappedBy="phase",orphanRemoval=true,cascade={"all"})
     * @Groups({"front_result"})
     */
    private $categories;
    /**
     * @var boolean
     * @ORM\Column(name="requires_video",type="boolean",nullable=false)
     */
    private $requiresVideo;
    /**
     * @var string
     * @ORM\Column(name="public_result_type",type="string",length=60)
     */
    private $publicResultType;
    /**
     * @var boolean
     * @ORM\Column(name="is_published",type="boolean")
     */
    private $published;
    /**
     * @var string
     * @ORM\Column(name="tie_resolution_type",type="string",length=60)
     */
    private $tieResolutionType;
    /**
     * @var string
     * @Groups({"front_result"})
     * @ORM\Column(name="deserted_resolution_type",type="string",length=60)
     */
    private $desertedResolutionType;

    /**
     * @var bool
     * @ORM\Column(name="accept_applies",type="boolean",nullable=false)
     */
    private $acceptApplies;
    /**
     * @var string
     * @ORM\Column(name="phase_type",type="string",length=50,nullable=false)
     */
    private $phase_type;

	/**
	 * @var Order
	 * @ORM\ManyToOne(targetEntity="BDS\RWPaymentBundle\Entity\Order",cascade={"all"})
	 */
    private $order;


    /**
     * @var string
     * @ORM\Column(name="type_results",type="string",nullable=true)
     */
    private $typeResults;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_date",type="datetime",nullable=true)
     */
    private $startDate;
    /**
     * @var \DateTime
     * @ORM\Column(name="end_date",type="datetime",nullable=true)
     */
    private $endDate;

    public function __construct()
    {
        $this->requiresVideo= false;
        $this->published= false;
        $this->publicResultType=self::PUBLISH_RESULT_MANUAL;
        $this->tieResolutionType=self::TIE_RESOLUTION_MANUAL;
        $this->publishedAt= new \DateTime("+1 day");
        $this->events=new ArrayCollection();
        $this->rounds=new ArrayCollection();
        $this->categories= new ArrayCollection();
        $this->athletes= new ArrayCollection();
        $this->acceptApplies=false;
        $this->desertedResolutionType=self::DESERTED_RESULT_GAMES;
        $this->typeResults=self::TYPE_RESULT_POSITION;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

    /**
     * @return Event[]|ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }
    public function countEvents()
    {
        $count=0;
        foreach($this->events as $event){
            $count+=$event->getMeasureGroups()->count();
        }
        return $count;
    }
    public function countExpiredEvents()
    {
        $count=0;
        foreach($this->events as $event){
            if($event->isExpired()){
                $count+=$event->getMeasureGroups()->count();
            }
        }
        return $count;
    }


    /**
     * @param Event[]|ArrayCollection $events
     */
    public function setEvents($events)
    {
        $this->events = $events;
    }

    /**
     * @param Event $event
     */
    public function addEvent(Event $event)
    {
        if(!$this->events->contains($event)){
            $this->events->add($event);
        }
    }

    /**
     * @param Event $event
     */
    public function removeEvent(Event $event)
    {
        if($this->events->contains($event)){
            $this->events->removeElement($event);
        }
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }
    public function getPublished(){
    	return $this->published;
    }

    public function setPublished($boolean){
    	$this->published=$boolean;
    }
    /**
     * @param \DateTime $publishedAt
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }
    public function isPublished()
    {
        return $this->published;

    }
    public function hasPublishedEvents()
    {
        /** @var Event $event */
        foreach($this->events as $event) {
            if($event->isPublished()){
                return true;
            }
        }
        return false;
    }
    public function publish()
    {
    	$this->published=true;
        $this->publishedAt=new \DateTime();
    }
    /**
     * @return Category[]|ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param \BDS\RWCategoryBundle\Entity\Category[]|ArrayCollection $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    public function addCategory(Category $category)
    {
        if(!$this->categories->contains($category)){
            $this->categories->add($category);
        }
    }
    public function removeCategory(Category $category)
    {
        if(!$this->categories->contains($category)){
            $this->categories->add($category);
        }
    }
    public function countUsers()
    {
        $count=0;
        foreach($this->categories as $category){
            $count+=$category->getAthletes()->count();
        }
        return $count;
    }
   /**
     * @return Overall
     */
    public function getOverall()
    {
        return $this->overall;
    }

    /**
     * @param Overall $overall
     */
    public function setOverall($overall)
    {
        $this->overall = $overall;
    }


    /**
     * @return Round[]|ArrayCollection
     */
    public function getRounds()
    {
        return $this->rounds;
    }
    /**
     * @return Round[]|ArrayCollection
     */
    public function hasRounds()
    {
        return ($this->rounds->count()>0);
    }

    /**
     * @param Round[]|ArrayCollection $rounds
     */
    public function setRounds($rounds)
    {
        $this->rounds = $rounds;
    }
    public function addRound(Round $round){
        if(!$this->rounds->contains($round)){
            $this->rounds->add($round);
        }
    }
    public function removeRound(Round $round){
        if($this->rounds->contains($round)){
            $this->rounds->removeElement($round);
        }
    }

    /**
     * @return boolean
     */
    public function isVideoRequired()
    {
        return $this->requiresVideo;
    }

    /**
     * @return boolean
     */
    public function isRequiresVideo()
    {
        return $this->requiresVideo;
    }

    /**
     * @param boolean $requiresVideo
     */
    public function setRequiresVideo($requiresVideo)
    {
        $this->requiresVideo = $requiresVideo;
    }

    /**
     * @return string
     */
    public function getPublicResultType()
    {
        return $this->publicResultType;
    }

    /**
     * @param string $publicResultType
     */
    public function setPublicResultType($publicResultType)
    {
        if(in_array($publicResultType,[self::PUBLISH_RESULT_INSTANT,self::PUBLISH_RESULT_EVENT_EXPIRATION,self::PUBLISH_RESULT_MANUAL])){
            $this->publicResultType = $publicResultType;
        }
    }
    public function isPublicResultManual()
    {
        return  ($this->publicResultType==self::PUBLISH_RESULT_MANUAL);
    }

    /**
     * @return string
     */
    public function getTieResolutionType()
    {

        return $this->tieResolutionType;
    }

    /**
     * @param string $tieResolutionType
     */
    public function setTieResolutionType($tieResolutionType)
    {
        if(in_array($tieResolutionType,[self::TIE_RESOLUTION_MANUAL,self::TIE_RESOLUTION_AUTO])){
            $this->tieResolutionType = $tieResolutionType;
        }
    }

    /**
     * @return boolean
     */
    public function AcceptApplies()
    {
        return $this->acceptApplies;
    }

    /**
     * @param boolean $acceptApplies
     */
    public function setAcceptApplies($acceptApplies)
    {
        $this->acceptApplies = $acceptApplies;
    }

    /**
     * @return string
     */
    public function getPhaseType()
    {
        return $this->phase_type;
    }

    /**
     * @param string $phase_type
     */
    public function setPhaseType($phase_type)
    {
        $this->phase_type = $phase_type;
    }
    /**
     * @param string $phase_type
     */
    public function areHeatsAvailable()
    {
        return (($this->phase_type!==self::TYPE_ONLINE)&& $this->rounds->count());
    }

    /**
     * @return string
     */
    public function getDesertedResolutionType()
    {
        return $this->desertedResolutionType;
    }
    /**
     * @return boolean
     */
    public function isDesertedResultLikeGames()
    {
        return ($this->desertedResolutionType===self::DESERTED_RESULT_GAMES);
    }

    /**
     * @param string $desertedResolutionType
     */
    public function setDesertedResolutionType($desertedResolutionType)
    {
        if(in_array($desertedResolutionType,[self::DESERTED_RESULT_COMPLETE,self::DESERTED_RESULT_GAMES])){
            $this->desertedResolutionType = $desertedResolutionType;
        }
    }
    public function removeAthleteFromRound(CategoryAthlete $categoryAthlete){
        foreach($this->rounds as $round){
            $round->removeAthlete($categoryAthlete);
        }
    }
    public function isAttendance()
    {
        return $this->phase_type==self::TYPE_ATTENDANCE;
    }
	public function isAutoTieBreaker()
	{
		return $this->tieResolutionType==self::TIE_RESOLUTION_AUTO;
	}
	public function isPublishInstant(){
    	return $this->publicResultType==self::PUBLISH_RESULT_INSTANT;
	}
	public function isPublishManual(){
    	return $this->publicResultType==self::PUBLISH_RESULT_MANUAL;
	}
	public function isPublishEventFinish(){
    	return $this->publicResultType==self::PUBLISH_RESULT_EVENT_EXPIRATION;
	}
	public function hasCategories(){
		return (!$this->categories->isEmpty());
	}
	/**
	 * @return Order
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * @param Order $order
	 */
	public function setOrder( $order ) {
		$this->order = $order;
	}

	public function isPaid() {
		if($this->order){
			return $this->order->isPaid();
		}
		return false;
	}

	public function hasAthletes()
	{
		return ($this->getPhaseType()!==self::TYPE_INTERNAL);
	}
	public function isOnline(){
		return $this->getPhaseType()===self::TYPE_ONLINE;
	}
	public function isInternal(){
		return $this->getPhaseType()===self::TYPE_INTERNAL;
	}
	/** @return CategoryAthlete[]|ArrayCollection */
	public function getAthletes(){
	    $athletes=new ArrayCollection();
	    /** @var Category $category */
        foreach($this->categories as $category){
            foreach($category->getAthletes()as $athlete){
                $athletes->add($athlete);
            }
        }
        return $athletes;
    }
    public function isEditable(){

	    if(!$this->getEvents()->isEmpty()){
	        return false;
        }
	    if($this->getAthletes()->count()){
	        return false;
        }
        if($this->getOrder()&&$this->getOrder()->isPaid()){
	        return false;
        }
	    return true;
    }

    /**
     * @return int
     */
    public function getCost():? float
    {
        if($this->getOrder()){
            return $this->getOrder()->getAmount();
        }
        return null;
    }
    public function hasResults()
    {
        foreach ($this->events as $event) {
            if ($event->isSortable()) {
                return true;
            }
        }
        return false;
    }
    public function getPublishedEvents(){
        $events=[];
        foreach($this->events as $event){
            if($event->isPublished()){
                $events[]=$event;
            }
        }
        return $events;
    }
    public function hasOngoingEvents(){
        if($this->isOnline() and $this->isPublished()){
            foreach($this->events as $event){
                if($event->isActive()){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getTypeResults(): string
    {
        return $this->typeResults;
    }

    /**
     * @param string $typeResults
     */
    public function setTypeResults(string $typeResults): void
    {
        $this->typeResults = $typeResults;
    }
    public function hasPoints(): bool
    {
        return $this->typeResults==self::TYPE_RESULT_POINTS;
    }
    public function hasIndividualCategory():bool{
        foreach($this->getCategories() as $category){
            if($category->getCategoryType()==Category::INDIVIDUAL_CATEGORY){
                return true;
            }
        }
        return false;
    }
    public function hasTeamCategory():bool{
        foreach($this->getCategories() as $category){
            if($category->getCategoryType()==Category::TEAM_CATEGORY){
                return true;
            }
        }
        return false;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate():? \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate(\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate():? \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function setEndDate(\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }
    public function hasIndividualsAndTeams():bool
    {

        return $this->hasIndividualCategory()&&$this->hasTeamCategory();
    }
    public function getStartPrice():int{
        $price=null;
        foreach($this->categories as $category){
            if($category->getPrice() &&(($price==null) || ($category->getPrice()<$price))){
                $price=$category->getPrice();
            }
        }
        return $price??0;
    }
    public function isReadyForApplies():bool{
        return (
            $this->competition->isPublished() &&
            $this->isPublished() &&
            $this->acceptApplies &&
            $this->hasCategories()
        );
    }
    public function hasPaidCategories():bool{
        /** @var Category $category */
        foreach($this->categories as $category){
            if($category->getPrice()){
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isOver():bool{
        return $this->endDate<(new \DateTime());
    }
}

