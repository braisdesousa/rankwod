<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 4/1/16
 * Time: 9:49 AM
 */

namespace BDS\RWCompetitionBundle\Entity;


use BDS\UserBundle\Entity\User;

interface RoninFoxUserInstance
{

    /**
     * @param User $user
     * @return null
     */
    public function setRoninFoxUser(User $user);

    /**
     * @return User
     */
    public function getRoninFoxUser();

    /*
     * @return integer|null
     */
    public function getUserId();

}