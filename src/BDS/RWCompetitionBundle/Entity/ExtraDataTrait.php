<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 26/10/18
 * Time: 10:36
 */

namespace BDS\RWCompetitionBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

trait ExtraDataTrait
{
    /**
     * @var AthleteExtraData[]|ArrayCollection
     */
    protected $extraDataCollection;

    public function addExtraData(AthleteExtraData $athleteExtraData):void{
        if(!$this->extraDataCollection->contains($athleteExtraData)){
            $this->extraDataCollection->add($athleteExtraData);
        }
    }
    public function removeExtraData(AthleteExtraData $athleteExtraData):void{
        if($this->extraDataCollection->contains($athleteExtraData)){
            $this->extraDataCollection->removeElement($athleteExtraData);
        }
    }
    public function hasExtraData(ExtraData $extraData):bool
    {
        foreach($this->extraDataCollection as $athleteExtraData){
            if($athleteExtraData->getExtraData()->getId()===$extraData->getId()){
                return true;
            }
        }
        return false;
    }
    public function getExtraDataCollection(): Collection{
        return $this->extraDataCollection;
    }
    public function getExtraData(ExtraData $extraData):? AthleteExtraData{
        foreach($this->extraDataCollection as $athleteExtraData){
            if($athleteExtraData->getExtraData()->getId()==$extraData->getId()){
                return $athleteExtraData;
            }
        }
        return null;
    }
    public function getExtraDataValue(ExtraData $extraData):? string{
        foreach($this->extraDataCollection as $athleteExtraData){
            if($athleteExtraData->getExtraData()->getId()==$extraData->getId()){
                return $athleteExtraData->getValue();
            }
        }
        return null;
    }
    public function getExtraDataFrontValue(ExtraData $extraData):? string{
        foreach($this->extraDataCollection as $athleteExtraData){
            if($athleteExtraData->getExtraData()->getId()==$extraData->getId()){
                return $athleteExtraData->getFrontValue();
            }
        }
        return null;
    }

}