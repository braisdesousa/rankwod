<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/16/16
 * Time: 9:33 AM
 */

namespace BDS\RWCompetitionBundle\Entity;


interface PublishedInterface
{

    public function isPublished();
    public function publish();


}