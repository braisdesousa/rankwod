<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractTextBaseEntity;
use BDS\GaufretteBundle\Entity\File;
use BDS\RWPaymentBundle\Entity\GatewayConfig;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use JMS\Serializer\Annotation\Groups;
use Sylius\Component\Addressing\Model\Address;

/**
 * Competition
 *
 * @ORM\Table(name="competition")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\CompetitionRepository")
 */
class Competition extends AbstractTextBaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection|Phase[]
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\Phase", mappedBy="competition",cascade={"all"})
     * @Groups({"front_result"})
     */
    private $phases;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\CompetitionAdmin",mappedBy="competition",cascade={"all"})
     */
    private $adminUsers;
    /**
     * @var array
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\CompetitionJudge",mappedBy="competition",cascade={"all"})
     */
    private $judgeUsers;
    /**
     * @var boolean
     * @ORM\Column(name="is_over",type="boolean",nullable=false)
     */
    private $isOver;

	/**
	 * @var boolean
	 * @ORM\Column(name="publish",type="boolean")
	 */
	private $publish;

	/**
	 * @var File
	 * @ORM\ManyToOne(targetEntity="BDS\GaufretteBundle\Entity\File",cascade={"all"})
	 * @ORM\JoinColumn(name="image_id", referencedColumnName="id",onDelete="SET NULL")
	 */
	private $image;
	/**
	 * @var File
	 * @ORM\ManyToOne(targetEntity="BDS\GaufretteBundle\Entity\File",cascade={"all"})
	 * @ORM\JoinColumn(name="background_image_id", referencedColumnName="id",onDelete="SET NULL")
	 */
	private $backgroundImage;

    /**
     * @var GatewayConfig
     * @ORM\ManyToOne(targetEntity="BDS\RWPaymentBundle\Entity\GatewayConfig",cascade={"all"})
     * @ORM\JoinColumn(name="gateway_id", referencedColumnName="id",onDelete="SET NULL")
     */
	private $gatewayConfig;

    /**
     * @var ArrayCollection|ExtraData[]
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\ExtraData",mappedBy="competition",cascade={"all"})
     */
	private $extraDataCollection;

    /**
     * @var Address
     * @ORM\ManyToOne(targetEntity="Sylius\Component\Addressing\Model\Address",cascade={"all"})
     */
	private $address;

    public function __construct()
    {
    	$this->publish=false;
        $this->phases=new ArrayCollection();
	    $this->bots=new ArrayCollection();
        $this->adminUsers=new ArrayCollection();
        $this->judgeUsers=new ArrayCollection();
        $this->isOver=false;
        $this->extraDataCollection=new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Phase[]|ArrayCollection
     */
    public function getPhases()
    {
        return $this->phases;
    }

    /**
     * @param Phase[]|ArrayCollection $phases
     * @return Competition
     */
    public function setPhases($phases)
    {
        $this->phases = $phases;
        return $this;
    }

    /**
     * @param Phase $phase
     */
    public function addPhase(Phase $phase)
    {
        if(!$this->phases->contains($phase)){
            $this->phases->add($phase);
        }
    }

    /**
     * @param Phase $phase
     */
    public function removePhase(Phase $phase)
    {
        if($this->phases->contains($phase)){
            $this->phases->removeElement($phase);
        }
    }
    /**
     * @return array
     */
    public function getAdminUsers()
    {
        return $this->adminUsers;
    }


    /**
     * @param array $adminUsers
     */
    public function setAdminUsers(ArrayCollection $adminUsers)
    {
        $this->adminUsers = $adminUsers;
    }

    /**
     * @return array
     */
    public function getJudgeUsers()
    {
        return $this->judgeUsers;
    }

    /**
     * @param array $judgeUsers
     */
    public function setJudgeUsers($judgeUsers)
    {
        $this->judgeUsers = $judgeUsers;
    }
    public function addAdmin(CompetitionAdmin $competitionAdmin){
    	if(!$this->adminUsers->contains($competitionAdmin)){
    		$this->adminUsers->add($competitionAdmin);
	    }
    }
    public function addAdminUser(UserInterface $user)
    {
            $aU=new CompetitionAdmin();
            $aU->setCompetition($this);
            $aU->setUserId($user->getId());
            $this->adminUsers->add($aU);
    }
    public function addJudgeUser(UserInterface $user)
    {
            $aU=new CompetitionJudge();
            $aU->setCompetition($this);
            $aU->setUserId($user->getId());
            $this->adminUsers->add($aU);
    }
    /**
     * @return Phase[]|ArrayCollection
     */
    public function getPublishedPhases()
    {
        $publishedPhases=new ArrayCollection();
        foreach($this->phases as $phase){
            if($phase->isPublished()){
                $publishedPhases->add($phase);
            }
        }
        return $publishedPhases;
    }
    /**
     * @return boolean
     */
    public function hasPaidPhases()
    {

        foreach($this->phases as $phase){
            if($phase->isPaid()){
                return true;
            }
        }
        return false;
    }
    /**
     * @return Phase[]|ArrayCollection
     */
    public function getUnPublishedPhases()
    {
        $publishedPhases=new ArrayCollection();
        foreach($this->phases as $phase){
            if(!$phase->isPublished()){
                $publishedPhases->add($phase);
            }
        }
        return $publishedPhases;
    }
    public function hasPublishedPhases()
    {
    	return $this->getPublishedPhases()->count()&&1;
    }
    public function hasPhases(){
    	return (!$this->phases->isEmpty());
    }
    public function hasAttendancePhase()
    {
        foreach($this->phases as $phase){
            if($phase->getPhaseType()==Phase::TYPE_ATTENDANCE){
                return true;
            }
        }
        return false;
    }
    public function getAttendancePhase()
    {
        foreach($this->phases as $phase){
            if($phase->getPhaseType()==Phase::TYPE_ATTENDANCE){
                return $phase;
            }
        }
        return false;
    }
    public function getPublishedOnlinePhases()
    {
        $phases=[];
        foreach($this->phases as $phase){
            if(($phase->getPhaseType()==Phase::TYPE_ONLINE)&&($phase->isPublished())){
                $phases[]=$phase;
            }
        }
        return $phases;
    }
    public function canRegister(){
	    foreach($this->phases as $phase){
		    if($phase->AcceptApplies()){
			    return true;
		    }
	    }
	    return false;
    }
    public function getAttendancePhases()
    {
        $attendancePhases= new ArrayCollection();
        foreach($this->phases as $phase){
            if($phase->getPhaseType()==Phase::TYPE_ATTENDANCE){
                $attendancePhases->add($phase);
            }
        }
        return $attendancePhases;
    }

    /**
     * @return boolean
     */
    public function isOver()
    {
        return $this->isOver;
    }

    /**
     * @param boolean $isOver
     */
    public function setOver($isOver)
    {
        $this->isOver = $isOver;
    }

	/**
	 * @return boolean
	 */
	public function isIsOver() {
		return $this->isOver;
	}

	/**
	 * @param boolean $isOver
	 */
	public function setIsOver( $isOver ) {
		$this->isOver = $isOver;
	}

	/**
	 * @return Bot[]|ArrayCollection
	 */
	public function getBots() {
		return $this->bots;
	}

	/**
	 * @param Bot[]|ArrayCollection $bots
	 */
	public function setBots( $bots ) {
		$this->bots = $bots;
	}

	/**
	 * @param Bot $bot
	 */
    public function addBot(Bot $bot)
    {
	    if(!$this->bots->contains($bot)){
		    $this->bots->add($bot);
	    }
    }

	/**
	 * @param Bot $bot
	 */
    public function removeBot(Bot $bot)
    {
	    if($this->bots->contains($bot)){
		    $this->bots->removeElement($bot);
	    }
    }

	/**
	 * @return bool
	 */
	public function isPublish() {
		return $this->publish;
	}
	public function isPublished() {
		return $this->publish;
	}

	/**
	 * @param bool $publish
	 */
	public function setPublish( $publish ) {
		$this->publish = $publish;
	}
	public function hasOnePhase(){
		return $this->phases->count()==1;
	}

    /**
     * @return Phase|null
     */
	public function getFirstPhase():? Phase{
		return $this->phases->first();
	}
	public function deleteAdmin(CompetitionAdmin $competitionAdmin){
		if($this->adminUsers->contains($competitionAdmin)){
			$this->adminUsers->removeElement($competitionAdmin);
		}
	}
	public function deleteJudge(CompetitionJudge $judge){
		if($this->judgeUsers->contains($judge)){
			$this->judgeUsers->removeElement($judge);
		}
	}

	/**
	 * @return File
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param File $image
	 */
	public function setImage( $image ) {
		$this->image = $image;
	}

	/**
	 * @return File
	 */
	public function getBackgroundImage() {
		return $this->backgroundImage;
	}

	/**
	 * @param File $backgroundImage
	 */
	public function setBackgroundImage( $backgroundImage ) {
		$this->backgroundImage = $backgroundImage;
	}
	public function hasImage(){
		return $this->image&&1;
	}
	public function hasBackgroundImage(){
		return $this->backgroundImage&&1;
	}

    /**
     * @return GatewayConfig
     */
    public function getGatewayConfig():? GatewayConfig
    {
        return $this->gatewayConfig;
    }

    /**
     * @param GatewayConfig $gatewayConfig
     */
    public function setGatewayConfig(GatewayConfig $gatewayConfig): void
    {
        $this->gatewayConfig = $gatewayConfig;
    }
    public function hasGatewayConfig(){
        return (($this->gatewayConfig instanceof GatewayConfig)&&($this->gatewayConfig->isFilled()));
    }
    public function hasResults(){
        foreach ($this->phases as $phase){
            if($phase->hasResults()){
                return true;
            }
        }
        return false;
    }
    public function hasOngoingEvents(){
        if($this->isPublished()){
            foreach ($this->phases as $phase){
                if($phase->hasOngoingEvents()){
                    return true;
                }
            }
        }
        return false;
    }
    public function addExtraData(ExtraData $extraData){
        if(!$this->extraDataCollection->contains($extraData)){
            $this->extraDataCollection->add($extraData);
        }
    }
    public function removeExtraData(ExtraData $extraData){
        if($this->extraDataCollection->contains($extraData)){
            $this->extraDataCollection->removeElement($extraData);
        }
    }

    /**
     * @return ExtraData[]|ArrayCollection
     */
    public function getExtraDataCollection()
    {
        return $this->extraDataCollection;
    }

    /**
     * @return Address
     */
    public function getAddress():? Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address=null): void
    {
        $this->address = $address;
    }

    public function preSave(){
        if($this->address && (!$this->address->getFirstName())){
            $this->address->setFirstName($this->name);
            $this->address->setLastName($this->address->getStreet());
        }
    }
    public function getLastPublishedPhase():? Phase{
        $lastPhase=null;
        foreach($this->phases as $phase){
            if($phase->isPublished()){
                $lastPhase=$phase;
            }
        }
        return $lastPhase;
    }

}

