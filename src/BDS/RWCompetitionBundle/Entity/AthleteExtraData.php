<?php

namespace BDS\RWCompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AthleteExtraData
 *
 * @ORM\Table(name="athlete_extra_data")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\AthleteExtraDataRepository")
 */
class AthleteExtraData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ExtraData
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\ExtraData")
     */
    private $extraData;
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;


    public function __construct(ExtraData $extraData=null,$value=null)
    {
        $this->extraData=$extraData;
        $this->value=$value;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return AthleteExtraData
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    public function getFrontValue()
    {
        if($this->extraData->getType()==ExtraData::TYPE_KG){
            return (intval($this->value)/100);
        }
        return $this->value;
    }


    /**
     * @return ExtraData
     */
    public function getExtraData(): ExtraData
    {
        return $this->extraData;
    }

    /**
     * @param ExtraData $extraData
     */
    public function setExtraData(ExtraData $extraData): void
    {
        $this->extraData = $extraData;
    }
}
