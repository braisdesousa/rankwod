<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * Round
 *
 * @ORM\Table(name="competition_round")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\RoundRepository")
 */
class Round extends AbstractNamedBaseEntity
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var Phase
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Phase",inversedBy="rounds")
     */
    private $phase;
    /**
     * @var ArrayCollection|CategoryAthlete[]
     * @ORM\ManyToMany(targetEntity="BDS\RWCategoryBundle\Entity\CategoryAthlete",cascade={"all"})
     * @ORM\JoinTable(name="round_athletes",
     *      joinColumns={@ORM\JoinColumn(name="round_id", referencedColumnName="id",onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="athlete_id", referencedColumnName="id",onDelete="CASCADE")}
     *      )
     */
    private $athletes;
        /**
     * Round constructor.
     */
    public function __construct()
    {
        $this->athletes= new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return \BDS\RWCategoryBundle\Entity\CategoryAthlete[]|ArrayCollection
     */
    public function getAthletes()
    {
        return $this->athletes;
    }

    /**
     * @param \BDS\RWCategoryBundle\Entity\CategoryAthlete[]|ArrayCollection $athletes
     */
    public function setAthletes($athletes)
    {
        $this->athletes = $athletes;
    }

    /**
     * @param CategoryAthlete $athlete
     */
    public function addAthlete(CategoryAthlete $athlete){
        if(!$this->athletes->contains($athlete)){
            $this->athletes->add($athlete);
        }
    }

    /**
     * @param CategoryAthlete $athlete
     */
    public function removeAthlete(CategoryAthlete $athlete){
        if($this->athletes->contains($athlete)){
            $this->athletes->removeElement($athlete);
        }
    }

    /**
     * @param Collection $athletes
     */
    public function AddAthletes(Collection $athletes){
        foreach($athletes as $athlete){
            $this->addAthlete($athlete);
        }
    }
        /**
     * @return Phase
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * @param Phase $phase
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;
    }
    public function hasAthlete(CategoryAthlete $categoryAthlete){
        return $this->athletes->contains($categoryAthlete);
    }



}

