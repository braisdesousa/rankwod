<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Phase
 *
 * @ORM\Table(name="competition_event_result")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\EventResultRepository")
 */
class EventResult extends AbstractBaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CategoryAthlete
     * @ORM\ManyToOne(targetEntity="BDS\RWCategoryBundle\Entity\CategoryAthlete",inversedBy="results",fetch="EXTRA_LAZY")
     */
    private $athlete;
    /**
     * @var Measure
     * @ORM\ManyToOne(targetEntity="BDS\RWMeasureBundle\Entity\Measure")
     */
    private $measure;
    /**
     * @var Event
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Event", inversedBy="results")
     */
    private $event;
    /**
     * @var float
     * @ORM\Column(name="result",type="float",nullable=false)
     */
    private $result;

    /**
     * @var boolean
     * @ORM\Column(name="corrected",type="boolean",nullable=true)
     */
    private $corrected;
        /**
     * @var integer
     * @ORM\Column(name="tie_breaker",type="integer",nullable=true);
     */
    private $tieBreaker;
    /**
     * @var string
     * @ORM\Column(name="event_name",type="string",nullable=false);
     */
    private $eventName;
    /**
     * @var boolean
     * @ORM\Column(name="has_tie_break",type="boolean",nullable=false)
     */
    private $tieBreak;
	/**
	 * @var string
	 * @ORM\Column(name="video",type="string",nullable=true);
	 */
    private $videoUrl;
    /**
     * @var string
     * @ORM\Column(name="comment",type="text",nullable=true);
     */
    private $comment;

    public function __construct(CategoryAthlete $categoryAthlete=null,Event $event=null,Measure $measure=null,$result=0,$eventName=null)
    {
        $this->athlete=$categoryAthlete;
        $this->event=$event;
        $this->measure=$measure;
        $this->result=$result;
        $this->eventName=$eventName;
        $this->tieBreaker=0;
        $this->tieBreak=false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CategoryAthlete
     */
    public function getAthlete()
    {
        return $this->athlete;
    }

    /**
     * @param CategoryAthlete $athlete
     */
    public function setAthlete($athlete)
    {
        $this->athlete = $athlete;
    }

    /**
     * @return Measure
     */
    public function getMeasure()
    {
        return $this->measure;
    }

    /**
     * @param Measure $measure
     */
    public function setMeasure($measure)
    {
        $this->measure = $measure;
    }

    /**
     * @return int
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }
    public function getPublicResult()
    {
        if($this->measure->getType()==Measure::MEASURE_TYPE_TIME){
            if($this->result){
                $dateTime = new \DateTime();
                $dateTime->setTimestamp($this->result);
                return $dateTime->format("i:s");
            } else {
                return "00:00:00";
            }

        }
        return $this->result;
    }

    /**
     * @param int $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return boolean
     */
    public function isCorrected()
    {
        return $this->corrected;
    }

    /**
     * @param boolean $corrected
     */
    public function setCorrected($corrected)
    {
        $this->corrected = $corrected;
    }

    /**
     * @return int
     */
    public function getTieBreaker()
    {
        return $this->tieBreaker;
    }

    /**
     * @param int $tieBreaker
     */
    public function setTieBreaker($tieBreaker)
    {
        $this->tieBreaker = $tieBreaker;
    }


    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     * @param string $eventName
     */
    public function setEventName($eventName)
    {
        $this->eventName = $eventName;
    }

    /**
     * @return boolean
     */
    public function hasTieBreak()
    {
        return $this->tieBreak;
    }

    /**
     * @param boolean $tieBreak
     */
    public function setTieBreak($tieBreak)
    {
        $this->tieBreak = $tieBreak;
    }

	/**
	 * @return string
	 */
	public function getVideoUrl() {
		return $this->videoUrl;
	}

	/**
	 * @param string $videoUrl
	 */
	public function setVideoUrl( $videoUrl ) {
		$this->videoUrl = $videoUrl;
	}

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment===null?'':$this->comment;
    }
    public function hasComment(): bool
    {
        return $this->comment!=null;
    }
    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }
   

}

