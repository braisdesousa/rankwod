<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\CoreBundle\Entity\AbstractTextBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Phase
 *
 * @ORM\Table(name="competition_event")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\EventRepository")
 * @UniqueEntity(em="default",fields={"phase","name"},errorPath="name",message="This name already exists for a event")
 */
class Event extends AbstractTextBaseEntity implements ExpiredInterface,PublishedInterface
{

    const MANUAL_PUBLISH_UNPUBLISHED = "UNPUBLISHED";
    const MANUAL_PUBLISH_PUBLISHED   = "PUBLISHED";
    const MANUAL_PUBLISH_DEPRECATED  = "DEPRECATED";

    const POINT_SYSTEM_DISABLED=0;
    const POINT_SYSTEM_50=50;
    const POINT_SYSTEM_100=100;
    const POINT_SYSTEM_200=200;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var Phase
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Phase",inversedBy="events")
     * @ORM\JoinColumn(name="phase_id", referencedColumnName="id",nullable=false)
     */
    private $phase;
    /**
     * @var ArrayCollection|MeasureGroup[]
     * @ORM\OneToMany(targetEntity="BDS\RWMeasureBundle\Entity\MeasureGroup", mappedBy="event",cascade={"all"},orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     **/
    private $measureGroups;

    /**
     * @var string
     * @ORM\Column(name="video_url",type="string",nullable=true)
     */
    private $videoUrl;

    /**
     * @var ArrayCollection|EventResult[];
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\EventResult",cascade={"all"},mappedBy="event",fetch="EXTRA_LAZY")
     */
    private $results;

    /**
     * @var \DateTime $publishDate
     * @ORM\Column(name="publish_at",type="datetime")
     * @Groups({"front_result"})
     */
    private $publishedAt;

    /**
     * @var \DateTime $limitDate
     * @ORM\Column(name="limit_date",type="datetime",nullable=true)
     */
    private $limitDate;
    /**
     * @var string
     * @ORM\Column(name="manual_published",type="string",nullable=false)
     */
    private $manualPublished;
    /**
     * @var boolean
     * @ORM\Column(name="is_final",type="boolean",nullable=false)
     */
    private $final;

    /** @var integer
     *  @ORM\Column(name="point_system",type="integer")
     */

    private $pointSystem;


    public function __construct()
    {
        $this->pointSystem=self::POINT_SYSTEM_DISABLED;
        $this->measureGroups=new ArrayCollection();
        $this->results=new ArrayCollection();
        $this->publishedAt= new \DateTime("+1 year");
        $this->manualPublished=self::MANUAL_PUBLISH_UNPUBLISHED;
        $this->final=false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Phase
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * @param Phase $phase
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;
    }
    /**
     * @return \BDS\RWMeasureBundle\Entity\MeasureGroup[]|ArrayCollection
     */
    public function getMeasureGroups()
    {
        return $this->measureGroups;
    }
    public function clearMeasures(){
        $this->measureGroups->clear();
    }

    /**
     * @param \BDS\RWMeasureBundle\Entity\MeasureGroup[]|ArrayCollection $measureGroups
     */
    public function setMeasureGroups($measureGroups)
    {
        $this->measureGroups = $measureGroups;
    }

    public function addMeasureGroup(MeasureGroup $measure){
        if(!$this->measureGroups->contains($measure)){
            $this->measureGroups->add($measure);
        }

    }
    public function removeMeasureGroups(MeasureGroup $measure){
        if($this->measureGroups->contains($measure)){
          $this->measureGroups->removeElement($measure);
          $measure->setEvent(null);
        }
    }


    /**
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * @param string $videoUrl
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;
    }

    /**
     * @return EventResult[]|ArrayCollection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param EventResult[]|ArrayCollection $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }
    public function addResult(EventResult $result){
        if(!$this->results->contains($result)){
            $this->results->add($result);
        }
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        $now=new \DateTime();
        return $now>$this->publishedAt;
    }
    /**
     *
     */
    public function publish()
    {
        $this->publishedAt=new \DateTime();
    }
    public function unPublish(){
	    $this->publishedAt=new \DateTime('+1 Year');
    }
    /**
     * @return \DateTime
     */
    public function getLimitDate()
    {
        return $this->limitDate;
    }

    public function isExpired()
    {
        $now= new \DateTime();
        $x= $this->limitDate?($this->limitDate<$now):false;
        return $x;

    }
    /**
     * @param \DateTime $limitDate
     */
    public function setLimitDate($limitDate)
    {
        $this->limitDate = $limitDate;
    }


    public function isSortable()
    {
        $sortable=false;
        if($this->isFinal()){
            return $this->getManualPublished()==self::MANUAL_PUBLISH_PUBLISHED;
        }
        switch ($this->phase->getPublicResultType()){
            case Phase::PUBLISH_RESULT_EVENT_EXPIRATION: $sortable=$this->isExpired(); break;
            case Phase::PUBLISH_RESULT_MANUAL: $sortable=in_array($this->getManualPublished(),[self::MANUAL_PUBLISH_DEPRECATED,self::MANUAL_PUBLISH_PUBLISHED]); break;
            case Phase::PUBLISH_RESULT_INSTANT: $sortable=$this->isPublished();
        }
        return ($sortable);
    }

    /**
     * @return string
     */
    public function getManualPublished()
    {
        return $this->manualPublished;
    }
    /**
     * @return boolean
     */
    public function isManualPublished()
    {
        return $this->manualPublished==self::MANUAL_PUBLISH_PUBLISHED;
    }

    /**
     * @param string $manualPublished
     */
    public function setManualPublished($manualPublished)
    {
        $this->manualPublished = $manualPublished;
    }

    /**
     * @return boolean
     */
    public function isFinal()
    {
        return $this->final;
    }

    /**
     * @param boolean $final
     */
    public function setFinal($final)
    {
        $this->final = $final;
    }
    public function isActive(){
        return
            $this->isPublished() &&
            (!$this->isExpired())
            ;
    }

    /**
     * @return int
     */
    public function getPointSystem(): int
    {
        return $this->pointSystem;
    }

    /**
     * @param int $pointSystem
     */
    public function setPointSystem(int $pointSystem): void
    {
        $this->pointSystem = $pointSystem;
    }



}

