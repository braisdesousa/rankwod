<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\CoreBundle\Entity\AbstractTextBaseEntity;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use JMS\Serializer\Annotation\Groups;

/**
 * Bot
 *
 * @ORM\Table(name="bot")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\BotRepository")
 */
class Bot extends AbstractNamedBaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	/**
	 * @var CategoryAthlete
	 * @ORM\OneToOne(targetEntity="BDS\RWCategoryBundle\Entity\CategoryAthlete",inversedBy="bot")
	 */
	private $categoryAthlete;

	public function __construct($name=null) {
		$this->name=$name;
	}

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
	/**
	 * @return CategoryAthlete
	 */
	public function getCategoryAthlete() {
		return $this->categoryAthlete;
	}

	/**
	 * @param CategoryAthlete $categoryAthlete
	 */
	public function setCategoryAthlete( $categoryAthlete ) {
		$this->categoryAthlete = $categoryAthlete;
	}



}

