<?php

namespace BDS\RWCompetitionBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Team Athlete
 *
 * @ORM\Table(name="team_athlete")
 * @ORM\Entity(repositoryClass="BDS\RWCompetitionBundle\Repository\TeamAthleteRepository")
 */
class TeamAthlete extends AbstractBaseEntity implements ExtraDataInterface
{
    const STATUS_PENDING = "PENDING";
    const STATUS_ACCEPTED = "ACCEPTED";
    const STATUS_DECLINED = "DECLINED";
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Team
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Team", inversedBy="athletes",cascade={"persist","refresh"})
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    private $team;
    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    private $position;
    /**
     * @var string
     * @ORM\Column(name="status",type="string",length=50)
     */
    private $status;

    /**
     * @var ArrayCollection|AthleteExtraData[]
     * @ORM\ManyToMany(targetEntity="BDS\RWCompetitionBundle\Entity\AthleteExtraData",cascade={"all"})
     * @ORM\JoinTable(name="team_athlete_extra_data",
     *      joinColumns={@ORM\JoinColumn(name="team_athlete_id", referencedColumnName="id",onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="extra_data_id", referencedColumnName="id", unique=true,onDelete="CASCADE")}
     *      )
     */
    protected $extraDataCollection;

    use UserExtensionTrait, ExtraDataTrait;

    public function __construct($status,Team $team,UserExtension $userExtension)
    {
        $this->status=$status?:self::STATUS_PENDING;
        $this->userExtension=$userExtension;
        $this->team=$team;
        $this->extraDataCollection=new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Team
     */
    public function getTeam():Team
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }


}

