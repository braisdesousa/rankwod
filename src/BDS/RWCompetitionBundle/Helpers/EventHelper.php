<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/03/16
 * Time: 12:08
 */

namespace BDS\RWCompetitionBundle\Helpers;


use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\Common\Collections\Collection;
use Gedmo\Sluggable\Util\Urlizer;

class EventHelper
{

    public static function getSplitEventsNamesForSerialization(Collection $events)
    {
        $names=[];
        $eventNames=self::getSplitEventsNames($events);
        $eventNameCount=count($eventNames);
        foreach($eventNames as $name){
            $names[]=["name"=>$name,"slug"=>Urlizer::transliterate($name)];
        }
        if($eventNameCount>1){
            $names[]=["name"=>"Overall","slug"=>Urlizer::transliterate("Overall")];
        }
        return $names;
    }
    /**
     * @param Collection|Event[] $events
     */
    public static function getSplitEventsNames(Collection $events){
        $names=[];
        foreach($events as $event){
            if($event->isSortable()){
                $names=array_merge($names,self::getSplitEventNames($event));
            }
        }
        return $names;
    }
    /**
     * @param Collection|Event[] $events
     */
    public static function getSplitEventsNamesWithKey(Collection $events){
        $names=[];
        foreach($events as $event){
            if($event->isExpired()){
                $names=array_merge($names,self::getSplitEventNamesWithKey($event));
            }
        }
        return $names;
    }
    public static function getMeasureNames(Event $event){
        $result=[];
        foreach(self::getSplitEventNames($event) as $eventName){
            $result[Urlizer::transliterate($eventName)]=$eventName;
        }
        return $result;
    }
    public static function getSplitEventNamesWithKey(Event $event)
    {
        $alphas= range("A","Z");
        $names=[];
        foreach($event->getMeasureGroups() as $key=>$group){

            if(($event->getMeasureGroups()->count())>1){
                $names[]=[sprintf("%s - %s",$event->getName(),$alphas[$key]),$key];
            } else {
                $names[]=[$event->getName(),$key];
            }
        }
        return $names;
    }
    public static function getSplitEventNames(Event $event)
    {
        $alphas= range("A","Z");
        $names=[];
        foreach($event->getMeasureGroups() as $key=>$group){
            if(($event->getMeasureGroups()->count())>1){
                $names[]=sprintf("%s - %s",$event->getName(),$alphas[$key]);
            } else {
                $names[]=$event->getName();
            }
        }
        return $names;
    }
    public static function getSplitEventNamesWithMeasureGroup(Event $event)
    {
        $alphas= range("A","Z");
        $names=[];
        foreach($event->getMeasureGroups() as $key=>$group){
            if(($event->getMeasureGroups()->count())>1){
                $names[]=["name"=>sprintf("%s - %s",$event->getName(),$alphas[$key]),"group"=>$group];
            } else {
                $names[]=["name"=>$event->getName(),"group"=>$group];
            }
        }
        return $names;
    }
}