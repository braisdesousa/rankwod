<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 4/1/16
 * Time: 9:44 AM
 */

namespace BDS\RWCompetitionBundle\Listener;


use BDS\RWCompetitionBundle\Entity\RoninFoxUserInstance;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;

class AthleteListener
{
    private $userEntityManager;
    
    public function __construct(EntityManager $userEntityManager)
    {
        $this->userEntityManager=$userEntityManager;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity=$args->getEntity();
        if(($entity instanceof RoninFoxUserInstance)){
            if($userId=$entity->getUserId()){
                if(!$this->userEntityManager->getRepository("BDSUserBundle:User")->findOneByIdCached($userId)){
                   throw  new \Exception("XX");
                }
                $entity->setRoninFoxUser($this->userEntityManager->getRepository("BDSUserBundle:User")->findOneByIdCached($userId));
            }
        }

    }
}