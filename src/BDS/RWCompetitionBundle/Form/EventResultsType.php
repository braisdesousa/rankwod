<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Helpers\EventHelper;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\UnexpectedResultException;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Tests\Extension\Validator\Type\SubmitTypeValidatorExtensionTest;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class EventResultsType extends AbstractType
{
    private $isHeat;
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var EventResult[] **/
        $results=$this->transformResults($options["results"]);
        /** @var Event $event */
        $event=$options["event"];
        /** @var CategoryAthlete $user */
        $athlete=$options["athlete"];
        /** @var boolean $isJudge */
        $isJudge=$options["is_judge"];
        $isHeat=$options["is_heat"];
        $this->isHeat=$isHeat;
        $video=null;
        $comment=null;
        $eventNames=EventHelper::getSplitEventNames($event);
        foreach ($event->getMeasureGroups() as $measureGroupKey=>$measureGroup){
            foreach($measureGroup->getMeasurements() as $measureKey=>$measure){
                if(array_key_exists($measureGroupKey,$results)&&array_key_exists($measure->getType(),$results[$measureGroupKey])){
                    $eventResult=$results[$measureGroupKey][$measure->getType()][0];
                } else {
                    $eventResult= new EventResult();
                    $eventResult->setMeasure($measure);
                    $eventResult->setTieBreaker($measure->hasTieBreak());
                    $eventResult->setEventName($eventNames[$measureGroupKey]);
                    $eventResult->setEvent($event);
                    if( $athlete instanceof CategoryAthlete){
                        $eventResult->setAthlete($athlete);
                    }
                }
                if($eventResult->getVideoUrl()){
                    $video=$eventResult->getVideoUrl();
                }
                if($eventResult->getComment()){
                    $comment=$eventResult->getComment();
                }
                $builder
                    ->add(sprintf("result_%s_%s",$measureGroupKey,$measureKey),EventResultType::class,
                        [
                            "measure"=>$measure,
                            "disabled"=>(!$isJudge&&$event->isExpired()),
                            "label"=>" ",
                            "data"=>$eventResult]);
                }
            }
        if($event->getPhase()->isVideoRequired()){
                $builder->add("video",UrlType::class,["label"=>"Url del Vídeo","disabled"=>(!$isJudge&&$event->isExpired()),"required"=>true,"attr"=>["class"=>"form-control","placeholder"=>"Url de Youtube | Vimeo | ...."],"data"=>$video]);
        }
        if($isJudge and !$isHeat){
            $builder->add("comment",TextareaType::class,[
                "label"=>"Comentario del Juez",
                "required"=>false,
                "attr"=>["class"=>"form-control","rows"=>4,"placeholder"=>"Si eres juez explica al atleta el porqué de las NO REP"],
                "data"=>$comment]);
        }
        if(!$isHeat){
            if(($isJudge||((!$isJudge)&&(!$event->isExpired())))){
                $builder->add("submit",SubmitType::class,["label"=>"Enviar Resultado","attr"=>["class"=>"btn bg-light-blue btn-link waves-effect"]]);
            }
        }
    }
        /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            "results"=>null,
            "athlete"=>null,
            "event"=>null,
            "is_heat"=>false,
            "is_judge"=>null,
            "csrf_protection"=>false,
            'constraints'=>[
                    new Callback(["callback"=>function ($data, ExecutionContextInterface $context){
	                    if(empty($data)||$this->isHeat){return;}
	                    $groups=$this->getMeasureGroups($data);
	                    foreach($groups as $measureGroup){
		                    if(!$this->isValidMeasureGroup($measureGroup)){
			                    $context->addViolation($this->getViolationMessage($measureGroup));
		                    }
	                    }
                    }])]
        ));
    }

    /** @var EventResult[] $results **/
    public function transformResults(array $results){
        $transformedResults=[];
        foreach($results as $result){
            $transformedResults[$result->getMeasure()->getMeasureGroup()->getPosition()][$result->getMeasure()->getType()][]=$result;
        }
        return array_values($transformedResults);
    }
    public function checkResults($data, ExecutionContextInterface $context){

        if(empty($data)||$this->isHeat){return;}
        $groups=$this->getMeasureGroups($data);
        foreach($groups as $measureGroup){
            if(!$this->isValidMeasureGroup($measureGroup)){
                $context->addViolation($this->getViolationMessage($measureGroup));
            }
        }
    }
    private function getViolationMessage(array $measureGroup)
    {
        $count=$this->getMeasureGroupCount($measureGroup);
        if($count==0){
            $message="Debes introducir almenos un resultado entre ";
            /** @var EventResult $eventResult **/
            foreach($measureGroup as $eventResult){
                $message.=sprintf("'%s',",$eventResult->getMeasure()->getName());
            }

        } else {
            $message="Solo debes introducir un resultado entre ";
            /** @var EventResult $eventResult **/
            foreach($measureGroup as $eventResult){
                $message.=sprintf("'%s',",$eventResult->getMeasure()->getName());
            }
        }
    return rtrim($message, ',');
    }
    private function getMeasureGroupCount(array $measureGroup)
    {
        $result=0;
        /** @var EventResult $eventResult **/
        foreach($measureGroup as $eventResult){
            if($eventResult->getResult()){$result++;}
        }
        return $result;
    }
    private function isValidMeasureGroup(array $measureGroup){
     return ($this->getMeasureGroupCount($measureGroup)===1);
    }
    private function getMeasureGroups(array $eventResults){
        $groups=[];
            /** @var EventResult $eventResult */
            /** @var Measure $measure */
        foreach($eventResults as $eventResult){
        	if($eventResult instanceof  EventResult){
		        $measure=$eventResult->getMeasure();
		        $groups[$measure->getMeasureGroup()->getId()][]=$eventResult;
	        }
        }
        return $groups;
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_eventresults';
    }
}
