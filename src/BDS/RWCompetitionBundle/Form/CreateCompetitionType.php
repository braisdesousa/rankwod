<?php

namespace BDS\RWCompetitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateCompetitionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,["label"=>"Nombre","required"=>true, "attr"=>["placeholder"=>"Como se llamará tu competición","class"=>"form-control"]])
//            ->add('text',TextareaType::class,["label"=>"Descripción","required"=>false])
            ->add("submit",SubmitType::class,["label"=>"Crear Competición"]);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Competition'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_create_competition';
    }
}
