<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Form\DataTransformer\TextToInputMaskTransformer;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWWebBundle\Form\TimeWithTenthsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

class EventResultType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Measure $measure */
        $measure=$options["measure"];
        if($measure->getType()==Measure::MEASURE_TYPE_TIME){
            $builder
                ->add('result',TimeWithTenthsType::class,["label"=>$measure->getName(),
                    "label_attr"=>["class"=>"display-block"],
                    "attr"=> ["placeholder"=>$measure->getName(),"class"=>"inline-flex"],
                    "widget"=>"choice",
                    "hours"=>[0],
                    "input"=>"timestamp",
                    "with_seconds"=>true,
                    "required"=>false,
                ]);
//            $builder
//                ->add('result',TextType::class,[
//                    "label"=>$measure->getName(),
//                    "attr"=> ["placeholder"=>$measure->getName(),"class"=>'inputmask form-control'],
//                    "required"=>false,
//                ]);
//            $builder->get("result")->addModelTransformer(new TextToInputMaskTransformer());
        } else {
            $builder
                ->add('result',TextType::class,["label"=>$measure->getName(),
                    "attr"=> array_merge($this->getAttrMeasure($measure),["placeholder"=>$measure->getName(),"class"=>'form-control']),
                    "required"=>false,
                    "constraints"=>[
                        new Range(
                            [
                                "min"=>0,
                                "max"=>99999,
                                "minMessage"=>"Debes introducir un valor mayor que {{ limit }}",
                                "maxMessage"=>"Venga Rich Fronning, no te pases, el máximo de Reps es {{ limit }}"]),
                    ]
                ]);
        }
        if($measure->hasTieBreak()){
            $builder
                ->add('tieBreaker',TextType::class,["label"=>"Desempate",
                    "attr"=> ["placeholder"=>"Desempate","class"=>"form-line"],
                    "required"=>false,
                    "constraints"=>[
                        new Range(
                            [
                                "min"=>0,
                                "max"=>9999,
                                "minMessage"=>"Debes introducir un valor mayor que {{ limit }}",
                                "maxMessage"=>"Venga Rich Fronning, no te pases, el máximo de Reps es {{ limit }}"]),
                    ]
                ]);
        }

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\EventResult',
            "measure"=>null,
            "csrf_protection"=>false
        ));
    }
    private function getAttrMeasure(Measure $measure){
        if($measure->getType()==Measure::MEASURE_TYPE_TIME){
            return  ["class"=>"timepicker"];

        }
        return [];
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_eventresult';
    }
}
