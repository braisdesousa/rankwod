<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextToDateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SinglePhaseEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $phaseType=$options["phase_type"];

	    if($phaseType===Phase::TYPE_ONLINE){
		    $builder
			    ->add('requiresVideo',ChoiceType::class,
				    [
					    "choices"=>$this->getYesNoChoices(),
					    "expanded"=>true,
					    "multiple"=>false,
					    "required"=>false,
				    ]);

	    }
	    if($phaseType!==Phase::TYPE_INTERNAL){
		    $builder
			    ->add('acceptApplies',ChoiceType::class,
				    [
					    "choices"=>$this->getYesNoChoices(),
					    "expanded"=>true,
					    "multiple"=>false,
					    "required"=>false,
				    ]);
	    }
        $builder
            ->add('publicResultType',ChoiceType::class,[
                "choices"=>$this->getResultTypeChoices($phaseType),
                "expanded"=>false,
                "multiple"=>false,
                "required"=>false
            ])
	        ->add('desertedResolutionType',ChoiceType::class,[
		        "choices"=>$this->getDesertedTypeChoices(),
		        "expanded"=>false,
		        "multiple"=>false,
		        "required"=>false
	        ])
	        ->add("published",ChoiceType::class,[
		        "choices"=>$this->getYesNoChoices(),
		        "expanded"=>true,
		        "multiple"=>false,
		        "required"=>false,
	        ])
            ->add('tieResolutionType',ChoiceType::class,[
                "label"=>"Resolución de Empates",
                "choices"=>$this->getTieResolutionChoices(),
                "expanded"=>false,
                "multiple"=>false,
                "required"=>false
            ]);

    }

    public function getDesertedTypeChoices(){
    	return [
    		"complete"=>Phase::DESERTED_RESULT_COMPLETE,
    		"games"=>Phase::DESERTED_RESULT_GAMES,
		    ];
    }
    public function getPhaseTypeChoices()
    {
        return [
	        "Presencial"=>Phase::TYPE_ATTENDANCE,
	        "Online"=>Phase::TYPE_ONLINE,
	        "Interna"=>Phase::TYPE_INTERNAL
        ];
    }
    public function getYesNoChoices()
    {
        return [
                 "Si" => true ,
                 "No" => false,
        ];
    }
    public function getResultTypeChoices($phaseType)
    {
        $resultTypes= [
	        "Manual"=>Phase::PUBLISH_RESULT_MANUAL,
            "Instantáneo"=>Phase::PUBLISH_RESULT_INSTANT,
        ];
        if($phaseType==Phase::TYPE_ONLINE){
	        $resultTypes["Al finalizar el Evento"]=Phase::PUBLISH_RESULT_EVENT_EXPIRATION;
        }
        return $resultTypes;
    }
    public function getTieResolutionChoices()
    {
        return [
            "Automático"=>Phase::TIE_RESOLUTION_AUTO,
            "Manual"=>Phase::TIE_RESOLUTION_MANUAL
        ];
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Phase',
	        'csrf_protection'=>false,
	        'phase_type'=>null,
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'phase_single_edit';
    }
}
