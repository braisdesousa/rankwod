<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextToDateTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoundType extends AbstractType
{
    
    private $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Phase $phase */
        $phase=$options["phase"];
        $builder->add('name',
                        TextType::class,
                        [
                            "label"=>"Nombre",
                            "attr"=> ["placeholder"=>"Nombre de la ronda","class"=>"form-control"]
                        ])
                ->add("athletes",EntityType::class,
                    [
                        "multiple"=>true,
                        "expanded"=>true,
                        "class"=>"BDS\RWCategoryBundle\Entity\CategoryAthlete",
                        "label"=>"Atletas",
                        "choices"=>$this->getAthletesInPhase($phase),
                        "choice_label"=>"getNameAndCategory"
                    ]);
        $builder->add("submit",SubmitType::class,["label"=>"Guardar","attr"=>["class"=>"btn btn-primary btn-lg pull-right"]]);

    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Round',
            'phase'=>"null"
        ));
    }
    public function getAthletesInPhase(Phase $phase){
        $result=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findByPhaseOrderedByCategory($phase);
        return $result;
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_round';
    }
}
