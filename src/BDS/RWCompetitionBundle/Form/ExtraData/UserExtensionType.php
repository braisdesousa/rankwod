<?php

namespace BDS\RWCompetitionBundle\Form\ExtraData;

use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserExtensionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var CategoryAthlete $athlete */
        $athlete=$options["athlete"];
        $userExtension=$athlete->getUserExtension();
        $user=$userExtension->getRoninFoxUser();
        if(($user->getFirstName()==(strstr($user->getEmail(), '@', true)))&&!$user->getLastName()){
            $builder->add("firstName",TextType::class,["required"=>true,"label"=>"Nombre","attr"=>["placeholder"=>"Tu Nombre"]]);
            $builder->add("lastName",TextType::class,["required"=>true,"label"=>"Apellidos","attr"=>["placeholder"=>"Tus Apellidos"]]);
        }
        if(!$user->getTelephone()){
            $builder->add("telephone",TextType::class,["required"=>true,"label"=>"Teléfono","attr"=>["placeholder"=>"Teléfono de Contacto"]]);
        }
        if(!$user->getDni()){
            $builder->add("dni",TextType::class,["required"=>true,"label"=>"DNI","attr"=>["placeholder"=>"DNI o Passaporte"]]);
        }
        if(!$user->getDateBirth()){
            $builder->add('dateBirth', DateType::class, array(
                'widget' => 'single_text',
                'label'=>"Fecha de Nacimiento",
                'format'=>"dd/mm/yyy",
                'required'=>true,
                'html5' => false,
                'attr' => ['class' => 'js-datepicker',"placeholder"=>"Haz Click Aquí para añadir tu fecha de nacimiento"],
            ));
        }
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'competition'=> null,
            'athlete'=>null
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user_extra_data';
    }
}
