<?php

namespace BDS\RWCompetitionBundle\Form\ExtraData;

use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterWithAthleteExtraDataCollectionType extends AthleteExtraDataCollectionType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var CategoryAthlete $athlete */
        $athlete=$options["athlete"];
        $userExtension=$athlete->getUserExtension();

        $options["label"]=false;
        $options["data_class"]=UserExtension::class;
        $options["data"]=$userExtension;
        $builder->add("user_extension",AthleteExtensionType::class,$options);

        parent::buildForm($builder,$options);
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'competition'=> null,
            'athlete'=>null
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_athlete_extra_data_collection';
    }
}
