<?php

namespace BDS\RWCompetitionBundle\Form\ExtraData;

use BDS\RWCompetitionBundle\Entity\AthleteExtraData;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\ExtraData;
use BDS\RWCompetitionBundle\Entity\ExtraDataInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AthleteExtraDataCollectionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Competition $competition */
        $competition=$options["competition"];
        /** @var ExtraDataInterface $athlete */
        $athlete=$options["athlete"];

        foreach($competition->getExtraDataCollection() as $key=>$extraData){
            $builder->add("extra_data_".$key,
                AthleteExtraDataType::class,
                [
                    "type"=>$extraData->getType(),
                    "data"=>$this->getValue($extraData,$athlete),
                    "label"=>$extraData->getName()
                ]);
        }
        $builder->add("submit",SubmitType::class,["label"=>"Guardar","attr"=>["class"=>"btn btn-lg btn-primary float-right g-mt-20"]]);
    }

    private function getValue(ExtraData $extraData,ExtraDataInterface $athlete):AthleteExtraData{
        if($athlete->hasExtraData($extraData)){
            return $athlete->getExtraData($extraData);
        }
        $athleteExtraData=new AthleteExtraData();
        $athleteExtraData->setExtraData($extraData);
        return $athleteExtraData;
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'competition'=> null,
            'athlete'=>null
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_athlete_extra_data_collection';
    }
}
