<?php

namespace BDS\RWCompetitionBundle\Form\ExtraData;

use BDS\RWCompetitionBundle\Entity\AthleteExtraData;
use BDS\RWCompetitionBundle\Entity\ExtraData;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextBoolTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextIntTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextKgTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AthleteExtraDataType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $type=$options["type"];

        if($type==ExtraData::TYPE_BOOL){
            $builder->add("value",CheckboxType::class,["label"=>" "]);
        } else {
            $builder->add("value",TextType::class,["required"=>true,"label"=>false]);
        }

        if($type==ExtraData::TYPE_KG){
            $builder->get("value")->addModelTransformer(new TextKgTransformer());
        }
        if($type==ExtraData::TYPE_INT){
            $builder->get("value")->addModelTransformer(new TextIntTransformer());
        }
        if($type==ExtraData::TYPE_BOOL){
            $builder->get("value")->addModelTransformer(new TextBoolTransformer());
        }
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AthleteExtraData::class,
            'type'=>null,
            'value'=>null,
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_athlete_extra_data';
    }
}
