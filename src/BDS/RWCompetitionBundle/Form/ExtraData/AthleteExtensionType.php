<?php

namespace BDS\RWCompetitionBundle\Form\ExtraData;

use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AthleteExtensionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var CategoryAthlete $athlete */
        $athlete=$options["athlete"];
        $userExtension=$athlete->getUserExtension();
        $user=$userExtension->getRoninFoxUser();
        $builder->add("tShirtSize",ChoiceType::class,
            [
                "required"=>true,
                "label"=>"Talla de Camiseta",
                "choices"=>[
                    "XS"=>"XS",
                    "S"=>"S",
                    "M"=>"M",
                    "L"=>"L",
                    "XL"=>"XL",
                ],
                "attr"=>["placeholder"=>"Talla de Camiseta"]]);
        if(!$userExtension->isUserCompleted()){
            $options["data"]=$userExtension->getRoninFoxUser();
            $options["data_class"]=User::class;
            $builder->add("roninFoxUser",UserExtensionType::class,$options);
        }
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserExtension::class,
            'competition'=> null,
            'athlete'=>null
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user_extension';
    }
}
