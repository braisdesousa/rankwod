<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Entity\MeasureGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class MeasureGroupTransformer implements DataTransformerInterface

{
    public function transform($value)
    {

        return $value;
    }
//        yyyy-mm-dd - HH:ii
    /** @var Collection|Measure[] $value */
    public function reverseTransform($value)
    {
        $measureGroups=new ArrayCollection();
        $measureGroup=new MeasureGroup();
        $measureGroups->add($measureGroup);
        $last=$value->last();
        foreach($value as $measure) {
            $measureGroup->addMeasure($measure);
            if($measure->isSplitResult() and ($measure!==$last)){
                $measureGroup=new MeasureGroup();
                $measureGroups->add($measureGroup);
            }
        }
       return $measureGroups;
    }

}