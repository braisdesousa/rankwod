<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class TextToInputMaskTransformer implements DataTransformerInterface

{

    public function transform($value)
    {
//        yyyy-mm-dd - HH:ii
       if($value!==null){
           return gmdate("i:s", $value);
       }
        return $value;
    }
//        yyyy-mm-dd - HH:ii
    public function reverseTransform($value)
    {
       $dateTime=\DateTime::createFromFormat("i:s",$value);
        return $dateTime->getTimestamp();
    }

}