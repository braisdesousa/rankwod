<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class UsernameTransformer implements DataTransformerInterface

{
    private $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function transform($value)
    {
        $response_array=[];
       if(is_array($value)){
           /** @var User $user */
           foreach($value as $user){
               $response_array[]=$user->getUsername();
           }
       }
       return $response_array;
    }

    public function reverseTransform($value)
    {
        $users=[];
        foreach($value as $username){
            if($user=$this->entityManager->getRepository("BDSUserBundle:User")->findOneByUsername($username)){
                $users[]=$user;
            }
        }
        return $users;
    }

}