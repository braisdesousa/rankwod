<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class TextToDateTransformer implements DataTransformerInterface

{

    public function transform($value)
    {
//        yyyy-mm-dd - HH:ii
       if($value instanceof \DateTime){
            return $value->format("j-n-Y H:i");
       }
        return $value;
    }
//        yyyy-mm-dd - HH:ii
    public function reverseTransform($value)
    {
       if(strpos($value,"Invalid")!==false){
           return null;
       }
        $dateTime=new \DateTime($value);
        return $dateTime;
    }

}