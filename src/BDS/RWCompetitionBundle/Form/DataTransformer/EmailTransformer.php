<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class EmailTransformer implements DataTransformerInterface

{

    public function transform($value)
    {
       if(is_array($value)){
           return implode(" ",$value);
       } else {
           return $value;
       }
    }

    public function reverseTransform($value)
    {
        $value=str_replace(array("\n", "\r","\r\n"), " ", $value);
        $value=str_replace(array("  "), " ", $value);
        $emails=(explode(" ",$value));
        return $emails;
    }

}