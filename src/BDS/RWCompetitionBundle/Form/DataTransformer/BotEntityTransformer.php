<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Service\CompetitionUserService;
use Symfony\Component\Form\DataTransformerInterface;


class BotEntityTransformer implements DataTransformerInterface

{
	private $competitionUserService;
	private $competition;

	public function __construct(CompetitionUserService $competitionUserService,Competition $competition) {
		$this->competitionUserService=$competitionUserService;
		$this->competition=$competition;
	}

	public function transform($value)
    {
        return $value;
    }

    public function reverseTransform($value)
    {
        $names=(explode(",",$value));
        return $this->competitionUserService->createBots($this->competition,$names);
    }

}