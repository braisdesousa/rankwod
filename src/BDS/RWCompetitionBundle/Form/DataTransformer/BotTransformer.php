<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class BotTransformer implements DataTransformerInterface

{

    public function transform($value)
    {
        return $value;
    }

    public function reverseTransform($value)
    {
        $names=(explode(",",$value));
        return $names;
    }

}