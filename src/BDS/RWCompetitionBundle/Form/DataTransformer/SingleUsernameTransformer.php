<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Egulias\EmailValidator\EmailValidator;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class SingleUsernameTransformer implements DataTransformerInterface

{
    private $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function transform($value)
    {

       if($value instanceof UserInterface){
           return $value->getUsername();
       }
       return $value;
    }

    public function reverseTransform($value)
    {

            if($user=$this->entityManager->getRepository("BDSUserBundle:User")->findOneByUsername($value)){
                return $user;

        }
        return null;
    }

}