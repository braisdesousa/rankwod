<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class YoutubeVideoTransformer implements DataTransformerInterface

{

    public function transform($value)
    {

        return $value;
    }
//        yyyy-mm-dd - HH:ii
    public function reverseTransform($value)
    {
        if($value){
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $value, $matches);
            if((count($matches)<1) || (empty($matches[1]))){
                throw new TransformationFailedException(sprintf(
                    'The Url "%s" is not valid',
                    $value
                ));
            }
            return sprintf("https://www.youtube.com/embed/%s",$matches[1]);
        }
        return $value;

    }

}