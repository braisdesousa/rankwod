<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/12/16
 * Time: 11:39 AM
 */

namespace BDS\RWCompetitionBundle\Form\DataTransformer;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Service\CompetitionUserService;
use Symfony\Component\Form\DataTransformerInterface;


class TextIntTransformer implements DataTransformerInterface

{


	public function transform($value)
    {
        return $value;
    }

    public function reverseTransform($value)
    {
        return intval($value);
    }

}