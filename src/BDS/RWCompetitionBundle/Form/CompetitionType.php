<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Form\Address\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class CompetitionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('name',TextType::class,
		        [
		            "label"=>false,
		        	"required"=>true,
		            "attr"=>["class"=>"form-control"],
		            "constraints"=>[new Length(["min"=>3,"max"=>200,"minMessage"=>"El nombre debe tener más de 3 letras","maxMessage"=>"El nombre no debe superar las 200 letras"])],
		            ])
	        ->add('text',TextareaType::class,["label"=>"Descripción","required"=>false,
                "attr"=>[
                    "rows"=>"4",
                    "class"=>"form-control no-resize trumbowyg"
                ]
            ])
	        ->add('slug',TextType::class,["label"=>"Slug","required"=>true,"attr"=>["class"=>"form-control"]])
	        ->add('address',AddressType::class,["label"=>"Dirección","required"=>true,"attr"=>["class"=>"form-control"]])
	        ->add("submit",SubmitType::class,["label"=>"GUARDAR","attr"=>["class"=>"btn btn-lg btn-primary waves-effect"]]);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Competition'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'competition';
    }
}
