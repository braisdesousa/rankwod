<?php

namespace BDS\RWCompetitionBundle\Form;


use BDS\CoreBundle\Form\DataTransformer\CamelCaseNameTransformer;
use BDS\CoreBundle\Form\DataTransformer\StrToUpperTransformer;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use BDS\UserBundle\Form\EditProfileType as BaseEditProfileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $disableEdit=$options["disable_edit"];
        $builder->setDisabled($disableEdit);
        $transformer= new CamelCaseNameTransformer();
        $strToUpperTransformer= new StrToUpperTransformer();
            $builder
                ->add("firstName",TextType::class,["label"=>"Nombre","required"=>true,"attr"=>["placeholder"=>"Nombre del usuario","autocomplete"=>'off',"class"=>"form-control"]])
                ->add("lastName",TextType::class,["label"=>"Apellidos","required"=>true,"attr"=>["placeholder"=>"Apellidos del usuario","autocomplete"=>'off',"class"=>"form-control"]])
                ->add('username',TextType::class,["label"=>'Nombre de Usuario ',"required"=>true,"attr"=>["placeholder"=>"Nombre de Usuario","class"=>"form-control"]])
                ->add('email',EmailType::class,["label"=>"Email","required"=>true,"attr"=>["placeholder"=>"tuemail@host.com","class"=>"form-control"]])
                ->add('dateBirth',DateType::class,[
                    "label"=>"Fecha de Nacimiento (Día - Mes - Año)",
                    "widget"=>"choice",
                    "format"=>'dd-MM-yyyy',
                    "years"=>$this->getYears(),
                    "required"=>(false),
                    'placeholder' => array(
                        'year' => 'Año', 'month' => 'Mes', 'day' => 'Día'
                    )
                ])
                ->add('gender',ChoiceType::class,[
                    "label"=>"Género",
                    "attr"=>["class"=>"form-control"],
                    "placeholder"=>"Selecciona tu Categoría",
                    "choices"=>["Masculino"=>User::GENDER_MALE,"Femenino"=>User::GENDER_FEMALE],
                    "choices_as_values"=>true,
                    "required"=>(true)
                ])

                ->add("telephone",TextType::class,["label"=>"Teléfono","required"=>false,"attr"=>["placeholder"=>"666666666","autocomplete"=>'off',"class"=>"form-control"]])
                ->add("dni",TextType::class,["label"=>"Dni","required"=>false,"attr"=>["placeholder"=>"00000000X","autocomplete"=>'off',"class"=>"form-control"]])
                ->add("tshirt",ChoiceType::class,
                    [
                        "required"=>true,
                        "label"=>"Talla de Camiseta",
                        "mapped"=>false,
                        "choices"=>[
                            "XS"=>"XS",
                            "S"=>"S",
                            "M"=>"M",
                            "L"=>"L",
                            "XL"=>"XL",
                        ],
                "attr"=>["placeholder"=>"Talla de Camiseta"]]);
        $builder->add("submit",SubmitType::class,["label"=>"Guardar ","attr"=>['class'=>'pull-right btn bg-light-blue btn-link waves-effect m-t-20']]);
        $builder->get("dni")->addModelTransformer($strToUpperTransformer);
        $builder->get("firstName")->addModelTransformer($transformer);
        $builder->get("lastName")->addModelTransformer($transformer);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\UserBundle\Entity\User',
            'add_submit'=>false,
            "admin_edit"=>false,
            "is_admin"=>false,
            "disable_edit"=>false,
        ));
    }
    public function getYears(){
        $date=new \DateTime("-16 years");
        $months=[];
        for($i=0;$i<60;$i++){
            $date->modify("-1 year");
            $months[]=$date->format("Y");
        }
        return $months;
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_userbundle_profile';
    }
}
