<?php

namespace BDS\RWCompetitionBundle\Form\Register;

use BDS\RWCompetitionBundle\Entity\Competition;

use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\MeasureGroupTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextToDateTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\YoutubeVideoTransformer;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Form\MeasureGroupType;
use BDS\RWMeasureBundle\Form\MeasureType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FullRegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        /** @var Phase $phase */
        $phase=$options["phase"];
        $builder->add("firstName",  TextType::class,["required"=>true,"label"=>false,"attr"=>["placeholder"=>"form.firstname"]])
        ->add("lastName",   TextType::class,["required"=>true,"label"=>false,"attr"=>["placeholder"=>"form.lastname"]])
        ->add("email",      EmailType::class,["label"=>false,"attr"=>["placeholder"=>"form.email"]])
        ->add("box",        TextType::class,["label"=>false,"attr"=>["placeholder"=>"form.box"]]);
        $builder->add('category',EntityType::class,
                [
                    "class"=>"BDS\RWCategoryBundle\Entity\Category",
                    "choice_value"=>"slug",
                    "label"=>false,
                    "placeholder"=>"Elije tu categoría",
                    "required"=>true,
                    "choices"=>$phase->getCategories(),
                    "choice_label"=>"name",
                    "attr"=>["class"=>"form-control"]
                ]
        );
        $builder->add("submit",SubmitType::class,
            [
                "label"=>($phase->getFloatPrice()?'Inscribirme y pagar':'Inscribirme'),"attr"=>["class"=>"btn btn-lg btn-primary"]]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'phase'=>null,
            "translation_domain"=>"games"
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rw_competition_register';
    }
}
