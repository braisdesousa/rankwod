<?php

namespace BDS\RWCompetitionBundle\Form\Register;

use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Validator\RegisterEmail;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailRegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Phase $phase */
        $phase=$options["phase"];
        $builder->add("email",      EmailType::class,
                [
                    "label"=>"form.email",
                    "attr"=>["placeholder"=>"Introduce un email válido", "class"=>"form-control"],
                    "constraints"=>[new RegisterEmail()]]);
        $builder->add('category',EntityType::class,
                [
                    "label"=>"Categoría",
                    "class"=>"BDS\RWCategoryBundle\Entity\Category",
                    "choice_value"=>"slug",
                    "choices"=>$phase->getCategories(),
                    "choice_label"=>"registerName",
                    "attr"=>["class"=>"form-control"]
                ]
        );
        $builder->add("submit",SubmitType::class,
            [
                "label"=>('Inscribirme'),"attr"=>["class"=>"btn btn-lg btn-primary float-right g-mt-20"]]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'phase'=>null,
            "translation_domain"=>'FOSUserBundle'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rw_competition_register';
    }
}
