<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\CoreBundle\Helpers\RepositoryHelper;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextToDateTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportPhaseAthletesType extends AbstractType
{
    private $entityManager;
    private $oldPhase;
    /** @var  Phase $phase */
    private $phase;
    private $autoInclude;
    private $phaseAthletes=[];

    private $testNames1=[];
    private $testNames2=[];
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Phase $oldPhase */
        $oldPhase=$options["old_phase"];
        /** @var Phase $phase */
        $this->phase=$options["phase"];
        $this->autoInclude=$options["auto_include"];
        $this->oldPhase=$oldPhase;
        $this->phaseAthletes=RepositoryHelper::getIdsFromResult($this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->getIdsByPhase($this->phase),'extension_id');
        $builder->add('category',EntityType::class,
            [   "class"=>'BDS\RWCategoryBundle\Entity\Category',
                "label"=>sprintf('Categoría de %s',$this->phase->getName()),
                "choice_label"=>'name',
                "choices"=>$this->phase->getCategories(),
                "attr"=>["class"=>"form-control"]
            ]
        );
        /** @var Category $category */
        foreach($oldPhase->getCategories() as $category) {
            
            $builder
                ->add(sprintf('%s_athletes',$category->getSlug()),
                    EntityType::class,
                    [
                        "label"=>sprintf("Atletas categoría %s",$category->getName()),
                        "multiple"=>true,
                        "expanded"=>true,
                        "class"=>'BDS\RWCategoryBundle\Entity\CategoryAthlete',
                        "choices"=>$this->getOrderedOverallAthletes($oldPhase,$category),
                        'choice_label' => function (CategoryAthlete $categoryAthlete) {
                            return $this->getDisplayName($categoryAthlete);
                        },
                        'choice_attr' => function($val, $key, $index) {
                            /** @var CategoryAthlete $val */
                            if(in_array($val->getUserExtension()->getId(),$this->phaseAthletes)){
                                return ['checked' => 'checked'];
                            } else {
                                return [];
                            }
                        },
                    ])

            ;
        }
        $builder->add("submit",SubmitType::class,["label"=>"Guardar"]);
    }
    public function getDisplayName(CategoryAthlete $categoryAthlete)
    {
        $result=$this->entityManager->getRepository("BDSRWCompetitionBundle:OverallResult")->findOneByAthleteAndPhase($categoryAthlete,$this->oldPhase);
        return sprintf("# %s - %s",$result->getResult(),$categoryAthlete->getUserExtension()->getRoninFoxUser()->getCompleteName());
    }
    public function getOrderedOverallAthletes(Phase $oldPhase, $category)
    {
        $athletesOrdered=[];
        $resultsOrdered= $this->entityManager->getRepository("BDSRWCompetitionBundle:OverallResult")->findByCategoryAndOverallOrderByResult($category,$oldPhase->getOverall(),$oldPhase->isDesertedResultLikeGames());
        foreach($resultsOrdered as $result ){
            $athletesOrdered[]=$result->getAthlete();
        }
        return $athletesOrdered;
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'method'=>"POST",
            'old_phase'=>null,
            'phase'=>null,
            'auto_include'=>null,
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_import_phase_athletes';
    }
}
