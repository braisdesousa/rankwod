<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Competition;

use BDS\RWCompetitionBundle\Form\DataTransformer\MeasureGroupTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextToDateTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\YoutubeVideoTransformer;
use BDS\RWMeasureBundle\Form\MeasureGroupType;
use BDS\RWMeasureBundle\Form\MeasureType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Competition $competition */
//        yyyy-mm-dd - HH:ii
        $showMeasures=$options["show_measures"];
        $finalAvailable=$options["final_available"];
        $is_online=$options['is_online'];
        $builder->add('name',TextType::class,["label"=>"Nombre del Evento"]);
        if($is_online){
	        $builder
		            ->add('videoUrl',UrlType::class,["label"=>"Url del Video",'required'=>false])
	                ->add('publishedAt',TextType::class,
		                ['label'=>"Fecha de Publicación",'attr'=>["class"=>"datetimepicker","data-date-format"=>"dd-mm-yyyy hh:ii"]] )
	                ->add('limitDate',TextType::class,
		                ['label'=>"Fecha Límite",'attr'=>["class"=>"datetimepicker","data-date-format"=>"dd-mm-yyyy hh:ii"]]
	                );
	        $builder->get('publishedAt')->addModelTransformer(new TextToDateTransformer());
	        $builder->get('limitDate')->addModelTransformer(new TextToDateTransformer());
	        $builder->get('videoUrl')->addModelTransformer(new YoutubeVideoTransformer());
        }



            $builder->add('text',TextareaType::class,["label"=>"Descripción del Evento","required"=>false,"attr"=>["class"=>"trumbowyg"]]);

            if($showMeasures){
                $builder->add("measureGroups",CollectionType::class,[
                    "label"=>" ",
                    "entry_type"=>MeasureType::class,
                    "allow_add"=>true,
                    "allow_delete"=>true,
                    "prototype"=>true,
                ]);
                $builder->get('measureGroups')->addModelTransformer(new MeasureGroupTransformer());
            }
//        if($finalAvailable){
//            $builder->add("final",CheckboxType::class,["label"=>"¿Es la final?",]);
//        }
        $builder->add("submit",SubmitType::class,["label"=>"Guardar","attr"=>["css"=>"btn btn-primary pull-right"]]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Event',
            'show_measures'=>null,
            'final_available'=>false,
	        'is_online'=>null,
        ));
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_event';
    }
}
