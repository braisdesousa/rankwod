<?php

namespace BDS\RWCompetitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class TeamFinderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text',TextType::class,[
            	    "label"=>"Email o DNI del Capitan",
	                "required"=>true,
	                "attr"=>["placeholder"=>"Email o DNI del Capitán"],
	                "constraints"=>[
	                	new NotBlank(),
		                new NotNull(),
		                new Length([
		                	"min"=>3,
			                "minMessage"=>"El nombre debe tener más de 3 carácteres",
		                    "max"=>100 ,
		                    "maxMessage"=>"El nombre debe tener menos de 100 carácteres"])
	                ]])
	        ->add("submit",SubmitType::class,["label"=>"Buscar","attr"=>["class"=>"btn-success pull-right"]]);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'team_finder';
    }
}
