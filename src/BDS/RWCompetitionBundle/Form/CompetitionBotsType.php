<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\BotEntityTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\EmailTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\UsernameTransformer;
use BDS\RWCompetitionBundle\Service\CompetitionUserService;
use BDS\RWCompetitionBundle\Validator\EmailArray;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class CompetitionBotsType extends AbstractType
{


	private $competitionUserService;


	public function __construct(CompetitionUserService $competitionUserService) {

		$this->competitionUserService=$competitionUserService;

	}

	/**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

		/** @var Phase $phase */
	    $phase=$options["phase"];
	    $competition=$phase->getCompetition();
        $builder
            ->add('category',EntityType::class,[
                "class"=>'BDS\RWCategoryBundle\Entity\Category',
                "choices"=>$this->getCategories($phase),
                "choice_label"=> "getName",
                "label"=>"Categoría"

            ])
            ->add('bots',TextType::class,[
            	    "label"=>"Atletas de la Competición",
	                "attr"=>['data-role'=>'tagsinput'],
	                "constraints"=>[new NotNull()],
            ])
            ->add('submit',SubmitType::class,["label"=>"Añadir Bots","attr"=>["class"=>"btn-success pull-right"]]);
        $builder->get('bots')->addModelTransformer(new BotEntityTransformer($this->competitionUserService,$competition));

    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "mapped"=>false,
            "phase"=>null
        ));

    }

    public function getCategories(Phase $phase)
    {
        return $phase->getCategories()->toArray();
    }


    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_competition_bots';
    }
}
