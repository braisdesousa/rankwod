<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Competition;

use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Form\DataTransformer\MeasureGroupTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextToDateTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\YoutubeVideoTransformer;
use BDS\RWMeasureBundle\Entity\Measure;
use BDS\RWMeasureBundle\Form\MeasureGroupType;
use BDS\RWMeasureBundle\Form\MeasureType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SimpleEditEventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


//        yyyy-mm-dd - HH:ii
        $is_online=$options['is_online'];
        $has_points=$options['has_points'];
        $builder->add('name',TextType::class,["label"=>"Nombre del Evento","attr"=>["class"=>"form-control"]]);
        if($is_online){
	        $builder
		            ->add('videoUrl',UrlType::class,["label"=>"Url del Video",'required'=>false,"attr"=>["class"=>"form-control"]]);
	        $builder->get('videoUrl')->addModelTransformer(new YoutubeVideoTransformer());
        }
        $builder->add('publishedAt',TextType::class,
        ['label'=>"Fecha de Publicación",'attr'=>["class"=>"datetimepicker form-control","data-date-format"=>"dd-mm-yyyy hh:ii"]] )
        ->add('limitDate',TextType::class,
            ['label'=>"Fecha Límite",'attr'=>["class"=>"datetimepicker form-control","data-date-format"=>"dd-mm-yyyy hh:ii"]]
        );
        $builder->get('publishedAt')->addModelTransformer(new TextToDateTransformer());
        $builder->get('limitDate')->addModelTransformer(new TextToDateTransformer());
        $builder->add('text',TextareaType::class,
                [
                    "label"=>"Descripción del Evento",
                    "required"=>false,
                    "attr"=>["rows"=>"4","class"=>"form-control no-resize trumbowyg"]
                ]);
        if($has_points){
            $builder->add('pointSystem',ChoiceType::class,[
                "label"=>"Puntos del Evento",
                "choices"=>$this->getPointChoices(),
                "expanded"=>false,
                "attr"=>[
                    "class"=>"form-control"
                ]]);
        }
//        if($finalAvailable){
//            $builder->add("final",CheckboxType::class,["label"=>"¿Es la final?",]);
//        }
        $builder->add("submit",SubmitType::class,["label"=>"Guardar","attr"=>["class"=>"btn btn-lg btn-primary pull-right"]]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Event',
            'show_measures'=>null,
            'final_available'=>false,
	        'is_online'=>null,
	        'has_points'=>null,
        ));
    }
    public function getPointChoices(){
        return [
            "50 Puntos"=>Event::POINT_SYSTEM_50,
            "100 Puntos"=>Event::POINT_SYSTEM_100,
            "200 Puntos"=>Event::POINT_SYSTEM_200
        ];
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_event';
    }
}
