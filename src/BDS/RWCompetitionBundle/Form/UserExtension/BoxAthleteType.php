<?php

namespace BDS\RWCompetitionBundle\Form\UserExtension;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxAthleteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('box',EntityType::class,
                [
                    "class"=>'BDS\RWBoxBundle\Entity\Box',
                    "choice_label"=>"name",
                    "choice_value"=>"slug",
                    "placeholder"=>"Encuentra tu Box",
                    "required"=>true,
                    "attr"=>["class"=>"form-control"]
                ])
            ->add('submit',SubmitType::class,["label"=>"Confirmar Box","attr"=>["class"=>'pull-right btn bg-light-blue btn-link waves-effect']]);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\UserExtension'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_boxathlete';
    }
}
