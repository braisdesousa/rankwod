<?php

namespace BDS\RWCompetitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class BotType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
            	    "label"=>"Nombre",
	                "required"=>true,
	                "constraints"=>[
	                	new NotBlank(),
		                new NotNull(),
		                new Length([
		                	"min"=>3,
			                "minMessage"=>"El nombre debe tener más de 3 carácteres",
		                    "max"=>100 ,
		                    "maxMessage"=>"El nombre debe tener menos de 100 carácteres"])
	                ]])
	        ->add("submit",SubmitType::class,["label"=>"Guardar","attr"=>["class"=>"btn-success pull-right"]]);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Bot'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_bot';
    }
}
