<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 28/03/16
 * Time: 18:10
 */

namespace BDS\RWCompetitionBundle\Form\Team;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Collection;

class TeamType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,["label"=>"Nombre"])
            ->add('text',TextareaType::class,["label"=>"Presentación","attr"=>["class"=>"trumbowyg"]])
            ->add('athletes', EntityType::class,
                [
                    "class"=>"BDS\RWCompetitionBundle\Entity\TeamAthlete",
                    "label" => false,
                    "multiple"=>true,
                    "attr"=>["class"=>"hide"]
                ])
	        ->add("submit",SubmitType::class,["label"=>"Crear Equipo","attr"=>["class"=>"btn btn-primary pull-right"]]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Team'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_team';
    }
}