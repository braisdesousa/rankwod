<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\EmailTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\UsernameTransformer;
use BDS\RWCompetitionBundle\Validator\EmailArray;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompetitionUsersType extends AbstractType
{
    protected $userEntityManager;

    public function __construct(EntityManager $userEntityManager)
    {
        $this->userEntityManager=$userEntityManager;
        
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category',EntityType::class,[
                "class"=>'BDS\RWCategoryBundle\Entity\Category',
                "choices"=>$this->getCategories($options["phase"]),
                "choice_label"=> "getName",
                "label"=>"Categoría"

            ])
            ->add("invitation",CheckboxType::class,["label"=>"¿Están Invitados? (exentos de pago)","required"=>false])
            ->add('athletes',EntityType::class,["class"=>'BDS\UserBundle\Entity\User',"label"=>false,"required"=>false,"multiple"=>true,"attr"=>["class"=>"hidden"]])
            ->add('text',TextareaType::class,["label"=>"Listado de Correos", "required"=>false,"constraints"=>[new EmailArray()]])
            ->add('submit',SubmitType::class,["label"=>"Añadir Ususarios","attr"=>["class"=>"pull-right btn-success"]])
        ;
        $builder->get("text")->addModelTransformer(new EmailTransformer());
//        $builder->get("text")->addModelTransformer(new UsernameTransformer($this->userEntityManager));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "mapped"=>false,
            "phase"=>null
        ));

    }

    public function getCategories(Phase $phase)
    {
        return $phase->getCategories()->toArray();
    }
    public function getPhases(Competition $competition){
        return $competition->getPhases()->toArray();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_competition_users';
    }
}
