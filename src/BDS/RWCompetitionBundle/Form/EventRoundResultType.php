<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWMeasureBundle\Entity\Measure;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Tests\Extension\Validator\Type\SubmitTypeValidatorExtensionTest;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;

class EventRoundResultType extends AbstractType
{
    private $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Collection|CategoryAthlete[] */
        $athletes=$options["athletes"];
        /** @var Event $event */
        $event=$options["event"];
        /** @var CategoryAthlete $athlete */
        foreach($athletes as $athlete){

            $results=$this->entityManager->getRepository("BDSRWCompetitionBundle:EventResult")->findByAthleteAndEventOrderByMeasure($athlete,$event);
            $builder
                ->add(
                    Urlizer::transliterate(sprintf("athlete_result_%s",$athlete->getName()),"_"),
                    EventResultsType::class,
                    [
                        "results"=>$results,
                        "athlete"=>$athlete,
                        "event"=>$event,
                        "is_judge"=>true,
                        "is_heat"=>true,
                        "required"=>false,
                        "label"=>sprintf("Resultado de '%s'",$athlete->getName())
                    ]);
        }
        $builder->add("submit",SubmitType::class,
            [
                "label"=>"Guardar Resultados Heat",
                "attr"=>["class"=>"btn btn-primary btn-lg pull-right"]
            ]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'athletes'=> null,
            "event"=>null,
            "method"=>"POST"
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_round_eventresult';
    }
}
