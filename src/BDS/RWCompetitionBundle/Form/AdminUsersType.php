<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\EmailTransformer;
use BDS\RWCompetitionBundle\Form\DataTransformer\UsernameTransformer;
use BDS\RWCompetitionBundle\Validator\EmailArray;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminUsersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('admins',EntityType::class,["class"=>'BDS\UserBundle\Entity\User',"label"=>false,"required"=>false,"multiple"=>true,"attr"=>["class"=>"hidden"]])
            ->add('text',TextareaType::class,["label"=>"Listado de Correos", "required"=>false,"constraints"=>[new EmailArray()]])
            ->add('submit',SubmitType::class,["label"=>"Añadir Administradores","attr"=>["class"=>"pull-right btn-success"]])
        ;
        $builder->get("text")->addModelTransformer(new EmailTransformer());
//        $builder->get("text")->addModelTransformer(new UsernameTransformer($this->userEntityManager));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "mapped"=>false,
        ));

    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'competition_admins';
    }
}
