<?php

namespace BDS\RWCompetitionBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use BDS\UserBundle\Form\UserType as BaseUserType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends BaseUserType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder,$options);
//        $builder->add("Box",EntityType::class,[
//            "class"=>'BDS\RWBoxBundle\Entity\Box',
//            "query_builder"=>function(EntityRepository $repository){
//                return $repository->findBoxesOrderedByNameQB();
//            },
//            "choice_label"=> "getName",
//            "data"=>$options["box"],
//            "label"=>"Box",
//            "placeholder"=>"Selecciona tu Box",
//            "em"=>'default',
//            "mapped"=>false,
//            "required"=>false
//        ]);
        $builder->add("submit",SubmitType::class,["label"=>"Crear Usuario"]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\UserBundle\Entity\User',
            'box' => null
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_competition_user';
    }
}
