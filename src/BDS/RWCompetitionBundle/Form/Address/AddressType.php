<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/01/19
 * Time: 9:32
 */

namespace BDS\RWCompetitionBundle\Form\Address;

use Sylius\Bundle\AddressingBundle\Form\Type\CountryCodeChoiceType;
use Sylius\Bundle\AddressingBundle\Form\Type\ProvinceCodeChoiceType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractResourceType
{
    /**
     * @var EventSubscriberInterface
     */
    private $buildAddressFormSubscriber;

    /**
     * @param string $dataClass
     * @param string[] $validationGroups
     * @param EventSubscriberInterface $buildAddressFormSubscriber
     */
    public function __construct(string $dataClass, array $validationGroups, EventSubscriberInterface $buildAddressFormSubscriber)
    {
        parent::__construct($dataClass, $validationGroups);

        $this->buildAddressFormSubscriber = $buildAddressFormSubscriber;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('street', TextType::class, [
                'label' => 'sylius.form.address.street',
                'attr'=>["class"=>"form-control","placeholder"=>"Dirección ( calle, numero, etc etc )"]
            ])
            ->add('city', TextType::class, [
                'label' => 'sylius.form.address.city',
                'attr'=>["class"=>"form-control","placeholder"=>"sylius.form.address.city"]
            ])
            ->add('postcode', TextType::class, [
                'label' => 'sylius.form.address.postcode',
                'attr'=>["class"=>"form-control","placeholder"=>"sylius.form.address.postcode"]
            ])

            ->add('provinceCode', ProvinceCodeChoiceType::class, [
                'label' => 'sylius.form.address.province',
                'attr'=>["class"=>"form-control","placeholder"=>"sylius.form.address.province"]
            ])
            ->add('countryCode', CountryCodeChoiceType::class, [
                'label' => 'sylius.form.address.country',
                'enabled' => true,
                'attr'=>["class"=>"form-control","placeholder"=>"sylius.form.address.countryCode"]
            ])
            ->addEventSubscriber($this->buildAddressFormSubscriber)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefaults([
                'validation_groups' => function (Options $options) {
                    if ($options['shippable']) {
                        return array_merge($this->validationGroups, ['shippable']);
                    }

                    return $this->validationGroups;
                },
                'shippable' => false,
            ])
            ->setAllowedTypes('shippable', 'bool')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'sylius_address';
    }
}
