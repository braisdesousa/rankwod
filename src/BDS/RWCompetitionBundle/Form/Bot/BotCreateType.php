<?php

namespace BDS\RWCompetitionBundle\Form\Bot;

use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Repository\CategoryRepository;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\BotTransformer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class BotCreateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Phase $phase */
        $phase=$options["phase"];

        $builder
            ->add('names',TextType::class,[
            	    "label"=>"Bots",
	                "required"=>true,
	                "attr"=>['data-role'=>'tagsinput'],
	                "constraints"=>[new NotNull()]])
            ->add('category',EntityType::class,
                [   "class"=>'BDS\RWCategoryBundle\Entity\Category',
                    "label"=>sprintf('Categoría de %s',$phase->getName()),
                    "choice_label"=>'name',
                    "choices"=>$phase->getCategories(),
                    "attr"=>["class"=>"form-control"]
                ]
            )
	        ->add("submit",SubmitType::class,["label"=>"Guardar","attr"=>["class"=>"btn-success pull-right"]]);
        ;
        $builder->get("names")->addModelTransformer(new BotTransformer());
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped'=>false,
            'phase'=>false,
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_bot';
    }
}
