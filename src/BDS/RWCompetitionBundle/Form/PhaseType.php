<?php

namespace BDS\RWCompetitionBundle\Form;

use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Form\DataTransformer\TextToDateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',
                        TextType::class,
                        [
                            "label"=>"Nombre",
                            "attr"=> ["placeholder"=>"Nombre de la fase 'online', 'presencial',...","class"=>"form-control"]
                        ])
            ->add('phase_type',
                        ChoiceType::class,
                        [
                            "label"=>"Tipo de Fase",
                            "choices"=>$this->getPhaseTypeChoices(),
                            "expanded"=>true,
                            "multiple"=>false
                        ])
            ->add('requiresVideo',ChoiceType::class,
                [
                    "label"=>"Se requiere Video?",
                    "choices"=>$this->getYesNoChoices(),
                    "expanded"=>true,
                    "multiple"=>false
                ])
            ->add('acceptApplies',ChoiceType::class,
                [
                    "label"=>"¿Acepta Solicitudes?",
                    "choices"=>$this->getYesNoChoices(),
                    "expanded"=>true,
                    "multiple"=>false
                ])
            ->add('publicResultType',ChoiceType::class,[
                "label"=>"Publicación de Resultados",
                "choappices"=>$this->getPublicResultTypeChoices(),
                "expanded"=>false,
                "multiple"=>false
            ])
            ->add('typeResults',ChoiceType::class,[
                "label"=>"Tipo de Resultados de Resultados",
                "choices"=>$this->getResultTypeChoices(),
                "expanded"=>false,
                "multiple"=>false
            ])
            ->add('startDate', DateType::class, array(
                'widget' => 'single_text',
                'label'=>"Fecha Inicio Competición",
                'format'=>"dd/MM/yyyy",
                'required'=>true,
                'html5' => false,
                'attr' => ['class' => 'js-datepicker form-control',"placeholder"=>"Haz Click aquí para establecer la fecha de inicio"],
            ))
            ->add('endDate', DateType::class, array(
                'widget' => 'single_text',
                'label'=>"Fecha fin de Competición",
                'format'=>"dd/MM/yyyy",
                'required'=>true,
                'html5' => false,
                'attr' => ['class' => 'js-datepicker form-control',"placeholder"=>"Haz Click aquí para establecer la fecha din de la competición"],
            ))
//            ->add('publishedAt',TextType::class,
//                ['label'=>"Fecha de Publicación",'attr'=>["class"=>"datetimepicker form-control","data-date-format"=>"dd-mm-yyyy hh:ii"]]
//            )
            ->add("submit",SubmitType::class,["label"=>"Guardar","attr"=>["class"=>"btn btn-primary btn-lg pull-right"]]);
            ;
//        $builder->get('publishedAt')->addModelTransformer(new TextToDateTransformer());
    }
    public function getPhaseTypeChoices()
    {
        return [
	        "Fase Online"=>Phase::TYPE_ONLINE,
	        "Competición Interna"=>Phase::TYPE_INTERNAL,
	        "Fase Presencial"=>Phase::TYPE_ATTENDANCE,
        ];
    }
    public function getPublicResultTypeChoices()
    {
        return [
            "Al finalizar el Evento"=>Phase::PUBLISH_RESULT_EVENT_EXPIRATION,
            "Instantáneo"=>Phase::PUBLISH_RESULT_INSTANT,
            "Manual"=>Phase::PUBLISH_RESULT_MANUAL,
        ];
    }

    public function getYesNoChoices()
    {
        return [
                 "Si" => true ,
                 "No" => false,
        ];
    }
    public function getResultTypeChoices()
    {
        return [
            "Suma de Posiciones"=>Phase::TYPE_RESULT_POSITION,
            "Eventos con Puntos"=>Phase::TYPE_RESULT_POINTS,
        ];
    }
    public function getTieResolutionChoices()
    {
        return [
            "Automático"=>Phase::TIE_RESOLUTION_AUTO,
            "Manual"=>Phase::TIE_RESOLUTION_MANUAL
        ];
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCompetitionBundle\Entity\Phase'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcompetitionbundle_phase';
    }
}
