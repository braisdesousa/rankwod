<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 3:31 PM
 */

namespace BDS\RWCategoryBundle\Repository;


use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;

class CategoryRepository extends EntityRepository
{
	public function findOneByPhaseAndSlug(Phase $phase, $slug){
		$qb=$this->createQueryBuilder("categoryRepository");
		$qb->setMaxResults(1);
		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq("categoryRepository.phase",$qb->expr()->literal($phase->getId())),
				$qb->expr()->eq("categoryRepository.slug",$qb->expr()->literal($slug))
			)
		);
		return $qb->getQuery()->getOneOrNullResult();
	}
    public function findOneByCompetitionAndSlug(Competition $competition, $slug)
    {
        $qb=$this->createQueryBuilder("categoryRepository");
        $qb->leftJoin("categoryRepository.phase","phase");
        $qb->setMaxResults(1);
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("phase.competition",$qb->expr()->literal($competition->getId())),
                $qb->expr()->eq("categoryRepository.slug",$qb->expr()->literal($slug))
            )
            );
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findUsersByCompetitionQb(Competition $competition){
        $qb=$this->createQueryBuilder("categoryRepository");
        $qb->leftJoin("categoryRepository.athletes","athletes");
        $qb->leftJoin("categoryRepository.phase","phase");
        $qb->select("athletes.user_id");
        $qb->where($qb->expr()->eq("phase.competition",$qb->expr()->literal($competition->getId())));
        return $qb;

    }
    public function findUsersByCompetition(Competition $competition){

        $query=$this->findUsersByCompetitionQb($competition)->getQuery();
        $query->useResultCache(true,60,"box_repository_find_box_name");
        return $query->getResult();

    }
    public function findByUserAndStatusQB(UserInterface $user,$status){
        $qb=$this->createQueryBuilder("categoryRepository");
        $qb->join("categoryRepository.athletes","athlete");
        $qb->where($qb->expr()->andX(
                $qb->expr()->eq("athlete.user_id",$qb->expr()->literal($user->getId())),
                $qb->expr()->eq("athlete.status",$qb->expr()->literal($status))
            ));
        return $qb;
    }
}