<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 1/15/16
 * Time: 3:31 PM
 */

namespace BDS\RWCategoryBundle\Repository;


use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Bot;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Event;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class CategoryAthleteRepository extends EntityRepository
{
    public function getTeamFromCompetition(UserExtension $extension,Competition $competition){
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->join("categoryAthlete.team", "team");
        $qb->join("categoryAthlete.category", "category");
        $qb->join("category.phase", "phase");
        $qb->join("team.athletes", "team_athletes");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
                $qb->expr()->eq("team_athletes.userExtension", $qb->expr()->literal($extension->getId()))
            )
        );
        return $qb->getQuery()->getOneOrNullResult();
    }
    public function findOneByCompetitionAndTeam(Competition $competition, Team $team)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->join("categoryAthlete.category", "category");
        $qb->join("category.phase", "phase");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
                $qb->expr()->eq("categoryAthlete.team", $qb->expr()->literal($team->getId()))
            )
        );

        return $qb->getQuery()->getOneOrNullResult();
    }
    public function findOneByCompetitionAndUser(Competition $competition, UserExtension $userExtension)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->join("categoryAthlete.category", "category");
        $qb->join("category.phase", "phase");
        $qb->setMaxResults(1);
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
                $qb->expr()->eq("categoryAthlete.userExtension", $qb->expr()->literal($userExtension->getId()))
            )
        );

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findCompetitionBoxes(Competition $competition)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->select("box.name as box_name");
        $qb->addSelect("user_extension.user_id");
        $qb->addSelect("box.slug as box_slug");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->leftJoin("categoryAthlete.userExtension", "user_extension");
        $qb->leftJoin("user_extension.box", "box");
        $qb->where($qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())));

        return $qb->getQuery()->getArrayResult();
    }

    public function getIdsByPhase(Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->select("categoryAthlete.id");
        $qb->addSelect("userExtension.id as extension_id");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->leftJoin("categoryAthlete.userExtension", "userExtension");
        $qb->where($qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())));

        return $qb->getQuery()->getArrayResult();
    }

    public function findByCompetitionOrderDesc(Competition $competition)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->select("category.name as category_name");
        $qb->addSelect("category.slug as category_slug");
        $qb->addSelect("categoryAthlete.status");
        $qb->addSelect("category.price");
        $qb->addSelect("user_extension.user_id");
        $qb->addSelect("phase.name as phase_name");
        $qb->addSelect("user_extension.user_id");
        $qb->addSelect("box.name as box_name");
        $qb->addSelect("bot.name as bot_name");
        $qb->addSelect("box.slug as box_slug");
        $qb->addSelect("team.name as team_name");
        $qb->addSelect("team.slug as team_slug");
        $qb->leftJoin("categoryAthlete.team", "team");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->leftJoin("categoryAthlete.bot", "bot");
        $qb->leftJoin("categoryAthlete.userExtension", "user_extension");
        $qb->leftJoin("user_extension.box", "box");

        $qb->where($qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())));
        $qb->orderBy("categoryAthlete.id", "DESC");
        $qb->setMaxResults(10);

        return $qb->getQuery()->getArrayResult();
    }

    public function countPaidByPhase(Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->leftJoin("categoryAthlete.order", "o");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->select($qb->expr()->count("categoryAthlete"));
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("o.paid", $qb->expr()->literal(true)),
                $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId()))
            )

        );

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findByPhase(Phase $phase)
    {

        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->select("category.name as category_name");
        $qb->addSelect("category.slug as category_slug");
        $qb->addSelect("categoryAthlete.status");
        $qb->addSelect("categoryAthlete.id");
        $qb->addSelect("user_extension.user_id");
        $qb->addSelect("box.name as box_name");
        $qb->addSelect("box.slug as box_slug");
        $qb->addSelect("bot.name as bot_name");
        $qb->addSelect("o.paid as order_paid");
        $qb->addSelect("o.status as order_status");
        $qb->addSelect("o.id as order_id");
        $qb->addSelect("team.name as team_name");
        $qb->addSelect("team.slug as team_slug");
        $qb->leftJoin("categoryAthlete.order", "o");
        $qb->leftJoin("categoryAthlete.team", "team");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->leftJoin("categoryAthlete.bot", "bot");
        $qb->leftJoin("categoryAthlete.userExtension", "user_extension");
        $qb->leftJoin("user_extension.box", "box");

        $qb->where($qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())));

        return $qb->getQuery()->getArrayResult();
    }

    public function findUserIdsByCompetition(Competition $competition)
    {

        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->select("user_extension.user_id as id");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->leftJoin("categoryAthlete.userExtension", "user_extension");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())));

        return $qb->getQuery()->getArrayResult();
    }

    public function findUserExtensionByCompetition(Competition $competition)
    {

        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->select("user_extension.id AS extension_id");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->leftJoin("categoryAthlete.userExtension", "user_extension");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())));

        return $qb->getQuery()->getArrayResult();
    }

    public function countPendingByOrganizerAndPhase(Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->select($qb->expr()->count("categoryAthlete"));
        $qb->leftJoin("categoryAthlete.category", "category");

        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())),
            $qb->expr()->like("categoryAthlete.status", $qb->expr()->literal(CategoryAthlete::STATUS_PENDING_ORGANIZER))
        ));

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findByPhaseAndStatus(Phase $phase, $status)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->leftJoin("categoryAthlete.category", "category");

        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())),
            $qb->expr()->like("categoryAthlete.status", $qb->expr()->literal($status))
        ));

        return $qb->getQuery()->getResult();
    }

    public function findOneByPhaseAndUser(Phase $phase, User $user)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->leftJoin("categoryAthlete.category", "category");

        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())),
            $qb->expr()->eq("categoryAthlete.user_id", $qb->expr()->literal($user->getId()))
        ));

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function countByUserAndType(User $user, $type)
    {
        $qb = $this->getByUserAndTypeQB($user, $type);
        $qb->select($qb->expr()->count("categoryAthlete"));

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getByUserAndType(User $user, $type)
    {

        $qb = $this->getByUserAndTypeQB($user, $type);
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->orderBy("phase.created", "DESC");

        return $qb->getQuery()->getResult();
    }

    private function getByUserAndTypeQB(User $user, $type)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("categoryAthlete.user_id", $qb->expr()->literal($user->getId())),
            $qb->expr()->like("categoryAthlete.status", $qb->expr()->literal($type))
        ));

        return $qb;
    }

    public function findOneByCategoryPhaseAndUserId(Category $category, Phase $phase, UserExtension $userExtension)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("categoryAthleteRepository.userExtension", $qb->expr()->literal($userExtension->getId())),
            $qb->expr()->eq("category.id", $qb->expr()->literal($category->getId())),
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId()))
        ));

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findOneByCategoryAndBot(Category $category, Bot $bot)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.bot", "bot");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("bot.id", $qb->expr()->literal($bot->getId())),
            $qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId()))
        ));

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findUserIdAndTeamNameByCategoryAndEvent(Category $category, Event $event)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select("user_extension.user_id");
        $qb->addSelect("categoryAthleteRepository.id AS athlete_id");
        $qb->addSelect("bot.name AS athlete_name");
        $qb->addSelect("bot.slug AS athlete_slug");
        $qb->addSelect("box.name AS box_name");
        $qb->addSelect("box.slug AS box_slug");
        $qb->addSelect("team.slug AS team_slug");
        $qb->addSelect("team.name as team_name");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("categoryAthleteRepository.team", "team");
        $qb->leftJoin("categoryAthleteRepository.bot", "bot");
        $qb->leftJoin("categoryAthleteRepository.userExtension", "user_extension");
        $qb->leftJoin("user_extension.box", "box");
        $qb->leftJoin("category.phase", "phase");
        $qb->leftJoin("phase.events", "event");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.id", $qb->expr()->literal($category->getId())),
            $qb->expr()->eq("category.phase", $qb->expr()->literal($event->getPhase()->getId())),
            $qb->expr()->eq("event.id", $qb->expr()->literal($event->getId()))
        ));

        return $qb->getQuery()->getArrayResult();

    }

    public function countAthletesInPhaseAndCategory(Phase $phase, Category $category)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select($qb->expr()->count("categoryAthleteRepository"));
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("phase.id", $qb->expr()->literal($phase->getId())),
                $qb->expr()->eq("category.id", $qb->expr()->literal($category->getId()))
            ));

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findByPhaseOrderedByCategory(Phase $phase)
    {

        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())));
        $qb->orderBy("category.name", "ASC");

        return $qb->getQuery()->getResult();
    }

    public function findAthleteIDsInCompetition(Competition $competition)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select("categoryAthleteRepository.user_id");
        $qb->distinct();
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())));

        return $qb->getQuery()->getArrayResult();
    }

    public function findAthleteIDsInCompetitionAndPhase(Competition $competition, Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select("categoryAthleteRepository.user_id");
        $qb->distinct();
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("phase.id", $qb->expr()->literal($phase->getId()))
        ));

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param Competition $competition
     * @param             $id
     *
     * @return CategoryAthlete|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByCompetitionAndId(Competition $competition, $id)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->setMaxResults(1);
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("categoryAthleteRepository.id", $qb->expr()->literal($id))
        ));

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Category $category
     * @param Phase    $phase
     *
     * @return array | CategoryAthlete[]
     */
    public function findByCategoryAndPhase(Category $category, Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())),
            $qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId()))
        ));

        return $qb->getQuery()->getResult();
    }

    public function countPendingAthletesInCompetition(Competition $competition)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->join("categoryAthleteRepository.category", "category");
        $qb->join("category.phase", "phase");
        $qb->select($qb->expr()->count("categoryAthleteRepository"));
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("categoryAthleteRepository.status",
                $qb->expr()->literal(CategoryAthlete::STATUS_PENDING_USER))));

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function isUserInCompetition(UserExtension $user, Competition $competition)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select($qb->expr()->count("categoryAthleteRepository"));
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("categoryAthleteRepository.team", "team");
        $qb->leftJoin("team.athletes", "team_athlete");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
            $qb->expr()->orX(
                $qb->expr()->eq("categoryAthleteRepository.userExtension", $qb->expr()->literal($user->getId())),
                $qb->expr()->eq("team_athlete.userExtension", $qb->expr()->literal($user->getId()))
            )
        ));

        return ($qb->getQuery()->getSingleScalarResult() != 0);
    }

    public function isUserInPhase(UserExtension $user, Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select($qb->expr()->count("categoryAthleteRepository"));
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())),
            $qb->expr()->eq("categoryAthleteRepository.userExtension", $qb->expr()->literal($user->getId()))
        ));

        return ($qb->getQuery()->getSingleScalarResult() != 0);
    }

    public function getAthleteInPhase(UserExtension $user, Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->setMaxResults(1);
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())),
            $qb->expr()->eq("categoryAthleteRepository.userExtension", $qb->expr()->literal($user->getId()))
        ));

        return ($qb->getQuery()->getOneOrNullResult());
    }

    public function isUserInCategory(UserExtension $user, Category $category)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select($qb->expr()->count("categoryAthleteRepository"));
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId())),
            $qb->expr()->eq("categoryAthleteRepository.userExtension", $qb->expr()->literal($user->getId()))
        ));

        return ($qb->getQuery()->getSingleScalarResult() != 0);
    }

    public function findOneByUserAndCategory(User $user, Category $category)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->setMaxResults(1);
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId())),
            $qb->expr()->eq("categoryAthleteRepository.user_id", $qb->expr()->literal($user->getId()))
        ));

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function countCompetitionAthletes(Competition $competition)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select($qb->expr()->count("categoryAthleteRepository"));
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())));

        return $qb->getQuery()->getSingleScalarResult();

    }

    public function countPhaseAthletes(Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->select($qb->expr()->count("categoryAthleteRepository"));
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())));

        return $qb->getQuery()->getSingleScalarResult();

    }

    public function findByCategoryAndEventOrderByResultQB(Category $category, Event $event)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.results", "result");
        $qb->leftJoin("result.measure", "measure");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId())),
                $qb->expr()->eq("result.event", $qb->expr()->literal($event->getId()))

            ));
        $qb->orderBy("measure.position", "ASC");
        $qb->addOrderBy("result.result", "DESC");
        $qb->addOrderBy("result.tieBreaker", "DESC");

        return $qb;
    }

    public function findByCategoryAndEventOrderByResult(Category $category, Event $event)
    {
        return $this->findByCategoryAndEventOrderByResultQB($category, $event)->getQuery()->getResult();
    }

    public function findUserIdByCategoryAndEventOrderByResult(Category $category, Event $event)
    {
        $qb = $this->findByCategoryAndEventOrderByResultQB($category, $event);
        $qb->select("categoryAthleteRepository.id");

        return $qb->getQuery()->getArrayResult();
    }

    public function findByCategoryAndEventNotInArray(Category $category, Event $event, array $ids = [])
    {
        return $this->findByCategoryAndEventNotInArrayQb($category, $event, $ids)->getQuery()->getResult();
    }

    public function findUserIdsByCategoryAndEventNotInArray(Category $category, Event $event, array $ids = [])
    {
        $qb = $this->findByCategoryAndEventNotInArrayQb($category, $event, $ids);
        $qb->select("categoryAthleteRepository.user_id");

        return $qb->getQuery()->getArrayResult();
    }

    private function findByCategoryAndEventNotInArrayQb(Category $category, Event $event, array $ids = [])
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId())));
        if (count($ids)) {
            $qb->andWhere($qb->expr()->notIn("categoryAthleteRepository.id", ":ids"));
            $qb->setParameter("ids", $ids);
        }

        return $qb;
    }

    public function findNotRankedAthletes(Category $category, Event $event)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId())),
                $qb->expr()->notIn("categoryAthleteRepository.id",
                    $this->getEntityManager()->getRepository("BDSRWCompetitionBundle:EventResult")->findUsersInEventWithResultsQB($event)->getDQL())
            ));

        return $qb->getQuery()->getResult();
    }

    public function findNotRankedAthletesByEventName(Category $category, $eventName)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq("categoryAthleteRepository.category", $qb->expr()->literal($category->getId())),
                $qb->expr()->notIn("categoryAthleteRepository.id",
                    $this->getEntityManager()->getRepository("BDSRWCompetitionBundle:EventResult")->findUsersInEventNameWithResultsQB($eventName)->getDQL())
            ));

        return $qb->getQuery()->getResult();
    }

    public function findAllNotRankedAthletesByEventAndEventName(Event $event, $eventName)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->where(
            $qb->expr()->notIn("categoryAthleteRepository.id",
                $this->getEntityManager()->getRepository("BDSRWCompetitionBundle:EventResult")->findUsersByEventNameAndEventWithResultsQB($eventName,
                    $event)->getDQL())
        );

        return $qb->getQuery()->getResult();
    }

    public function findPhaseAthletes(Phase $phase, $user_ids = null)
    {

        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())));
        if ($user_ids) {
            $qb->andWhere($qb->expr()->in("categoryAthleteRepository.user_id", ":user_ids"));
            $qb->setParameter(":user_ids", $user_ids);
        }

        $qb->orderBy("categoryAthleteRepository.updated", "DESC");

        return $qb->getQuery();
    }

    public function findAthleteInPhase(UserExtension $user, Phase $phase)
    {

        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId())),
            $qb->expr()->eq("categoryAthleteRepository.userExtension", $qb->expr()->literal($user->getId())))
        );

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findPendingByAthleteQuery(Competition $competition)
    {
        return $this->findPendingBy($competition, CategoryAthlete::STATUS_PENDING_USER);
    }

    public function findPendingByCompetitionQuery(Competition $competition)
    {
        return $this->findPendingBy($competition, CategoryAthlete::STATUS_PENDING_ORGANIZER);
    }

    private function findPendingBy(Competition $competition, $pendingBy)
    {
        $qb = $this->createQueryBuilder("categoryAthleteRepository");
        $qb->leftJoin("categoryAthleteRepository.category", "category");
        $qb->leftJoin("category.phase", "phase");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("phase.competition", $qb->expr()->literal($competition->getId())),
            $qb->expr()->eq("categoryAthleteRepository.status", $qb->expr()->literal($pendingBy))
        ));
        $qb->orderBy("categoryAthleteRepository.updated", "DESC");

        return $qb;
    }

    public function findOneByUserExtensionAndPhase(UserExtension $userExtension, Phase $phase)
    {
        $qb = $this->createQueryBuilder("categoryAthlete");
        $qb->leftJoin("categoryAthlete.category", "category");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("categoryAthlete.userExtension", $qb->expr()->literal($userExtension->getId())),
            $qb->expr()->eq("category.phase", $qb->expr()->literal($phase->getId()))
        ));

        return $qb->getQuery()->getOneOrNullResult();
    }

}