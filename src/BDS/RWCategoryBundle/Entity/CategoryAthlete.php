<?php

namespace BDS\RWCategoryBundle\Entity;

use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\RWCompetitionBundle\Entity\AthleteExtraData;
use BDS\RWCompetitionBundle\Entity\Bot;
use BDS\RWCompetitionBundle\Entity\EventResult;
use BDS\RWCompetitionBundle\Entity\ExtraDataInterface;
use BDS\RWCompetitionBundle\Entity\ExtraDataTrait;
use BDS\RWCompetitionBundle\Entity\Team;
use BDS\RWCompetitionBundle\Entity\UserExtensionTrait;
use BDS\RWPaymentBundle\Entity\Order;
use BDS\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Competition
 *
 * @ORM\Table(name="phase_category_athletes")
 * @ORM\Entity(repositoryClass="BDS\RWCategoryBundle\Repository\CategoryAthleteRepository")
 * @UniqueEntity(fields={"user_id","category"},errorPath="category",message="User is already on this category")
 */
class CategoryAthlete extends AbstractBaseEntity implements ExtraDataInterface
{
    const STATUS_PENDING_USER = "PENDING";
    const STATUS_PENDING_ORGANIZER = "PENDING_ORGANIZER";
    const STATUS_PENDING_PAYMENT = "PENDING_PAYMENT";
    const STATUS_PENDING_EXTRA_DATA="PENDING_EXTRA_DATA";
    const STATUS_ACCEPTED = "ACCEPTED";
    const STATUS_DECLINED = "DECLINED";
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="BDS\RWCategoryBundle\Entity\Category", inversedBy="athletes",cascade={"refresh"})
     */
    private $category;
    /**
     * @var integer
     * @ORM\Column(name="user_id",type="integer",nullable=true)
     */
    private $user_id;
    /**
     * @var string
     * @ORM\Column(name="status",type="string",nullable=false,length=40)
     */
    private $status;

    /**
     * @var ArrayCollection|EventResult[]
     * @ORM\OneToMany(targetEntity="BDS\RWCompetitionBundle\Entity\EventResult",mappedBy="athlete",cascade={"all"})
     */
    private $results;
    /**
     * @var Team $team
     * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Team")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    private $team;

	/**
	 * @var  Bot
	 * @ORM\OneToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Bot",mappedBy="categoryAthlete",cascade={"all"},orphanRemoval=true)
	 */
	private $bot;
    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="BDS\RWPaymentBundle\Entity\Order",cascade={"all"})
     */
    private $order;
    /**
     * @var ArrayCollection|AthleteExtraData[]
     * @ORM\ManyToMany(targetEntity="BDS\RWCompetitionBundle\Entity\AthleteExtraData")
     * @ORM\JoinTable(name="category_athlete_extra_data",
     *      joinColumns={@ORM\JoinColumn(name="team_athlete_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="extra_data_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $extraDataCollection;

    private $roninFoxUser;

    use UserExtensionTrait,ExtraDataTrait;
    public function __construct()
    {
        $this->results=new ArrayCollection();
        $this->status=self::STATUS_PENDING_USER;
        $this->extraDataCollection=new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return \BDS\RWCategoryBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param \BDS\RWCategoryBundle\Entity\Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return User
     */
    public function getRoninFoxUser()
    {
        return $this->getUserExtension()?$this->getUserExtension()->getRoninFoxUser():null;
    }

    /**
     * @param User $roninFoxUser
     */
    public function setRoninFoxUser(User $roninFoxUser)
    {
        $this->roninFoxUser = $roninFoxUser;
    }

    /**
     * @return \BDS\RWCompetitionBundle\Entity\EventResult[]|ArrayCollection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param \BDS\RWCompetitionBundle\Entity\EventResult[]|ArrayCollection $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }
    public function addResult(EventResult $result){

        if(!$this->results->contains($result)){
            $this->results->add($result);
        }
    }
    public function removeResult(EventResult $result){

        if($this->results->contains($result)){
            $this->results->removeElement($result);
        }
    }
    public function isPending()
    {
        return in_array($this->status,[self::STATUS_PENDING_USER,self::STATUS_PENDING_ORGANIZER]);
    }
    public function isPendingByCompetition()
    {
        return ($this->status==self::STATUS_PENDING_ORGANIZER);
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
        $this->user_id=-1;
    }
    public function getNameAndCategory()
    {
        return sprintf("%s (%s)",$this->getName(),$this->category->getName());
    }
    public function getName()
    {

	    if($this->getTeam() instanceof Team){
            return $this->getTeam()->getName();
        } elseif($this->getRoninFoxUser() instanceof User){
	    	if(trim($this->getRoninFoxUser()->getCompleteName())){
			    return $this->getRoninFoxUser()->getCompleteName();
		    } else {
	    		return $this->getRoninFoxUser()->getEmail();
		    }

        } elseif($this->getBot() instanceof Bot){
            return $this->getBot()->getName();
        } else {
            return "Unnamed";
        }
    }
    public function getBox(){
        if($this->getUserExtension()->getBox()){
            return $this->getUserExtension()->getBox()->getName();
        } else {
            return "Independiente";
        }
    }
	/**
	 * @return Bot
	 */
	public function getBot() {
		return $this->bot;
	}

	/**
	 * @param Bot $bot
	 */
	public function setBot( $bot ) {
		$this->bot = $bot;
	}
	public function isBot(){
		return $this->getBot()&&1;
	}
	public function hasAccepted()
	{
		return $this->status==self::STATUS_ACCEPTED;
	}
	public function isPendingOrganizer(){
		return $this->status==self::STATUS_PENDING_ORGANIZER;
	}
	public function isAccepted(){
		return $this->status==self::STATUS_ACCEPTED;
	}
	public function isDeclined(){
		return $this->status==self::STATUS_DECLINED;
	}
    /**
     * @return Order
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder( $order ) {
        $this->order = $order;
    }

    public function isPaid() {
        if($this->order){
            return $this->order->isPaid();
        }
        return true;
    }
    public function isTeam(){
        return $this->team&&1;
    }
    public function isInvited(){
        if($this->order){
            return $this->order->isInvitation();
        }
        return false;
    }
}

