<?php

namespace BDS\RWCategoryBundle\Entity;

use BDS\CoreBundle\Entity\AbstractNamedBaseEntity;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="BDS\RWCategoryBundle\Repository\CategoryRepository")
 */
class Category extends AbstractNamedBaseEntity
{
    const INDIVIDUAL_CATEGORY= "INDIVIDUAL";
    const TEAM_CATEGORY= "TEAM";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="category_type",type="string",length=50,nullable=false)
     */
    private $categoryType;

    /**
	 * @var Phase
	 * @ORM\ManyToOne(targetEntity="BDS\RWCompetitionBundle\Entity\Phase",inversedBy="categories")
	 */
	private $phase;

	/**
	 * @var int
	 * @ORM\Column(name="max_athletes",type="integer",nullable=true)
	 */
	private $maxAthletes;
	/**
	 * @var array
	 * @ORM\OneToMany(targetEntity="BDS\RWCategoryBundle\Entity\CategoryAthlete",mappedBy="category",cascade={"all"})
	 */
	private $athletes;

    /**
     * @var int
     * @ORM\Column(name="price",type="integer")
     */
	private $price;

    /**
     * @var int
     * @ORM\Column(name="max_team_athletes",type="integer",nullable=true)
     */
    private $maxTeamAthletes;
    /**
     * @var int
     * @ORM\Column(name="min_team_athletes",type="integer",nullable=true)
     */
    private $minTeamAthletes;

    public function __construct()
    {
        $this->categoryType=self::INDIVIDUAL_CATEGORY;
        $this->athletes=new ArrayCollection();
        $this->maxTeamAthletes=0;
        $this->minTeamAthletes=0;
        $this->price=0;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCategoryType():string
    {
        return $this->categoryType;
    }

    /**
     * @param string $categoryType
     */
    public function setCategoryType($categoryType)
    {
        if(in_array($categoryType,[self::INDIVIDUAL_CATEGORY,self::TEAM_CATEGORY])){
            $this->categoryType = $categoryType;
            if($categoryType==self::INDIVIDUAL_CATEGORY){
                $this->maxTeamAthletes=0;
                $this->minTeamAthletes=0;
            }
        }
    }
	/**
	 * @return Phase
	 */
	public function getPhase() {
		return $this->phase;
	}

	/**
	 * @param Phase $phase
	 */
	public function setPhase( $phase ) {
		$this->phase = $phase;
	}

	/**
	 * @return int
	 */
	public function getMaxAthletes() {
		return $this->maxAthletes;
	}

	/**
	 * @param int $maxAthletes
	 */
	public function setMaxAthletes( $maxAthletes ) {
		$this->maxAthletes = $maxAthletes;
	}

	/**
	 * @return int
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @param int $price
	 */
	public function setPrice( $price ) {
		$this->price = $price;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getAthletes() {
		return $this->athletes;
	}

    /**
     * @return $athletes
     */
	public function countAthletes() {
		return $this->athletes->count();
	}

	/**
	 * @param array $athletes
	 */
	public function setAthletes( $athletes ) {
		$this->athletes = $athletes;
	}
	public function addAthlete(CategoryAthlete $categoryAthlete)
	{
		if(!$this->athletes->contains($categoryAthlete)){
			$this->athletes->add($categoryAthlete);
		}
	}
	public function removeAthlete(CategoryAthlete $categoryAthlete)
	{
		if($this->athletes->contains($categoryAthlete)){
			$this->athletes->removeElement($categoryAthlete);
		}
	}

	public function getCompleteName()
	{
		return sprintf("%s | %s | %s",$this->getPhase()->getCompetition()->getName(),$this->getPhase()->getName(),$this->getName());
	}
     /**
     * @return int
     */
    public function getMaxTeamAthletes():? int
    {
        return $this->maxTeamAthletes;
    }

    /**
     * @param int $maxTeamAthletes
     */
    public function setMaxTeamAthletes(int $maxTeamAthletes): void
    {
        $this->maxTeamAthletes = $maxTeamAthletes;
    }

    /**
     * @return int
     */
    public function getMinTeamAthletes():? int
    {
        return $this->minTeamAthletes;
    }

    /**
     * @param int $minTeamAthletes
     */
    public function setMinTeamAthletes(int $minTeamAthletes): void
    {
        $this->minTeamAthletes = $minTeamAthletes;
    }
    public function getType():string{
        return $this->getCategoryType();
    }
    public function isTeam():bool{
        return $this->categoryType===self::TEAM_CATEGORY;
    }
    public function getRegisterName(){
        if($this->price){
            $price=number_format(($this->getPrice()/100),2,".",",");
            return sprintf("%s (%s€)",$this->getName(),$price);
        } else {
            return sprintf("%s (Gratuita)",$this->getName());
        }
    }
}

