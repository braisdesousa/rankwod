<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 22/02/18
 * Time: 22:31
 */

namespace BDS\RWCategoryBundle\Service;


use BDS\CoreBundle\Helpers\RepositoryHelper;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\UserExtension;
use Doctrine\ORM\EntityManager;

class AthleteService
{
    private $entityManager;
    private $userEntityManager;
    public function __construct(EntityManager $entityManager, EntityManager $userEntityManager)
    {
        $this->entityManager=$entityManager;
        $this->userEntityManager=$userEntityManager;
    }
    public function getCompetitionAthletes(Competition $competition){
        $athletes=[];
        $athetesId=RepositoryHelper::getIdsFromResult($this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findUserIdsByCompetition($competition));
        $users=RepositoryHelper::reformatArrayWithKey($this->userEntityManager->getRepository("BDSUserBundle:User")->findBasicDataByIds($athetesId),"id");
        foreach($competition->getPhases() as $phase){
            $athletes[$phase->getSlug()]=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findByPhase($phase);
        }
        return ["users"=>$users,"athletes"=>$athletes];
    }
    public function getCompetitionBoxes(Competition $competition){
        $userExtensionIds=RepositoryHelper::getIdsFromResult($this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findUserExtensionByCompetition($competition),'extension_id');
        $boxes=$this->entityManager->getRepository("BDSRWCompetitionBundle:UserExtension")->findDistinctBoxes($userExtensionIds);
        return $boxes;
    }
    public function getLastRegistrations(Competition $competition){
        $athletes=$this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findByCompetitionOrderDesc($competition);
        $athetesId=RepositoryHelper::getIdsFromResult($this->entityManager->getRepository("BDSRWCategoryBundle:CategoryAthlete")->findUserIdsByCompetition($competition));
        $users=RepositoryHelper::reformatArrayWithKey($this->userEntityManager->getRepository("BDSUserBundle:User")->findBasicDataByIds($athetesId),"id");
        return ["users"=>$users,"athletes"=>$athletes];
    }

}