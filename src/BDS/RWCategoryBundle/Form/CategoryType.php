<?php

namespace BDS\RWCategoryBundle\Form;

use BDS\RWCategoryBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,["label"=>'Nombre Categoría',"attr"=>["placeholder"=>"Nombre de la Categoría, 'RX','Scaled',..."]])
            ->add('categoryType',ChoiceType::class,
                    ["label"=>'Tipo de Categoría',
                     "choices"=>$this->getCategoryTypeChoices()
                    ]
            )
            ->add('minTeamAthletes',IntegerType::class, ["label"=>"Número Minimo de atletas por equipo ( si corresponde )","empty_data"=>0,"attr"=>["placeholder"=>"Número Mínimo por equipo"]])
            ->add('maxTeamAthletes',IntegerType::class, ["label"=>"Número Máximo de atletas por equipo ( si corresponde )","empty_data"=>0,"attr"=>["placeholder"=>"Número Máximo por equipo"]])
	        ->add("maxAthletes",    IntegerType::class, ["label"=>"Límite Participantes","attr"=>["placeholder"=>"Limite de Participantes/Equipos"],"required"=>false])
	        ->add("price",MoneyType::class,
                [
                    "label"=>"Precio de Inscripcion",
                    "divisor"=>100,
                    "currency"=>false,
                    "attr"=>["placeholder"=>"Precio en €, 0 para Gratis"]
                ])
            ->add("submit",SubmitType::class, ["label"=>"Guardar",'attr'=>["class"=>'btn btn-primary pull-right']])
        ;
    }
    private function getCategoryTypeChoices()
    {
        return [
            "Individual"=>Category::INDIVIDUAL_CATEGORY,
	        "Equipos"=>Category::TEAM_CATEGORY
        ];
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCategoryBundle\Entity\Category'
        ));
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcategorybundle_category';
    }
}
