<?php

namespace BDS\RWCategoryBundle\Form;

use BDS\RWCategoryBundle\Entity\Restriction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class RestrictionType extends AbstractType
{
    private $translator;
    public function __construct(Translator $translator)
    {
        $this->translator=$translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('max',IntegerType::class)
            ->add('min',IntegerType::class)
            ->add('type',ChoiceType::class,[
                'choices'=>$this->getRestrictionChoices(),
                'choices_as_values'=>true
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDS\RWCategoryBundle\Entity\Restriction'
        ));
    }

    public function getRestrictionChoices()
    {
        $choices=[];
        foreach(Restriction::getRestrictionsArray() as $restriction){
            $choices[$this->translator->trans($restriction,[],"restrictions",null,"es")]=$restriction;
        }
        return $choices;
    }
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bds_rwcategorybundle_restriction';
    }
}
