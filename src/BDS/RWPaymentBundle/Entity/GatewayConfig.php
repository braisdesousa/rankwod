<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 21/03/18
 * Time: 11:15
 */

namespace BDS\RWPaymentBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\GatewayConfig as BaseGatewayConfig;

/**
 * @ORM\Table(name="payment_gateway_config")
 * @ORM\Entity
 */
class GatewayConfig extends BaseGatewayConfig
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer $id
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="factory_name", type="string")
     */
    protected $factoryName;

    /**
     * @var string
     * @ORM\Column(name="gateway_name", type="string")
     */
    protected $gatewayName;

    /**
     * @var array
     * @ORM\Column(name="config", type="json_array")
     */
    protected $config;

    /**
     * Note: This should not be persisted to database
     * @var array
     */
    protected $decryptedConfig;


    public function getDataForForm(){
        return [
           'secret_key'=> (isset($this->config['secret_key'])?$this->config['secret_key']:''),
           'publishable_key'=>(isset($this->config['publishable_key'])?$this->config['publishable_key']:'')
        ];
    }
    public function isFilled(){

        return (
                    isset($this->config['secret_key'])&&
                    isset($this->config['publishable_key'])&&
                    $this->config["secret_key"]&&
                    $this->config["publishable_key"]
        );
    }
}