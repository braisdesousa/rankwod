<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 31/05/17
 * Time: 17:47
 */

namespace BDS\RWPaymentBundle\Entity;


interface WithOrderInterface {

	public function isPaid();

}