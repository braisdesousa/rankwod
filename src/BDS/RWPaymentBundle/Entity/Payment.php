<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 21/03/18
 * Time: 11:12
 */

namespace BDS\RWPaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\Payment as BasePayment;

/**
 * @ORM\Table(name="payment")
 * @ORM\Entity
 */
class Payment extends BasePayment
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer $id
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(name="number", type="string",nullable=true)
     */
    protected $number;

    /**
     * @var string
     * @ORM\Column(name="description", type="string",nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(name="client_email", type="string",nullable=true)
     */
    protected $clientEmail;

    /**
     * @var string
     * @ORM\Column(name="client_id", type="string",nullable=true)
     */
    protected $clientId;

    /**
     * @var int
     * @ORM\Column(name="total_amount", type="integer",nullable=true)
     */
    protected $totalAmount;

    /**
     * @var string
     * @ORM\Column(name="currency_code", type="string",nullable=true)
     */
    protected $currencyCode;

    /**
     * @var array
     * @ORM\Column(name="details", type="json_array")
     */
    protected $details;

    public function getId(){
        return $this->id;
    }
}