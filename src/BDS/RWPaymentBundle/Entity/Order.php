<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 23/05/17
 * Time: 11:41
 */

namespace BDS\RWPaymentBundle\Entity;


use BDS\CoreBundle\Entity\AbstractBaseEntity;
use BDS\CoreBundle\Entity\AbstractTextBaseEntity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Model\PaymentInstructionInterface;

/**
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="BDS\RWPaymentBundle\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Order extends AbstractTextBaseEntity
{
    const STATUS_UNPAID= "UNPAID";
    const STATUS_PAID= "PAID";
    const STATUS_INVITATION= "INVITATION";
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/** @ORM\Column(name="amount",type="integer") */
	private $amount;

	/**
	 * @var Payment
     * @ORM\ManyToOne(targetEntity="BDS\RWPaymentBundle\Entity\Payment",cascade={"all"})
     * @ORM\JoinColumn(name="gateway_id", referencedColumnName="id",onDelete="SET NULL")
     */
	private $payment;

	/** @var  boolean
	 * @ORM\Column(name="paid",type="boolean",nullable=false)
	 */
	private $paid;

    /**
     * @var string
     * @ORM\Column(name="status",type="string",nullable=true)
     */
	private $status;


	public function __construct($amount)
	{
		$this->amount = $amount;
		$this->paid=false;
		$this->status=$amount?self::STATUS_UNPAID:self::STATUS_PAID;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getPrice()
	{
		return $this->amount;
	}
	public function getAmount()
	{
		$price=$this->amount/100;
		return number_format((float)$price, 2, '.', '');
	}

	/**
	 * @param mixed $amount
	 */
	public function setAmount( $amount ) {
		$this->amount = $amount;
	}
	/**
	 * @return bool
	 */
	public function isPaid() {
		return $this->paid;
	}

	/**
	 * @param bool $paid
	 */
	public function setPaid( $paid ) {
		$this->paid = $paid;
		$this->status=self::STATUS_PAID;
	}

    /**
     * @return Payment
     */
    public function getPayment():? Payment
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment): void
    {
        $this->payment = $payment;
    }

    /**
     * @return string
     */
    public function getStatus():? string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
    public function invite(){
        $this->setPaid(true);
        $this->setStatus(self::STATUS_INVITATION);
    }
    public function isInvitation(){
        return $this->status===self::STATUS_INVITATION;
    }
    /** @ORM\PreFlush() */
    public function preFlush(){
        if(!$this->amount){
            $this->setPaid(true);
            $this->setStatus(self::STATUS_PAID);
        }
    }


}
