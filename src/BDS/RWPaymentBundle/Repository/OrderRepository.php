<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 14/09/18
 * Time: 13:32
 */

namespace BDS\RWPaymentBundle\Repository;


use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{

    public function findOneByPaymentDescription(string $description){
        $qb=$this->createQueryBuilder("o");
        $qb->leftJoin("o.payment","payment");
        $qb->where($qb->expr()->eq("payment.description",$qb->expr()->literal($description)));
        return $qb->getQuery()->getOneOrNullResult();
    }
}