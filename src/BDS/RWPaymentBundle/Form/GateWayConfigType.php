<?php

namespace BDS\RWPaymentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GateWayConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('private_key',TextType::class,["required"=>true,"attr"=>["class"=>"form-control","placeholder"=>'Clave Privada (private_key)']])
            ->add("publishable_key",TextType::class,["required"=>true,"attr"=>["class"=>"form-control","placeholder"=>'Clave Pública (publishable_key)']])
            ->add("submit",SubmitType::class,["label"=>'Guardar']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false
        ));
    }

    public function getBlockPrefix()
    {
        return 'bdsrwpayment_bundle_gate_way_config_type';
    }
}
