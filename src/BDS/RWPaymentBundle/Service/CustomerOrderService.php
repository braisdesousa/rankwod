<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 31/05/17
 * Time: 17:26
 */

namespace BDS\RWPaymentBundle\Service;


use BDS\CoreBundle\Service\PushBulletService;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Entity\TeamAthlete;
use BDS\RWCompetitionBundle\Form\PhaseType;
use BDS\RWPaymentBundle\Entity\Order;
use BDS\RWPaymentBundle\Entity\Payment;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Flosch\Bundle\StripeBundle\Stripe\StripeClient;
use Payum\Core\Payum;

class CustomerOrderService {


	const PHASE_PRICE=         [Phase::TYPE_ONLINE=>19900,Phase::TYPE_ATTENDANCE=>19900,Phase::TYPE_INTERNAL=>9900];
	const PHASE_DISCOUNT_PRICE=[Phase::TYPE_ONLINE=>9900,Phase::TYPE_ATTENDANCE=>9900,Phase::TYPE_INTERNAL=>4900];
	/** @var EntityManager  */
	private $entityManager;
	/** @var  PushBulletService */
	private $pushBulletService;

	public function __construct(EntityManager $entityManager,PushBulletService $pushBulletService) {

		$this->entityManager=$entityManager;
		$this->pushBulletService=$pushBulletService;
	}

	/*** CUSTOMERS ***/
	public function createOrderFromPhase(Phase $phase){

		$order=new Order(self::PHASE_PRICE[$phase->getPhaseType()]);
		$order->setName(sprintf("%s | %s",$phase->getCompetition()->getName(),$phase->getName()));
		$order->setText(sprintf("Pago para la publicacion de la fase '%s' de la competición '%s'",$phase->getName(),$phase->getCompetition()->getName()));
		$phase->setOrder($order);
		$this->entityManager->persist($order);
		$this->entityManager->persist($phase);
		return $order;
	}
	public function createPaymentForOrder(Phase $phase,User $user){
	    if(!$order=$phase->getOrder()){
	        $order=$this->createOrderFromPhase($phase);
        }
        $payment=$this->createPayment($user,$order->getPrice(),$order->getText());
        $order->setPayment($payment);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
	    return $payment;
    }
	private function createPayment(User $user,$price,$description){

        /** @var Payment $payment */
        $payment = new Payment();
        $payment->setNumber(uniqid());
        $payment->setCurrencyCode('EUR');
        $payment->setTotalAmount($price);
        $payment->setDescription($description);
        $payment->setClientId($user->getUsername());
        $payment->setClientEmail($user->getEmail());
        $this->entityManager->persist($payment);
        $this->entityManager->flush($payment);
        return $payment;
    }
    public function createRegistrationOrderFromPhase(CategoryAthlete $categoryAthlete,Category $category):? Order{


        $order=new Order($category->getPrice());
        $phase=$category->getPhase();
        $competition=$phase->getCompetition();
        $athleteName=$categoryAthlete->getUserExtension()->getRoninFoxUser()->getCompleteName();
        $order->setName(sprintf("%s | %s | Registro %s | %s",$competition->getName(),$phase->getName(),$category->getName(),$athleteName));
        $order->setText(sprintf("Pago para el registro de %s en la categoria %s de la fase '%s' de la competición '%s'",$athleteName,$category->getName(),$phase->getName(),$competition->getName()));
        $categoryAthlete->setOrder($order);
        $this->entityManager->persist($order);
        $this->entityManager->persist($categoryAthlete);
        return $order;
    }
}