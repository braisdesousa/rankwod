<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 11/01/19
 * Time: 17:18
 */

namespace BDS\RWPaymentBundle\Service\Register;


use BDS\CoreBundle\Service\PushBulletService;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWCompetitionBundle\Entity\Phase;
use BDS\RWCompetitionBundle\Service\MailerService\UserMailerService;
use BDS\RWPaymentBundle\Entity\Order;
use BDS\RWPaymentBundle\Entity\Payment;
use BDS\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class StripeRegisterPayment
{
    const CHARGE_STATUS_SUCCEDED    = "succeeded";
    const CHARGE_STATUS_PENDING     = "pending";
    const CHARGE_STATUS_FAILED      = "failed";

    /** @var EntityManager */
    private $entityManager;
    /** @var UserMailerService */
    private $mailerService;
    private $stripeSecretKey;
    private $bulletService;
    public function __construct(EntityManager $entityManager,UserMailerService $mailerService,PushBulletService $bulletService,$stripeSecretKey)
    {
        $this->entityManager=$entityManager;
        $this->mailerService=$mailerService;
        $this->stripeSecretKey=$stripeSecretKey;
        $this->bulletService=$bulletService;
    }

    public function createAthleteStripePayment(Competition $competition, CategoryAthlete $categoryAthlete,User $user,$token){
        $category=$categoryAthlete->getCategory();
        Stripe::setApiKey($competition->getGatewayConfig()->getConfig()["secret_key"]);
        $customer=$this->getOrCreateCustomer($user->getEmail(),$token);
        $charge=$this->createCharge($customer,$category->getPrice());
        $paid=false;
        if($charge->status===self::CHARGE_STATUS_SUCCEDED){
            $paid=true;
            $order=$categoryAthlete->getOrder();
            $this->setOrderPaid($order,$user,$customer,$charge);
            $categoryAthlete->setStatus(CategoryAthlete::STATUS_ACCEPTED);
            $this->entityManager->persist($categoryAthlete);
            $this->entityManager->flush($categoryAthlete);
            $this->mailerService->mailSucceedPayment($user,$category);
            $this->bulletService->notify("Pago Stripe Atleta",serialize($charge->jsonSerialize()));
        }
        $charge->description;

        return ["success"=>$paid,"message"=>$charge->description];
    }

    public function createCustomerStripePayment(Phase $phase,User $user,int $price,string $token){

        Stripe::setApiKey($this->stripeSecretKey);
        $customer=$this->getOrCreateCustomer($user->getEmail(),$token);
        $charge=$this->createCharge($customer,$price);
        $paid=false;
        if($charge->status===self::CHARGE_STATUS_SUCCEDED){
            $paid=true;
            $order=$phase->getOrder();
            $this->setOrderPaid($order,$user,$customer,$charge);
            $this->bulletService->notify("Pago Stripe Cliente",$charge->jsonSerialize());
        }
        return ["success"=>$paid,"message"=>$charge->description];

    }


    private function setOrderPaid(Order $order,User $user,Customer $customer,Charge $charge){
        $order->setPaid(true);
        $payment= new Payment();
        $payment->setClientEmail($user->getEmail());
        $payment->setClientId($customer->id);
        $payment->setCurrencyCode("EUR");
        $payment->setDescription($charge->description);
        $payment->setTotalAmount($charge->amount);
        $payment->setDetails($charge->jsonSerialize());
        $order->setPayment($payment);
        $this->entityManager->persist($payment);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }
    private function getOrCreateCustomer(string $email, string $token):Customer
    {
        if(!$customer=$this->findCustomer($email))
            $customer = Customer::create([
                'email' => $email,
                'source'  => $token,
        ]);
        return $customer;
    }
    private function createCharge(Customer $customer,int $price):Charge{
        $charge = Charge::create([
            'customer' => $customer->id,
            'amount'   => $price,
            'currency' => 'eur',
        ]);
        return $charge;
    }
    private function findCustomer(string $email):?Customer{

        $search=Customer::all(["limit" => 1,"email"=>$email]);
        return empty($search["data"])?null:($search["data"][0]);

    }
}