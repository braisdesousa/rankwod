<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 21/03/18
 * Time: 16:43
 */

namespace BDS\RWPaymentBundle\Service;


use BDS\RWCompetitionBundle\Entity\Competition;
use BDS\RWPaymentBundle\Entity\GatewayConfig;
use Doctrine\ORM\EntityManager;

class GatewayService
{
    /** @var EntityManager */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager=$entityManager;
    }

    public function createGateWayConfig(Competition $competition,$privateKey='',$publishableKey=''){

        if(!$gatewayConfig=$competition->getGatewayConfig())
        {
            $gatewayConfig=new GatewayConfig();
            $gatewayConfig->setFactoryName("stripe_js");
            $gatewayConfig->setGatewayName(sprintf('stripe_%s',$competition->getSlug()));
            $competition->setGatewayConfig($gatewayConfig);
        }
        $gatewayConfig->setConfig([
            "publishable_key"=> $publishableKey,
            "secret_key"=> $privateKey,
        ]);
        $this->entityManager->persist($gatewayConfig);
        $this->entityManager->persist($competition);
        return $gatewayConfig;
    }
}