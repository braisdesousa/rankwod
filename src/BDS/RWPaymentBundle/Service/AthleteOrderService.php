<?php

namespace BDS\RWPaymentBundle\Service;


use BDS\CoreBundle\Service\PushBulletService;
use BDS\RWCategoryBundle\Entity\Category;
use BDS\RWCategoryBundle\Entity\CategoryAthlete;
use BDS\RWPaymentBundle\Entity\Order;
use Doctrine\ORM\EntityManager;

class AthleteOrderService {


	/** @var EntityManager  */
	private $entityManager;
	/** @var  PushBulletService */
	private $pushBulletService;

	public function __construct(EntityManager $entityManager,PushBulletService $pushBulletService) {

		$this->entityManager=$entityManager;
		$this->pushBulletService=$pushBulletService;

	}

	/*** ATHLETES ***/
    public function createOrderFromCategoryTeam(CategoryAthlete $categoryAthlete,int $price){
        $teamName=$categoryAthlete->getTeam()->getName();
        $categoryName=$categoryAthlete->getCategory()->getName();
        $phaseName=$categoryAthlete->getCategory()->getPhase()->getName();
        $competitionName=$categoryAthlete->getCategory()->getPhase()->getCompetition()->getName();
        $name=sprintf("%s | %s | %s | %s",$teamName,$categoryName,$phaseName,$competitionName);
        $unique=md5(uniqid());
        $text=sprintf("Pago team %s | cat: %s | fase: %s | competicion: %s | code: %s",$teamName,$categoryName,$phaseName,$competitionName,$unique);
        $order=new Order($price);
        $order->setName($name);
        $order->setText($text);
        $categoryAthlete->setOrder($order);
        $this->entityManager->persist($order);
        $this->entityManager->persist($categoryAthlete);
        return $order;
    }
    public function createOrderFromCategory(CategoryAthlete $categoryAthlete,$invited=false){
        $athleteName=$categoryAthlete->getCompleteName();
        $price=$categoryAthlete->getCategory()->getPrice();
        $categoryName=$categoryAthlete->getCategory()->getName();
        $phaseName=$categoryAthlete->getCategory()->getPhase()->getName();
        $competitionName=$categoryAthlete->getCategory()->getPhase()->getCompetition()->getName();
        $name=sprintf("%s | %s | %s | %s",$athleteName,$categoryName,$phaseName,$competitionName);
        if($invited){
            $name="INVITACION | ".$name;
        }
        $unique=md5(uniqid());
        $text=sprintf("Pago Atleta %s | cat: %s | fase: %s | competicion: %s | code: %s",$athleteName,$categoryName,$phaseName,$competitionName,$unique);
        $order=new Order($price);
        $order->setName($name);
        $order->setText($text);

        if($invited){$order->invite();}

        $categoryAthlete->setOrder($order);
        $this->entityManager->persist($order);
        $this->entityManager->persist($categoryAthlete);
        return $order;
    }
    public function createRegistrationOrderFromPhase(CategoryAthlete $categoryAthlete,Category $category):? Order{

        $order=new Order($category->getPrice());
        $phase=$category->getPhase();
        $competition=$phase->getCompetition();
        $athleteName=$categoryAthlete->getUserExtension()->getRoninFoxUser()->getCompleteName();
        $order->setName(sprintf("%s | %s | Registro %s | %s",$competition->getName(),$phase->getName(),$category->getName(),$athleteName));
        $order->setText(sprintf("Pago para el registro de %s en la categoria %s de la fase '%s' de la competición '%s'",$athleteName,$category->getName(),$phase->getName(),$competition->getName()));
        $categoryAthlete->setOrder($order);
        $this->entityManager->persist($order);
        $this->entityManager->persist($categoryAthlete);
        return $order;
    }
}