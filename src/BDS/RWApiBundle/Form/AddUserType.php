<?php

namespace BDS\RWApiBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("email",EmailType::class,["required"=>true])
            ->add("firstName",TextType::class,["required"=>true])
            ->add("lastName",TextType::class,["required"=>true])
            ->add("competition",EntityType::class,
                    [
                        "required"=>true,
                        "class"=>'BDS\RWCompetitionBundle\Entity\Competition',
                        "choice_value"=>"slug"
                    ])
            ->add("category",EntityType::class,[
                "required"=>true,
                "class"=>'BDS\RWCompetitionBundle\Entity\Competition',
                "choice_value"=>"slug"
            ])
            ->add("phase",EntityType::class,[
                "required"=>true,
                "class"=>'BDS\RWCompetitionBundle\Entity\Competition',
                "choice_value"=>"slug"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "mapped"=>false,
                "csrf_protection"=>false
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'bdsrwapi_bundle_add_user_type';
    }
}
