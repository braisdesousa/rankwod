<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 28/12/17
 * Time: 10:50
 */

namespace BDS\RWApiBundle\Controller;


use BDS\RWApiBundle\Form\AddUserType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends FOSRestController
{

    /**
     * @return array
     * @Rest\Get("/status",name="rw_api_status",defaults={"_format"="json"},options={"expose"=true})
     */
    public function echoAction(){
        return ["status"=>"ok","message"=>"Hey, I'm here and alive"];
    }
    /**
     * @return array
     * @Rest\Post("/status",name="rw_api_add_user",defaults={"_format"="json"},options={"expose"=true})
     */
    public function addUserAction(Request $request){

        $form=$this->createForm(AddUserType::class,null,["method"=>"POST"]);
        $form->handleRequest($request);
        if($form->isValid()){
            $email=$form->get("email")->getData();
            if(!$user=$this->get("fos_user.user_manager")->findUserByEmail($email)){

            }
        }
    }
}