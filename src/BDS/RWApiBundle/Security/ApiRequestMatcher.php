<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 28/12/17
 * Time: 10:16
 */

namespace BDS\RWApiBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
class ApiRequestMatcher implements RequestMatcherInterface
{
    public function matches(Request $request)
    {
        return (strpos($request->getHost(),'api.rankwod')!==false);
    }

}