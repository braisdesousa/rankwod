<?php

namespace BDS\RWApiBundle\Security;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;


class TokenAuthenticator extends AbstractGuardAuthenticator
{
    private $em;
    private $encoderFactory;

    public function __construct(EntityManager $em,EncoderFactory $encoderFactory)
    {
        $this->em = $em;
        $this->encoderFactory=$encoderFactory;
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function getCredentials(Request $request)
    {

        $user=$request->headers->get('X-AUTH-USER',false);
        $token = $request->headers->get('X-AUTH-TOKEN',false);
        if ((!$token)||(!$user) ) {
            // no token? Return null and no other methods will be called
            return null;
        }

        // What you return here will be passed to getUser() as $credentials
        return array(
            'token' => $token,
            'username'=>$user
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $userName=$credentials["username"];
        return $this->em->getRepository('BDSUserBundle:User')
            ->findOneBy(['username' => $userName]);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $apiKey=$credentials["token"];

        return $apiKey=="APIKEYFAKE";

        return ($this->encoderFactory->getEncoder($user)->encodePassword($apiKey,$user->getSalt())==$user->getApiKey());
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if($exception->getMessage()){
            $data = array(
                'message' => $exception->getMessage()

                // or to translate this message
                // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
            );
        } else {
            $data = array(
                'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
            );
        }


        return new JsonResponse($data, 403);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, 401);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}