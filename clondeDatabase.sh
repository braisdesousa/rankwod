#!/bin/bash
databaseName="$1"
sh -c 'echo $databaseName'
sh -c "app/console bds:core:clean-database $databaseName"
sh -c "app/console bds:core:clean-database roninfox_users"
sh -c "(ssh root@ctscrossfit.com 'mysqldump --login-path=local -q --single-transaction $databaseName')|mysql --login-path=local  $databaseName"
sh -c "(ssh root@ctscrossfit.com 'mysqldump --login-path=local -q --single-transaction roninfox_users')|mysql --login-path=local  roninfox_users"
sh -c "app/console doc:sche:up --force"
sh -c "app/console doc:sche:up --force --em=user"
sh -c "app/console bds:user:postload"
sh -c "app/console bds:rw:multi"