#!/bin/bash
sh -c './app/node_modules/twig/bin/twigjs --output src/BDS/RWWebBundle/Resources/public/js/front/templates/ src/BDS/RWWebBundle/Resources/views/Front/TwigJs/'
sh -c 'rm app/cache/* -Rf'
sh -c 'app/console cache:clear --env=prod'
sh -c "composer dumpautoload -o"
sh -c 'app/console fos:js:dump'
sh -c 'app/console assets:install --env=prod'
sh -c 'app/console bds:gaufrette:s3-assets-install rankwod'
#sh -c 'app/console assetic:dump --env=prod'
sh -c 'app/console bds:assetic:dump --env=prod'
sh -c 'app/console bds:version:update'
sh -c 'app/console cache:clear --env=prod'
